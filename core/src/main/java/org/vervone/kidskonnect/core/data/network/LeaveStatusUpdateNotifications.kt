package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.*
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.data.network.model.*
import java.util.concurrent.atomic.AtomicBoolean

object LeaveStatusUpdateNotifications {


    private val subscribers = HashMap<String, LeaveStatusListener>()
    private val isFirst = AtomicBoolean(true)
    private val leaveMap = HashMap<String, FbLeave>()

    private val queries = ArrayList<Removable>()

    fun subscribe(studentParams: StudentParams, listener: LeaveStatusListener): String {
        val uuid = ApiUtils.getUUID()
        subscribers[uuid] = listener
        if (isFirst.getAndSet(false)) {
            listenForNewLeaves(studentParams)
        }
        return uuid
    }

    fun resetAll() {
        subscribers.clear()
        isFirst.set(true)
        queries.forEach {
            it.remove()
        }
        queries.clear()
    }

    private fun getRef(studentParams: StudentParams): DatabaseReference {
        return ConnectionHelper.getDB()!!.reference
            .child(studentParams.academicYr)
            .child(CLASS)
            .child(studentParams.className)
            .child(STUDENTS)
            .child(studentParams.studentId)
            .child(LEAVES_LIST)
    }

    private fun listenForNewLeaves(studentParams: StudentParams) {
        val ref = getRef(studentParams)
        val newLeaveListener = NewLeaveListener(ref, studentParams.academicYr)
        queries.add(newLeaveListener)
        ref.addChildEventListener(newLeaveListener)
    }


    private class NewLeaveListener(private val ref: DatabaseReference,
                                   academicYr: String) : DefChildEventListener(), Removable {
        private val statusListener = StatusListener(academicYr)
        override fun remove() {
            ref.removeEventListener(this)
            statusListener.remove()
        }

        override fun onChildAdded(snapshot: DataSnapshot, childName: String?) {
            val leaveId = snapshot.key
            if (leaveId != null) {
                statusListener.subscribe(leaveId)
            }
        }
    }

    private class StatusListener(academicYr: String) : ValueEventListener, Removable {
        private val queries = ArrayList<DatabaseReference>()
        val leavesRef = ConnectionHelper.getDB()!!.reference
            .child(academicYr)
            .child(LEAVES_LIST)

        fun subscribe(leaveId: String) {
            val ref = leavesRef.child(leaveId)
            queries.add(ref)
            ref.addValueEventListener(this)
        }

        override fun onCancelled(dbError: DatabaseError) {}
        override fun onDataChange(snapshot: DataSnapshot) {
            val leave = snapshot.getValue(FbLeave::class.java)
            leave?.id = snapshot.key
            if (leave != null) {
                if (leaveMap.containsKey(leave.id)) {
                    val cachedLeave = leaveMap[leave.id]
                    if (cachedLeave?.leaveStatus != leave.leaveStatus) {
                        subscribers.values.forEach { it.onStatusChange(leave) }
                        DLog.v(this, "leave status updated")
                    }
                }
                leaveMap[leave.id!!] = leave
            }
        }


        override fun remove() {
            queries.forEach {
                it.removeEventListener(this)
            }
            queries.clear()
        }
    }


    /**
     *  Listener for leave status change.
     */
    interface LeaveStatusListener {
        fun onStatusChange(leave: FbLeave)
    }

}