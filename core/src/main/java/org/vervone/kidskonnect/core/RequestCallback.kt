package org.vervone.kidskonnect.core

/**
 *  Generic callback
 */
interface RequestCallback<RESPONSE, ERROR> {
    fun onSuccess(res: RESPONSE?)
    fun onError(error: ERROR?)
}