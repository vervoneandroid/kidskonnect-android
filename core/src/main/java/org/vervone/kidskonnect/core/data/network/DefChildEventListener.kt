package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import org.vervone.kidskonnect.core.DLog

/**
 *  Default implementation of ChildEventListener
 */
open class DefChildEventListener : ChildEventListener {

    override fun onCancelled(dbError: DatabaseError) {
        DLog.w(this, "onCancelled")
    }

    override fun onChildMoved(snapshot: DataSnapshot, childName: String?) {
        DLog.v(this, "onChildMoved")
    }

    override fun onChildChanged(snapshot: DataSnapshot, childName: String?) {
        DLog.v(this, "onChildChanged")
    }

    override fun onChildAdded(snapshot: DataSnapshot, childName: String?) {
        DLog.v(this, "onChildAdded")
    }

    override fun onChildRemoved(snapshot: DataSnapshot) {
        DLog.v(this, "onChildRemoved")
    }
}