package org.vervone.kidskonnect.core.cloudstorage.aws

import android.content.Context
import com.amazonaws.AmazonClientException
import com.amazonaws.HttpMethod
import com.amazonaws.event.ProgressEvent
import com.amazonaws.mobile.client.AWSMobileClient
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.AmazonS3Exception
import com.amazonaws.services.s3.model.CannedAccessControlList
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest
import com.amazonaws.services.s3.model.GetObjectRequest
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.cloudstorage.CloudStorage
import org.vervone.kidskonnect.core.helper.AppHelper
import org.vervone.kidskonnect.core.helper.ThreadUtils
import java.io.File
import java.net.URL
import java.util.*
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import kotlin.collections.HashMap


class AwsCloudStorage(val context: Context) : CloudStorage {


    companion object {
        const val BUCKET_NAME = "com.vervone.kidskonnect"
    }

    private val executor: Executor = Executors.newFixedThreadPool(3)
    private val lastModifiedTime = HashMap<String?, Long?>()
    private var isInialized = false
    private var awsClient: AWSMobileClient = AWSMobileClient.getInstance()
    private var transferUtility: TransferUtility? = null
    private var s3client: AmazonS3? = null


    init {
        init(context)
    }

    private fun init(context: Context) {
        awsClient.initialize(context) {
            isInialized = true
            DLog.v(this, "AwsCloudStorage initialized")
            //it.isIdentityIdAvailable
            setTransferUtility()

        }?.execute()

    }

    private fun setTransferUtility() {
        this.s3client =
            AmazonS3Client(awsClient.credentialsProvider, Region.getRegion(Regions.AP_SOUTH_1))
        this.transferUtility = TransferUtility.builder()
            .context(AppHelper.get())
            .awsConfiguration(awsClient.configuration)
            .s3Client(s3client)
            .build()

    }


    override fun uploadPublic(key: String, file: File,
                              callback: CloudStorage.UploadListener, deleteAfter: Boolean): Int {
        val uploadObserver = transferUtility!!.upload(key, file, CannedAccessControlList.PublicRead)
        uploadObserver?.setTransferListener(object : TransferListener {
            override fun onStateChanged(id: Int, state: TransferState) {
                if (TransferState.COMPLETED === state) {
                    if (deleteAfter && file.delete()) {
                        DLog.v(this, " file deleted ${file.absolutePath}")
                    }
                    callback.onComplete()
                } else if (TransferState.FAILED === state) {
                    callback.onFailed()
                }
            }

            override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                val percentDonef = bytesCurrent.toFloat() / bytesTotal.toFloat() * 100
                val percentDone = percentDonef.toInt()

                DLog.d(this, "ID:" + id + " bytesCurrent: " + bytesCurrent
                        + " bytesTotal: " + bytesTotal + " " + percentDone + "%")

                callback.onProgress(percentDone)
            }

            override fun onError(id: Int, ex: Exception) {
                ex.printStackTrace()
                ThreadUtils.runOnBg {
                    callback.onFailed()
                }
            }
        })

        return uploadObserver.id
    }


    override fun upload(key: String, file: File,
                        callback: CloudStorage.UploadListener, deleteAfter: Boolean): Int {
        val uploadObserver = transferUtility!!.upload(key, file)
        uploadObserver?.setTransferListener(object : TransferListener {
            override fun onStateChanged(id: Int, state: TransferState) {
                if (TransferState.COMPLETED === state) {
                    if (deleteAfter && file.delete()) {
                        DLog.v(this, " file deleted ${file.absolutePath}")
                    }
                    callback.onComplete()
                } else if (TransferState.FAILED === state) {
                    callback.onFailed()
                }
            }

            override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                val percentDonef = bytesCurrent.toFloat() / bytesTotal.toFloat() * 100
                val percentDone = percentDonef.toInt()

                DLog.d(
                        this, "ID:" + id + " bytesCurrent: " + bytesCurrent
                        + " bytesTotal: " + bytesTotal + " " + percentDone + "%"
                )

                callback.onProgress(percentDone)
            }

            override fun onError(id: Int, ex: Exception) {
                ex.printStackTrace()
                ThreadUtils.runOnBg {
                    callback.onFailed()
                }
            }
        })

        return uploadObserver.id
    }

    override fun getSignedUrl(objectKey: String, callback: CloudStorage.Callback) {

        executor.execute(Runnable {

            val expiration = Date()
            var expTimeMillis = expiration.time
            expTimeMillis += (1000 * 60 * 60).toLong()
            expiration.time = expTimeMillis


            val request = GeneratePresignedUrlRequest(BUCKET_NAME, objectKey)
                .withMethod(HttpMethod.GET)
                .withExpiration(expiration)
            var preSignedUrl: URL? = null
            try {
                preSignedUrl = s3client?.generatePresignedUrl(request)
            } catch (ex: AmazonClientException) {
                DLog.e(this, "exception in getSignedUrl()", ex)
            } finally {
                val toString = if (preSignedUrl == null) null else preSignedUrl.toString()
                callback.onResult(toString)
            }
        })
    }


    override fun downloadIfNeeded(key: String?,
                                  file: File?,
                                  callback: CloudStorage.DownloadListener) {
        executor.execute {
            val lastUpdated = lastModifiedTime[key]
            var date: Date? = null
            if (lastUpdated != null) {
                date = Date(lastUpdated)
            }
            val getRequest = GetObjectRequest(BUCKET_NAME, key)
                .withModifiedSinceConstraint(date)
            getRequest.setGeneralProgressListener { event ->
                when {
                    event.eventCode == ProgressEvent.COMPLETED_EVENT_CODE -> {
                        callback.onComplete(file?.absolutePath)
                    }
                    event.eventCode == ProgressEvent.FAILED_EVENT_CODE -> {
                        callback.onError(Error(""))
                    }
                    // RESET_EVENT_CODE means we can use the last updated file.
                    event.eventCode == ProgressEvent.CANCELED_EVENT_CODE -> {
                        callback.onComplete(file?.absolutePath)
                    }
                    else -> {
                        DLog.v(this, "$key ${event.eventCode}")
                    }
                }
            }
            try {
                val metadata = s3client?.getObject(getRequest, file)
                lastModifiedTime[key] = metadata?.lastModified?.time
                DLog.d(this, "$key - ${lastModifiedTime[key]}")
            } catch (ex: AmazonS3Exception) {

            } catch (ex: Exception) {
                DLog.w(this, "exception in downloadIfNeeded()", ex)
            }
        }
    }

    //TODO : Download file are keeping , we need to delete old files in future
    override fun download(key: String?, file: File?, callback: CloudStorage.DownloadListener) {
        executor.execute {

            transferUtility?.download(key, file, object : TransferListener {
                override fun onStateChanged(id: Int, state: TransferState?) {
                    if (TransferState.COMPLETED === state) {
                        callback.onComplete(file?.absolutePath)
                    } else if (TransferState.FAILED === state) {
                        callback.onError(Error(""))
                    }
                }

                override fun onError(id: Int, ex: Exception?) {
                    callback.onError(Error(ex))
                    DLog.e(this, "download() ", ex!!)
                }

                override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                    val percentDonef = bytesCurrent.toFloat() / bytesTotal.toFloat() * 100
                    val percentDone = percentDonef.toInt()
                    DLog.d(this, "ID:" + id + " bytesCurrent: " + bytesCurrent
                            + " bytesTotal: " + bytesTotal + " " + percentDone + "%")
                    callback.onProgress(percentDone)
                }
            })
        }
    }


    override fun deleteFileFromCloud(objectKey: String) {

        ThreadUtils.runOnBg {
            try {
                this.s3client?.deleteObject(BUCKET_NAME, objectKey)
            } catch (ex: Exception) {
                DLog.w(this, "deleteFileFromCloud", ex)
            }
        }

    }

    override fun cancel(id: Int) {
        transferUtility?.cancel(id)
    }


}