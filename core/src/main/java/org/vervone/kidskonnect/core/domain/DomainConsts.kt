package org.vervone.kidskonnect.core.domain

object DomainConsts {
    const val KEY_CUR_ACADEMIC_YR = "current_academic_year"
    const val KEY_CUR_USER_ID = "current_user_uid"
    const val KEY_CURRENT_USER = "current_user"
    const val KEY_CURRENT_SCHOOL_ID = "current_school_id"
    const val KEY_CURRENT_SCHOOL_CONFIG = "current_school_conf"
}