package org.vervone.kidskonnect.core.domain.mappers

import org.vervone.kidskonnect.core.Mapper
import org.vervone.kidskonnect.core.data.network.model.student.FbAcademicReport
import org.vervone.kidskonnect.core.data.network.model.student.FbComments
import org.vervone.kidskonnect.core.data.network.model.student.FbProfileDetails
import org.vervone.kidskonnect.core.data.network.model.student.FbStudent
import org.vervone.kidskonnect.core.domain.model.student.*

class StudentMapper : Mapper<FbStudent?, Student> {

    override fun map(input: FbStudent?): Student {
        val student = Student()
        student.fbAcademicReports = input?.accademicReports?.map { map(it) }
        student.ProfileDetails = map(input?.ProfileDetails)
        student.classes = input?.classes
        student.likedNews = input?.likedNews
        student.studentName = input?.studentName
        student.studentId = input?.id
        student.profileImageUrl = input?.profileImageUrl
        student.comments = input?.comments?.map { map(it) }
        return student
    }

    fun map(input: FbComments?): Comment {
        val comments = Comment()
        comments.comment = input?.comment
        comments.commentType = CommentType.fromInt(input?.commentType)
        comments.commentedPersonId = input?.commentedPersonId
        comments.commentedPersonName = input?.commentedPersonName
        comments.postTime = input?.postTime
        return comments
    }

    fun map(fbAcademicReport: FbAcademicReport?): AcademicReport {
        val academicReport = AcademicReport()
        fbAcademicReport?.let {
            academicReport.date = fbAcademicReport.date
            academicReport.reportName = fbAcademicReport.reportName
            academicReport.reportUrl = fbAcademicReport.reportUrl
        }
        return academicReport
    }

    private fun map(fbProfile: FbProfileDetails?): ProfileDetails {
        val profile = ProfileDetails()

        fbProfile?.let {
            profile.LocalGuardian = fbProfile.LocalGuardian
            profile.LocalGuardianAddress = fbProfile.LocalGuardianAddress
            profile.localGuardianNumber = fbProfile.localGuardianNumber
            profile.ParentEmail = fbProfile.ParentEmail
            profile.ParentNumber = fbProfile.ParentNumber
            profile.parentName = fbProfile.parentName
            profile.parentAdderss = fbProfile.parentAdderss

        }

        return profile
    }


    fun map(input: Comment): FbComments {
        val fbComment = FbComments()
        fbComment.comment = input.comment
        fbComment.commentType = input.commentType?.value
        fbComment.commentedPersonId = input.commentedPersonId
        fbComment.commentedPersonName = input.commentedPersonName
        fbComment.postTime = input.postTime
        return fbComment
    }
}