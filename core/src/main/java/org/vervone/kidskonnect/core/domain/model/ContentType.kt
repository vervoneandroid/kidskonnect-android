package org.vervone.kidskonnect.core.domain.model

enum class ContentType(val value: Int) {

    IMAGE(0),
    VIDEO(1),
    WEB_LINKS(2),
    TEXT(99),
    MULTI_MEDIA(98),
    UNKNOWN(-1);

    companion object {
        fun fromInt(value: Int?): ContentType {
            return when (value) {
                0 -> IMAGE
                1 -> VIDEO
                2 -> WEB_LINKS
                99 -> TEXT
                98 -> MULTI_MEDIA
                else -> UNKNOWN
            }
        }


    }
}