package org.vervone.kidskonnect.core.push

import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.Volley
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.domain.model.MessageChannel
import org.vervone.kidskonnect.core.helper.AppHelper
import org.vervone.kidskonnect.core.helper.LocalizedStrings

class FcmPush(private val localizedStrings: LocalizedStrings) : PushApi {


    override fun publishCreateLeave(teacherId: String,
                                    studentName: String,
                                    className: String) {
        val string = localizedStrings.getString(LocalizedStrings.NEW_LEAVE_APPLICATION)
        val formatted = string.format(studentName, className)

        val msg = Message.builder()
            .setNotification(Notification(studentName, formatted))
            .setTopic(NOTIFICATION_TOPIC + teacherId)
            .putData(TYPE, NotificationType.LEAVE_APPROVAL.value)
            .build()


        send(msg)
    }

    override fun publishApprove(teacherName: String,
                                studentId: String) {
        val string = localizedStrings.getString(LocalizedStrings.LEAVE_APPROVED)

        val msg = Message.builder()
            .setNotification(Notification(teacherName, string))
            .setTopic(NOTIFICATION_TOPIC + studentId)
            .putData(TYPE, NotificationType.LEAVE_APPROVAL.value)
            .build()

        send(msg)
    }

    override fun publishRejected(teacherName: String, studentId: String) {
        val string = localizedStrings.getString(LocalizedStrings.LEAVE_REJECTED)

        val msg = Message.builder()
            .setNotification(Notification(teacherName, string))
            .setTopic(NOTIFICATION_TOPIC + studentId)
            .putData(TYPE, NotificationType.LEAVE_APPROVAL.value)
            .build()

        send(msg)
    }

    override fun publishNewsCreated(senderName: String, classes: List<String>) {
        val string = localizedStrings.getString(LocalizedStrings.NEWS_ADDED)


        classes.forEach {
            val replacedSpace = it.replace(" ", "_")
            val msg = Message.builder()
                .setNotification(Notification(senderName, string))
                .setTopic(NOTIFICATION_TOPIC + replacedSpace)
                .putData(TYPE, NotificationType.NEWS_FEEDS.value)
                .build()

            send(msg)
        }


    }

    override fun publishProfileUpdated(className: String,
                                       commentedPersonName: String,
                                       commentType: String,
                                       studentId: String,
                                       studentName: String) {

        val replacedSpace = className.replace(" ", "_")
        val condition = "'$INITIAL_SUBSCRPTION$replacedSpace' in topics && 'teacher' in topics"

        val message = localizedStrings.getString(LocalizedStrings.COMMENT_ADDED)

        val formatted = message.format(commentedPersonName, studentName)


        val commentPush = Message.builder()
            .setNotification(Notification(commentedPersonName, formatted))
            .setCondition(condition)
            .putData(TYPE, NotificationType.NEW_COMMENT.value)
            .putData(STUDENT_NAME, studentName)
            .putData(STUDENT_ID, studentId)
            .putData(CLASS_NAME, className)
            .putData(COMMENT_TYPE, commentType)
            .build()

        send(commentPush)


        val topic = NOTIFICATION_TOPIC + studentId
        val msg = Message.builder()
            .setNotification(Notification(commentedPersonName, formatted))
            .setTopic(topic)
            .putData(TYPE, NotificationType.NEW_COMMENT.value)
            .putData(STUDENT_NAME, studentName)
            .putData(STUDENT_ID, studentId)
            .putData(CLASS_NAME, className)
            .putData(COMMENT_TYPE, commentType)
            .build()

        send(msg)

    }

    override fun publishMessage(channelId: String, senderName: String) {
        val message = localizedStrings.getString(LocalizedStrings.NEW_MESSAGE)

        val msg = Message.builder()
            .setNotification(Notification(senderName, message))
            .setTopic(channelId)
            .putData(TYPE, NotificationType.NEW_MESSAGE_EXISTING_GROUP.value)
            .putData(GROUP_ID, channelId)
            .build()

        send(msg)
    }

    override fun publishCreateChannel(senderName: String, senderId: String,
                                      messageChannel: MessageChannel) {


        val message = localizedStrings.getString(LocalizedStrings.NEW_MESSAGE)
        if (messageChannel.isGroupMessage != null && messageChannel.isGroupMessage!!) {

            // inform class
            messageChannel.classes?.let {
                if (it.isNotEmpty()) {
                    sendPushForClass(it, senderName, message, messageChannel.id!!)
                }
            }

            //inform teachers
            messageChannel.teacherIds?.let {
                if (it.isNotEmpty()) {
                    sendPushForTeachers(it, senderName, message, messageChannel.id!!)
                }
            }

            // inform students
            messageChannel.studentIdsAndClass?.let {
                if (it.isNotEmpty()) {
                    sendPushForStudents(it, senderName, message, messageChannel.id!!)
                }
            }

        } else {

            val map = messageChannel.targets?.filterKeys { it != senderId }
            map?.forEach { entry ->

                val topic = NOTIFICATION_TOPIC + entry.key
                val msg = Message.builder()
                    .setNotification(Notification(senderName, message))
                    .setTopic(topic)
                    .putData(TYPE, NotificationType.NEW_MESSAGE_EXISTING_GROUP.value)
                    .putData(GROUP_ID, messageChannel.id!!)
                    .build()

                send(msg)

                return@forEach
            }

        }
    }

    private fun sendPushForStudents(studentIdsAndClass: HashMap<String, String>,
                                    senderName: String,
                                    body: String,
                                    channelId: String) {

        if (studentIdsAndClass.keys.count() > 1) {
            val students = studentIdsAndClass.keys.chunked(5)
            students.forEach {
                val condition = buildCondition(it, NOTIFICATION_TOPIC)
                DLog.v(this, "CONDITION#### $condition")
                val msg = Message.builder()
                    .setNotification(Notification(senderName, body))
                    .setCondition(condition)
                    .putData(TYPE, NotificationType.NEW_MESSAGE_EXISTING_GROUP.value)
                    .putData(GROUP_ID, channelId)
                    .build()

                send(msg)
            }
        } else {
            val studentId = studentIdsAndClass.keys.firstOrNull()!!
            val topic = NOTIFICATION_TOPIC + studentId
            DLog.v(this, "topic#### $topic")
            val msg = Message.builder()
                .setNotification(Notification(senderName, body))
                .setTopic(topic)
                .putData(TYPE, NotificationType.NEW_MESSAGE_EXISTING_GROUP.value)
                .putData(GROUP_ID, channelId)
                .build()

            send(msg)
        }
    }

    private fun sendPushForClass(_classes: List<String>,
                                 senderName: String,
                                 body: String,
                                 channelId: String) {

        if (_classes.count() > 1) {
            val classes = _classes.chunked(5)
            classes.forEach {
                val condition = buildCondition(it, INITIAL_SUBSCRPTION)
                DLog.v(this, "CONDITION#### $condition")
                val msg = Message.builder()
                    .setNotification(Notification(senderName, body))
                    .setCondition(condition)
                    .putData(TYPE, NotificationType.NEW_MESSAGE_EXISTING_GROUP.value)
                    .putData(GROUP_ID, channelId)
                    .build()

                send(msg)
            }
        } else {
            val className = _classes.firstOrNull()
            val topic = INITIAL_SUBSCRPTION + replaceSpace(className ?: "")
            DLog.v(this, "topic#### $topic")
            val msg = Message.builder()
                .setNotification(Notification(senderName, body))
                .setTopic(topic)
                .putData(TYPE, NotificationType.NEW_MESSAGE_EXISTING_GROUP.value)
                .putData(GROUP_ID, channelId)
                .build()

            send(msg)
        }
    }


    private fun sendPushForTeachers(teacherIds: List<String>,
                                    senderName: String,
                                    body: String,
                                    channelId: String) {

        if (teacherIds.count() > 1) {
            val teachers = teacherIds.chunked(5)
            teachers.forEach {
                val condition = buildCondition(it, NOTIFICATION_TOPIC)
                DLog.v(this, "CONDITION#### $condition")
                val msg = Message.builder()
                    .setNotification(Notification(senderName, body))
                    .setCondition(condition)
                    .putData(TYPE, NotificationType.NEW_MESSAGE_EXISTING_GROUP.value)
                    .putData(GROUP_ID, channelId)
                    .build()

                send(msg)
            }
        } else {
            val teacherId = teacherIds.firstOrNull()!!

            val topic = NOTIFICATION_TOPIC + teacherId
            DLog.v(this, "topic#### $topic")
            val msg = Message.builder()
                .setNotification(Notification(senderName, body))
                .setTopic(topic)
                .putData(TYPE, NotificationType.NEW_MESSAGE_EXISTING_GROUP.value)
                .putData(GROUP_ID, channelId)
                .build()

            send(msg)
        }
    }

    private fun replaceSpace(className: String): String {
        return className.replace(" ", "_")
    }

    private fun buildCondition(list: List<String>, topicPrefx: String): String {
        val topics = list.map { "'$topicPrefx${replaceSpace(it)}'" }
        return topics.joinToString(" in topics || ", postfix = " in topics")
    }

    private val requestQueue: RequestQueue by lazy {
        Volley.newRequestQueue(AppHelper.getContext())
    }


    override fun send(message: Message) {

        val apiRequest = ApiRequest(FCM_API, message,
                                    Response.Listener { DLog.v(this, "apiRequest success ${message.to}") },
                                    Response.ErrorListener { e ->
                                        DLog.v(this,
                                               "apiRequest failed ${message.to}  || ${String(e.networkResponse.data)}")
                                    })

        requestQueue.add(apiRequest)
    }


    companion object {
        // ARGS

        const val TYPE = PushApiConst.TYPE
        const val STUDENT_NAME = PushApiConst.STUDENT_NAME
        const val STUDENT_ID = PushApiConst.STUDENT_ID
        const val COMMENT_TYPE = PushApiConst.COMMENT_TYPE
        const val CLASS_NAME = PushApiConst.CLASS_NAME
        const val GROUP_ID = PushApiConst.GROUP_ID

        // TOPICS
        const val INITIAL_SUBSCRPTION = PushApiConst.INITIAL_SUBSCRPTION
        const val NOTIFICATION_TOPIC = PushApiConst.NOTIFICATION_TOPIC

        // TOPICS -- END --
        private const val FCM_API = PushApiConst.FCM_API
        const val HEADER_AUTH = PushApiConst.HEADER_AUTH
        const val HEADER_CONTENT_TYPE = PushApiConst.HEADER_CONTENT_TYPE

        const val CONTENT_TYPE = "application/json"
        const val SERVER_KEY =
            "key=" + "AAAAdHJr1v0:APA91bFybt05V1VcglTseRFCbPdd625gvzFDfTl01eJPdAiMKEIAfDSyoZOZY-9gl0aPlyAPuS-xJ5DPbCTZZFjwcfv9mLz9GQ_Hl3mvjGHkjeuEQEWz4Sg6lJ1kITaSg01cvKd_7722"
    }
}