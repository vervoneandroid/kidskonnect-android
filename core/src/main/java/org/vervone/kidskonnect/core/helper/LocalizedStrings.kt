package org.vervone.kidskonnect.core.helper

interface LocalizedStrings {
    fun getString(type: String): String

    companion object {
        const val NEW_LEAVE_APPLICATION = "new_leave_application"
        const val LEAVE_APPROVED = "leave_approved"
        const val LEAVE_REJECTED = "leave_rejected"
        const val NEWS_ADDED = "news_added"
        const val COMMENT_ADDED = "comment_added"
        const val NEW_MESSAGE = "new_message"
    }
}