package org.vervone.kidskonnect.core.push

enum class NotificationType(val value: String) {
    /*
    0 - New message
1 - New message in existing group
2 - Leave approval
3 - Exam
4 - News feeds
5 - Common notification
6 - New comment on student profile
7 - Network Reachability

     */

    NEW_MESSAGE("0"),
    NEW_MESSAGE_EXISTING_GROUP("1"),
    LEAVE_APPROVAL("2"),
    EXAM("3"),
    NEWS_FEEDS("4"),
    COMMON("5"),
    NEW_COMMENT("6"),
    NETWORK_REACHABILITY("7"),
    UNKNOWN("-100");

    companion object {
        fun fromValue(value: String?): NotificationType {
            return when (value) {
                "0" -> NEW_MESSAGE
                "1" -> NEW_MESSAGE_EXISTING_GROUP
                "2" -> LEAVE_APPROVAL
                "3" -> EXAM
                "4" -> NEWS_FEEDS
                "5" -> COMMON
                "6" -> NEW_COMMENT
                "7" -> NETWORK_REACHABILITY
                else -> UNKNOWN
            }
        }

    }

}