package org.vervone.kidskonnect.core.helper

import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.domain.DomainConsts.KEY_CURRENT_SCHOOL_CONFIG
import org.vervone.kidskonnect.core.domain.DomainConsts.KEY_CURRENT_SCHOOL_ID
import org.vervone.kidskonnect.core.domain.model.school.SchoolConfig

object SchoolConfigHelper {

    private var schoolConfig: SchoolConfig? = null

    fun set(schoolConfig: SchoolConfig) {
        this.schoolConfig = schoolConfig
    }

    fun saveExisting() {
        val config = schoolConfig
        if (config != null) {
            val json = JsonHelper.toJson(config)
            PreferenceHelper.save(json, KEY_CURRENT_SCHOOL_CONFIG)
            PreferenceHelper.save(config.id, KEY_CURRENT_SCHOOL_ID)
        }
    }

    fun load() {
        val json = PreferenceHelper.getString(KEY_CURRENT_SCHOOL_CONFIG)
        if (json != null) {
            try {
                this.schoolConfig = JsonHelper.fromJson(json, SchoolConfig::class.java)
            } catch (ex: Exception) {
                this.schoolConfig = null
                DLog.v(this, "getConfig", ex)
            }
        }
    }

    fun getConfig(): SchoolConfig? {
        return this.schoolConfig
    }

    fun schoolId(): String {
        return this.schoolConfig?.id ?: ""
    }

    fun hasValidConfig(): Boolean {
        return PreferenceHelper.getSafeString(KEY_CURRENT_SCHOOL_ID).isNotBlank()
    }

    fun invalidateConfig() {
        PreferenceHelper.remove(KEY_CURRENT_SCHOOL_CONFIG)
        this.schoolConfig = null
    }

    fun clear() {
        PreferenceHelper.remove(KEY_CURRENT_SCHOOL_CONFIG)
        PreferenceHelper.remove(KEY_CURRENT_SCHOOL_ID)
    }
}