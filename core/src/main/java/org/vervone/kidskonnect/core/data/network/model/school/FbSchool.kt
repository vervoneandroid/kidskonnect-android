package org.vervone.kidskonnect.core.data.network.model.school

data class FbSchool(
        var GCMSenderId: String? = null,
        var address: String? = null,
        var apiKey: String? = null,
        var bundleId: String? = null,
        var clientId: String? = null,
        var databaseUrl: String? = null,
        var district: String? = null,
        var googleAppId: String? = null,
        var projectId: String? = null,
        var schoolName: String? = null,
        var storageBucketUrl: String? = null
)