package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener


class EventListener(private val ref: DatabaseReference,
                    private val changeListener: DataChangeListener) : ValueEventListener {

    override fun onCancelled(dbError: DatabaseError) {
        //Ignore
    }

    override fun onDataChange(snapshot: DataSnapshot) {
        changeListener.onChange()
    }

    fun remove() {
        ref.removeEventListener(this)
    }
}