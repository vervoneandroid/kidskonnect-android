package org.vervone.kidskonnect.core.data.network

interface DataChangeListener {
    fun onChange()
}