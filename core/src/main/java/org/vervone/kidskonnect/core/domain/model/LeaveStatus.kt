package org.vervone.kidskonnect.core.domain.model

import java.io.Serializable

enum class LeaveStatus(val value: Int) : Serializable {

    PENDING(0),
    APPROVED(1),
    REJECTED(2);

    companion object {
        fun fromInt(value: Int?): LeaveStatus {
            return when (value) {
                0 -> PENDING
                1 -> APPROVED
                2 -> REJECTED
                else -> PENDING
            }
        }
    }

}