package org.vervone.kidskonnect.core.domain

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.ApiError
import org.vervone.kidskonnect.core.domain.model.Teacher
import org.vervone.kidskonnect.core.domain.model.User
import org.vervone.kidskonnect.core.domain.model.student.Student

interface AuthFacade {
    fun isLoggedIn(): Boolean
    fun doLogin(email: String, pwd: String, callback: RequestCallback<User, Error>)
    fun fetchTeacherProfile(uid: String?, callback: RequestCallback<Teacher, Error>)
    fun fetchStudentProfile(uid: String?, callback: RequestCallback<Student, Error>)
    fun logout()
    fun getAcademicYearData(uid: String?, callback: RequestCallback<Teacher, Error>)

    fun getTeacherAllDetails(uid: String, callback: RequestCallback<Teacher, ApiError>)

    fun getStudentAllDetails(uid: String, callback: RequestCallback<Student, DomainError>)
}