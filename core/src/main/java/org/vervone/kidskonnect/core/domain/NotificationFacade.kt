package org.vervone.kidskonnect.core.domain

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.model.Notification

interface NotificationFacade {
    fun getNotification(academicYr: String,
                        callback: RequestCallback<List<Notification>, DomainError>?)
}