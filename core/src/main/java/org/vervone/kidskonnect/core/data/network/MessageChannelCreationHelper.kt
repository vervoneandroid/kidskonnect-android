package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.*
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.model.*
import java.util.concurrent.atomic.AtomicInteger

/**
 *  Helper class for  Message channel creation.
 */
class MessageChannelCreationHelper() {

    private lateinit var academicYr: String
    private lateinit var messageChannel: FbMessageChannel
    private lateinit var callback: RequestCallback<Pair<String, FbMessageChannel>, ApiError>

    private var teacherId: String? = null
    private var className: String? = null
    private var studentId: String? = null

    private val requestCnt = AtomicInteger()

    constructor(academicYr: String,
                messageChannel: FbMessageChannel,
                callback: RequestCallback<Pair<String, FbMessageChannel>,
                        ApiError>,
                teacherId: String) : this() {
        this.academicYr = academicYr
        this.messageChannel = messageChannel
        this.callback = callback
        this.teacherId = teacherId
    }

    constructor(academicYr: String,
                className: String,
                studentId: String,
                messageChannel: FbMessageChannel,
                callback: RequestCallback<Pair<String, FbMessageChannel>,
                        ApiError>) : this() {
        this.academicYr = academicYr
        this.messageChannel = messageChannel
        this.callback = callback
        this.className = className
        this.studentId = studentId
    }


    fun execute() {
        insertAllMessageChannels()
    }


    private fun updateTeacherTable(teacherId: String) {
        val ref = ConnectionHelper.getDB()!!.reference
            .child(academicYr)
            .child(TEACHERS)
            .child(teacherId)
            .child(USER_CHAT_DICT)

        ref.runTransaction(object : Transaction.Handler {

            override fun doTransaction(mutableData: MutableData): Transaction.Result {
                val typeIndicator = object : GenericTypeIndicator<HashMap<String, Boolean>>() {}
                var userChatDict = mutableData.getValue(typeIndicator)
                if (userChatDict == null) {
                    userChatDict = hashMapOf()
                }
                userChatDict[messageChannel.channelId!!] = true
                //Updating comments list.
                mutableData.value = userChatDict
                return Transaction.success(mutableData)
            }

            override fun onComplete(dbError: DatabaseError?, p1: Boolean, snap: DataSnapshot?) {
                requestCnt.decrementAndGet()
                notifyCallback()
                if (dbError != null) {
                    //callback.onError(ApiError(Exception(dbError.message)))
                } else {
                    // callback.onSuccess(comment)


                }
            }
        })

    }

    private fun updateStudentTable(className: String,
                                   studentId: String) {
        val ref = ConnectionHelper.getDB()!!.reference
            .child(academicYr)
            .child(CLASS)
            .child(className)
            .child(STUDENTS)
            .child(studentId)
            .child(USER_CHAT_DICT)
        ref.runTransaction(object : Transaction.Handler {

            override fun doTransaction(mutableData: MutableData): Transaction.Result {
                val typeIndicator = object : GenericTypeIndicator<HashMap<String, Boolean>>() {}
                var userChatDict = mutableData.getValue(typeIndicator)
                if (userChatDict == null) {
                    userChatDict = hashMapOf()
                }
                userChatDict[messageChannel.channelId!!] = true
                mutableData.value = userChatDict
                return Transaction.success(mutableData)
            }

            override fun onComplete(dbError: DatabaseError?, p1: Boolean, snap: DataSnapshot?) {
                requestCnt.decrementAndGet()
                notifyCallback()
                if (dbError != null) {
                    //callback.onError(ApiError(Exception(dbError.message)))
                } else {
                    // callback.onSuccess(comment)
                }
            }
        })
    }

    private fun updateClassTable(className: String) {
        val ref = ConnectionHelper.getDB()!!.reference
            .child(academicYr)
            .child(CLASS)
            .child(className)
            .child(USER_CHAT_DICT)
        ref.runTransaction(object : Transaction.Handler {

            override fun doTransaction(mutableData: MutableData): Transaction.Result {
                val typeIndicator = object : GenericTypeIndicator<HashMap<String, Boolean>>() {}
                var userChatDict = mutableData.getValue(typeIndicator)
                if (userChatDict == null) {
                    userChatDict = hashMapOf()
                }
                userChatDict[messageChannel.channelId!!] = true
                mutableData.value = userChatDict
                return Transaction.success(mutableData)
            }

            override fun onComplete(dbError: DatabaseError?, p1: Boolean, snap: DataSnapshot?) {
                requestCnt.decrementAndGet()
                notifyCallback()
                if (dbError != null) {
                    //callback.onError(ApiError(Exception(dbError.message)))
                } else {
                    // callback.onSuccess(comment)
                }
            }
        })
    }

    private fun insertAllMessageChannels() {
        val ref = ConnectionHelper.getDB()!!.reference
            .child(academicYr)
            .child(ALL_MESSAGE_CHANNELS)

        val generateId = ref.push().key!!

        ref.child(generateId).setValue(messageChannel)
            .addOnSuccessListener {
                DLog.v(this, "MessageChannel created successfully")
                messageChannel.channelId = generateId
                update()
            }.addOnFailureListener { ex ->
                DLog.v(this, "MessageChannel creation failed. $ex")
                callback.onError(ApiError(ex.message ?: ""))
            }
    }

    private fun update() {

        var allRequests = messageChannel.classes?.size ?: 0
        allRequests += messageChannel.teacherIds?.size ?: 0
        allRequests += messageChannel.studentIdsAndClass?.size ?: 0
        allRequests += 1
        requestCnt.set(allRequests)

        messageChannel.classes?.forEach {
            updateClassTable(it)
        }

        messageChannel.teacherIds?.forEach {
            updateTeacherTable(it)
        }

        messageChannel.studentIdsAndClass?.entries?.forEach {
            updateStudentTable(it.value, it.key)
        }

        if (teacherId != null) {
            updateTeacherTable(teacherId!!)
        } else if (className != null && studentId != null) {
            updateStudentTable(className!!, studentId!!)
        }

    }


    private fun notifyCallback() {
        if (requestCnt.get() == 0) {
            callback.onSuccess(Pair(messageChannel.channelId!!, messageChannel))
        }
    }


}