package org.vervone.kidskonnect.core.domain

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.cloudstorage.CloudStorage
import org.vervone.kidskonnect.core.domain.model.News
import org.vervone.kidskonnect.core.domain.model.StudentParamsBm
import org.vervone.kidskonnect.core.domain.model.TeacherParamsBm

interface NewsFacade {
    fun getNews(academicYr: String, teacherUid: String, callback: RequestCallback<List<News>, Error>)
    fun getNewsForAllClasses(academicYr: String, teacherUid: String, callback: RequestCallback<List<News>, Error>)
    fun postNews(academicYr: String, news: News, cloudStorage: CloudStorage, callback: RequestCallback<Void, Error>)
    fun generateNewsId(academicYr: String): String
    fun deleteNews(academicYr: String, newsId: String, callback: RequestCallback<String, Error>)
    fun updateNews(
            academicYr: String, newsId: String, user: String, isLike: Boolean,
            callback: RequestCallback<String, Error>
    )

    fun isLikedNews(newsId: String): Boolean
    fun getLikedNews(academicYr: String, user: String)
    fun getStudentNews(academicYr: String, className: String, callback: RequestCallback<List<News>, Error>)

    fun getLikedNewsStudent(
            academicYr: String,
            user: String, className: String
    )

    fun updateStudentNews(
            academicYr: String, newsId: String, user: String, className: String, isLike: Boolean,
            callback: RequestCallback<String, Error>
    )

    /**
     *  Only used for if app is in foreground.
     */
    fun subscribeForNewsNotification(teacherParamsBm: TeacherParamsBm, listener: NewsObserver)

    fun subscribeForNewsNotification(studentParamsBm: StudentParamsBm, listener: NewsObserver)


    interface NewsObserver {
        fun onSnapshot(newsCount: Int)
        fun onAdded(count: Int, news: News)
    }
}