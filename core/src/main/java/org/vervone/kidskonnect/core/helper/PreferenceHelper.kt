package org.vervone.kidskonnect.core.helper

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

object PreferenceHelper {

    fun save(value: Boolean, key: String) {
        val ctx = AppHelper.getContext()
        with(defaultPref(ctx).edit()) {
            putBoolean(key, value)
                .apply()
        }
    }

    /**
     * Default SharedPreference
     * @param context   Context for getting default sharedPreference.
     * */
    fun defaultPref(context: Context): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)


    fun getBoolean(key: String, defValue: Boolean = false): Boolean {
        val ctx = AppHelper.getContext()
        return defaultPref(ctx).getBoolean(key, defValue)
    }

    fun getInt(key: String, def: Int): Int {
        val ctx = AppHelper.getContext()
        return defaultPref(ctx).getInt(key, def)
    }

    fun getString(key: String): String? {
        val ctx = AppHelper.getContext()
        return defaultPref(ctx).getString(key, null)
    }

    fun getSafeString(key: String): String {
        val ctx = AppHelper.getContext()
        return defaultPref(ctx).getString(key, "")
    }

    fun save(value: String, key: String) {
        val ctx = AppHelper.getContext()
        with(defaultPref(ctx).edit()) {
            putString(key, value)
                .apply()
        }
    }

    fun remove(key: String) {
        val ctx = AppHelper.getContext()
        with(defaultPref(ctx).edit()) {
            remove(key).apply()
        }
    }

    fun save(value: Int, key: String) {
        val ctx = AppHelper.getContext()
        with(defaultPref(ctx).edit()) {
            putInt(key, value)
                .apply()
        }
    }

    fun getAllKeys(): List<String> {
        val ctx = AppHelper.getContext()
        return defaultPref(ctx).all.keys.toList()
    }

    fun save(value: Set<String>, key: String) {
        val ctx = AppHelper.getContext()
        with(defaultPref(ctx).edit()) {
            putStringSet(key, value)
                .apply()
        }
    }

    fun getStringSet(key: String): Set<String> {
        return try {
            val ctx = AppHelper.getContext()
            defaultPref(ctx).getStringSet(key, emptySet())
        } catch (ex: Exception) {
            emptySet()
        }
    }


}