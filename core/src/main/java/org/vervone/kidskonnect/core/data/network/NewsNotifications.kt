package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.*
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.data.network.model.*
import org.vervone.kidskonnect.core.data.network.model.academicyear.FbAcademicYrTeacher
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger
import kotlin.collections.HashMap

object NewsNotifications {


    private val allNewsIds = HashMap<String, Boolean>()
    private val subscribers = Hashtable<String, NewsListener>()


    fun subscribe(params: StudentParams, newsListener: NewsListener) {
        val uuid = ApiUtils.getUUID()
        subscribers[uuid] = newsListener
        fetchNewsForClasses(listOf(params.className), params.academicYr)
    }


    fun subscribe(params: TeacherParams, newsListener: NewsListener) {
        DLog.v(this, " started ")

        val uuid = ApiUtils.getUUID()
        subscribers[uuid] = newsListener

        if (!params.isAdmin) {
            ConnectionHelper.getDB()!!.getReference("${params.academicYr}/teachers/${params.teacherId}")
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(dbError: DatabaseError) {}
                    override fun onDataChange(dataSnap: DataSnapshot) {
                        val obj = dataSnap.getValue(FbAcademicYrTeacher::class.java)
                        fetchNewsForClasses(obj, params.academicYr)
                    }
                })
        } else {
            ConnectionHelper.getDB()!!.getReference("classList").addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {}
                override fun onDataChange(snapshot: DataSnapshot) {
                    val schoolClasses = snapshot.getValue(ApiHelperImpl.TYPE_INDICATOR_STRINGS)
                    fetchNewsForClasses(schoolClasses, params.academicYr)
                }
            })

        }
    }

    private fun fetchNewsForClasses(academicYrTeacher: FbAcademicYrTeacher?, academicYr: String) {
        academicYrTeacher?.let {
            val classesAndSubjects = academicYrTeacher.classesAndSubjects
            if (classesAndSubjects != null) {
                fetchNewsForClasses(classesAndSubjects.keys.toList(), academicYr)
            }
        }
    }


    private fun fetchNewsForClasses(classes: List<String>?, academicYr: String) {
        if (classes != null) {
            NewsListeners(classes, academicYr)
        }
    }


    private class NewsListeners(val classes: List<String>, val academicYr: String) : ValueEventListener {

        val reqCount = AtomicInteger(classes.size)
        val isFirst = AtomicBoolean(true)

        init {
            classes.forEach {
                ConnectionHelper.getDB()!!.reference.child(academicYr)
                    .child(CLASS)
                    .child(it)
                    .child(NEWS_FEEDS)
                    .addValueEventListener(this)
            }
        }


        fun removeChangeListener() {
            classes.forEach {
                ConnectionHelper.getDB()!!.reference.child(academicYr)
                    .child(CLASS)
                    .child(it)
                    .child(NEWS_FEEDS)
                    .removeEventListener(this)
            }
        }


        private inner class ChildHandler : DefChildEventListener() {
            override fun onChildAdded(snapshot: DataSnapshot, childName: String?) {
                if (allNewsIds.containsKey(snapshot.key!!)) {
                    return
                }
                allNewsIds[snapshot.key!!] = true
                ConnectionHelper.getDB()!!.reference.child(academicYr)
                    .child(NEWS_FEEDS)
                    .child(snapshot.key!!)
                    .addListenerForSingleValueEvent(valueEventListener)
            }
        }


        private val valueEventListener = object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}
            override fun onDataChange(snapShot: DataSnapshot) {
                val typeIndicator = object : GenericTypeIndicator<FbNews>() {}
                val fbnews = snapShot.getValue(typeIndicator)
                if (fbnews != null) {
                    subscribers.forEach {
                        it.value.onNewsAdded(allNewsIds.size, fbnews)
                    }
                }
            }
        }


        override fun onCancelled(dbError: DatabaseError) {
            if (reqCount.decrementAndGet() == 0) {
                inform()

                listenChilds()
            }
        }

        private fun listenChilds() {
            classes.forEach {

                ConnectionHelper.getDB()!!.reference.child(academicYr)
                    .child(CLASS)
                    .child(it)
                    .child(NEWS_FEEDS)
                    .addChildEventListener(ChildHandler())
            }
        }

        override fun onDataChange(snapShot: DataSnapshot) {
            val typeIndicator = object : GenericTypeIndicator<HashMap<String, Boolean>>() {}
            val newsIds = snapShot.getValue(typeIndicator)

            if (newsIds != null) {
                allNewsIds.putAll(newsIds)
            }

            if (reqCount.decrementAndGet() == 0) {
                isFirst.set(false)
                inform()
                listenChilds()
            } else if (!isFirst.get()) {
                inform()
                listenChilds()
            }
        }

        fun inform() {
            removeChangeListener()
            subscribers.forEach {
                it.value.onSnapshot(allNewsIds.size)
            }
        }
    }


    interface NewsListener {
        fun onSnapshot(count: Int)
        fun onNewsAdded(count: Int, news: FbNews)
    }

}