package org.vervone.kidskonnect.core.domain

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.ApiError
import org.vervone.kidskonnect.core.data.network.ApiHelper
import org.vervone.kidskonnect.core.data.network.model.FbExam
import org.vervone.kidskonnect.core.domain.mappers.DomainErrorMapper
import org.vervone.kidskonnect.core.domain.mappers.ExamsMapper
import org.vervone.kidskonnect.core.domain.model.Exam

class ExamsFacadeImpl : ExamsFacade {

    private val apiHelper = ApiHelper.Factory.apiHelper

    override fun getScheduledExams(academicYr: String,
                                   classes: List<String>,
                                   callback: RequestCallback<List<Exam>, DomainError>?) {
        apiHelper.getScheduledExams(academicYr, classes, object : RequestCallback<List<FbExam>, ApiError> {

            override fun onSuccess(res: List<FbExam>?) {
                val mapper = ExamsMapper()
                val list = res?.map { mapper.map(it) }
                callback?.onSuccess(list)
            }

            override fun onError(error: ApiError?) {
                val mapper = DomainErrorMapper()
                callback?.onError(mapper.map(error!!))
            }
        })
    }
}