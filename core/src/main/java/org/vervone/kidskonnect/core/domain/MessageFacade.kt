package org.vervone.kidskonnect.core.domain

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.UploadListener
import org.vervone.kidskonnect.core.data.network.Callback
import org.vervone.kidskonnect.core.data.network.MessageCallback
import org.vervone.kidskonnect.core.data.network.Subscription
import org.vervone.kidskonnect.core.domain.model.ChatMessage
import org.vervone.kidskonnect.core.domain.model.MessageChannel
import org.vervone.kidskonnect.core.domain.model.StudentParamsBm
import org.vervone.kidskonnect.core.domain.model.TeacherParamsBm
import org.vervone.kidskonnect.core.helper.Task

interface MessageFacade {

    fun getAllChatRooms(
            academicYr: String, userUid: String,
            classes: List<String>,
            callback: RequestCallback<List<MessageChannel>, Error>?
    )

    fun getChatRoomIfAvailable(
            academicYr: String, targetsId: String,
            callback: RequestCallback<MessageChannel?, DomainError>?
    )

    fun getChatMessages(
            academicYr: String, chatRoomId: String,
            callback: RequestCallback<List<ChatMessage>, Error>?
    ): Subscription

    fun sendMessage(
            academicYr: String, chatRoomId: String, chatMessage: ChatMessage,
            callback: UploadListener<ChatMessage, Error>?
    ): Task


    fun createMessageChannelByTeacher(
            academicYr: String,
            teacherId: String,
            messageChannel: MessageChannel,
            callback: RequestCallback<MessageChannel, DomainError>
    )

    fun createMessageChannelByStudent(
            academicYr: String,
            className: String,
            studentId: String,
            messageChannel: MessageChannel,
            callback: RequestCallback<MessageChannel, DomainError>
    )

    /**
     *  Return Last Message with chat room id  for the specified chat room
     *
     *  Key - Chat Room Id (String)
     *  Value - ChatMessage
     *
     */
    fun getLastMessage(academicYr: String, chatRoomId: String,
                       callback: RequestCallback<HashMap<String, ChatMessage>, DomainError>?)


    fun getStudentChatRooms(academicYr: String,
                            userUid: String,
                            className: String,
                            callback: RequestCallback<List<MessageChannel>, Error>?)

    fun subscribeForTeacherMessageChannels(paramsBm: TeacherParamsBm,
                                           callback: Callback): String

    fun subscribeForStudentMessageChannels(paramsBm: StudentParamsBm,
                                           callback: Callback): String

    fun unsubscribe(subscriptionId: String?)


    /**
     *  Subscribe chat message if the user a teacher.
     */
    fun subscribeForTeacherMessages(paramsBm: TeacherParamsBm,
                                    callback: MessageCallback): String

    /**
     *  Subscribe chat messages if the user a student.
     */
    fun subscribeForStudentMessage(paramsBm: StudentParamsBm,
                                   callback: MessageCallback): String

    /**
     *
     */
    fun subscribeTeacherMessageNotifications(paramsBm: TeacherParamsBm,
                                             callback: NotificationListener)

    /**
     *
     */
    fun subscribeStudentMessageNotifications(paramsBm: StudentParamsBm,
                                             callback: NotificationListener)


    interface NotificationListener {
        fun onMessage(fbChatMessage: ChatMessage, msgCount: Int, messageChannel: MessageChannel?)
        fun onSnapshot(messageChannel: MessageChannel?, msgCount: Int)
    }

}