package org.vervone.kidskonnect.core.domain.model

import com.google.gson.annotations.SerializedName

class UserData(
    @SerializedName("uid") val uid: String,
    @SerializedName("isTeacher") val isTeacher: Boolean
)