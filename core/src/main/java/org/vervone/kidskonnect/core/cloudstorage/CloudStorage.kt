package org.vervone.kidskonnect.core.cloudstorage

import java.io.File

interface CloudStorage {

    /**
     *  Upload the file to the cloud storage .
     *  and it will delete the local file after uploading if deleteAfter
     *  is true.
     */
    fun upload(key: String, file: File, callback: UploadListener, deleteAfter: Boolean = true): Int

    fun uploadPublic(key: String, file: File,
                     callback: UploadListener, deleteAfter: Boolean = true): Int

    //TODO : Remove this method and use download method
    fun getSignedUrl(objectKey: String, callback: Callback)

    fun deleteFileFromCloud(objectKey: String)
    fun download(key: String?, file: File?, callback: DownloadListener)

    fun cancel(id: Int)

    fun downloadIfNeeded(key: String?, file: File?, callback: DownloadListener)

    interface Callback {
        fun onResult(url: String?)
    }

    interface UploadListener {
        fun onComplete()
        fun onFailed()
        fun onProgress(pecentage: Int) {}
    }

    /**
     *  Listener for file download
     */
    interface DownloadListener {
        /**
         *  Callback after successful download
         */
        fun onComplete(downloadedPath: String?)

        /**
         * Download failure callback
         */
        fun onError(error: Error)

        /**
         *  callback for upload progress
         */
        fun onProgress(percentage: Int) {}
    }
}