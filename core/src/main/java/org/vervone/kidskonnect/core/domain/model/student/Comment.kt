package org.vervone.kidskonnect.core.domain.model.student

import java.io.Serializable

class Comment : Serializable {
    var comment: String? = null
    var commentType: CommentType? = null
    var commentedPersonId: String? = null
    var commentedPersonName: String? = null
    var postTime: Long? = null
}