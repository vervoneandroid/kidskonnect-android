package org.vervone.kidskonnect.core.data.network

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.UploadListener
import org.vervone.kidskonnect.core.cloudstorage.CloudStorage
import org.vervone.kidskonnect.core.data.network.model.*
import org.vervone.kidskonnect.core.data.network.model.academicyear.FbAcademicYrTeacher
import org.vervone.kidskonnect.core.data.network.model.school.FbSchool
import org.vervone.kidskonnect.core.data.network.model.student.FbAcademicReport
import org.vervone.kidskonnect.core.data.network.model.student.FbComments
import org.vervone.kidskonnect.core.data.network.model.student.FbProfileDetails
import org.vervone.kidskonnect.core.data.network.model.student.FbStudent
import org.vervone.kidskonnect.core.domain.model.MessageType
import org.vervone.kidskonnect.core.helper.AppHelper
import org.vervone.kidskonnect.core.helper.Task
import org.vervone.kidskonnect.core.helper.cast
import org.vervone.kidskonnect.core.helper.safeMap
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class ApiHelperImpl : ApiHelper {

    private val uploadCache = HashMap<String, MediaMessageUploader>()
    private val subscribers = Hashtable<String, EventListener>()

    private val auth: FirebaseAuth
        get() = FirebaseAuth.getInstance(ConnectionHelper.fbApp())

    private val database: FirebaseDatabase
        get() = FirebaseDatabase.getInstance(ConnectionHelper.fbApp())


    override fun isLoggedIn(): Boolean {
        return auth.currentUser != null
    }


    override fun doLoginCall(email: String,
                             pwd: String,
                             callback: RequestCallback<FirebaseUser, Exception>) {
        auth.signInWithEmailAndPassword(email, pwd).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                // Sign in success, update UI with the signed-in user's information
                DLog.v(this, "Login Successfull")
                val user = auth.currentUser

                DLog.v(this, user.toString())

                callback.onSuccess(user)

            } else {
                // If sign in fails, display a message to the user.
                DLog.w(this, "signInWithEmail:failure", task.exception)

                callback.onError(task.exception)
            }

        }

    }

    private fun getRef(path: String): DatabaseReference = database.getReference(path)

    override fun fetchTeacherProfile(uid: String?,
                                     callback: RequestCallback<FbTeacher, ApiError>) {
        getRef(TEACHERS).child(uid!!)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnap: DataSnapshot) {
                    val obj = dataSnap.getValue(FbTeacher::class.java)
                    //System.out.println(obj)
                    callback.onSuccess(obj)
                }

                override fun onCancelled(dbError: DatabaseError) {
                    //System.out.println(dbError)
                    callback.onError(error = ApiError(dbError.toException()))
                }
            })
    }

    override fun fetchStudentsProfile(uid: String?,
                                      callback: RequestCallback<FbStudent, DatabaseError>) {
        database.getReference(STUDENTS)
            .child(uid!!)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnap: DataSnapshot) {
                    val student = dataSnap.getValue(FbStudent::class.java)
                    //DLog.d(this , student)
                    student?.id = uid
                    callback.onSuccess(student)
                }

                override fun onCancelled(dbError: DatabaseError) {
                    //System.out.println(dbError)
                    callback.onError(error = dbError)
                }
            })
    }


    override fun getCurrentAcademicYear(callback: RequestCallback<String, DatabaseError>) {
        database.getReference(CUR_ACAD_YEAR)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnap: DataSnapshot) {
                    val obj = dataSnap.getValue(String::class.java)
                    //System.out.println(obj)
                    callback.onSuccess(obj)
                }

                override fun onCancelled(dbError: DatabaseError) {
                    //System.out.println(dbError)
                    callback.onError(error = dbError)
                }
            })
    }

    override fun getAcademicYearData(uid: String?,
                                     academicYr: String,
                                     callback: RequestCallback<FbAcademicYrTeacher, ApiError>) {

        database.getReference("$academicYr/teachers/$uid")
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnap: DataSnapshot) {
                    val obj = dataSnap.getValue(FbAcademicYrTeacher::class.java)
                    if (obj != null) {
                        DLog.v(this, obj.toString())
                        callback.onSuccess(obj)
                    } else {
                        callback.onError(error = NoDataError())
                    }
                }

                override fun onCancelled(dbError: DatabaseError) {
                    DLog.v(this, dbError.toString())
                    val apiError = ApiError(dbError.toException())
                    callback.onError(error = apiError)
                }
            })

    }


    override fun getNews(
            academicYr: String, teacherUid: String, isAdmin: Boolean,
            callback: RequestCallback<List<FbNews>, DatabaseError>
    ) {
        TeachersNewsFetcherHelper(academicYr, teacherUid, isAdmin, callback).fetch()
    }

    override fun getClasses(callback: RequestCallback<List<String>, DatabaseError>) {
        database.getReference("classList").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                //System.out.println(dbError)
                callback.onError(dbError)
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val schoolClasses = snapshot.getValue(TYPE_INDICATOR_STRINGS)
                //System.out.println(schoolClasses)
                callback.onSuccess(schoolClasses)
            }
        })
    }

    override fun getClasses(academicYr: String,
                            teacherUid: String,
                            callback: RequestCallback<List<String>, DatabaseError>) {
        database.getReference("$academicYr/teachers/$teacherUid")
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnap: DataSnapshot) {
                    val obj = dataSnap.getValue(FbAcademicYrTeacher::class.java)
                    callback.onSuccess(obj?.classesAndSubjects?.keys?.toList() ?: emptyList())
                }

                override fun onCancelled(dbError: DatabaseError) {
                    System.out.println(dbError)
                    callback.onSuccess(emptyList())
                }
            })
    }


    override fun postNews(academicYr: String,
                          news: FbNews,
                          callback: RequestCallback<Void, Error>) {
        val newsRef = database.reference.child(academicYr).child("newsFeeds");
        val generatedId = news.id!!
        newsRef.child(generatedId).setValue(news)
            .addOnSuccessListener {
                DLog.v(this, "News post successfully")
                if (news.targets != null) {
                    addNewsForClass(academicYr, generatedId, news.targets!!, callback)
                }

            }.addOnFailureListener { ex ->
                DLog.v(this, "News post failed. $ex")
                callback.onError(Error(ex.message))
            }

    }


    /**
     *  when we add a news in the newsfeed node , we also should add
     *  the news to the newsfeed inside each class node
     */
    private fun addNewsForClass(
            academicYr: String, newsId: String,
            targets: HashMap<String, String>, callback: RequestCallback<Void, Error>
    ) {

        val keys = ArrayList<String>(targets.keys)

        targets.forEach { entry ->
            val newsRef = database.reference.child(academicYr).child(CLASS).child(entry.key)
                .child(NEWS_FEEDS);
            newsRef.child(newsId).setValue(true)
                .addOnSuccessListener {
                    DLog.v(this, "News post for class ${entry.key} success")

                    if (keys.remove(entry.key) && keys.size == 0) {
                        DLog.v(this, "News post completed for all classes")
                        callback.onSuccess(null);
                    }

                }.addOnFailureListener { err ->
                    DLog.v(this, "News post for class failed $err")
                    if (keys.remove(entry.key) && keys.size == 0) {
                        DLog.v(this, "News post completed for all classes")
                        callback.onError(Error(err.message))
                    }
                }
        }
    }


    override fun generateNewsId(academicYr: String): String {
        val newsRef = database.reference.child(academicYr)
            .child(NEWS_FEEDS);
        return newsRef.push().key!!
    }

    @Suppress("UNCHECKED_CAST")
    override fun deleteNews(
            academicYr: String, newsId: String, cloudStorage: CloudStorage?,
            callback: RequestCallback<String, Error>
    ) {
        val refForNewsListOfClass = database.reference.child(academicYr).child(CLASS)
        refForNewsListOfClass.runTransaction(object : Transaction.Handler {
            override fun onComplete(dbError: DatabaseError?, p1: Boolean, p2: DataSnapshot?) {
                DLog.v(this, "deleteNews completed")

                if (dbError == null) {
                    deleteNewsFromAllNews(academicYr, newsId, cloudStorage, callback)
                }

            }

            override fun doTransaction(mutableData: MutableData): Transaction.Result {

                if (mutableData.value == null || !mutableData.hasChildren()) {
                    return Transaction.success(mutableData)
                }

                val classes = mutableData.value as? HashMap<String, Any>

                for (clas in classes!!.entries) {
                    val classDetails = clas.value as? HashMap<String, Any>
                    if (classDetails != null && classDetails.contains(NEWS_FEEDS)) {
                        val newsFeeds = classDetails[NEWS_FEEDS]?.cast<HashMap<String, Any>?>()
                        if (newsFeeds != null && newsFeeds.containsKey(newsId)) {
                            newsFeeds.remove(newsId)
                        }
                    }
                }

                mutableData.value = classes
                return Transaction.success(mutableData)
            }
        })

    }

    /**
     * Used to remove news from the master news table .
     */
    @Suppress("UNCHECKED_CAST")
    private fun deleteNewsFromAllNews(
            academicYr: String, newsId: String, cloudStorage: CloudStorage?,
            callback: RequestCallback<String, Error>
    ) {
        val newsRef = database.reference.child(academicYr).child(NEWS_FEEDS)
        newsRef.runTransaction(object : Transaction.Handler {
            override fun onComplete(dbError: DatabaseError?, p1: Boolean, p2: DataSnapshot?) {
                DLog.v(this, "deleteNews master completed ")
                callback.onSuccess(newsId)
            }

            override fun doTransaction(mutableData: MutableData): Transaction.Result {
                val allNewsDict =
                    mutableData.value as HashMap<String, Any>?
                        ?: return Transaction.success(mutableData)

                if (allNewsDict.containsKey(newsId)) {
                    val news = allNewsDict.remove(newsId) as HashMap<String, Any>
                    val attachments = news[IMAGES] as? HashMap<String, String>
                    deleteNewsAttachmentsCloud(cloudStorage, attachments)
                    mutableData.value = allNewsDict
                }
                return Transaction.success(mutableData)
            }
        })
    }

    private fun deleteNewsAttachmentsCloud(
            cloudStorage: CloudStorage?,
            attachments: HashMap<String, String>?
    ) {
        attachments?.entries?.forEach { e ->
            cloudStorage?.deleteFileFromCloud(e.value)
            DLog.v(this, "#attachemnts : # ${e.value}")
        }
    }


    override fun updateLikes(
            academicYr: String, newsId: String, user: String, isLike: Boolean,
            callback: RequestCallback<String, Error>
    ) {
        database.reference.child(academicYr).child(NEWS_FEEDS).child(newsId)
            .runTransaction(object : Transaction.Handler {
                override fun onComplete(dbError: DatabaseError?, p1: Boolean, p2: DataSnapshot?) {
                    updateLikeForTeacher(academicYr, newsId, user, isLike, callback)
                }

                override fun doTransaction(mutableData: MutableData): Transaction.Result {
                    val news = mutableData.getValue(FbNews::class.java)
                        ?: return Transaction.success(mutableData)
                    val likes = news.likes ?: 0
                    val cnt = if (isLike) 1 else -1
                    news.likes = likes.plus(cnt)
                    mutableData.value = news
                    return Transaction.success(mutableData)
                }
            })
    }

    private fun updateLikeForTeacher(
            academicYr: String, newsId: String,
            user: String, isLike: Boolean, callback: RequestCallback<String, Error>
    ) {
        database.reference.child(academicYr).child(TEACHERS)
            .child(user)
            .child(LIKED_NEWS)
            .child(newsId).setValue(isLike) { dbError, _ ->
                if (dbError != null) {
                    //TODO News : Error callback
                    callback.onError(Error(dbError.message))
                } else {
                    DLog.v(this, "updateLikeForTeacher()")
                    callback.onSuccess(newsId)
                }
            }
    }

    private var likedNews: HashMap<String, Boolean>? = null

    override fun getLikedNews(academicYr: String, userUid: String) {
        database.reference.child(academicYr).child(TEACHERS)
            .child(userUid)
            .child(LIKED_NEWS).addValueEventListener(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {
                    DLog.v(this, " ## $dbError")
                }

                override fun onDataChange(snapShot: DataSnapshot) {
                    DLog.v(this, " ## " + snapShot.value.toString())
                    snapShot.value?.let {
                        likedNews = it.cast()
                    }
                }
            })
    }

    override fun updateStudentNews(
            academicYr: String,
            newsId: String,
            user: String,
            className: String,
            isLike: Boolean,
            callback: RequestCallback<String, Error>
    ) {
        database.reference
            .child(academicYr)
            .child(NEWS_FEEDS)
            .child(newsId)
            .runTransaction(object : Transaction.Handler {
                override fun onComplete(dbError: DatabaseError?, p1: Boolean, p2: DataSnapshot?) {
                    updateLikeForStudent(academicYr, newsId, user, className, isLike, callback)
                }

                override fun doTransaction(mutableData: MutableData): Transaction.Result {
                    val news = mutableData.getValue(FbNews::class.java)
                        ?: return Transaction.success(mutableData)
                    val likes = news.likes ?: 0
                    val cnt = if (isLike) 1 else -1
                    news.likes = likes.plus(cnt)
                    if (news.likes!! < 0) {
                        news.likes = 0
                    }
                    mutableData.value = news
                    return Transaction.success(mutableData)
                }
            })

    }


    private fun updateLikeForStudent(
            academicYr: String,
            newsId: String,
            user: String,
            className: String,
            isLike: Boolean,
            callback: RequestCallback<String, Error>
    ) {

        val dbRef = database.reference
            .child(academicYr)
            .child("class")
            .child(className)
            .child("students")
            .child(user)
            .child("likedNews")
            .child(newsId)

        if (isLike) {
            dbRef.setValue(true) { dbError, _ ->
                if (dbError != null) {
                    //TODO News : Error callback
                    callback.onError(Error(dbError.message))
                } else {
                    DLog.v(this, "updateLikeForStudent()")
                    callback.onSuccess(newsId)
                }
            }
        } else {
            dbRef.removeValue { dbError, _ ->
                if (dbError != null) {
                    //TODO News : Error callback
                    callback.onError(Error(dbError.message))
                } else {
                    DLog.v(this, "updateLikeForStudent()")
                    callback.onSuccess(newsId)
                }
            }
        }
    }

    override fun getStudentLikedNews(
            academicYr: String,
            userUid: String, className: String
    ) {
        database.reference.child(academicYr)
            .child("class")
            .child(className)
            .child("students")
            .child(userUid)
            .child("likedNews")
            .addValueEventListener(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {
                    DLog.v(this, " ## " + dbError.toString())
                }

                override fun onDataChange(snapShot: DataSnapshot) {
                    DLog.v(this, " ## " + snapShot.value.toString())
                    snapShot.value?.let {
                        likedNews = it.cast()
                    }
                }
            })

    }

    fun isLikedNews(newsId: String): Boolean {
        likedNews?.let {
            return it[newsId] ?: false
        }
        return false
    }


    override fun getAllChatRooms(
            academicYr: String, userUid: String,
            classes: List<String>,
            callback: RequestCallback<HashMap<String, FbMessageChannel>, ApiError>?
    ) {
        GetTeacherChatRooms(academicYr, userUid, classes, callback).execute()
    }

    override fun getChatRoomsForStudent(
            academicYr: String,
            userUid: String,
            className: String,
            callback: RequestCallback<HashMap<String, FbMessageChannel>, ApiError>?) {
        GetStudentChatRooms(academicYr, userUid, className, callback).execute()
    }


    override fun getLastMessage(academicYr: String, chatRoomId: String,
                                callback: RequestCallback<HashMap<String, FbChatMessage>, ApiError>?) {
        val query = database.reference
            .child(academicYr)
            .child(ALL_MESSAGES)
            .child(chatRoomId)
            .limitToLast(1)
        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    val typeIndicator =
                        object : GenericTypeIndicator<HashMap<String, FbChatMessage>>() {}
                    val map = dataSnapshot.getValue(typeIndicator)
                    val adjusted = map?.map { chatRoomId to it.value }?.toMap()
                    callback?.onSuccess(HashMap(adjusted))
                } else {
                    callback?.onError(ApiError())
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                callback?.onError(ApiError(databaseError.message))
            }
        })

    }


    override fun getChatRoomIfAvailable(
            academicYr: String, targetsId: String,
            callback: RequestCallback<Pair<String, FbMessageChannel>?, DatabaseError>?
    ) {

        //getServerTime()
        database.reference.child(academicYr)
            .child(ALL_MESSAGE_CHANNELS)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val typeIndicator =
                        object : GenericTypeIndicator<HashMap<String, FbMessageChannel>>() {}
                    val allChatRooms = snapshot.getValue(typeIndicator)

                    val chatRoom = allChatRooms?.entries?.firstOrNull { entry ->
                        entry.value.targetId == targetsId
                    }

                    if (chatRoom != null) {
                        callback?.onSuccess(Pair(chatRoom.key, chatRoom.value))
                    } else {
                        callback?.onSuccess(null)
                    }
                }

                override fun onCancelled(dbError: DatabaseError) {
                    callback?.onError(dbError)
                }
            })
    }


    override fun getAllMessages(
            academicYr: String,
            chatRoomId: String,
            callback: RequestCallback<HashMap<String, FbChatMessage>, DatabaseError>?
    ): MessagesSubscription {
        return MessagesSubscription(academicYr, chatRoomId, callback)
    }

    // Only for removing some test data from server
    //TODO TP : remove this code once we complete this project
    private fun delete(allMessages: HashMap<String, FbChatMessage>?) {
        Thread(Runnable {

            allMessages?.forEach { (_, fbChatMessage) ->
                if (fbChatMessage.attachmentsUrl != null) {
                    AppHelper.getCloudStorage()?.deleteFileFromCloud(fbChatMessage.attachmentsUrl!!)
                }

                if (fbChatMessage.thumpNailImagePath != null) {
                    AppHelper.getCloudStorage()
                        ?.deleteFileFromCloud(fbChatMessage.thumpNailImagePath!!)
                }
            }
            //

        }).start()
    }


    private fun generateChatId(academicYr: String, chatRoomId: String): String {
        val ref = database.reference.child(academicYr)
            .child(ALL_MESSAGES).child(chatRoomId)

        return ref.push().key!!
    }

    override fun sendMessage(
            academicYr: String,
            chatRoomId: String,
            chatMessage: FbChatMessage,
            callback: UploadListener<FbChatMessage, DatabaseError>?): Task {

        val chatMsgId = generateChatId(academicYr, chatRoomId)
        chatMessage.chatId = chatMsgId

        if (chatMessage.messageType == MessageType.TEXT.value) {
            updateMessage(academicYr, chatRoomId, chatMessage, callback)
            return EmptyTask()
        } else {
            val uploader = MediaMessageUploader(
                    academicYr, chatRoomId, chatMessage, object : MediaMessageUploader.MediaUploadListener {
                override fun onComplete() {
                    DLog.v(this, "MediaMessageUploader.OnCompleteListener")
                    uploadCache.remove(chatMsgId)
                    updateMessage(academicYr, chatRoomId, chatMessage, callback)
                }

                override fun onFailed() {
                    //todo : media message upload error handling
                }

                override fun onProgress(percent: Int) {
                    callback?.onProgress(percent)
                }
            })
            uploadCache[chatMsgId] = uploader
            uploader.upload()
            return uploader
        }

    }

    private class EmptyTask : Task {
        override fun cancel() {
        }
    }

    private fun updateMessage(
            academicYr: String,
            chatRoomId: String,
            chatMessage: FbChatMessage,
            callback: RequestCallback<FbChatMessage, DatabaseError>?
    ) {
        database.reference.child(academicYr)
            .child(ALL_MESSAGES).child(chatRoomId).child(chatMessage.chatId!!)
            .setValue(chatMessage, object : DatabaseReference.CompletionListener {
                override fun onComplete(dbError: DatabaseError?, dbRef: DatabaseReference) {
                    if (dbError == null) {
                        DLog.v(this, "DatabaseReference.CompletionListener.onComplete()")
                        callback?.onSuccess(chatMessage)
                    } else {
                        callback?.onError(dbError)
                    }
                }
            })
    }


    override fun getScheduledExams(academicYr: String,
                                   classes: List<String>,
                                   callback: RequestCallback<List<FbExam>, ApiError>?) {
        GetExamSchedules(academicYr, classes, callback).execute()
    }

    override fun getAllLeaveRequestsByStudent(
            academicYr: String,
            schoolClass: String,
            userId: String?,
            callback: RequestCallback<List<FbLeave>, ApiError>) {
        database.reference.child(academicYr)
            .child(CLASS)
            .child(schoolClass)
            .child(STUDENTS)
            .child(userId!!)
            .child(LEAVES_LIST)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {
                    callback.onError(ApiError(dbError.message))
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    val typeIndicator = object : GenericTypeIndicator<HashMap<String, Boolean>>() {}
                    val leaves = snapshot.getValue(typeIndicator)
                    LeavesDetailsFetch(leaves?.keys?.toList(), academicYr, callback)
                        .queryLeaveDetails()
                }
            })

    }

    override fun subscribeAllLeaveRequestsByStudent(
            academicYr: String,
            schoolClass: String,
            userId: String?,
            changeListener: DataChangeListener): String {

        val ref = database.reference
            .child(academicYr)
            .child(CLASS)
            .child(schoolClass)
            .child(STUDENTS)
            .child(userId!!)
            .child(LEAVES_LIST)
        val listener = EventListener(ref, changeListener)
        ref.addValueEventListener(listener)
        val uuid = ApiUtils.getUUID()
        subscribers[uuid] = listener
        return uuid
    }

    override fun unsubscribeAllLeaveRequestsByStudent(subscriberId: String) {
        val subscriber = subscribers.remove(subscriberId)
        subscriber?.remove()
    }

    override fun subscribeAllLeavesRequestToTeacher(academicYr: String,
                                                    userId: String?,
                                                    changeListener: DataChangeListener): String {

        val ref = database.reference.child(academicYr)
            .child(TEACHERS).child(userId!!).child(LEAVES_LIST)
        val listener = EventListener(ref, changeListener)
        ref.addValueEventListener(listener)

        val uuid = ApiUtils.getUUID()
        subscribers[uuid] = listener
        return uuid
    }

    override fun unsubscribeAllLeavesRequestToTeacher(subscriberId: String) {
        val subscriber = subscribers.remove(subscriberId)
        subscriber?.remove()
    }

    override fun getAllLeavesRequestToTeacher(
            academicYr: String,
            userId: String?,
            callback: RequestCallback<List<FbLeave>, ApiError>
    ) {


        database.reference.child(academicYr)
            .child(TEACHERS).child(userId!!).child(LEAVES_LIST)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {
                    callback.onError(ApiError(dbError.message))
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    val typeIndicator = object : GenericTypeIndicator<HashMap<String, Boolean>>() {}
                    val leaves = snapshot.getValue(typeIndicator)
                    LeavesDetailsFetch(leaves?.keys?.toList(), academicYr, callback)
                        .queryLeaveDetails()
                }
            })
    }

    override fun updateLeave(
            academicYr: String,
            leave: FbLeave,
            callback: RequestCallback<FbLeave, ApiError>
    ) {

        //val successCnt = AtomicInteger(2)

        val dbRef = database.reference
            .child(academicYr)
            .child(LEAVES_LIST)
            .child(leave.id!!)
        dbRef.runTransaction(object : Transaction.Handler {

            override fun doTransaction(mutableData: MutableData): Transaction.Result {
                val leaveTemp = mutableData.getValue(FbLeave::class.java)
                    ?: return Transaction.success(mutableData)
                leaveTemp.leaveStatus = leave.leaveStatus
                leaveTemp.replyString = leave.replyString
                mutableData.value = leaveTemp
                return Transaction.success(mutableData)
            }

            override fun onComplete(dbError: DatabaseError?, p1: Boolean, snap: DataSnapshot?) {
                if (dbError != null) {
                    callback.onError(ApiError(Exception(dbError.message)))
                } else {
                    callback.onSuccess(leave)
                }
            }
        })
    }

    override fun getAllNotifications(
            academicYr: String,
            callback: RequestCallback<List<FbNotification>, ApiError>
    ) {
        val dbRef = database.reference.child(academicYr)
            .child(NOTIFICATION)
        dbRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                callback.onError(ApiError(Exception(dbError.message)))
            }

            override fun onDataChange(snapShot: DataSnapshot) {
                val typeIndicator =
                    object : GenericTypeIndicator<HashMap<String, FbNotification>>() {}
                val notificationMap = snapShot.getValue(typeIndicator)
                val notifications = notificationMap.safeMap { ApiUtils.mapId(it) }
                callback.onSuccess(notifications)
            }
        })
    }

    override fun getSchoolContact(callback: RequestCallback<FbSchoolContact, ApiError>) {
        val dbRef = database.reference.child(CONTACT_DETAILS)
        dbRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                callback.onError(ApiError(Exception(dbError.message)))
            }

            override fun onDataChange(snapShot: DataSnapshot) {
                val typeIndicator = object : GenericTypeIndicator<FbSchoolContact>() {}
                val schoolContact = snapShot.getValue(typeIndicator)
                callback.onSuccess(schoolContact)
            }
        })
    }

    override fun getAllStudents(
            academicYr: String,
            query: String?,
            schoolClass: String?,
            callback: RequestCallback<List<FbStudent>, ApiError>
    ) {
        StudentsFetch.getStudents(
                academicYr,
                schoolClass,
                query,
                callback
        )
    }

    override fun addComment(
            studentId: String,
            comment: FbComments,
            callback: RequestCallback<FbComments, ApiError>
    ) {
        val dbRef = database.getReference(STUDENTS).child(studentId)
        dbRef.runTransaction(object : Transaction.Handler {

            override fun doTransaction(mutableData: MutableData): Transaction.Result {
                val typeIndicator = object : GenericTypeIndicator<FbStudent>() {}
                val student = mutableData.getValue(typeIndicator)

                student?.let {
                    val newCommentList = ArrayList<FbComments>()
                    if (it.comments != null) {
                        newCommentList.addAll(it.comments!!)
                    }
                    newCommentList.add(comment)
                    it.comments = newCommentList
                }
                //Updating comments list.
                mutableData.value = student
                return Transaction.success(mutableData)
            }

            override fun onComplete(dbError: DatabaseError?, p1: Boolean, snap: DataSnapshot?) {
                if (dbError != null) {
                    callback.onError(ApiError(Exception(dbError.message)))
                } else {
                    callback.onSuccess(comment)
                }
            }
        })

    }


    override fun subscribeForStudent(listener: StudentsFetch.DateChangeListener): String {
        return StudentsFetch.subscribe(listener)
    }

    override fun unsubscribeForStudent(subscriberId: String) {
        StudentsFetch.unsubscribe(subscriberId)
    }

    override fun getComments(studentId: String, commentType: Int): ArrayList<FbComments> {
        return StudentsFetch.getComments(studentId, commentType)
    }

    override fun getAllTeacher(
            academicYr: String,
            callback: RequestCallback<Map<String, FbTeacherDetails>, ApiError>
    ) {
        Teachers.create(academicYr, object : Teachers.Callback {
            override fun onError() {}
            override fun onSuccess(teacherDetails: Map<String, FbTeacherDetails>) {
                callback.onSuccess(teacherDetails)
            }
        })
    }

    override fun getTeacherDetails(academicYr: String, className: String,
                                   callback: RequestCallback<Map<String, FbTeacherDetails>, ApiError>) {
        Teachers.create(academicYr, object : Teachers.Callback {
            override fun onError() {}
            override fun onSuccess(teacherDetails: Map<String, FbTeacherDetails>) {
                fetchTeacherIds(academicYr, className, teacherDetails, callback)
            }
        })
    }

    private fun fetchTeacherIds(academicYr: String,
                                className: String,
                                teacherDetails: Map<String, FbTeacherDetails>,
                                callback: RequestCallback<Map<String, FbTeacherDetails>, ApiError>) {
        getTeachers(academicYr, className, object : RequestCallback<HashMap<String, String>, ApiError> {
            override fun onSuccess(res: HashMap<String, String>?) {
                val studentId = res?.map { it.value.split("|")[0] }
                if (studentId != null) {
                    val map = teacherDetails.filter { studentId.contains(it.key) }
                    callback.onSuccess(map)
                } else {
                    callback.onSuccess(emptyMap())
                }
            }

            override fun onError(error: ApiError?) {
                callback.onSuccess(emptyMap())
            }
        })
    }


    override fun doLogout() {
        FirebaseAuth.getInstance().signOut()
        likedNews?.clear()
        MessageChannelsListener.unsubscribeAll()
        MessageSubscriber.unsubscribeAll()
        StudentSubscribe.unsubscribeAll()
        LeaveRequestsNotifications.resetAll()
        CommentsSubscribe.unsubscribeAll()
        MessageNotifications.resetAll()
        LeaveStatusUpdateNotifications.resetAll()
    }

    override fun getStudentNews(
            academicYr: String,
            className: String,
            callback: RequestCallback<List<FbNews>, Error>) {
        StudentsNewsFetcherHelper(academicYr, className, callback).fetch()
    }


    override fun createMessageChannelByTeacher(
            academicYr: String,
            teacherId: String,
            messageChannel: FbMessageChannel,
            callback: RequestCallback<Pair<String, FbMessageChannel>, ApiError>
    ) {
        MessageChannelCreationHelper(academicYr,
                                     messageChannel,
                                     callback,
                                     teacherId).execute()
    }

    override fun createMessageChannelByStudent(
            academicYr: String,
            className: String,
            teacherId: String,
            messageChannel: FbMessageChannel,
            callback: RequestCallback<Pair<String, FbMessageChannel>, ApiError>
    ) {
        MessageChannelCreationHelper(academicYr,
                                     className,
                                     teacherId,
                                     messageChannel,
                                     callback).execute()
    }


    //TODO :  Not using now
    private fun getServerTime() {
        val offsetRef = FirebaseDatabase.getInstance().getReference(".info/serverTimeOffset")
        offsetRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val offset = snapshot.getValue(Double::class.java) ?: 0.0
                val estimatedServerTimeMs = System.currentTimeMillis() + offset

                DLog.w(this, "estimatedServerTimeMs ${Date(estimatedServerTimeMs.toLong())}")
            }

            override fun onCancelled(error: DatabaseError) {
                DLog.w(this, "Listener was cancelled")
            }
        })

    }

    override fun getTeachers(academicYr: String,
                             className: String,
                             callback: RequestCallback<HashMap<String, String>, ApiError>) {
        //2019-2020/class/1 A/teachers
        database.reference
            .child(academicYr)
            .child(CLASS)
            .child(className)
            .child(TEACHERS)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {
                    callback.onError(ApiError(Exception(dbError.message)))
                }

                override fun onDataChange(snapShot: DataSnapshot) {
                    val subjectTeacherMap = snapShot.getValue(TYPE_INDICATOR_STRING_MAP)
                    callback.onSuccess(subjectTeacherMap)
                }
            })

    }

    override fun sendLeaveRequest(academicYr: String,
                                  teacherId: String,
                                  leave: FbLeave,
                                  callback: RequestCallback<FbLeave, ApiError>) {
        ApplyLeaveHelper(academicYr, teacherId, leave, callback).execute()
    }


    override fun subscribe(topics: Topics,
                           params: TeacherParams,
                           callback: Callback): String {
        when (topics) {
            Topics.CHAT_ROOMS ->
                return MessageChannelsListener.subscribe(params,
                                                         callback)
        }
    }

    override fun subscribe(topics: Topics,
                           params: StudentParams,
                           callback: Callback): String {
        when (topics) {
            Topics.CHAT_ROOMS ->
                return MessageChannelsListener.subscribe(params,
                                                         callback)
        }
    }

    override fun unsubscribe(subscriberId: String) {
        MessageChannelsListener.unsubscribe(subscriberId)
    }

    override fun subscribeForMessages(params: StudentParams,
                                      callback: MessageCallback): String {
        return MessageSubscriber.subscribe(params, callback)
    }

    override fun subscribeForMessages(params: TeacherParams,
                                      callback: MessageCallback): String {
        return MessageSubscriber.subscribe(params, callback)
    }

    fun unsubscribeMessages(subsID: String) {
        MessageSubscriber.unsubscribe(subsID)
    }

    override fun subscribeStudent(studentId: String,
                                  listener: StudentSubscribe.StudentListener): String {
        return StudentSubscribe.subscribe(studentId, listener)
    }

    override fun unsubscribeStudent(subscriptionId: String) {
        StudentSubscribe.unsubscribe(subscriptionId)
    }

    override fun getAcademicReports(studentId: String,
                                    callback: RequestCallback<ArrayList<FbAcademicReport>, ApiError>) {

        database.reference
            .child(STUDENTS)
            .child(studentId)
            .child(ACADEMIC_REPORTS)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {
                    callback.onError(ApiError(Exception(dbError.message)))
                }

                override fun onDataChange(snapShot: DataSnapshot) {
                    val academicReport = snapShot.getValue(TYPE_INDICATOR_ACADEMIC_REPORTS)
                    callback.onSuccess(academicReport)
                }
            })

    }

    override fun getRequiredAppVersions(callback: RequestCallback<FbAppVersion, ApiError>) {
        database.reference
            .child(APP_VERSION)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {
                    callback.onError(ApiError(Exception(dbError.message)))
                }

                override fun onDataChange(snapShot: DataSnapshot) {
                    val appVersion = snapShot.getValue(FbAppVersion::class.java)
                    callback.onSuccess(appVersion)
                }
            })
    }

    override fun subscribeForAppVersion(subscriber: AppVersionSubsribers.VersionSubscriber) {
        AppVersionSubsribers.subscribe(subscriber)
    }

    override fun updateStudentPhoneNumber(studentId: String, phoneNo: String,
                                          callback: RequestCallback<String, ApiError>) {
        val dbRef = database.getReference(STUDENTS)
            .child(studentId)
            .child(PROFILE_DETAILS)
        dbRef.runTransaction(object : Transaction.Handler {

            override fun doTransaction(mutableData: MutableData): Transaction.Result {
                val typeIndicator = object : GenericTypeIndicator<FbProfileDetails>() {}
                val profileDetails = mutableData.getValue(typeIndicator)

                profileDetails?.let {
                    profileDetails.localGuardianNumber = phoneNo
                }
                //Updating comments list.
                mutableData.value = profileDetails
                return Transaction.success(mutableData)
            }

            override fun onComplete(dbError: DatabaseError?, p1: Boolean, snap: DataSnapshot?) {
                if (dbError != null) {
                    callback.onError(ApiError(Exception(dbError.message)))
                } else {
                    callback.onSuccess(phoneNo)
                }
            }
        })
    }


    companion object {
        val TYPE_INDICATOR_ACADEMIC_REPORTS = object : GenericTypeIndicator<ArrayList<FbAcademicReport>>() {}
        val TYPE_INDICATOR_STRINGS = object : GenericTypeIndicator<ArrayList<String>>() {}
        val TYPE_INDICATOR_STRING_MAP = object : GenericTypeIndicator<HashMap<String, String>>() {}
        val TYPE_INDICATOR_STRING_BOOL_MAP = object : GenericTypeIndicator<HashMap<String, Boolean>>() {}
        val TYPE_INDICATOR_SCHOOLS = object : GenericTypeIndicator<HashMap<String, FbSchool>>() {}
    }

}