package org.vervone.kidskonnect.core.domain

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.model.Exam

/**
 *  Facade for exams
 */
interface ExamsFacade {
    fun getScheduledExams(academicYr: String,
                          classes: List<String>,
                          callback: RequestCallback<List<Exam>, DomainError>?)
}