package org.vervone.kidskonnect.core.data.network.model.student

class FbComments {
    var comment: String? = null
    var commentType: Int? = null
    var commentedPersonId: String? = null
    var commentedPersonName: String? = null
    var postTime: Long? = null
}