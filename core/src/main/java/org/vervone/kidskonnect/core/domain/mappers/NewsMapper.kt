package org.vervone.kidskonnect.core.domain.mappers

import org.vervone.kidskonnect.core.Mapper
import org.vervone.kidskonnect.core.data.network.model.FbNews
import org.vervone.kidskonnect.core.domain.model.ContentType
import org.vervone.kidskonnect.core.domain.model.News

class NewsMapper : Mapper<FbNews, News> {
    override fun map(input: FbNews): News {
        val news = News()
        news.date = input.date
        news.description = input.description
        news.images = input.images
        news.likes = input.likes

        news.newsType = input.newsType
        news.senderId = input.senderId
        news.senderName = input.senderName

        news.targetList = ArrayList(input.targets?.keys)
        news.webLink = input.webLink
        news.contentType = ContentType.TEXT
        news.id = input.id

        input.images?.let {
            if (it.isNotEmpty()) {
                news.contentType = ContentType.MULTI_MEDIA
                news.attachments = ArrayList(it.values)
            }
        }

        input.webLink?.let {
            if (it.isNotEmpty()) {
                news.contentType = ContentType.WEB_LINKS
            }
        }

        return news
    }
}