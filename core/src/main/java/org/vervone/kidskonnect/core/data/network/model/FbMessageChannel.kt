package org.vervone.kidskonnect.core.data.network.model

import com.google.firebase.database.Exclude

class FbMessageChannel {

    var groupName: String? = null
    @Exclude
    @set:Exclude
    @get:Exclude
    var isGroupMessage: Boolean? = null
    var targetId: String? = null
    var targets: HashMap<String, String>? = null

    @Exclude
    @set:Exclude
    @get:Exclude
    var channelId: String? = null
    @Exclude
    @set:Exclude
    @get:Exclude
    var studentIdsAndClass: HashMap<String, String>? = null
    @Exclude
    @set:Exclude
    @get:Exclude
    var teacherIds: List<String>? = null
    @Exclude
    @set:Exclude
    @get:Exclude
    var classes: List<String>? = null

    fun getIsGroupMessage(): Boolean? {
        return this.isGroupMessage
    }

}