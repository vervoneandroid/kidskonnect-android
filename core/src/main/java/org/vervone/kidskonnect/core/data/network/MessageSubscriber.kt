package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.DatabaseError
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.model.FbChatMessage
import org.vervone.kidskonnect.core.data.network.model.FbMessageChannel
import org.vervone.kidskonnect.core.data.network.model.StudentParams
import org.vervone.kidskonnect.core.data.network.model.TeacherParams


typealias MCMap = HashMap<String, FbMessageChannel>
typealias ChatMessageMap = HashMap<String, FbChatMessage>


object MessageSubscriber : Callback {


    private var teacherParams: TeacherParams? = null
    private var studentParams: StudentParams? = null
    private val apiHelper = ApiHelper.Factory.apiHelper

    private val subscribers = HashMap<String, MessageCallback>()

    fun unsubscribe(subsId: String) {
        subscribers.remove(subsId)
    }

    fun subscribe(params: TeacherParams,
                  callback: MessageCallback): String {
        this.teacherParams = params
        this.studentParams = null
        val subsId = ApiUtils.getUUID()
        subscribers[subsId] = (callback)
        MessageChannelsListener.subscribe(params, this)
        return subsId
    }

    fun subscribe(params: StudentParams,
                  callback: MessageCallback): String {
        this.teacherParams = null
        this.studentParams = params
        val subsId = ApiUtils.getUUID()
        subscribers[subsId] = (callback)
        MessageChannelsListener.subscribe(params, this)

        return subsId
    }


    override fun onDataChange() {
        DLog.v(this, "onDataChange")
        if (teacherParams != null) {
            val params = teacherParams!!
            apiHelper.getAllChatRooms(params.academicYr,
                                      params.teacherId,
                                      params.classes,
                                      mcCallback)

        } else if (studentParams != null) {
            val params = studentParams!!
            apiHelper.getChatRoomsForStudent(params.academicYr,
                                             params.studentId,
                                             params.className,
                                             mcCallback)
        }
    }


    private fun getAcademicYear(): String {
        return studentParams?.academicYr ?: teacherParams?.academicYr ?: ""
    }

    private val mcCallback = object : RequestCallback<MCMap, ApiError> {

        override fun onError(error: ApiError?) {}
        override fun onSuccess(res: MCMap?) {
            if (res != null) {
                DLog.v(this, "mcCallback")
                getMessages(getAcademicYear(), ArrayList(res.keys))
            }
        }
    }


    private fun getMessages(academicYr: String,
                            chatIds: List<String>) {

        chatIds.forEach {
            apiHelper.getAllMessages(academicYr, it, MsgHandler(it))
        }
    }

    private class MsgHandler(val msgChannelId: String) : RequestCallback<ChatMessageMap, DatabaseError> {

        override fun onError(error: DatabaseError?) {}
        override fun onSuccess(res: ChatMessageMap?) {
            subscribers.forEach {
                it.value.onChange(msgChannelId, res?.size ?: 0)
            }
        }
    }

    fun unsubscribeAll() {
        this.subscribers.clear()
        this.studentParams = null
        this.teacherParams = null
    }

}