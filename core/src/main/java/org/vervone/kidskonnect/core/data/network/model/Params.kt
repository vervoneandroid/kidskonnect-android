package org.vervone.kidskonnect.core.data.network.model

data class StudentParams(val academicYr: String,
                         val className: String,
                         val studentId: String)

data class TeacherParams(val academicYr: String,
                         val isAdmin: Boolean = false,
                         val classes: List<String>,
                         val teacherId: String)