package org.vervone.kidskonnect.core.data.network.model

class FbTeacher {

    /*  {
         qualification=M.Mus,
         ProfileDetails={
             permanentAddress=thodupuzhap.o,
             idukki,
             dateOfJoining=1526036085903,
             emaiId=charan@gmail.com,
             dateOfBirth=1426036085903,
             residenceAddress=thodupuzhap.o,
             idukki,
             mobileNumber=95494949494
         },
         teacherName=Admin1
     } */


    var qualification: String? = null
    var teacherName: String? = null
    var profileImageUrl: String? = null
    var ProfileDetails: FbProfile? = null
    var subjects: List<String>? = null

}