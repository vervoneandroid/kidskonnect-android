package org.vervone.kidskonnect.core.data.network.model

class FbProfile {

    /*
    ProfileDetails={
             permanentAddress=thodupuzhap.o,idukki,
             dateOfJoining=1526036085903,
             emaiId=charan@gmail.com,
             dateOfBirth=1426036085903,
             residenceAddress=thodupuzhap.o,idukki,
             mobileNumber=95494949494
         },
     */

    var permanentAddress: String? = null
    var dateOfJoining: Long? = null
    var emaiId: String? = null
    var dateOfBirth: Long? = null
    var residenceAddress: String? = null
    var mobileNumber: String? = null

}