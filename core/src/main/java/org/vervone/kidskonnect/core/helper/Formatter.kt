package org.vervone.kidskonnect.core.helper

import java.text.SimpleDateFormat
import java.util.*


object Formatter {

    fun getDate(dateInMs: Long?): String {
        if (dateInMs != null) {
            val date = Date(dateInMs)
            val formatter = SimpleDateFormat("dd MMMM yyyy", Locale.getDefault())
            return formatter.format(date)
        }

        return ""
    }

    fun getTime(dateInMs: Long?): String {
        if (dateInMs != null) {
            val date = Date(dateInMs)
            val formatter = SimpleDateFormat("hh : mm a", Locale.getDefault())
            return formatter.format(date)
        }
        return ""
    }

    fun getDate(dateString: String): Date? {
        return try {
            val formatter = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            formatter.parse(dateString)
        } catch (ex: Exception) {
            null
        }
    }

    fun getFormattedDate(date: Date): String {
        val formatter = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        return formatter.format(date)
    }

    fun getFormatted1Date(date: Date): String {
        val formatter = SimpleDateFormat("dd MMM", Locale.getDefault())
        return formatter.format(date)
    }

}