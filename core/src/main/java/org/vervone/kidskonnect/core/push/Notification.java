package org.vervone.kidskonnect.core.push;

import com.google.gson.annotations.SerializedName;

/**
 * Represents the notification parameters that can be included in a {@link Message}. Instances
 * of this class are thread-safe and immutable.
 */
public class Notification {

    @SerializedName("title")
    private final String title;

    @SerializedName("body")
    private final String body;

    @SerializedName("image")
    private final String image;

    /**
     * Creates a new {@code Notification} using the given title and body.
     *
     * @param title Title of the notification.
     * @param body  Body of the notification.
     */
    public Notification(String title, String body) {
        this(title, body, null);
    }

    /**
     * Creates a new {@code Notification} using the given title, body, and image.
     *
     * @param title    Title of the notification.
     * @param body     Body of the notification.
     * @param imageUrl URL of the image that is going to be displayed in the notification.
     */
    public Notification(String title, String body, String imageUrl) {
        this.title = title;
        this.body = body;
        this.image = imageUrl;
    }

}