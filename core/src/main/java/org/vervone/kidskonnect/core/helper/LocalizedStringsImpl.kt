package org.vervone.kidskonnect.core.helper

import android.content.Context
import org.vervone.kidskonnect.core.R
import org.vervone.kidskonnect.core.helper.LocalizedStrings.Companion.NEW_LEAVE_APPLICATION

class LocalizedStringsImpl : LocalizedStrings {

    private var context: Context = AppHelper.getContext()

    override fun getString(type: String): String {

        return when (type) {
            NEW_LEAVE_APPLICATION -> context.getString(R.string.new_leave_application)
            LocalizedStrings.LEAVE_APPROVED -> context.getString(R.string.leave_approved)
            LocalizedStrings.LEAVE_REJECTED -> context.getString(R.string.leave_rejected)
            LocalizedStrings.NEWS_ADDED -> context.getString(R.string.news_has_been_added)
            LocalizedStrings.COMMENT_ADDED -> context.getString(R.string.added_comment)
            LocalizedStrings.NEW_MESSAGE -> context.getString(R.string.new_message)
            else -> ""
        }
    }


}