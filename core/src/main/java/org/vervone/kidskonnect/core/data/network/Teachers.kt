package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.*
import org.vervone.kidskonnect.core.data.network.model.FbTeacher
import org.vervone.kidskonnect.core.data.network.model.FbTeacherDetails
import org.vervone.kidskonnect.core.data.network.model.TEACHERS
import org.vervone.kidskonnect.core.data.network.model.academicyear.FbAcademicYrTeacher
import java.util.concurrent.atomic.AtomicInteger

/**
 *   Teachers memory cache.
 */
class Teachers(private val academicYear: String, private var callback: Callback?) {

    private var isDataReady = false


    init {
        if (teacherDetailsMap.isEmpty()) {
            ConnectionHelper.getDB()!!.reference.child(TEACHERS).addListenerForSingleValueEvent(teacherDataListener)
            ConnectionHelper.getDB()!!.reference.child(academicYear).child(TEACHERS).addValueEventListener(teacherDataListener)
            subscribeToTeachers()

        } else {
            isDataReady = true
            callback?.onSuccess(HashMap(teacherDetailsMap))
            callback = null
        }
    }

    private fun subscribeToTeachers() {
        ConnectionHelper.getDB()!!.reference.child(TEACHERS).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {}
            override fun onDataChange(snapshot: DataSnapshot) {
                val type = object : GenericTypeIndicator<HashMap<String, FbTeacher>>() {}
                val map = snapshot.getValue(type)
                if (map != null) {
                    teachersMap.putAll(map)
                }

                fetchAcademicYearTeacherInfo()
            }
        })
    }


    private fun fetchAcademicYearTeacherInfo() {
        ConnectionHelper.getDB()!!.reference.child(academicYear).child(TEACHERS)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {}
                override fun onDataChange(snapshot: DataSnapshot) {
                    val type = object : GenericTypeIndicator<HashMap<String, FbAcademicYrTeacher>>() {}
                    val map = snapshot.getValue(type)
                    if (map != null) {
                        academicMap.putAll(map)
                    }

                    map?.entries?.forEach { entry ->
                        val teacher = teachersMap[entry.key]
                        val academicYrTeacher = entry.value
                        if (teacher != null) {
                            val teacherInfo = FbTeacherDetails(teacher, academicYrTeacher)
                            teacherDetailsMap[entry.key] = teacherInfo
                        }
                    }
                    isDataReady = true
                    callback?.onSuccess(HashMap(teacherDetailsMap))
                }
            })
    }


    companion object {

        private val teachersMap = HashMap<String, FbTeacher>()
        private val academicMap = HashMap<String, FbAcademicYrTeacher>()
        private val teacherDetailsMap = HashMap<String, FbTeacherDetails>()
        private var ignoredCount = AtomicInteger(0)

        // Listener used to listen for the teachers data .
        private val teacherDataListener = object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}
            override fun onDataChange(snapshot: DataSnapshot) {

                val cnt = ignoredCount.incrementAndGet()
                // Ignore first 2 callbacks since , This is the initial data .
                // If there is any changes in the database we will clear the memory cache.
                if (cnt > 2) {
                    teacherDetailsMap.clear()
                    teachersMap.clear()
                    academicMap.clear()
                }
            }
        }


        fun create(academicYear: String, callback: Callback): Teachers {
            return Teachers(academicYear, callback)
        }
    }


    interface Callback {
        fun onSuccess(teacherDetails: Map<String, FbTeacherDetails>)
        fun onError()
    }

}