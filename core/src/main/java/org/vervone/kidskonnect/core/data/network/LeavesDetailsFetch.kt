package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.*
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.model.FbLeave
import org.vervone.kidskonnect.core.data.network.model.LEAVES_LIST

class LeavesDetailsFetch(leaves: List<String>?,
                         private val academicYr: String,
                         private val callback: RequestCallback<List<FbLeave>, ApiError>?) {

    private val keysLeft = ArrayList(leaves ?: emptyList())


    fun queryLeaveDetails() {

        ConnectionHelper.getDB()!!.reference.child(academicYr).child(LEAVES_LIST)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {
                    callback?.onError(ApiError(dbError.message))
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    val typeIndicator = object : GenericTypeIndicator<HashMap<String, FbLeave>>() {}
                    val leavesMap = snapshot.getValue(typeIndicator)

                    val leaves = leavesMap?.filterKeys { containsKey(it) }
                        ?.map { assignId(it) }

                    callback?.onSuccess(leaves)
                }
            })
    }


    /**
     *   It will check contains the specified [key] and remove it from the
     *   list if exists in the list.
     *   Note : Removing the keys from the list to reduce the iteration.
     */
    private fun containsKey(key: String): Boolean {
        return keysLeft.remove(key)
    }

    private fun assignId(entry: Map.Entry<String, FbLeave>): FbLeave {
        entry.value.id = entry.key
        return entry.value
    }


}