package org.vervone.kidskonnect.core.data.network.model


class FbExam {
    var dateAdded: String? = null
    var description: String? = null
    var pdfUrl: String? = null
    var title: String? = null
}