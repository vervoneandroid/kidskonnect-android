package org.vervone.kidskonnect.core.domain

import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseError
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.ApiError
import org.vervone.kidskonnect.core.data.network.ApiHelper
import org.vervone.kidskonnect.core.data.network.Errors
import org.vervone.kidskonnect.core.data.network.model.FbTeacher
import org.vervone.kidskonnect.core.data.network.model.academicyear.FbAcademicYrTeacher
import org.vervone.kidskonnect.core.data.network.model.student.FbStudent
import org.vervone.kidskonnect.core.domain.mappers.CommonMapper
import org.vervone.kidskonnect.core.domain.mappers.StudentMapper
import org.vervone.kidskonnect.core.domain.mappers.TeacherMapper
import org.vervone.kidskonnect.core.domain.mappers.UserMapper
import org.vervone.kidskonnect.core.domain.model.Teacher
import org.vervone.kidskonnect.core.domain.model.User
import org.vervone.kidskonnect.core.domain.model.student.Student
import org.vervone.kidskonnect.core.helper.PreferenceHelper

class AuthFacadeImpl : AuthFacade {

    private val apiHelper = ApiHelper.Factory.apiHelper

    override fun doLogin(email: String, pwd: String, callback: RequestCallback<User, Error>) {
        apiHelper.doLoginCall(email, pwd, object : RequestCallback<FirebaseUser, Exception> {
            override fun onSuccess(res: FirebaseUser?) {
                val user = UserMapper().map(res)
                PreferenceHelper.save(res?.uid!!, DomainConsts.KEY_CUR_USER_ID)
                callback.onSuccess(user)
            }

            override fun onError(error: Exception?) {
                val authError = Error(error?.localizedMessage)
                callback.onError(authError)
            }

        })
    }

    override fun isLoggedIn(): Boolean {
        return apiHelper.isLoggedIn()
    }

    override fun logout() {
        apiHelper.doLogout()
    }


    override fun fetchTeacherProfile(uid: String?, callback: RequestCallback<Teacher, Error>) {
        apiHelper.fetchTeacherProfile(uid, object : RequestCallback<FbTeacher, ApiError> {
            override fun onSuccess(res: FbTeacher?) {
                if (res != null) {
                    val user = TeacherMapper().map(res)
                    user.teacherId = uid
                    callback.onSuccess(user)
                } else {
                    val authError = Error(Errors.ERROR_USER_NOT_FOUND)
                    callback.onError(authError)
                }
            }

            override fun onError(error: ApiError?) {
                val authError = Error(error)
                callback.onError(authError)
            }
        })
    }

    override fun fetchStudentProfile(uid: String?, callback: RequestCallback<Student, Error>) {
        apiHelper.fetchStudentsProfile(uid, object : RequestCallback<FbStudent, DatabaseError> {
            override fun onSuccess(res: FbStudent?) {
                if (res != null) {
                    val user = StudentMapper().map(res)
                    callback.onSuccess(user)
                } else {
                    val authError = Error(Errors.getErrorMessage(Errors.ERROR_USER_NOT_FOUND))
                    callback.onError(authError)
                }
            }

            override fun onError(error: DatabaseError?) {
                val authError = Error(error?.toException())
                callback.onError(authError)
            }
        })
    }

    override fun getAcademicYearData(uid: String?, callback: RequestCallback<Teacher, Error>) {
        val year = PreferenceHelper.getString(DomainConsts.KEY_CUR_ACADEMIC_YR)
        apiHelper.getAcademicYearData(uid, year!!, object : RequestCallback<FbAcademicYrTeacher, ApiError> {
            override fun onSuccess(res: FbAcademicYrTeacher?) {
                val teacher = CommonMapper.map(res!!)
                callback.onSuccess(teacher)
            }

            override fun onError(error: ApiError?) {
                val err = Error(error)
                callback.onError(err)
            }
        })
    }

    override fun getTeacherAllDetails(uid: String, callback: RequestCallback<Teacher, ApiError>) {
        apiHelper.fetchTeacherProfile(uid, TeacherProfileCallback(uid, callback))
    }


    override fun getStudentAllDetails(
            uid: String,
            callback: RequestCallback<Student, DomainError>
    ) {
        //apiHelper.fetchStudentsProfile(uid, null!!)
    }


    private class TeacherProfileCallback(
            val uid: String,
            val callback: RequestCallback<Teacher, ApiError>
    ) : RequestCallback<FbTeacher, ApiError> {

        private var teacher: Teacher? = null
        private val apiHelper = ApiHelper.Factory.apiHelper


        override fun onSuccess(res: FbTeacher?) {
            if (res != null) {
                this.teacher = TeacherMapper().map(res)
                teacher?.teacherId = uid
                val year = PreferenceHelper.getString(DomainConsts.KEY_CUR_ACADEMIC_YR)
                apiHelper.getAcademicYearData(uid, year!!, AcademicYearDetailsCallback(teacher!!, callback))

            } else {
                val authError = ApiError(Errors.ERROR_USER_NOT_FOUND)
                callback.onError(authError)
            }

        }

        override fun onError(error: ApiError?) {
            val authError = ApiError(error!!)
            callback.onError(authError)
        }
    }


    private class AcademicYearDetailsCallback(
            val teacher: Teacher,
            val callback: RequestCallback<Teacher, ApiError>) : RequestCallback<FbAcademicYrTeacher, ApiError> {
        override fun onSuccess(res: FbAcademicYrTeacher?) {
            CommonMapper.mapToTeacher(res!!, teacher)
            callback.onSuccess(teacher)
        }

        override fun onError(error: ApiError?) {
            val err = ApiError(error)
            callback.onError(err)
        }
    }

}