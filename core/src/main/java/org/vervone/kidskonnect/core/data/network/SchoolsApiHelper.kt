package org.vervone.kidskonnect.core.data.network

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.model.school.SchoolConfig

interface SchoolsApiHelper {
    fun getSchools(callback: RequestCallback<List<SchoolConfig>, ApiError>)
    fun getSchool(schoolId: String,
                  callback: RequestCallback<SchoolConfig, ApiError>)

    object Factory {
        var apiHelper: SchoolsApiHelper = SchoolsApiService()
    }
}