package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.*
import org.vervone.kidskonnect.core.data.network.model.*
import org.vervone.kidskonnect.core.data.network.model.student.FbComments
import org.vervone.kidskonnect.core.data.network.model.student.FbStudent
import java.util.concurrent.atomic.AtomicBoolean

object CommentsSubscribe {


    private val subscribers = HashMap<String, CommentsListener>()
    private val commentQueries: ArrayList<CommentsQuery> = ArrayList()
    private val isFirst = AtomicBoolean(true)

    fun subscribe(studentParams: StudentParams, listener: CommentsListener): String {
        val subsId = ApiUtils.getUUID()
        subscribers[subsId] = listener

        if (isFirst.getAndSet(false)) {
            object : StudentQuery(studentParams) {
                override fun onResult(student: FbStudent?) {
                    if (student != null) {
                        createQuery(student)
                    }
                }
            }.getData()
        }

        return subsId
    }


    private fun createQuery(student: FbStudent) {
        val commentsQuery = CommentsQuery(student)
        commentsQuery.subscribe()
        commentQueries.add(commentsQuery)
    }

    fun unsubscribe(subsId: String) {
        subscribers.remove(subsId)
    }

    fun unsubscribeAll() {
        subscribers.clear()
        commentQueries.forEach { it.unsubscribe() }
        isFirst.set(true)
    }

    fun subscribe(teacherParams: TeacherParams,
                  listener: CommentsListener): String {

        val subsId = ApiUtils.getUUID()
        subscribers[subsId] = listener

        if (isFirst.getAndSet(false)) {
            teacherParams.classes.forEach { className ->
                object : StudentsQuery(teacherParams.academicYr, className) {
                    override fun onResult(students: List<FbStudent>) {
                        students.forEach {
                            //TODO : Can be improve this logic
                            it.classes = hashMapOf(Pair(teacherParams.academicYr, className))
                            createQuery(it)
                        }
                    }
                }.getData()
            }
        }

        return subsId
    }


    private abstract class StudentQuery(val params: StudentParams) : ValueEventListener {
        private val ref: DatabaseReference

        init {
            with(params)
            {
                ref = ConnectionHelper.getDB()!!.reference
                    .child(academicYr)
                    .child(CLASS)
                    .child(className)
                    .child(STUDENTS)
                    .child(studentId)
            }
        }


        fun getData() {
            ref.addListenerForSingleValueEvent(this)
        }

        override fun onCancelled(dbError: DatabaseError) {}
        override fun onDataChange(snapshot: DataSnapshot) {
            val typeIndicator = object : GenericTypeIndicator<FbStudent>() {}
            val student = snapshot.getValue(typeIndicator)
            student?.id = params.studentId
            onResult(student)
        }

        abstract fun onResult(student: FbStudent?)
    }

    private abstract class StudentsQuery(academicYr: String, className: String) : ValueEventListener {

        private val ref: DatabaseReference = ConnectionHelper.getDB()!!.reference
            .child(academicYr)
            .child(CLASS)
            .child(className)
            .child(STUDENTS)

        fun getData() {
            ref.addListenerForSingleValueEvent(this)
        }

        override fun onCancelled(dbError: DatabaseError) {}
        override fun onDataChange(snapshot: DataSnapshot) {
            val typeIndicator = object : GenericTypeIndicator<HashMap<String, FbStudent>>() {}
            val studentMap = snapshot.getValue(typeIndicator)
            val students = studentMap?.map { ApiUtils.mapId(it) } ?: emptyList()
            onResult(students)
        }

        abstract fun onResult(students: List<FbStudent>)
    }


    private class CommentsQuery(val student: FbStudent) : DefChildEventListener() {

        val ref: DatabaseReference = ConnectionHelper.getDB()!!.reference
            .child(STUDENTS)
            .child(student.id ?: "")
            .child(COMMENTS)

        private val comments = ArrayList<FbComments>()

        private fun fetchSnapshot() {
            ref.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {}
                override fun onDataChange(snapshot: DataSnapshot) {
                    val typeIndicator = object : GenericTypeIndicator<ArrayList<FbComments>>() {}
                    val commentList = snapshot.getValue(typeIndicator)
                    if (commentList != null) {
                        comments.addAll(commentList)
                    }
                    subscribers.values.forEach {
                        it.onSnapshot(comments.size, student)
                    }
                    listenForChild()
                }
            })
        }

        private fun listenForChild() {
            ref.addChildEventListener(this)
        }

        fun subscribe() {
            fetchSnapshot()
        }

        fun unsubscribe() {
            ref.removeEventListener(this)
        }

        override fun onChildAdded(snapshot: DataSnapshot, childName: String?) {
            val typeIndicator = object : GenericTypeIndicator<FbComments>() {}
            val comment = snapshot.getValue(typeIndicator)
            if (comment != null && !contains(comment)) {
                comments.add(comment)
                subscribers.values.forEach {
                    it.onNewComment(comments.size, student, comment)
                }
            }
        }

        private fun contains(comment: FbComments): Boolean {
            return comments.any {
                it.postTime == comment.postTime &&
                        it.commentedPersonId == it.commentedPersonId
            }
        }

    }


    // Listener
    interface CommentsListener {
        fun onSnapshot(count: Int, student: FbStudent?)
        fun onNewComment(count: Int, student: FbStudent?, comment: FbComments)
    }

}