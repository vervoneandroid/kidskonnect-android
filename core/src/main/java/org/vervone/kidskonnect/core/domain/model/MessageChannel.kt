package org.vervone.kidskonnect.core.domain.model

import java.io.Serializable

class MessageChannel : Serializable {

    var groupName: String? = null

    var id: String? = null
    var isGroupMessage: Boolean? = null
    var targetId: String? = null
    var targets: HashMap<String, String>? = null

    //
    var studentIdsAndClass: HashMap<String, String>? = null
    var teacherIds: List<String>? = null
    var classes: List<String>? = null

    fun getChatName(uid: String): String {

        if (isGroupMessage == true) {
            return groupName ?: ""
        }
        // If its not group chat , remove the user name from chat name.
        val targetList = targets?.filter { it.key != uid }?.map { it.value }?.sorted()
        return targetList?.joinToString { it } ?: ""
    }


    var lastMessageTime: Long = 0

    override fun toString(): String {
        return "MessageChannel(isGroupMessage=$isGroupMessage, targetId=$targetId, targets=$targets)"
    }
}