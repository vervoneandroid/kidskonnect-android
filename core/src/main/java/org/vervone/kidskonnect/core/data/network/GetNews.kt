package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.data.network.model.FbNews
import java.util.concurrent.Executors

abstract class GetNews(
        private val academicYr: String,
        private val newsIds: ArrayList<String>
) : ValueEventListener {

    private val reqCount = newsIds.size
    private var resCount = 0

    private val executor = Executors.newFixedThreadPool(3)
    private val newsList = ArrayList<FbNews>()

    fun execute() {
        executor.execute {
            newsIds.forEach {
                ConnectionHelper.getDB()!!.getReference("$academicYr/newsFeeds/$it")
                    .addListenerForSingleValueEvent(this)
            }
        }
    }

    override fun onCancelled(dbError: DatabaseError) {
        resCount++
        DLog.v(this, "news fetch failed ${dbError.toException()}")
        inform()
    }

    override fun onDataChange(snapshot: DataSnapshot) {
        resCount++
        DLog.v(this, "getNewsForId ${snapshot.key}")
        val news = snapshot.getValue(FbNews::class.java)
        if (news != null) {
            news.id = snapshot.key
            newsList.add(news)
        }
        inform()
    }

    private fun dataReady(): Boolean = (reqCount == resCount)

    private fun inform() {
        if (dataReady()) {
            DLog.v(this, "data received ")

            onResult(ArrayList(newsList))
        }
    }

    abstract fun onResult(news: ArrayList<FbNews>)
}