package org.vervone.kidskonnect.core.domain.mappers

import org.vervone.kidskonnect.core.Mapper
import org.vervone.kidskonnect.core.data.network.model.FbExam
import org.vervone.kidskonnect.core.domain.model.Exam

class ExamsMapper : Mapper<FbExam, Exam> {
    override fun map(input: FbExam): Exam {
        val exam = Exam()
        exam.dateAdded = input.dateAdded?.toLongOrNull()
        exam.description = input.description
        exam.pdfUrl = input.pdfUrl
        exam.title = input.title
        return exam
    }
}