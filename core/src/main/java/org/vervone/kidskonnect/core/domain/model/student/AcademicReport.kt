package org.vervone.kidskonnect.core.domain.model.student

import java.io.Serializable

class AcademicReport : Serializable {
    var date: Long? = null
    var reportName: String? = null
    var reportUrl: String? = null
}