package org.vervone.kidskonnect.core.data.network.model

/**
 *  This is used to map id property to firebase data model.
 */
interface Id {
    var id: String?
}