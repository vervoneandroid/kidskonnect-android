package org.vervone.kidskonnect.core.domain

class DomainError : Error {
    constructor(message: String?) : super(message) {}
    constructor(message: String, cause: Throwable) : super(message, cause) {}
    constructor(cause: Throwable) : super(cause) {}
}