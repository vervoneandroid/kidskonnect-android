package org.vervone.kidskonnect.core.domain.model

class SchoolContact {
    var address: String? = null
    var contactNo: String? = null
    var email: String? = null
    var place: String? = null
    var schoolName: String? = null
}