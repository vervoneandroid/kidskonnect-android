package org.vervone.kidskonnect.core.domain.mappers

import org.vervone.kidskonnect.core.Mapper
import org.vervone.kidskonnect.core.data.network.model.FbTeacher
import org.vervone.kidskonnect.core.domain.model.Profile
import org.vervone.kidskonnect.core.domain.model.Teacher

class TeacherMapper : Mapper<FbTeacher?, Teacher> {
    override fun map(input: FbTeacher?): Teacher {
        val teacher = Teacher()
        teacher.teacherName = input?.teacherName
        teacher.qualification = input?.qualification
        teacher.profileImageUrl = input?.profileImageUrl
        teacher.subjects = input?.subjects

        input?.ProfileDetails?.let {
            val profile = Profile()
            profile.dateOfBirth = it.dateOfBirth
            profile.dateOfJoining = it.dateOfJoining
            profile.emaiId = it.emaiId
            profile.mobileNumber = it.mobileNumber
            profile.permanentAddress = it.permanentAddress
            profile.residenceAddress = it.residenceAddress
            teacher.ProfileDetails = profile
        }

        return teacher
    }
}