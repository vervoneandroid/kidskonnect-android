package org.vervone.kidskonnect.core.data.network.model

import com.google.firebase.database.Exclude

/**
 *  Firebase model for news
 */
class FbNews {
    var date: Long? = null
    var description: String? = null
    var images: HashMap<String, String>? = null
    var likes: Int? = null
    var newsType: Int? = null
    var senderId: String? = null
    var senderName: String? = null
    var targets: HashMap<String, String>? = null
    var webLink: String? = null
    @Exclude
    var id: String? = null
}