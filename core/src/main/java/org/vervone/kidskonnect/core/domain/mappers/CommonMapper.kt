package org.vervone.kidskonnect.core.domain.mappers

import org.vervone.kidskonnect.core.data.network.model.FbAppVersion
import org.vervone.kidskonnect.core.data.network.model.FbNews
import org.vervone.kidskonnect.core.data.network.model.FbTeacherDetails
import org.vervone.kidskonnect.core.data.network.model.academicyear.FbAcademicYrTeacher
import org.vervone.kidskonnect.core.data.network.model.school.FbSchool
import org.vervone.kidskonnect.core.domain.model.AppVersion
import org.vervone.kidskonnect.core.domain.model.ContentType
import org.vervone.kidskonnect.core.domain.model.News
import org.vervone.kidskonnect.core.domain.model.Teacher
import org.vervone.kidskonnect.core.domain.model.school.SchoolConfig

object CommonMapper {


    fun map(yrTeacher: FbAcademicYrTeacher): Teacher {
        val teacher = Teacher()
        teacher.teacherName = yrTeacher.teacherName

        teacher.classesAndSubjects = yrTeacher.classesAndSubjects
        teacher.isClassTeacher = yrTeacher.isClassTeacher
        teacher.leavesList = yrTeacher.leavesList
        teacher.likedNews = yrTeacher.likedNews
        teacher.teacherDesignationTag = yrTeacher.teacherDesignationTag
        teacher.userChatDict = yrTeacher.userChatDict

        return teacher
    }


    fun mapToTeacher(yrTeacher: FbAcademicYrTeacher, teacher: Teacher) {
        teacher.teacherName = yrTeacher.teacherName
        teacher.classesAndSubjects = yrTeacher.classesAndSubjects
        teacher.isClassTeacher = yrTeacher.isClassTeacher
        teacher.leavesList = yrTeacher.leavesList
        teacher.likedNews = yrTeacher.likedNews
        teacher.teacherDesignationTag = yrTeacher.teacherDesignationTag
        teacher.userChatDict = yrTeacher.userChatDict
    }


    fun map(teacherDetails: FbTeacherDetails, teacherId: String): Teacher {
        val teacher = TeacherMapper().map(teacherDetails.teacher)
        teacher.teacherId = teacherId
        val academicYrTeacher = teacherDetails.academicYrTeacher
        academicYrTeacher?.let { yrTeacher ->
            teacher.teacherName = yrTeacher.teacherName
            teacher.classesAndSubjects = yrTeacher.classesAndSubjects
            teacher.isClassTeacher = yrTeacher.isClassTeacher
            teacher.leavesList = yrTeacher.leavesList
            teacher.likedNews = yrTeacher.likedNews
            teacher.teacherDesignationTag = yrTeacher.teacherDesignationTag
            teacher.userChatDict = yrTeacher.userChatDict
        }

        return teacher
    }


    fun map(news: News): FbNews {
        val fbNews = FbNews()

        fbNews.webLink = news.webLink
        fbNews.date = news.date
        fbNews.description = news.description

        news.targetList?.let {
            val map = it.map { item -> item to "true" }.toMap()
            fbNews.targets = HashMap(map)
        }

        fbNews.senderName = news.senderName
        fbNews.senderId = news.senderId

        when (news.contentType) {
            ContentType.TEXT,
            ContentType.IMAGE,
            ContentType.MULTI_MEDIA -> fbNews.newsType = ContentType.IMAGE.value
            ContentType.WEB_LINKS -> fbNews.newsType = ContentType.WEB_LINKS.value
            ContentType.VIDEO -> fbNews.newsType = ContentType.VIDEO.value
            else -> fbNews.newsType = ContentType.WEB_LINKS.value
        }

        fbNews.images = news.images //TODO News: map from attachments
        fbNews.likes = news.likes
        fbNews.id = news.id

        return fbNews
    }

    fun map(version: FbAppVersion): AppVersion {
        return AppVersion(version.appCurrentVersion,
                          version.appMinimumVersion, false)
    }

    fun map(school: FbSchool?, id: String?): SchoolConfig {
        if (school == null) return SchoolConfig()
        return SchoolConfig(name = school.schoolName ?: "",
                            address = school.address ?: "",
                            district = school.district ?: "",
                            id = id ?: "",
                            apiKey = school.apiKey ?: "",
                            applicationId = school.googleAppId ?: "",
                            dbUrl = school.databaseUrl ?: "",
                            projectId = school.projectId ?: "")
    }

}