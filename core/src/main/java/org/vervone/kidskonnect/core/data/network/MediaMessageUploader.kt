package org.vervone.kidskonnect.core.data.network

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ThumbnailUtils
import android.provider.MediaStore
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.cloudstorage.CloudStorage
import org.vervone.kidskonnect.core.data.network.model.FbChatMessage
import org.vervone.kidskonnect.core.domain.model.MessageType
import org.vervone.kidskonnect.core.helper.AppHelper
import org.vervone.kidskonnect.core.helper.Task
import org.vervone.kidskonnect.core.helper.ThreadUtils
import org.vervone.kidskonnect.core.helper.copyToFile
import java.io.File
import java.io.FileOutputStream
import java.util.concurrent.atomic.AtomicInteger

class MediaMessageUploader(
        private val academicYear: String?,
        private val chatGroupId: String?,
        private val mediaMessage: FbChatMessage,
        val mediaUploadListener: MediaUploadListener?) : Task {

    private var thumbnailFile: File? = null
    private var fileToUpload: File? = null
    private var uploadIds = arrayListOf<Int>()
    private var uploadKeys = arrayListOf<String>()

    init {
        prepare()
    }

    private fun prepare() {
        val path = mediaMessage.attachmentsUrl
        val originalFile = File(path)
        val context = AppHelper.get()

        val dirPath = "$academicYear/chats/$chatGroupId"
        val uploadFileName = "${mediaMessage.chatId}"
        val thumbUploadFileName = "${mediaMessage.chatId}_thumpnail"

        val uploadKey = "$dirPath/$uploadFileName"
        DLog.v(this, "prepare() $uploadKey")

        val thumbUploadKey = "$dirPath/$thumbUploadFileName"
        DLog.v(this, "prepare() $thumbUploadKey")


        //If the attachment is video we have to upload the thumbnail
        if (mediaMessage.messageType == MessageType.VIDEOS.value) {
            val thumbnail = ThumbnailUtils.createVideoThumbnail(
                    path, MediaStore.Images.Thumbnails.MICRO_KIND)

            val parentDir = File(context.cacheDir, dirPath)
            if (!parentDir.exists()) {
                parentDir.mkdirs()
            }

            val thumbnailFile = File(parentDir, "$thumbUploadFileName.png")
            this.thumbnailFile = thumbnailFile
            val outputStream = FileOutputStream(thumbnailFile)
            thumbnail.compress(Bitmap.CompressFormat.PNG, 90, outputStream)
        } else {
            val bitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(path), 100, 100)

            val parentDir = File(context.cacheDir, dirPath)
            if (!parentDir.exists()) {
                parentDir.mkdirs()
            }

            val thumbnailFile = File(context.cacheDir, "$thumbUploadFileName.jpg")
            this.thumbnailFile = thumbnailFile
            val outputStream = FileOutputStream(thumbnailFile)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, outputStream)
        }

        this.fileToUpload = File(context.cacheDir, "$uploadKey.${originalFile.extension}")

        if (originalFile.exists()) {
            originalFile.copyToFile(this.fileToUpload!!)
            DLog.v(this, "file $path copied to ${fileToUpload!!.absolutePath} .")
        }
    }

    fun upload() {
        val uploadKey = "$academicYear/chats/$chatGroupId/${mediaMessage.chatId}.${fileToUpload?.extension}"
        DLog.v(this, "upload() $uploadKey")

        val thumbUploadKey =
            "$academicYear/chats/$chatGroupId/${mediaMessage.chatId}_thumpnail.${thumbnailFile?.extension}"
        DLog.v(this, "upload() $thumbUploadKey")

        mediaMessage.thumpNailImagePath = thumbUploadKey
        mediaMessage.attachmentsUrl = uploadKey

        val cloudStorage = AppHelper.getCloudStorage()!!

        uploadKeys.add(uploadKey)
        uploadKeys.add(thumbUploadKey)
        uploadIds.add(cloudStorage.uploadPublic(uploadKey, fileToUpload!!, uploadListener, false))
        uploadIds.add(cloudStorage.uploadPublic(thumbUploadKey, thumbnailFile!!, thumpUploadListener, false))
    }


    override fun cancel() {
        DLog.v(this, "MediaUploader cancel called $uploadIds")
        for (id in uploadIds) {
            val cloudStorage = AppHelper.getCloudStorage()!!
            cloudStorage.cancel(id)
        }
    }


    /**
     *   Upload listener for original file upload
     */
    private val uploadListener = object : CloudStorage.UploadListener {

        override fun onComplete() {
            onUploadComplete()
        }

        override fun onFailed() {
            // Any one(original file or thumbnail file) of the upload failed we consider this
            // as a media message failed .
            onUploadFailed()
        }

        override fun onProgress(pecentage: Int) {
            mediaUploadListener?.onProgress(pecentage)
        }
    }

    /**
     *   Upload listener for thumbnail file upload
     */
    private val thumpUploadListener = object : CloudStorage.UploadListener {

        override fun onComplete() {
            onUploadComplete()
        }

        override fun onFailed() {
            // Any one(original file or thumbnail file) of the upload failed we consider this
            // as a media message failed .
            onUploadFailed()
        }

        override fun onProgress(pecentage: Int) {
            // Ignoring this now
        }
    }


    // Total files to upload Original file and Thumbnail
    private val uploadCount = AtomicInteger(2)

    private fun onUploadComplete() {
        if (uploadCount.decrementAndGet() == 0) {
            mediaUploadListener?.onComplete()
        }
    }

    private fun onUploadFailed() {
        ThreadUtils.runOnBg {
            val cloudStorage = AppHelper.getCloudStorage()
            uploadKeys.forEach {
                cloudStorage?.deleteFileFromCloud(it)
            }
            mediaUploadListener?.onFailed()
        }

    }


    /**
     *  Callback for upload completion
     */
    interface MediaUploadListener {
        fun onComplete()
        fun onFailed()
        fun onProgress(percent: Int)
    }

}