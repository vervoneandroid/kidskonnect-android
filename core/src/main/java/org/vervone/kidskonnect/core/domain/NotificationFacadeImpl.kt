package org.vervone.kidskonnect.core.domain

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.ApiError
import org.vervone.kidskonnect.core.data.network.ApiHelper
import org.vervone.kidskonnect.core.data.network.model.FbNotification
import org.vervone.kidskonnect.core.domain.mappers.DomainErrorMapper
import org.vervone.kidskonnect.core.domain.mappers.NotificationMapper
import org.vervone.kidskonnect.core.domain.model.Notification

class NotificationFacadeImpl : NotificationFacade {

    private val apiHelper = ApiHelper.Factory.apiHelper

    override fun getNotification(academicYr: String,
                                 callback: RequestCallback<List<Notification>, DomainError>?) {
        apiHelper.getAllNotifications(academicYr,
                object : RequestCallback<List<FbNotification>, ApiError> {
                    override fun onError(error: ApiError?) {
                        val domainError = DomainErrorMapper().map(error!!)
                        callback?.onError(domainError)
                    }

                    override fun onSuccess(res: List<FbNotification>?) {
                        callback?.onSuccess(res?.map { NotificationMapper().map(it) })
                    }
                })
    }
}