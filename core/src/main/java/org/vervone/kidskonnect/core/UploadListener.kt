package org.vervone.kidskonnect.core


interface UploadListener<RESPONSE, ERROR> : RequestCallback<RESPONSE, ERROR> {
    fun onProgress(percentage: Int)
}