package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import org.vervone.kidskonnect.core.data.network.model.CLASS
import org.vervone.kidskonnect.core.data.network.model.TEACHERS
import org.vervone.kidskonnect.core.data.network.model.TeacherParams
import org.vervone.kidskonnect.core.data.network.model.USER_CHAT_DICT
import java.util.concurrent.atomic.AtomicInteger

/**
 *   To Retrieve all the chat room ids for the teacher.
 */
abstract class GetChatRoomIds(private val params: TeacherParams) {

    private val reqCount = AtomicInteger(1 + params.classes.size)
    private val db = ConnectionHelper.getDB()

    private val chatRoomIds: MutableSet<String> = mutableSetOf()

    private fun isReady() = (reqCount.decrementAndGet() == 0)

    private fun invoke() {
        if (isReady()) {
            onResult(chatRoomIds)
        }
    }

    abstract fun onResult(chatRoomIds: Set<String>)


    fun get() {
        val ref = db!!.reference
            .child(params.academicYr)
            .child(TEACHERS)
            .child(params.teacherId)
            .child(USER_CHAT_DICT)
        ref.addListenerForSingleValueEvent(valueEventListener)
        params.classes.forEach {
            getChatRoomsForClass(params.academicYr, it)
        }
    }


    private fun getChatRoomsForClass(academicYr: String,
                                     schoolClass: String) {

        val ref = db!!.reference
            .child(academicYr)
            .child(CLASS)
            .child(schoolClass)
            .child(USER_CHAT_DICT)
        ref.addListenerForSingleValueEvent(valueEventListener)
    }


    private val valueEventListener = object : ValueEventListener {
        override fun onCancelled(dbError: DatabaseError) {
            invoke()
        }

        override fun onDataChange(snaspshot: DataSnapshot) {
            val allChatRooms = snaspshot.asStringBoolMap()
            if (allChatRooms != null) {
                chatRoomIds.addAll(allChatRooms.keys)
            }
            invoke()
        }
    }

}