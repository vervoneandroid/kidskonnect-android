package org.vervone.kidskonnect.core.data.network

import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.database.FirebaseDatabase
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.domain.model.school.SchoolConfig
import org.vervone.kidskonnect.core.helper.AppHelper
import org.vervone.kidskonnect.core.helper.SchoolConfigHelper

object ConnectionHelper {
    fun initialize(schoolConfig: SchoolConfig) {
        val ctx = AppHelper.get()
        val options = FirebaseOptions.Builder()
            .setProjectId(schoolConfig.projectId)
            .setApplicationId(schoolConfig.applicationId)
            .setApiKey(schoolConfig.apiKey)
            .setDatabaseUrl(schoolConfig.dbUrl)
            .build()
        try {
            FirebaseApp.initializeApp(ctx, options, schoolConfig.id)
        } catch (ex: Exception) {
            DLog.e(this, "initialize Error", ex)
        }
    }

    fun fbApp(): FirebaseApp = FirebaseApp.getInstance(SchoolConfigHelper.schoolId())
    fun getDB(): FirebaseDatabase? {
        return FirebaseDatabase.getInstance(fbApp())
    }
}