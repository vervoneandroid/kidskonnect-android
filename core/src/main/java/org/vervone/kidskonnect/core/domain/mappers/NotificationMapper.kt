package org.vervone.kidskonnect.core.domain.mappers

import org.vervone.kidskonnect.core.Mapper
import org.vervone.kidskonnect.core.data.network.model.FbNotification
import org.vervone.kidskonnect.core.domain.model.Notification

class NotificationMapper : Mapper<FbNotification, Notification> {
    override fun map(input: FbNotification): Notification {
        val notification = Notification()
        notification.date = input.date
        notification.description = input.description
        notification.title = input.title
        notification.id = input.id
        return notification
    }
}