package org.vervone.kidskonnect.core.domain

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.model.AppVersion
import org.vervone.kidskonnect.core.domain.model.ConfigData

interface ConfigFacade {
    fun syncAcademicYear(callback: RequestCallback<ConfigData, Error>)
    fun requiredAppVersions(callback: RequestCallback<AppVersion, DomainError>)

    fun subscribeAppVersionConfig(versionListener: VersionListener)

    interface VersionListener {
        fun onVersionChange(appVersions: AppVersion)
    }
}