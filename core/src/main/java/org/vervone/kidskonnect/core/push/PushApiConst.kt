package org.vervone.kidskonnect.core.push

object PushApiConst {
    const val TYPE = "type"
    const val STUDENT_NAME = "studentName"
    const val STUDENT_ID = "studentId"
    const val COMMENT_TYPE = "commentType"
    const val CLASS_NAME = "className"
    const val GROUP_ID = "groupId"

    //

    const val TOPIC_PREFIX = "/topics/"

    // TOPICS
    const val INITIAL_SUBSCRPTION = "initialSubscrption-"
    const val NOTIFICATION_TOPIC = "notificationTopic-"

    // TOPICS -- END --
    const val FCM_API = "https://fcm.googleapis.com/fcm/send"
    const val HEADER_AUTH = "Authorization"
    const val HEADER_CONTENT_TYPE = "Content-Type"


}