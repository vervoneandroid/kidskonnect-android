package org.vervone.kidskonnect.core.data.network.model.student

import com.google.firebase.database.Exclude
import org.vervone.kidskonnect.core.data.network.model.Id

fun String.capitalizeWords(): String = split(" ").joinToString(" ") { it.capitalize() }


class FbStudent : Id {

    var ProfileDetails: FbProfileDetails? = null
    var accademicReports: List<FbAcademicReport?>? = null
    var classes: Map<String, String>? = null
    var likedNews: Map<String, Boolean>? = null
    var comments: List<FbComments>? = null
    var studentName: String? = null
        get() {
            return field?.capitalizeWords()
        }
    var profileImageUrl: String? = null

    @Exclude
    override var id: String? = null

    fun getClass(academicYr: String): String? {
        return classes?.get(academicYr)
    }
}