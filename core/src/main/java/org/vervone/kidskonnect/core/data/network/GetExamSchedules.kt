package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.GenericTypeIndicator
import com.google.firebase.database.ValueEventListener
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.model.CLASS
import org.vervone.kidskonnect.core.data.network.model.EXAM_SCHEDULES
import org.vervone.kidskonnect.core.data.network.model.FbExam
import org.vervone.kidskonnect.core.helper.filterToList
import java.util.concurrent.atomic.AtomicInteger

/**
 *   Get exams schedules.
 */
class GetExamSchedules(private val year: String,
                       private val classes: List<String>,
                       private val callback: RequestCallback<List<FbExam>, ApiError>?) {


    private val requestCount = AtomicInteger(classes.size)
    private val allExamSchedules = mutableListOf<String>()

    init {

    }

    fun execute() {
        //2019-2020/class/1%20A/examSchedules
        classes.forEach { schoolClass ->
            ConnectionHelper.getDB()!!.reference.child(year)
                .child(CLASS)
                .child(schoolClass)
                .child(EXAM_SCHEDULES).addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(dbError: DatabaseError) {
                        //callback?.onError(ApiError(dbError.message))
                        fetchIfAllDataReceived()
                    }

                    override fun onDataChange(snapshot: DataSnapshot) {
                        val typeIndicator = object : GenericTypeIndicator<HashMap<String, Boolean>>() {}
                        val scheduledExamsMap = snapshot.getValue(typeIndicator)

                        scheduledExamsMap?.keys?.let {
                            allExamSchedules.addAll(it)
                        }

                        fetchIfAllDataReceived()
                        //callback?.onSuccess(scheduledExams)
                    }
                })
        }

    }


    private fun fetchIfAllDataReceived() {
        if (requestCount.decrementAndGet() == 0) {
            GetExamDetails(allExamSchedules, year, callback).get()
        }
    }


    private class GetExamDetails(private val schedules: List<String>,
                                 private val year: String,
                                 private val callback: RequestCallback<List<FbExam>, ApiError>?) {

        private val copySchedules = schedules.toMutableList()

        fun get() {
            ConnectionHelper.getDB()!!.reference.child(year)
                .child(EXAM_SCHEDULES).addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(dbError: DatabaseError) {
                        callback?.onError(ApiError(dbError.message))
                    }

                    override fun onDataChange(snapshot: DataSnapshot) {
                        val typeIndicator = object : GenericTypeIndicator<HashMap<String, FbExam>>() {}
                        val scheduledExamsMap = snapshot.getValue(typeIndicator)

                        val scheduledExams = scheduledExamsMap?.filterToList(
                                ArrayList(),
                                { copySchedules.remove(it.key) }
                        )

                        callback?.onSuccess(scheduledExams)
                    }
                })
        }


    }

}