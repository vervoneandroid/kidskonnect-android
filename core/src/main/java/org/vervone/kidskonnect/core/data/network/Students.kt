package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.*
import org.vervone.kidskonnect.core.data.network.model.CLASS
import org.vervone.kidskonnect.core.data.network.model.STUDENTS
import org.vervone.kidskonnect.core.data.network.model.student.FbStudent
import java.util.concurrent.atomic.AtomicInteger

class Students(val academicYr: String, val classes: List<String>, val callback: Callback) {

    private val reqCount = AtomicInteger(classes.size)

    private val db = ConnectionHelper.getDB()
    val syncedStudents = HashMap<String, List<String>>()


    fun execute() {
        classes.forEach { getStudentSynced(it) }
    }

    private fun getStudentSynced(className: String) {
        db!!.reference
            .child(academicYr)
            .child(CLASS)
            .child(className)
            .child(STUDENTS).addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {
                    if (reqCount.decrementAndGet() == 0) {
                        callback.onSync()
                    }
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    val typeIndicator = object : GenericTypeIndicator<HashMap<String, FbStudent>>() {}
                    val studentMap = snapshot.getValue(typeIndicator)
                    val studentIds = studentMap?.keys ?: emptyList<String>()
                    syncedStudents[className] = ArrayList(studentIds)

                    if (reqCount.decrementAndGet() == 0) {
                        callback.onSync()
                    }
                }
            })
    }


    interface Callback {
        fun onSync()
    }
}