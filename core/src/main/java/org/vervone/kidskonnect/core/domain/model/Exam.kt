package org.vervone.kidskonnect.core.domain.model

import org.vervone.kidskonnect.core.helper.Formatter

class Exam {

    var dateAdded: Long? = null
    var description: String? = null
    var pdfUrl: String? = null
    var title: String? = null

    fun getFormattedDate(): String {
        return Formatter.getDate(dateAdded)
    }
}