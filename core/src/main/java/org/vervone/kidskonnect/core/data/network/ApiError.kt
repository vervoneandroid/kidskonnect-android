package org.vervone.kidskonnect.core.data.network

open class ApiError : Error {
    constructor() {}
    constructor(msg: String) : super(msg) {}
    constructor(throwable: Throwable?) : super(throwable) {}
}

