package org.vervone.kidskonnect.core.domain.model

import java.io.Serializable

class User(
    var name: String?,
    var photoUrl: String?,
    var uid: String?, isTeacher: Boolean = false
) : Serializable {

}