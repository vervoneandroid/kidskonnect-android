package org.vervone.kidskonnect.core.data.network.model

class FbChatMessage {
    var chatId: String? = null
    var message: String? = null
    var messageStatus: Int? = null
    var messageType: Int? = null
    var senderId: String? = null
    var senderName: String? = null
    var time: Long? = null

    var attachmentsUrl: String? = null
    var thumpNailImagePath: String? = null
}
