package org.vervone.kidskonnect.core.helper

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executors


object ThreadUtils {

    private val uiHandler = Handler(Looper.getMainLooper())

    fun runOnUi(task: Runnable) {
        uiHandler.post(task)
    }


    fun runOnUi(task: () -> Unit) {
        uiHandler.post(task)
    }

    private val executor = Executors.newFixedThreadPool(5)

    fun runOnBg(task: () -> Unit) {
        executor.execute(task)
    }


}