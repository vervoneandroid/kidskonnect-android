package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.*
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.model.FbMessageChannel
import org.vervone.kidskonnect.core.data.network.model.TEACHERS
import org.vervone.kidskonnect.core.data.network.model.USER_CHAT_DICT
import java.util.concurrent.atomic.AtomicInteger

/**
 *   Used to get All the chat rooms available for the logged in user.
 */
class GetTeacherChatRooms(private val academicYr: String,
                          private val userUid: String,
                          private val classes: List<String>,
                          private val callback: RequestCallback<HashMap<String, FbMessageChannel>, ApiError>?) {

    private val chatRoomIds = mutableListOf<String>()
    private val reqCount = AtomicInteger(2)

    fun execute() {
        getChatRoomsForTeacher()

        object : GetClassChatRooms(classes, academicYr) {
            override fun onSuccess(chatRooms: HashMap<String, Boolean>) {
                chatRoomIds.addAll(chatRooms.keys)
                if (reqCount.decrementAndGet() == 0) {
                    fetch()
                }
            }
        }

    }

    private fun getChatRoomsForTeacher() {
        ConnectionHelper.getDB()!!.reference
            .child(academicYr)
            .child(TEACHERS)
            .child(userUid)
            .child(USER_CHAT_DICT)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {
                    if (reqCount.decrementAndGet() == 0) {
                        callback?.onError(ApiError(dbError.message))
                    }

                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    val typeIndicator = object : GenericTypeIndicator<HashMap<String, Boolean>>() {}
                    val allChatRooms = snapshot.getValue(typeIndicator)

                    if (allChatRooms != null) {
                        chatRoomIds.addAll(allChatRooms.keys)
                    }

                    if (reqCount.decrementAndGet() == 0) {
                        fetch()
                    } else {
                        //callback?.onSuccess(hashMapOf())
                    }
                }

            })
    }


    private fun fetch() {
        object : GetChatRooms(academicYr, chatRoomIds) {
            override fun onError(apiError: ApiError) {
                callback?.onError(apiError)
            }

            override fun onResult(result: HashMap<String, FbMessageChannel>) {
                callback?.onSuccess(result)
            }
        }
    }


}