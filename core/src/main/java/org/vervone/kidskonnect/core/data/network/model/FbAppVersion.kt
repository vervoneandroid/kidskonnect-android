package org.vervone.kidskonnect.core.data.network.model

class FbAppVersion {
    var appCurrentVersion: String? = null
    var appMinimumVersion: String? = null
}