package org.vervone.kidskonnect.core.data.network

import android.graphics.Bitmap
import android.media.ThumbnailUtils
import android.provider.MediaStore
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.helper.AppHelper
import org.vervone.kidskonnect.core.helper.copyToFile
import java.io.File
import java.io.FileOutputStream

/**
 *  Class represents attachment file details.
 */
class Attachment(private val path: String, val index: Int) {

    var fileToUpload: File? = null
        private set

    // This is only available for video files.
    var thumbnailFile: File? = null
        private set

    var isImage = false
        private set

    var isVideo = false
        private set

    var nameInCloud: String? = null
        private set

    var thumbnailNameInCloud: String? = null
        private set

    init {

        prepare()

        val context = AppHelper.get()

        val originalFile = File(path)

        //If the attachment is video we have to upload the thumbnail
        if (isVideo) {
            val thumbnail = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Images.Thumbnails.MICRO_KIND)
            this.thumbnailFile = File.createTempFile("thumpnail-", ".jpeg", context.cacheDir)
            val outputStream = FileOutputStream(this.thumbnailFile)
            thumbnail.compress(Bitmap.CompressFormat.PNG, 90, outputStream)
        }

        this.fileToUpload =
            File.createTempFile("attachment-", ".${originalFile.extension}", context.cacheDir)
        if (originalFile.exists()) {
            originalFile.copyToFile(this.fileToUpload!!)
            DLog.v(this, "file $path copied to ${fileToUpload!!.absolutePath} .")
        }

        generateName()

    }

    private fun generateName() {
        this.nameInCloud = "$index.${fileToUpload?.extension}"
        this.thumbnailNameInCloud = "${index}_thumpnail.png"
    }


    private fun prepare() {
        for (format in VIDEO_FORMATS) {
            if (path.endsWith(format)) {
                this.isVideo = true
                return
            }
        }

        for (format in IMAGE_FORMATS) {
            if (path.endsWith(format)) {
                this.isImage = true
                return
            }
        }
    }

    override fun toString(): String {
        return "Attachment(path='$path', index=$index, fileToUpload=$fileToUpload, thumbnailFile=$thumbnailFile, isImage=$isImage, isVideo=$isVideo, nameInCloud=$nameInCloud)"
    }


    companion object {
        private val VIDEO_FORMATS = listOf(".mp4", ".MOV")
        private val IMAGE_FORMATS = listOf(".png", ".jpg", ".jpeg")
    }
}
