package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.GenericTypeIndicator
import com.google.firebase.database.ValueEventListener
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.model.FbNews
import org.vervone.kidskonnect.core.data.network.model.academicyear.FbAcademicYrTeacher


/**
 *  Helper class for fetching news
 */
class TeachersNewsFetcherHelper(
        private val academicYr: String,
        private val teacherUid: String,
        private val isAdmin: Boolean = false,
        private val callback: RequestCallback<List<FbNews>, DatabaseError>
) {

    //private val executor = Executors.newFixedThreadPool(3)

    fun fetch() {
        DLog.v(this, " started ")
        if (!isAdmin) {
            ConnectionHelper.getDB()!!.getReference("$academicYr/teachers/$teacherUid")
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(dataSnap: DataSnapshot) {
                        val obj = dataSnap.getValue(FbAcademicYrTeacher::class.java)
                        System.out.println(obj)
                        fetchNewsForClasses(obj)
                    }

                    override fun onCancelled(dbError: DatabaseError) {
                        System.out.println(dbError)
                    }
                })
        } else {
            ConnectionHelper.getDB()!!.getReference("classList")
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(dbError: DatabaseError) {
                        System.out.println(dbError)
                        callback.onError(dbError)
                    }

                    override fun onDataChange(snapshot: DataSnapshot) {
                        val schoolClasses = snapshot.getValue(ApiHelperImpl.TYPE_INDICATOR_STRINGS)
                        System.out.println(schoolClasses)
                        fetchNewsForClasses(schoolClasses)
                    }
                })

        }
    }

    private fun fetchNewsForClasses(academicYrTeacher: FbAcademicYrTeacher?) {
        academicYrTeacher?.let {
            val classesAndSubjects = academicYrTeacher.classesAndSubjects

            if (classesAndSubjects != null) {
                val request = object : GetNewsIds(classesAndSubjects.keys) {
                    override fun onResult(list: ArrayList<String>) {
                        if (list.isNullOrEmpty()) {
                            callback.onSuccess(emptyList())
                            return
                        }
                        getNewsForId(list)
                    }
                }
                request.execute()
            }
        }
    }

    private fun fetchNewsForClasses(classes: List<String>?) {
        if (classes != null) {
            val request = object : GetNewsIds(classes.toMutableSet()) {
                override fun onResult(list: ArrayList<String>) {
                    if (list.isNullOrEmpty()) {
                        callback.onSuccess(emptyList())
                        return
                    }
                    getNewsForId(list)
                }
            }
            request.execute()
        }
    }


    private fun getNewsForId(newsIds: ArrayList<String>) {
        val getNewsReq = object : GetNews(academicYr, newsIds) {
            override fun onResult(news: ArrayList<FbNews>) {
                callback.onSuccess(news)
            }
        }
        getNewsReq.execute()
    }

    abstract inner class GetNewsIds(private val classNames: MutableSet<String>) : ValueEventListener {

        private val reqCount = classNames.size
        private var resCount = 0

        private val requestedIds = ArrayList<String>()

        fun execute() {
            classNames.forEach {
                ConnectionHelper.getDB()!!.getReference("$academicYr/class/$it/newsFeeds")
                    .addListenerForSingleValueEvent(this)
            }
        }

        override fun onCancelled(dbError: DatabaseError) {
            resCount++
            DLog.v(this, "news fetch failed ${dbError.toException()}")
            inform()
        }

        override fun onDataChange(snapshot: DataSnapshot) {
            resCount++
            DLog.v(this, "news fetch onDataChange ")

            val typeIndicator = object : GenericTypeIndicator<HashMap<String, Boolean>>() {}
            val newsIds = snapshot.getValue(typeIndicator)
            newsIds?.keys?.forEach {
                if (!requestedIds.contains(it)) {
                    requestedIds.add(it)
                }
            }
            inform()
        }

        abstract fun onResult(list: ArrayList<String>)
        private fun dataReady(): Boolean = (reqCount == resCount)

        private fun inform() {
            if (dataReady()) {
                onResult(ArrayList(requestedIds))
            }
        }

    }
}