package org.vervone.kidskonnect.core.data.network.model

import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties

/**
 *
 */
@IgnoreExtraProperties
class FbNotification : Id {

    var date: Long? = null
    var description: String? = null
    var title: String? = null

    @Exclude
    override var id: String? = null
}