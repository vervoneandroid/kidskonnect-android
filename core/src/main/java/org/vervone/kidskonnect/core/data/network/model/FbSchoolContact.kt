package org.vervone.kidskonnect.core.data.network.model

class FbSchoolContact {
    var address: String? = null
    var contactNo: String? = null
    var email: String? = null
    var place: String? = null
    var schoolName: String? = null
}