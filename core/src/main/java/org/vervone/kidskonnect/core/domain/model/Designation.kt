package org.vervone.kidskonnect.core.domain.model

import java.io.Serializable

enum class Designation(val tag: Int) : Serializable {
    //100 - faculty, 101 - principal , 102 - Admin

    FACULTY(100),
    PRINCIPAL(101),
    ADMIN(102),
    UNKNOWN(-1);

    companion object {
        fun fromTag(tag: Int?): Designation {
            return when (tag) {
                100 -> FACULTY
                101 -> PRINCIPAL
                102 -> ADMIN
                else -> UNKNOWN
            }
        }
    }

}