package org.vervone.kidskonnect.core.domain.model.student

import java.io.Serializable

enum class CommentType(val value: Int) : Serializable {
    PROFILE(0),
    TALENTS_ACHIEVEMENT(1),
    HEALTH_MATTERS(2),
    FAMILY_MATTERS(3),

    UNKNOWN(-1);

    companion object {
        fun fromInt(value: Int?): CommentType {
            return when (value) {
                1 -> TALENTS_ACHIEVEMENT
                2 -> HEALTH_MATTERS
                3 -> FAMILY_MATTERS
                else -> UNKNOWN
            }
        }
    }

}