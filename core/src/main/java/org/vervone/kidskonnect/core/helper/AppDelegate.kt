package org.vervone.kidskonnect.core.helper

import android.app.Activity
import android.content.Context
import org.vervone.kidskonnect.core.cloudstorage.CloudStorage

interface AppDelegate {
    fun getActivity(): Activity?
    fun getCloudStorage(): CloudStorage?
    fun getAppContext(): Context
}