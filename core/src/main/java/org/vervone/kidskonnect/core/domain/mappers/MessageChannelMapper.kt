package org.vervone.kidskonnect.core.domain.mappers

import org.vervone.kidskonnect.core.Mapper
import org.vervone.kidskonnect.core.data.network.model.FbMessageChannel
import org.vervone.kidskonnect.core.domain.model.MessageChannel

class MessageChannelMapper : Mapper<FbMessageChannel, MessageChannel> {

    override fun map(input: FbMessageChannel): MessageChannel {
        val messageChannel = MessageChannel()
        messageChannel.isGroupMessage = input.isGroupMessage
        messageChannel.targetId = input.targetId
        messageChannel.targets = input.targets
        messageChannel.id = input.channelId
        messageChannel.groupName = input.groupName
        return messageChannel
    }


    fun map(id: String, input: FbMessageChannel): MessageChannel {
        val messageChannel = MessageChannel()
        messageChannel.isGroupMessage = input.isGroupMessage
        messageChannel.targetId = input.targetId
        messageChannel.targets = input.targets
        messageChannel.id = id
        messageChannel.groupName = input.groupName
        return messageChannel
    }

    fun map(input: MessageChannel): FbMessageChannel {
        val messageChannel = FbMessageChannel()
        messageChannel.channelId = input.id
        messageChannel.groupName = input.groupName ?: "notDefined"
        messageChannel.isGroupMessage = input.isGroupMessage
        messageChannel.targetId = input.targetId
        messageChannel.targets = input.targets
        messageChannel.studentIdsAndClass = input.studentIdsAndClass
        messageChannel.teacherIds = input.teacherIds
        messageChannel.classes = input.classes

        return messageChannel
    }

}