package org.vervone.kidskonnect.core.domain

import com.google.firebase.database.DatabaseError
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.UploadListener
import org.vervone.kidskonnect.core.data.network.*
import org.vervone.kidskonnect.core.data.network.model.FbChatMessage
import org.vervone.kidskonnect.core.data.network.model.FbMessageChannel
import org.vervone.kidskonnect.core.domain.mappers.ChatMessageMapper
import org.vervone.kidskonnect.core.domain.mappers.MessageChannelMapper
import org.vervone.kidskonnect.core.domain.mappers.ParamsMapper
import org.vervone.kidskonnect.core.domain.model.ChatMessage
import org.vervone.kidskonnect.core.domain.model.MessageChannel
import org.vervone.kidskonnect.core.domain.model.StudentParamsBm
import org.vervone.kidskonnect.core.domain.model.TeacherParamsBm
import org.vervone.kidskonnect.core.helper.Task

class MessageFacadeImpl : MessageFacade {


    private val apiHelper = ApiHelper.Factory.apiHelper
    //private val bgExecutor = Executors.newFixedThreadPool(3)

    override fun getAllChatRooms(academicYr: String,
                                 userUid: String,
                                 classes: List<String>,
                                 callback: RequestCallback<List<MessageChannel>, Error>?) {
        apiHelper.getAllChatRooms(
                academicYr, userUid, classes, object : RequestCallback<HashMap<String, FbMessageChannel>, ApiError> {
            override fun onSuccess(res: HashMap<String, FbMessageChannel>?) {
                val mapper = MessageChannelMapper()
                val list = res?.map { mapper.map(it.key, it.value) }
                callback?.onSuccess(list)
            }

            override fun onError(error: ApiError?) {
                callback?.onError(Error(error?.message))
            }
        })
    }


    override fun getChatMessages(
            academicYr: String, chatRoomId: String,
            callback: RequestCallback<List<ChatMessage>, Error>?
    ): Subscription {
        return apiHelper.getAllMessages(
                academicYr, chatRoomId, object : RequestCallback<HashMap<String, FbChatMessage>, DatabaseError> {
            override fun onSuccess(res: HashMap<String, FbChatMessage>?) {
                val mapper = ChatMessageMapper()
                val list = res?.map { mapper.map(it.value) }
                callback?.onSuccess(list)
            }

            override fun onError(error: DatabaseError?) {
                callback?.onError(Error(error?.message))
            }
        })
    }

    override fun sendMessage(academicYr: String,
                             chatRoomId: String,
                             chatMessage: ChatMessage,
                             callback: UploadListener<ChatMessage, Error>?): Task {
        val mapper = ChatMessageMapper()
        return apiHelper.sendMessage(
                academicYr, chatRoomId, mapper.map(chatMessage), object : UploadListener<FbChatMessage, DatabaseError> {
            override fun onSuccess(res: FbChatMessage?) {
                callback?.onSuccess(mapper.map(res!!))
            }

            override fun onError(error: DatabaseError?) {
                callback?.onError(Error(error?.message))
            }

            override fun onProgress(percentage: Int) {
                callback?.onProgress(percentage)
            }
        })
    }


    override fun createMessageChannelByTeacher(
            academicYr: String,
            teacherId: String,
            messageChannel: MessageChannel,
            callback: RequestCallback<MessageChannel, DomainError>
    ) {

        val channel = MessageChannelMapper().map(messageChannel)
        apiHelper.createMessageChannelByTeacher(
                academicYr, teacherId, channel,
                object : RequestCallback<Pair<String, FbMessageChannel>, ApiError> {
                    override fun onSuccess(res: Pair<String, FbMessageChannel>?) {
                        val mapped = MessageChannelMapper().map(res!!.first, res.second)
                        callback.onSuccess(mapped)
                    }

                    override fun onError(error: ApiError?) {
                        callback.onError(DomainError(error?.message ?: ""))
                    }
                })

    }

    override fun createMessageChannelByStudent(academicYr: String,
                                               className: String,
                                               studentId: String,
                                               messageChannel: MessageChannel,
                                               callback: RequestCallback<MessageChannel, DomainError>) {
        val channel = MessageChannelMapper().map(messageChannel)
        apiHelper.createMessageChannelByStudent(
                academicYr, className, studentId, channel,
                object : RequestCallback<Pair<String, FbMessageChannel>, ApiError> {
                    override fun onSuccess(res: Pair<String, FbMessageChannel>?) {
                        val mapped = MessageChannelMapper().map(res!!.first, res.second)
                        callback.onSuccess(mapped)
                    }

                    override fun onError(error: ApiError?) {
                        callback.onError(DomainError(error?.message ?: ""))
                    }
                })
    }

    override fun getChatRoomIfAvailable(
            academicYr: String,
            targetsId: String,
            callback: RequestCallback<MessageChannel?, DomainError>?
    ) {
        apiHelper.getChatRoomIfAvailable(
                academicYr,
                targetsId,
                object : RequestCallback<Pair<String, FbMessageChannel>?, DatabaseError> {
                    override fun onSuccess(res: Pair<String, FbMessageChannel>?) {
                        if (res != null) {
                            val messageChannel = MessageChannelMapper().map(res.first, res.second)
                            callback?.onSuccess(messageChannel)
                        } else {
                            callback?.onSuccess(null)
                        }
                    }

                    override fun onError(error: DatabaseError?) {
                        callback?.onError(DomainError(error?.message ?: ""))
                    }
                })
    }

    override fun getLastMessage(academicYr: String,
                                chatRoomId: String,
                                callback: RequestCallback<HashMap<String, ChatMessage>, DomainError>?) {
        apiHelper.getLastMessage(academicYr, chatRoomId, object :
            RequestCallback<HashMap<String, FbChatMessage>, ApiError> {
            override fun onSuccess(res: HashMap<String, FbChatMessage>?) {
                val mapper = ChatMessageMapper()
                val map = res?.map { it.key to mapper.map(it.value) }?.toMap()
                callback?.onSuccess(HashMap(map))
            }

            override fun onError(error: ApiError?) {
                callback?.onError(DomainError(error?.message))
            }

        })
    }


    override fun getStudentChatRooms(academicYr: String,
                                     userUid: String,
                                     className: String,
                                     callback: RequestCallback<List<MessageChannel>, Error>?) {
        apiHelper.getChatRoomsForStudent(
                academicYr, userUid, className, object : RequestCallback<HashMap<String, FbMessageChannel>, ApiError> {
            override fun onSuccess(res: HashMap<String, FbMessageChannel>?) {
                val mapper = MessageChannelMapper()
                val list = res?.map { mapper.map(it.key, it.value) }
                callback?.onSuccess(list)
            }

            override fun onError(error: ApiError?) {
                callback?.onError(Error(error?.message))
            }
        })
    }

    override fun subscribeForStudentMessageChannels(paramsBm: StudentParamsBm,
                                                    callback: Callback): String {
        val params = ParamsMapper().map(paramsBm)
        return apiHelper.subscribe(Topics.CHAT_ROOMS, params, callback)
    }

    override fun subscribeForTeacherMessageChannels(paramsBm: TeacherParamsBm,
                                                    callback: Callback): String {
        val params = ParamsMapper().map(paramsBm)
        return apiHelper.subscribe(Topics.CHAT_ROOMS, params, callback)
    }

    override fun unsubscribe(subscriptionId: String?) {
        subscriptionId?.let {
            apiHelper.unsubscribe(it)
        }
    }


    override fun subscribeForTeacherMessages(paramsBm: TeacherParamsBm,
                                             callback: MessageCallback): String {
        val params = ParamsMapper().map(paramsBm)
        return apiHelper.subscribeForMessages(params, callback)
    }

    override fun subscribeForStudentMessage(paramsBm: StudentParamsBm,
                                            callback: MessageCallback): String {
        val params = ParamsMapper().map(paramsBm)
        return apiHelper.subscribeForMessages(params, callback)
    }

    override fun subscribeTeacherMessageNotifications(paramsBm: TeacherParamsBm,
                                                      callback: MessageFacade.NotificationListener) {

        val params = ParamsMapper().map(paramsBm)
        val msgMapper = ChatMessageMapper()
        val msgChannelMapper = MessageChannelMapper()
        MessageNotifications.subscribe(params, object : NotificationListener {

            override fun onMessage(fbChatMessage: FbChatMessage, msgCount: Int, messageChannel: FbMessageChannel?) {
                val chatMsg = msgMapper.map(fbChatMessage)
                if (messageChannel != null) {
                    val mc = msgChannelMapper.map(messageChannel)
                    callback.onMessage(chatMsg, msgCount, mc)
                }
            }

            override fun onSnapshot(messageChannel: FbMessageChannel?, msgCount: Int) {
                if (messageChannel != null) {
                    val mc = msgChannelMapper.map(messageChannel)
                    callback.onSnapshot(mc, msgCount)
                }
            }
        })

    }

    override fun subscribeStudentMessageNotifications(paramsBm: StudentParamsBm,
                                                      callback: MessageFacade.NotificationListener) {
        val msgMapper = ChatMessageMapper()
        val msgChannelMapper = MessageChannelMapper()
        val params = ParamsMapper().map(paramsBm)
        MessageNotifications.subscribe(params, object : NotificationListener {

            override fun onMessage(fbChatMessage: FbChatMessage, msgCount: Int, messageChannel: FbMessageChannel?) {
                val chatMsg = msgMapper.map(fbChatMessage)
                if (messageChannel != null) {
                    val mc = msgChannelMapper.map(messageChannel)
                    callback.onMessage(chatMsg, msgCount, mc)
                }
            }

            override fun onSnapshot(messageChannel: FbMessageChannel?, msgCount: Int) {
                if (messageChannel != null) {
                    val mc = msgChannelMapper.map(messageChannel)
                    callback.onSnapshot(mc, msgCount)
                }
            }
        })

    }

}