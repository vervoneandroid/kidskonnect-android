package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.*
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.model.*

/**
 *   Update Master Leave Table , Student Table and Teacher Table
 */
class ApplyLeaveHelper(private val academicYr: String,
                       private val teacherId: String,
                       private val leave: FbLeave,
                       private val callback: RequestCallback<FbLeave, ApiError>) {

    private val database = ConnectionHelper.getDB()

    fun execute() {
        updateLeaveTable()
    }

    private fun updateLeaveTable() {
        val ref = database!!.reference
            .child(academicYr)
            .child(LEAVES_LIST)

        val generateId = ref.push().key!!

        ref.child(generateId).setValue(leave)
            .addOnSuccessListener {
                DLog.v(this, "Leaves Table updated")
                updateTeacherLeave(generateId)
            }
            .addOnFailureListener { ex ->
                DLog.v(this, "Leaves Table update failed. $ex")
                callback.onError(ApiError(ex.message ?: ""))
            }
    }

    private fun updateTeacherLeave(leaveId: String) {
        //2019-2020/teachers/dpljMJTYfuXawOyYdIy7ZY0Li7L2/leavesList

        database!!.reference
            .child(academicYr)
            .child(TEACHERS)
            .child(teacherId)
            .child(LEAVES_LIST)
            .runTransaction(object : Transaction.Handler {
                override fun onComplete(dbError: DatabaseError?, p1: Boolean, p2: DataSnapshot?) {
                    if (dbError == null) {
                        updateStudentLeave(leaveId)
                    } else {
                        DLog.v(this, "Teacher Leaves Table update failed. $dbError")
                    }
                }

                override fun doTransaction(mutableData: MutableData): Transaction.Result {
                    val leavesMap =
                        mutableData.getValue(ApiHelperImpl.TYPE_INDICATOR_STRING_BOOL_MAP)
                            ?: return Transaction.success(mutableData)
                    leavesMap[leaveId] = true
                    mutableData.value = leavesMap
                    return Transaction.success(mutableData)
                }
            })
    }

    private fun updateStudentLeave(
            leaveId: String) {
        //2019-2020/class/1 A/students/AvZzFl7dxraUrCqEvxUlcfnYo3c2/leavesList

        database!!.reference
            .child(academicYr)
            .child(CLASS)
            .child(leave.getClass()!!)
            .child(STUDENTS)
            .child(leave.studentId!!)
            .child(LEAVES_LIST)
            .runTransaction(object : Transaction.Handler {
                override fun onComplete(dbError: DatabaseError?, p1: Boolean, snapshot: DataSnapshot?) {
                    if (dbError == null) {
                        leave.id = leaveId
                        if (snapshot?.value != null) {
                            callback.onSuccess(leave)
                        } else {
                            createChild(leaveId)
                        }
                    } else {
                        DLog.v(this, "Student Leaves Table update failed. $dbError")
                    }
                }

                override fun doTransaction(mutableData: MutableData): Transaction.Result {
                    val leavesMap =
                        mutableData.getValue(ApiHelperImpl.TYPE_INDICATOR_STRING_BOOL_MAP)
                            ?: return Transaction.success(mutableData)
                    leavesMap[leaveId] = true
                    mutableData.value = leavesMap
                    return Transaction.success(mutableData)
                }
            })

    }

    private fun createChild(leaveId: String) {

        val map = HashMap<String, HashMap<String, Boolean>>()
        map[LEAVES_LIST] = hashMapOf(Pair(leaveId, true))

        database!!.reference
            .child(academicYr)
            .child(CLASS)
            .child(leave.getClass()!!)
            .child(STUDENTS)
            .child(leave.studentId!!).setValue(map)
            .addOnSuccessListener { callback.onSuccess(leave) }
            .addOnFailureListener { ex -> callback.onError(ApiError(ex.message ?: "")) }
    }


}