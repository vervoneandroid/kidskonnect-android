package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.GenericTypeIndicator
import org.vervone.kidskonnect.core.data.network.TypeIndicators.STRING_BOOLEAN_MAP


fun DataSnapshot.asStringBoolMap(): HashMap<String, Boolean>? {
    return this.getValue(STRING_BOOLEAN_MAP)
}


object TypeIndicators {
    val STRING_BOOLEAN_MAP = object : GenericTypeIndicator<HashMap<String, Boolean>>() {}
}