package org.vervone.kidskonnect.core.domain

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.*
import org.vervone.kidskonnect.core.data.network.model.FbLeave
import org.vervone.kidskonnect.core.domain.mappers.DomainErrorMapper
import org.vervone.kidskonnect.core.domain.mappers.LeavesMapper
import org.vervone.kidskonnect.core.domain.mappers.ParamsMapper
import org.vervone.kidskonnect.core.domain.model.Leave
import org.vervone.kidskonnect.core.domain.model.StudentParamsBm
import org.vervone.kidskonnect.core.domain.model.TeacherParamsBm
import org.vervone.kidskonnect.core.push.PushApi

class LeavesFacadeImpl : LeavesFacade {

    private val apiHelper = ApiHelper.Factory.apiHelper

    override fun getAllLeavesRequestToTeacher(academicYr: String,
                                              userId: String?,
                                              callback: RequestCallback<List<Leave>, DomainError>) {

        apiHelper.getAllLeavesRequestToTeacher(
                academicYr, userId,
                object : RequestCallback<List<FbLeave>, ApiError> {
                    override fun onError(error: ApiError?) {
                        callback.onError(DomainError(error!!.message))
                    }

                    override fun onSuccess(res: List<FbLeave>?) {
                        val leaves = res?.map { LeavesMapper().map(it) }
                        callback.onSuccess(leaves)
                    }

                }
        )
    }

    override fun subscribeAllLeaveRequestsByStudent(academicYr: String,
                                                    schoolClass: String,
                                                    userId: String?,
                                                    changeListener: DataChangeListener): String {
        return apiHelper.subscribeAllLeaveRequestsByStudent(academicYr, schoolClass, userId, changeListener)
    }

    override fun unsubscribeAllLeaveRequestsByStudent(subscriberId: String) {
        apiHelper.unsubscribeAllLeaveRequestsByStudent(subscriberId)
    }

    override fun getAllLeaveRequestsByStudent(academicYr: String,
                                              schoolClass: String,
                                              userId: String?,
                                              callback: RequestCallback<List<Leave>, DomainError>) {

        apiHelper.getAllLeaveRequestsByStudent(
                academicYr, schoolClass, userId, object : RequestCallback<List<FbLeave>, ApiError> {
            override fun onError(error: ApiError?) {
                callback.onError(DomainError(error!!.message))
            }

            override fun onSuccess(res: List<FbLeave>?) {
                val leaves = res?.map { LeavesMapper().map(it) }
                callback.onSuccess(leaves)
            }
        })
    }

    override fun subscribeAllLeavesRequestToTeacher(academicYr: String,
                                                    userId: String?,
                                                    changeListener: DataChangeListener): String {
        return apiHelper.subscribeAllLeavesRequestToTeacher(academicYr, userId, changeListener)
    }

    override fun unsubscribeAllLeavesRequestToTeacher(subscriberId: String) {
        apiHelper.unsubscribeAllLeavesRequestToTeacher(subscriberId)
    }


    override fun approveLeaveRequest(academicYr: String,
                                     leave: Leave,
                                     callback: RequestCallback<Leave, DomainError>) {

        val mapper = LeavesMapper()
        apiHelper.updateLeave(academicYr,
                              mapper.map(leave), object : RequestCallback<FbLeave, ApiError> {
            override fun onSuccess(res: FbLeave?) {
                callback.onSuccess(mapper.map(res!!))
                if (leave.teacherName != null && leave.studentId != null) {
                    PushApi.Factory.pushApi.publishApprove(leave.teacherName!!, leave.studentId!!)
                }
            }

            override fun onError(error: ApiError?) {
                callback.onError(DomainErrorMapper().map(error!!))
            }
        })
    }

    override fun rejectLeaveRequest(academicYr: String,
                                    leave: Leave,
                                    callback: RequestCallback<Leave, DomainError>) {
        val mapper = LeavesMapper()
        apiHelper.updateLeave(academicYr,
                              mapper.map(leave), object : RequestCallback<FbLeave, ApiError> {
            override fun onSuccess(res: FbLeave?) {
                callback.onSuccess(mapper.map(res!!))
                if (leave.teacherName != null && leave.studentId != null) {
                    PushApi.Factory.pushApi.publishRejected(leave.teacherName!!, leave.studentId!!)
                }
            }

            override fun onError(error: ApiError?) {
                callback.onError(DomainErrorMapper().map(error!!))
            }

        })
    }

    override fun subscribeLeaveRequests(teacherParamsBm: TeacherParamsBm,
                                        observer: LeavesFacade.LeaveRequestListener) {
        LeaveRequestsNotifications.subscribe(
                ParamsMapper().map(teacherParamsBm), object : LeaveRequestsNotifications.LeaveRequestListener {
            override fun onSnapshot(count: Int) {
                observer.onSnapshot(count)
            }

            override fun onRequest(count: Int, leave: FbLeave) {
                observer.onRequest(count, LeavesMapper().map(leave))
            }
        })
    }

    override fun subscribeLeaveStatusChange(studentParamsBm: StudentParamsBm,
                                            observer: LeavesFacade.LeaveStatusChange) {
        LeaveStatusUpdateNotifications.subscribe(
                ParamsMapper().map(studentParamsBm), object : LeaveStatusUpdateNotifications.LeaveStatusListener {
            override fun onStatusChange(leave: FbLeave) {
                observer.onChange(LeavesMapper().map(leave))
            }
        })

    }

}