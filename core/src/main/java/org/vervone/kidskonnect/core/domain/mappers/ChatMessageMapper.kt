package org.vervone.kidskonnect.core.domain.mappers

import org.vervone.kidskonnect.core.Mapper
import org.vervone.kidskonnect.core.data.network.model.FbChatMessage
import org.vervone.kidskonnect.core.domain.model.ChatMessage
import org.vervone.kidskonnect.core.domain.model.MessageType

class ChatMessageMapper : Mapper<FbChatMessage, ChatMessage> {

    override fun map(input: FbChatMessage): ChatMessage {
        val chatMessage = ChatMessage()
        chatMessage.chatId = input.chatId
        chatMessage.message = input.message
        chatMessage.messageStatus = input.messageStatus
        chatMessage.messageType = MessageType.fromInt(input.messageType)
        chatMessage.senderId = input.senderId
        chatMessage.senderName = input.senderName
        chatMessage.thumpNailImagePath = input.thumpNailImagePath
        chatMessage.attachmentsUrl = input.attachmentsUrl
        chatMessage.time = input.time
        return chatMessage
    }


    fun map(input: ChatMessage): FbChatMessage {
        val chatMessage = FbChatMessage()
        chatMessage.chatId = input.chatId
        chatMessage.message = input.message
        chatMessage.messageStatus = input.messageStatus
        chatMessage.messageType = input.messageType.value
        chatMessage.time = input.time
        chatMessage.senderId = input.senderId
        chatMessage.senderName = input.senderName
        chatMessage.thumpNailImagePath = input.thumpNailImagePath
        chatMessage.attachmentsUrl = input.localFilePath
        return chatMessage
    }
}