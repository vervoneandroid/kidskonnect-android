package org.vervone.kidskonnect.core.data.network.model.academicyear

import org.vervone.kidskonnect.core.data.network.model.student.capitalizeWords

class FbAcademicYrTeacher {

    var isClassTeacher: String? = null
    var classesAndSubjects: HashMap<String, ArrayList<String>>? = null
    var leavesList: HashMap<String, Boolean>? = null
    var likedNews: HashMap<String, Boolean>? = null
    var teacherDesignationTag: Int? = null
    var teacherName: String? = null
        get() {
            return field?.capitalizeWords()
        }
    var userChatDict: HashMap<String, Boolean>? = null

    /**
     * Note : This stupid getter is written for Firebase .
     * Kotlin generating automatic getter and setter for each fields
     * and It changes the getter name for field names starting with 'is'
     * in a different way .
     *
     *  Eg: For variable name  String isClassTeacher
     *
     *   Kotlin generate getter like , isClassTeacher()
     *
     *  And Firebase failing to find this method.
     *
     */
    fun getIsClassTeacher(): String? {
        return this.isClassTeacher
    }

    override fun toString(): String {
        return "FbAcademicYrTeacher(classesAndSubjects=$classesAndSubjects, isClassTeacher=$isClassTeacher, leavesList=$leavesList, likedNews=$likedNews, teacherDesignationTag=$teacherDesignationTag, teacherName=$teacherName, userChatDict=$userChatDict)"
    }


}