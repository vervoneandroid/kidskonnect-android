package org.vervone.kidskonnect.core.helper

import java.io.File

fun File.copyToFile(file: File) {
    inputStream().use { input ->
        file.outputStream().use { output ->
            input.copyTo(output)
        }
    }
}

@Suppress("UNCHECKED_CAST")
inline fun <reified T : Any> List<*>.checkItemsAre() =
    if (all { it is T })
        this as List<T>
    else null


@Suppress("UNCHECKED_CAST")
fun <T> Any.cast(): T {
    return this as T
}


/**
 *  Return a empty list if the specified [this] map
 */
fun <K, V, R> Map<out K, V>?.safeMap(transform: (Map.Entry<K, V>) -> R): List<R> {
    return this?.map(transform) ?: emptyList()
}


/**
 * Appends all entries matching the given [predicate] into the mutable list given as [destination] parameter.
 *
 * @return the destination list.
 */
inline fun <K, V, M : MutableList<V>> Map<out K, V>.filterToList(destination: M,
                                                                 predicate: (Map.Entry<K, V>) -> Boolean): M {
    for (element in this) {
        if (predicate(element)) {
            destination.add(element.value)
        }
    }
    return destination
}