package org.vervone.kidskonnect.core.data.network

import org.vervone.kidskonnect.core.R
import org.vervone.kidskonnect.core.helper.AppHelper

object Errors {


    /*
    ERROR_USER_DISABLED – The account with which the email is associated exists but has been disabled.

•	ERROR_USER_NOT_FOUND – No account could be found that matches the specified email address. The user has either entered the wrong email address, has yet to create an account or the account has been deleted.

•	ERROR_USER_TOKEN_EXPIRED – The user’s token has been invalidated by the Firebase system. This usually occurs when the user changes the password associated with the account from another device or platform (such as a web site associated with the app).

•	ERROR_INVALID_USER_TOKEN – The system does not recognize the user’s token as being correctly formed. FirebaseAuthUserCollisionException, on the other hand, supports the following error codes:

•	ERROR_EMAIL_ALREADY_IN_USE
     */

    const val ERROR_USER_NOT_FOUND = "ERROR_USER_NOT_FOUND"

    private val errorMap = HashMap<String, String>()

    fun init() {
        val res = AppHelper.getContext()
        errorMap[ERROR_USER_NOT_FOUND] = res.getString(R.string.user_not_found)
    }

    fun getErrorMessage(code: String): String {
        return if (errorMap[code] == null) code else errorMap[code]!!
    }

}