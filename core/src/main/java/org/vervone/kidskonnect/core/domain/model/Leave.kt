package org.vervone.kidskonnect.core.domain.model

import java.io.Serializable

class Leave() : Serializable {
    var schoolClass: String? = null
    var leaveDays: List<String?>? = null
    var leaveEndsAt: String? = null
    var leaveStartsFrom: String? = null
    var leaveStatus: LeaveStatus = LeaveStatus.PENDING
    var noOfDays: String? = null
    var reason: String? = null
    var replyString: String? = null
    var sendDate: String? = null
    var studentId: String? = null
    var studentName: String? = null
    var teacherName: String? = null
    var id: String? = null

    // Only used for view rendering
    var isExpanded: Boolean = false

    // copy constructor
    constructor(leave: Leave) : this() {
        this.schoolClass = leave.schoolClass
        this.leaveDays = leave.leaveDays
        this.leaveEndsAt = leave.leaveEndsAt
        this.leaveStartsFrom = leave.leaveStartsFrom
        this.leaveStatus = leave.leaveStatus
        this.noOfDays = leave.noOfDays
        this.reason = leave.reason
        this.replyString = leave.replyString
        this.sendDate = leave.sendDate
        this.studentId = leave.studentId
        this.studentName = leave.studentName
        this.teacherName = leave.teacherName
        this.id = leave.id
        this.isExpanded = leave.isExpanded
    }


}