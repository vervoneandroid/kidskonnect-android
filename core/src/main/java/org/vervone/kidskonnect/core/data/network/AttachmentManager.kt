package org.vervone.kidskonnect.core.data.network

import org.vervone.kidskonnect.core.DLog

class AttachmentManager(
    private val attachments: List<String>,
    private val academicYr: String?,
    private val newsId: String?
) {


    private var attachmentMap: HashMap<String, String>? = null
    var attachmentList: List<Attachment>? = null
        private set

    init {
        prepare()
    }


    private fun prepare() {
        // This is used to generate unique id for each attachment.
        var imageId = 1
        this.attachmentList = attachments.map { Attachment(it, imageId++) }


    }

    fun asMap(): HashMap<String, String>? {
        if (attachmentList.isNullOrEmpty()) return null

        if (attachmentMap == null) {
            val map = attachmentList!!.map { "attachment-${it.index}" to buildKey(it) }.toMap()
            attachmentMap = HashMap(map)
        }

        DLog.v(this, "map : $attachmentMap")

        return attachmentMap
    }

    fun buildKey(attachment: Attachment): String {
        return "$academicYr/newsFeeds/$newsId/${attachment.nameInCloud}"
    }

    fun buildKeyForThumbnail(attachment: Attachment): String {
        return "$academicYr/newsFeeds/$newsId/${attachment.thumbnailNameInCloud}"
    }

}