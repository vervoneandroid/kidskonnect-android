package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.*
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.data.network.model.*
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/**
 *
 */
object MessageNotifications {

    private val subscribers = Hashtable<String, NotificationListener>()
    private val isFirst = AtomicBoolean(true)
    //private val reqCount = AtomicInteger(0)
    private val channelsMap = HashMap<String, FbMessageChannel>()
    private val TYPE_MESSAGE_CHANNEL = object : GenericTypeIndicator<FbMessageChannel>() {}
    private val TYPE_MESSAGES_MAP =
        object : GenericTypeIndicator<HashMap<String, FbChatMessage>>() {}
    private val channelIds = mutableSetOf<String>()
    private val channelListeners = ArrayList<ChannelListener>()
    private val channelIdsCached = mutableSetOf<String>()

    fun subscribe(params: TeacherParams,
                  callback: NotificationListener): String {

        val uuid = ApiUtils.getUUID()
        subscribers[uuid] = callback

        if (isFirst.getAndSet(false)) {

            object : GetChatRoomIds(params) {
                override fun onResult(chatRoomIds: Set<String>) {
                    DLog.v(this, "GetChatRoomIds $chatRoomIds")
                    channelIdsCached.addAll(chatRoomIds)

                    getChatRoomsForTeacher(params.academicYr, params.teacherId)
                    params.classes.forEach {
                        getChatRoomsForClass(params.academicYr, it)
                    }
                }
            }.get()

        }

        return uuid
    }

    fun subscribe(params: StudentParams,
                  callback: NotificationListener): String {

        val uuid = ApiUtils.getUUID()
        subscribers[uuid] = callback

        if (isFirst.getAndSet(false)) {
            //reqCount.set(2)
            getChatRoomsForStudent(params.academicYr,
                                   params.studentId,
                                   params.className)
            getChatRoomsForClass(params.academicYr, params.className)
        }

        return uuid
    }


    private fun getChatRoomsForStudent(academicYr: String,
                                       studentId: String,
                                       className: String) {
        val ref = ConnectionHelper.getDB()!!.reference
            .child(academicYr)
            .child(CLASS)
            .child(className)
            .child(STUDENTS)
            .child(studentId)
            .child(USER_CHAT_DICT)
        val channelListener = ChannelListener(academicYr, ref)
        this.channelListeners.add(channelListener)
        ref.addChildEventListener(channelListener)
    }


    private fun getChatRoomsForClass(academicYr: String,
                                     schoolClass: String) {

        val ref = ConnectionHelper.getDB()!!.reference.child(academicYr)
            .child(CLASS)
            .child(schoolClass)
            .child(USER_CHAT_DICT)
        val channelListener = ChannelListener(academicYr, ref)
        this.channelListeners.add(channelListener)
        ref.addChildEventListener(channelListener)
    }

    private fun getChatRoomsForTeacher(academicYr: String,
                                       teacherId: String) {

        val ref = ConnectionHelper.getDB()!!.reference
            .child(academicYr)
            .child(TEACHERS)
            .child(teacherId)
            .child(USER_CHAT_DICT)

        val channelListener = ChannelListener(academicYr, ref)
        this.channelListeners.add(channelListener)
        ref.addChildEventListener(channelListener)
    }

    fun resetAll() {
        subscribers.clear()
        isFirst.set(true)
        channelsMap.clear()
        channelIds.clear()

        channelListeners.forEach {
            it.remove()
        }

        channelListeners.clear()
    }

    private class ChannelListener(val academicYr: String,
                                  val def: DatabaseReference) : DefChildEventListener() {

        // DB refs for messages
        private val dbQueries = ArrayList<MessagesListener>()
        private val messageMap = HashMap<String, HashMap<String, FbChatMessage>>()

        fun remove() {
            def.removeEventListener(this)
            dbQueries.forEach {
                it.remove()
            }
            dbQueries.clear()
        }

        /**
         *  Listener for Message Channel.
         */
        private val valueEventListener = object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {}
            override fun onDataChange(snapshot: DataSnapshot) {
                val key = snapshot.key
                val msgChannel = snapshot.getValue(TYPE_MESSAGE_CHANNEL)
                if (msgChannel != null && key != null) {
                    msgChannel.channelId = key
                    channelsMap[key] = msgChannel
                    fetchSnapshot(key)
                }

            }
        }

        private fun getMessagesRef(channelId: String): DatabaseReference {
            return ConnectionHelper.getDB()!!.reference
                .child(academicYr)
                .child(ALL_MESSAGES)
                .child(channelId)
        }

        private fun fetchSnapshot(channelId: String) {
            getMessagesRef(channelId).addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {}
                override fun onDataChange(snapshot: DataSnapshot) {
                    val allMessages = snapshot.getValue(TYPE_MESSAGES_MAP)
                    val messages = allMessages ?: HashMap()
                    messageMap[channelId] = messages
                    subscribers.forEach {
                        it.value.onSnapshot(channelsMap[channelId], snapshot.childrenCount.toInt())
                    }
                    listenMessages(channelId)
                }
            })
        }


        private fun listenMessages(channelId: String) {
            val ref = getMessagesRef(channelId)
            val listener = MessagesListener(ref, channelId)
            dbQueries.add(listener)
            ref.addChildEventListener(listener)
        }

        override fun onChildAdded(snapshot: DataSnapshot, childName: String?) {
            snapshot.key?.let { channelId ->
                if (!channelIds.contains(channelId)) {
                    channelIds.add(channelId)
                    ConnectionHelper.getDB()!!.reference.child(academicYr)
                        .child(ALL_MESSAGE_CHANNELS)
                        .child(channelId)
                        .addListenerForSingleValueEvent(valueEventListener)
                }
            }
        }

        private inner class MessagesListener(private val ref: DatabaseReference,
                                             private val channelId: String) :
            DefChildEventListener() {
            fun remove() {
                ref.removeEventListener(this)
            }


            private fun handleEvent(chatMessage: FbChatMessage, size: Int) {
                subscribers.forEach {
                    it.value.onMessage(chatMessage, size, channelsMap[channelId])
                }
            }

            override fun onChildAdded(snapshot: DataSnapshot, childName: String?) {
                val messages = messageMap[channelId]
                val key = snapshot.key

                if (messages != null && key != null) {
                    if (messages.containsKey(key)) {
                        return
                    } else if (!messages.containsKey(key)) {
                        val chatMessage = snapshot.getValue(FbChatMessage::class.java)
                        if (chatMessage != null) {
                            messages[key] = chatMessage
                            handleEvent(chatMessage, messages.size)
                        }
                    } else if (!channelIdsCached.contains(channelId)) {
                        //Consider this as new chat message group.
                        channelIdsCached.add(channelId)
                        val chatMessage = snapshot.getValue(FbChatMessage::class.java)
                        if (chatMessage != null) {
                            handleEvent(chatMessage, messages.size)
                        }
                    }
                }
            }
        }
    }
}
