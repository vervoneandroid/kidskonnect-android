package org.vervone.kidskonnect.core.domain.model.student

import java.io.Serializable

class Student : Serializable {
    var ProfileDetails: ProfileDetails? = null
    var fbAcademicReports: List<AcademicReport?>? = null
    var classes: Map<String, String>? = null
    var likedNews: Map<String, Boolean>? = null
    var studentName: String? = null
    var comments: List<Comment>? = null
    var profileImageUrl: String? = null
    var studentId: String? = null

    fun getClass(academicYr: String): String? {
        return classes?.get(academicYr)
    }
}