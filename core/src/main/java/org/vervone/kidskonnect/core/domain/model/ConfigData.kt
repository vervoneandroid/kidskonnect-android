package org.vervone.kidskonnect.core.domain.model

data class ConfigData(val academicYear: String, val isChanged: Boolean)