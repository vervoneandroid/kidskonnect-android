package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.*
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.model.ALL_MESSAGES
import org.vervone.kidskonnect.core.data.network.model.FbChatMessage

class MessagesSubscription(academicYr: String,
                           chatRoomId: String,
                           private val callback: RequestCallback<HashMap<String, FbChatMessage>, DatabaseError>?
) : Subscription {

    private val ref: DatabaseReference = ConnectionHelper.getDB()!!
        .reference.child(academicYr)
        .child(ALL_MESSAGES)
        .child(chatRoomId)
    private val eventListener: ValueEventListener

    init {
        eventListener = ref.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {
                callback?.onError(dbError)
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val typeIndicator =
                    object : GenericTypeIndicator<HashMap<String, FbChatMessage>>() {}
                val allMessages = snapshot.getValue(typeIndicator)
                callback?.onSuccess(allMessages)
            }
        })
    }

    override fun stop() {
        ref.removeEventListener(eventListener)
    }
}