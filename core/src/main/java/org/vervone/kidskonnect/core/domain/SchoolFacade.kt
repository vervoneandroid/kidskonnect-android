package org.vervone.kidskonnect.core.domain

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.model.*
import org.vervone.kidskonnect.core.domain.model.school.SchoolConfig
import org.vervone.kidskonnect.core.domain.model.student.AcademicReport
import org.vervone.kidskonnect.core.domain.model.student.Comment
import org.vervone.kidskonnect.core.domain.model.student.CommentType
import org.vervone.kidskonnect.core.domain.model.student.Student

interface SchoolFacade {
    fun getClasses(academicYr: String,
                   teacherId: String,
                   callback: RequestCallback<List<String>, DomainError>)

    fun getClasses(callback: RequestCallback<List<String>, DomainError>)
    fun getSchoolContact(callback: RequestCallback<SchoolContact, DomainError>)

    fun getStudents(
            academicYr: String,
            query: String?,
            schoolClass: String?,
            callback: RequestCallback<List<Student>, DomainError>
    )

    /**
     *  To add a comment about the student.
     */
    fun addCommentOnStudent(
            studentId: String,
            comment: Comment,
            callback: RequestCallback<Comment, DomainError>
    )


    fun getComments(
            studentId: String,
            commentType: CommentType,
            callback: RequestCallback<List<Comment>, DomainError>
    )

    fun subscribeStudentByTeacher(studentId: String, subscriber: Subscriber): String

    fun unsubscribe(subscriberId: String)

    fun getTeachers(
            academicYr: String,
            callback: RequestCallback<List<Teacher>, DomainError>
    )

    fun getTeachersDetails(
            academicYr: String, className: String,
            callback: RequestCallback<List<Teacher>, DomainError>
    )

    /**
     *  To get Teachers of the specified class,
     *  This will return only teacherId & Teacher name
     */
    fun getTeachers(academicYr: String, className: String,
                    callback: RequestCallback<TeachersForTheClass, DomainError>)

    /**
     *  Create Leave request
     */
    fun createLeaveRequest(academicYr: String,
                           teacherId: String,
                           leave: Leave,
                           callback: RequestCallback<Leave, DomainError>)

    interface Subscriber {
        fun onChange()
    }

    /**
     *  subscribe student
     */
    fun subscribeStudent(studentId: String, listener: StudentListener): String

    fun unsubscribeStudent(subscriberId: String)


    fun getAcademicReport(studentId: String,
                          callback: RequestCallback<ArrayList<AcademicReport>, DomainError>)

    fun updateAlternatePhoneNumber(studentId: String,
                                   phoneNumber: String,
                                   callback: RequestCallback<String, DomainError>)


    fun subscribeComment(studentParams: StudentParamsBm, listener: CommentsListener)
    fun subscribeComment(teacherParams: TeacherParamsBm, listener: CommentsListener)

    fun getSchoolsConfigs(callback: RequestCallback<List<SchoolConfig>, DomainError>)
    fun getSchoolConfigs(schoolId: String,
                         callback: RequestCallback<SchoolConfig, DomainError>)

    interface CommentsListener {
        fun onSnapshot(count: Int, student: Student)
        fun onNewComment(count: Int, student: Student, comment: Comment)
    }

    interface StudentListener {
        fun onUpdate(student: Student)
    }

}