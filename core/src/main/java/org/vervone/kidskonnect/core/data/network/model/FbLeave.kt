package org.vervone.kidskonnect.core.data.network.model

import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties
import com.google.firebase.database.PropertyName

@IgnoreExtraProperties
class FbLeave {

    @get:PropertyName("class")
    @set:PropertyName("class")
    var sClass: String? = null

    //var sClass: String? = null
    var leaveDays: List<String?>? = null
    var leaveEndsAt: String? = null
    var leaveStartsFrom: String? = null
    var leaveStatus: Int? = null
    var noOfDays: String? = null
    var reason: String? = null
    var replyString: String? = null
    var sendDate: String? = null
    var studentId: String? = null
    var studentName: String? = null
    var teacherName: String? = null
    //var leaveId: String? = null

    @Exclude
    var id: String? = null

    @Exclude
    fun getClass(): String? {
        return this.sClass
    }
}