package org.vervone.kidskonnect.core.domain

import org.vervone.kidskonnect.core.domain.model.Teacher

/**
 *  Cache for keeping teachers and the subjects
 *  for the current logged-in student.
 */
object TeachersForTheClass {
    /**
     *  Map for subjects and teachers
     */
    private val subjectTeacher = HashMap<String, Teacher>()

    fun setMap(map: Map<String, Teacher>?) {
        map?.let {
            subjectTeacher.putAll(it)
        }
    }

    fun getInstance(): TeachersForTheClass {
        return this
    }

    fun clear() {
        subjectTeacher.clear()
    }

    fun getClassTeacher(): Teacher? {
        return subjectTeacher[CLASS_TEACHER]
    }

    // const
    private const val CLASS_TEACHER = "classTeacher"
}