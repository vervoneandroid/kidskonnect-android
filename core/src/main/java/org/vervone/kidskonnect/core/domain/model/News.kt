package org.vervone.kidskonnect.core.domain.model

class News {

    var date: Long? = null
    var description: String? = null
        set(value) {
            field = value?.trim()
        }

    var likes: Int? = null
    var newsType: Int? = null
    var senderId: String? = null
    var senderName: String? = null
    var webLink: String? = null
    var contentType: ContentType = ContentType.UNKNOWN

    // Note : Attachments path as array of string.
    var attachments: List<String> = ArrayList()

    //Note : Attachments path as key value pair.
    var images: HashMap<String, String>? = null

    var id: String? = null

    var targetList: List<String>? = null

    //var attachmentMgr : Atta
}