package org.vervone.kidskonnect.core.data.network.model.student

class FbAcademicReport {
    var date: Long? = null
    var reportName: String? = null
    var reportUrl: String? = null
}