package org.vervone.kidskonnect.core.domain.model

class Notification {
    var date: Long? = null
    var description: String? = null
    var title: String? = null
    var id: String? = null
}