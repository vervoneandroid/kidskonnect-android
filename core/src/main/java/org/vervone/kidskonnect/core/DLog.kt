package org.vervone.kidskonnect.core

import android.util.Log

object DLog {

    private const val TAG = "KidsKonnect"
    private const val FORCE_DEBUG = true /* STOPSHIP if true */
    private val DEBUG = FORCE_DEBUG || Log.isLoggable(TAG, Log.DEBUG)
    private val VERBOSE = FORCE_DEBUG || Log.isLoggable(TAG, Log.VERBOSE)
    private const val TAG_DELIMETER = " - "

    fun d(tag: String, msg: String) {
        if (DEBUG) {
            Log.d(TAG, delimit(tag) + msg)
        }
    }

    fun d(obj: Any, msg: String) {
        if (DEBUG) {
            Log.d(TAG, getPrefix(obj) + msg)
        }
    }

    fun d(obj: Any, str1: String, str2: Any) {
        if (DEBUG) {
            Log.d(TAG, getPrefix(obj) + str1 + str2)
        }
    }

    fun v(obj: Any, msg: String) {
        if (VERBOSE) {
            Log.v(TAG, getPrefix(obj) + msg)
        }
    }

    fun v(obj: Any, str1: String, str2: Any) {
        if (VERBOSE) {
            Log.d(TAG, getPrefix(obj) + str1 + str2)
        }
    }

    fun e(tag: String, msg: String, e: Exception) {
        Log.e(TAG, delimit(tag) + msg, e)
    }

    fun e(tag: String, msg: String) {
        Log.e(TAG, delimit(tag) + msg)
    }

    fun e(obj: Any, msg: String, e: Exception) {
        Log.e(TAG, getPrefix(obj) + msg, e)
    }

    fun e(obj: Any, msg: String) {
        Log.e(TAG, getPrefix(obj) + msg)
    }

    fun i(tag: String, msg: String) {
        Log.i(TAG, delimit(tag) + msg)
    }

    fun i(obj: Any, msg: String) {
        Log.i(TAG, getPrefix(obj) + msg)
    }

    fun w(obj: Any, msg: String) {
        Log.w(TAG, getPrefix(obj) + msg)
    }

    fun w(obj: Any, msg: String, e: java.lang.Exception?) {
        Log.w(TAG, getPrefix(obj) + msg, e)
    }

    fun wtf(obj: Any, msg: String) {
        Log.wtf(TAG, getPrefix(obj) + msg)
    }


    private fun getPrefix(obj: Any?): String {
        return if (obj == null) "" else obj.javaClass.simpleName + TAG_DELIMETER
    }

    private fun delimit(tag: String): String {
        return tag + TAG_DELIMETER
    }

}