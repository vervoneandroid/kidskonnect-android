package org.vervone.kidskonnect.core.data.network.model.student

class FbProfileDetails {
    var LocalGuardian: String? = null
    var LocalGuardianAddress: String? = null
    var ParentEmail: String? = null
    var ParentNumber: String? = null
    var localGuardianNumber: String? = null
    var parentAdderss: String? = null
    var parentName: String? = null
}