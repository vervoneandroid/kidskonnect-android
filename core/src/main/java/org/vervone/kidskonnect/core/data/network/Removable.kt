package org.vervone.kidskonnect.core.data.network

interface Removable {
    fun remove()
}