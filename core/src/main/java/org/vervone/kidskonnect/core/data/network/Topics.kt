package org.vervone.kidskonnect.core.data.network

import org.vervone.kidskonnect.core.data.network.model.FbChatMessage
import org.vervone.kidskonnect.core.data.network.model.FbMessageChannel
import org.vervone.kidskonnect.core.domain.model.ChatMessage

enum class Topics {
    CHAT_ROOMS
}

interface Callback {
    fun onDataChange()
}

interface MessageCallback {
    fun onChange(msgChannelId: String, msgCount: Int)
    fun onMessage(chatMessage: ChatMessage) {}
}


interface NotificationListener {
    fun onMessage(fbChatMessage: FbChatMessage, msgCount: Int, messageChannel: FbMessageChannel?)
    fun onSnapshot(messageChannel: FbMessageChannel?, msgCount: Int)
}