package org.vervone.kidskonnect.core.domain.mappers

import org.vervone.kidskonnect.core.Mapper
import org.vervone.kidskonnect.core.data.network.model.FbLeave
import org.vervone.kidskonnect.core.domain.model.Leave
import org.vervone.kidskonnect.core.domain.model.LeaveStatus

class LeavesMapper : Mapper<FbLeave, Leave> {

    override fun map(input: FbLeave): Leave {
        val leave = Leave()
        leave.schoolClass = input.sClass
        leave.leaveDays = input.leaveDays
        leave.leaveEndsAt = input.leaveEndsAt
        leave.leaveStartsFrom = input.leaveStartsFrom

        leave.leaveStatus = LeaveStatus.fromInt(input.leaveStatus)
        leave.noOfDays = input.noOfDays
        leave.reason = input.reason

        leave.replyString = input.replyString
        leave.sendDate = input.sendDate
        leave.studentId = input.studentId

        leave.studentName = input.studentName
        leave.teacherName = input.teacherName
        leave.id = input.id

        return leave
    }


    fun map(input: Leave?): FbLeave {
        val leave = FbLeave()
        leave.sClass = input?.schoolClass
        leave.leaveDays = input?.leaveDays
        leave.leaveEndsAt = input?.leaveEndsAt
        leave.leaveStartsFrom = input?.leaveStartsFrom

        leave.leaveStatus = input?.leaveStatus?.value
        leave.noOfDays = input?.noOfDays
        leave.reason = input?.reason

        leave.replyString = input?.replyString
        leave.sendDate = input?.sendDate
        leave.studentId = input?.studentId

        leave.studentName = input?.studentName
        leave.teacherName = input?.teacherName
        leave.id = input?.id

        return leave
    }
}