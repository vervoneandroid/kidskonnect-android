package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.GenericTypeIndicator
import com.google.firebase.database.ValueEventListener
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.model.STUDENTS
import org.vervone.kidskonnect.core.data.network.model.student.FbComments
import org.vervone.kidskonnect.core.data.network.model.student.FbStudent
import org.vervone.kidskonnect.core.helper.ThreadUtils
import java.lang.ref.WeakReference
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


/**
 *    Helper class to fetch student details and caching it for certain time .
 */
object StudentsFetch {

    private val studentsClassMap = HashMap<String, List<FbStudent>>()
    private var isPending = false

    private val pendingCallbacks = ArrayList<QueryRequest>()

    private val subscribers = Hashtable<String, DateChangeListener>()

    private var academicYr: String? = null

    private val allStudents = HashMap<String, FbStudent>()


    init {
        subscribeToChanges()
    }


    fun getStudents(academicYr: String,
                    schoolClass: String?,
                    query: String?,
                    callback: RequestCallback<List<FbStudent>, ApiError>
    ) {

        this.academicYr = academicYr
        if (isPending) {
            val request = QueryRequest(academicYr, schoolClass, query, callback)
            pendingCallbacks.add(request)

            return
        }

        if (studentsClassMap.isEmpty()) {
            DLog.v(this, "studentsClassMap isEmpty ... sync from backend")
            this.isPending = true
            ConnectionHelper.getDB()!!.getReference(STUDENTS)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(dbError: DatabaseError) {
                        isPending = false
                        callback.onError(ApiError(dbError.message))
                    }

                    override fun onDataChange(snapshot: DataSnapshot) {
                        ThreadUtils.runOnBg {
                            isPending = false
                            studentsClassMap.clear()
                            parseData(snapshot, academicYr)
                            executeQuery(schoolClass, query, callback)
                            executePendingRequest()
                        }
                    }
                })

        } else {
            ThreadUtils.runOnBg {
                DLog.v(this, "studentsClassMap is not empty")
                executeQuery(schoolClass, query, callback)
            }
        }
    }

    private fun subscribeToChanges() {
        ConnectionHelper.getDB()!!.getReference(STUDENTS).addValueEventListener(object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {}
            override fun onDataChange(snapshot: DataSnapshot) {
                if (academicYr != null) {
                    DLog.v(this, "Student data changed")
                    ThreadUtils.runOnBg {
                        studentsClassMap.clear()

                        parseData(snapshot, academicYr!!)
                        subscribers.values.forEach {
                            DLog.v(this, "SUBSCRIBER ${it}")
                            it?.onChange()
                        }
                    }
                }
            }
        })
    }

    private fun executePendingRequest() {
        val iterator = pendingCallbacks.iterator()
        while (iterator.hasNext()) {
            val request = iterator.next()
            if (request.weakCallback?.get() != null) {
                executeQuery(request.schoolClass, request.query, request.weakCallback.get()!!)
            }
            iterator.remove()
        }
    }

    @Synchronized
    private fun parseData(snapshot: DataSnapshot, academicYr: String) {
        val typeIndicator = object : GenericTypeIndicator<HashMap<String, FbStudent>>() {}
        val studentMap = snapshot.getValue(typeIndicator)
        allStudents.clear()
        if (studentMap != null) {
            this.allStudents.putAll(studentMap)
        }
        // Group Students by class.
        groupStudentsByClass(academicYr, studentMap)

    }

    private fun executeQuery(
            schoolClass: String?,
            query: String?,
            callback: RequestCallback<List<FbStudent>, ApiError>
    ) {
        if (schoolClass != null && query != null) {
            //getStudentSynced(academicYr!!, schoolClass, callback)
            val students = getStudents(query, schoolClass)
            callback.onSuccess(ArrayList(students ?: emptyList()))
        } else if (schoolClass == null && query != null) {
            val students = getStudents(query)
            callback.onSuccess(ArrayList(students ?: emptyList()))
        } else if (query == null && schoolClass != null) {
            val students = getStudentsForClass(schoolClass)
            callback.onSuccess(ArrayList(students ?: emptyList()))
        } else {
            val students = getAllStudents()
            callback.onSuccess(ArrayList(students ?: emptyList()))
        }
    }

    val lock = Object()

    private fun groupStudentsByClass(
            academicYr: String,
            studentMap: HashMap<String, FbStudent>?
    ) {
        DLog.v(this, "groupStudentsByClass called")
        studentsClassMap.clear()

        if (studentMap != null) {
            // group by class and students.
            val grouped = studentMap.asSequence()
                .groupBy(
                        { getClass(it.value, academicYr) },
                        { map(it.key, it.value) })
                // filter empty keys
                .filter { it.key.isNotEmpty() }


            val students = Students(academicYr, grouped.keys.toList(), object : Students.Callback {
                override fun onSync() {
                    synchronized(lock) {
                        lock.notifyAll()
                    }
                }
            })

            students.execute()
            synchronized(lock) {
                lock.wait()
            }

            val syncedMap = students.syncedStudents
            grouped.entries.forEach { e ->
                val studentIds = syncedMap[e.key]
                if (studentIds != null) {
                    val studentList = e.value
                    val filtered = studentList.filter { s -> studentIds.contains(s.id) }.sortedBy { it.id }
                    studentsClassMap[e.key] = filtered
                }
            }
        }

    }

    private fun getClass(student: FbStudent, academicYr: String): String {
        return student.getClass(academicYr) ?: ""
    }

    private fun map(id: String, student: FbStudent): FbStudent {
        student.id = id
        return student
    }


    private fun getStudentsForClass(schoolClass: String): List<FbStudent>? {
        return studentsClassMap[schoolClass]
    }


    private fun getAllStudents(): List<FbStudent>? {
        val queryResult = ArrayList<FbStudent>()
        studentsClassMap.keys.forEach {
            val filtered = studentsClassMap[it]
            if (filtered != null) {
                queryResult.addAll(filtered)
            }
        }
        return queryResult
    }

    private fun getStudents(query: String): List<FbStudent>? {
        val queryResult = ArrayList<FbStudent>()
        studentsClassMap.keys.forEach {
            val filtered = getStudents(query, it)
            if (filtered != null) {
                queryResult.addAll(filtered)
            }
        }
        return queryResult
    }

    private fun getStudents(query: String, schoolClass: String): List<FbStudent>? {
        val list = studentsClassMap[schoolClass]
        return list?.filter { st -> st.studentName!!.contains(query, true) }
    }


    class QueryRequest(
            val academicYr: String,
            val schoolClass: String?,
            val query: String?,
            callback: RequestCallback<List<FbStudent>, ApiError>
    ) {
        val weakCallback: WeakReference<RequestCallback<List<FbStudent>, ApiError>>?

        init {
            weakCallback = WeakReference(callback)
        }
    }

    private fun getUUID(): String {
        return UUID.randomUUID().toString()
    }

    fun subscribe(dataListener: DateChangeListener): String {
        val uid = getUUID()
        subscribers.put(uid, dataListener)
        return uid
    }

    fun unsubscribe(uid: String) {
        try {
            subscribers.remove(uid)
        } catch (ex: Exception) {
            DLog.e(this, "exception in unsubscribeAll", ex)
        }
    }

    fun getComments(studentId: String,
                    commentType: Int): ArrayList<FbComments> {
        val student = this.allStudents[studentId]
        if (student != null) {
            return filter(student, commentType)
        }
        return ArrayList()
    }


    private fun filter(student: FbStudent, commentType: Int): ArrayList<FbComments> {
        val comments = ArrayList<FbComments>()

        val filtered = student.comments?.filter { comment ->
            comment.commentType == commentType
        }

        if (filtered != null) {
            comments.addAll(filtered)
        }

        return comments
    }


    interface DateChangeListener {
        fun onChange()
    }

}