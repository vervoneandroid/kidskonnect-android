package org.vervone.kidskonnect.core.domain.mappers

import org.vervone.kidskonnect.core.Mapper
import org.vervone.kidskonnect.core.data.network.model.FbSchoolContact
import org.vervone.kidskonnect.core.domain.model.SchoolContact

class SchoolContactMapper : Mapper<FbSchoolContact, SchoolContact> {
    override fun map(input: FbSchoolContact): SchoolContact {
        val schoolContact = SchoolContact()
        schoolContact.address = input.address
        schoolContact.contactNo = input.contactNo
        schoolContact.email = input.email
        schoolContact.place = input.place
        schoolContact.schoolName = input.schoolName
        return schoolContact
    }
}