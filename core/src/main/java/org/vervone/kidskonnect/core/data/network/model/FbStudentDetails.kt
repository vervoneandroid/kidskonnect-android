package org.vervone.kidskonnect.core.data.network.model

data class FbStudentDetails(
        var leavesList: HashMap<String, Boolean>? = null,
        var likedNews: HashMap<String, Boolean>? = null,
        var rollNumber: Int? = null,
        var studentName: String? = null,
        var userChatDict: HashMap<String, Boolean>? = null
)