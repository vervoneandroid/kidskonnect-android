package org.vervone.kidskonnect.core.data.network.model

import org.vervone.kidskonnect.core.data.network.model.academicyear.FbAcademicYrTeacher

data class FbTeacherDetails(
    val teacher: FbTeacher?,
    val academicYrTeacher: FbAcademicYrTeacher?
)