package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.*
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.model.FbNews

class StudentsNewsFetcherHelper(
        val academicYr: String,
        val schoolClass: String,
        val callback: RequestCallback<List<FbNews>, Error>) {



    fun fetch() {
        ConnectionHelper.getDB()!!.getReference("$academicYr/class/$schoolClass/newsFeeds")
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnap: DataSnapshot) {
                    val map = dataSnap.getValue(MAP_STRING_BOOLEAN)
                    if (map?.keys != null) {
                        getNews(map.keys.toList())
                    }
                }

                override fun onCancelled(dbError: DatabaseError) {
                    callback.onError(Error(dbError.message))
                }
            })
    }

    private fun getNews(newsIds: List<String>) {
        val getNewsReq = object : GetNews(academicYr, ArrayList(newsIds)) {
            override fun onResult(news: ArrayList<FbNews>) {
                callback.onSuccess(news)
            }
        }

        getNewsReq.execute()
    }

    companion object {
        private val MAP_STRING_BOOLEAN = object : GenericTypeIndicator<HashMap<String, Boolean>>() {}
    }

}