package org.vervone.kidskonnect.core.domain

import com.google.firebase.database.DatabaseError
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.ApiError
import org.vervone.kidskonnect.core.data.network.ApiHelper
import org.vervone.kidskonnect.core.data.network.AppVersionSubsribers
import org.vervone.kidskonnect.core.data.network.model.FbAppVersion
import org.vervone.kidskonnect.core.domain.DomainConsts.KEY_CUR_ACADEMIC_YR
import org.vervone.kidskonnect.core.domain.mappers.CommonMapper
import org.vervone.kidskonnect.core.domain.model.AppVersion
import org.vervone.kidskonnect.core.domain.model.ConfigData
import org.vervone.kidskonnect.core.helper.AppHelper
import org.vervone.kidskonnect.core.helper.PreferenceHelper

class ConfigFacadeImpl : ConfigFacade {
    private val apiHelper = ApiHelper.Factory.apiHelper

    override fun syncAcademicYear(callback: RequestCallback<ConfigData, Error>) {
        apiHelper.getCurrentAcademicYear(object : RequestCallback<String, DatabaseError> {
            override fun onSuccess(res: String?) {
                val academicYear = PreferenceHelper.getString(KEY_CUR_ACADEMIC_YR)
                val isChanged = (academicYear == null || !academicYear.equals(res, false))
                val configData = ConfigData(res!!, isChanged)
                PreferenceHelper.save(res, KEY_CUR_ACADEMIC_YR)
                callback.onSuccess(configData)
            }

            override fun onError(error: DatabaseError?) {
                callback.onError(Error(error?.toException()))
            }

        })
    }

    override fun requiredAppVersions(callback: RequestCallback<AppVersion, DomainError>) {
        apiHelper.getRequiredAppVersions(object : RequestCallback<FbAppVersion, ApiError> {
            override fun onSuccess(res: FbAppVersion?) {
                if (res != null) {
                    val appVersion = CommonMapper.map(res)
                    checkVersionCode(appVersion)
                    callback.onSuccess(appVersion)
                }
            }

            override fun onError(error: ApiError?) {
                callback.onError(DomainError(error!!))
            }
        })
    }

    private fun checkVersionCode(appVersion: AppVersion) {
        if (appVersion.appMinimumVersion != null) {
            val minimum = Integer.parseInt(appVersion.appMinimumVersion!!)
            if (AppHelper.getAppVersion() < minimum) {
                appVersion.rejected = true
            }
        }
    }


    override fun subscribeAppVersionConfig(versionListener: ConfigFacade.VersionListener) {
        apiHelper.subscribeForAppVersion(object : AppVersionSubsribers.VersionSubscriber {
            override fun onChange(fbAppVersion: FbAppVersion) {
                val appVersion = CommonMapper.map(fbAppVersion)
                checkVersionCode(appVersion)
                versionListener.onVersionChange(appVersion)
            }
        })
    }
}