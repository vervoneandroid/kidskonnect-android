package org.vervone.kidskonnect.core.domain.mappers

import org.vervone.kidskonnect.core.data.network.model.StudentParams
import org.vervone.kidskonnect.core.data.network.model.TeacherParams
import org.vervone.kidskonnect.core.domain.model.StudentParamsBm
import org.vervone.kidskonnect.core.domain.model.TeacherParamsBm

class ParamsMapper {


    fun map(paramsBm: StudentParamsBm): StudentParams {
        return StudentParams(paramsBm.academicYr,
                             paramsBm.className,
                             paramsBm.studentId)
    }

    fun map(paramsBm: TeacherParamsBm): TeacherParams {
        return TeacherParams(paramsBm.academicYr,
                             paramsBm.isAdmin,
                             paramsBm.classes,
                             paramsBm.teacherId)
    }

}