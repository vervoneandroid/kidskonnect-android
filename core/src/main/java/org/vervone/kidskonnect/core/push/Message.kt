package org.vervone.kidskonnect.core.push

import com.google.gson.annotations.SerializedName
import java.util.*


/**
 * Represents a message that can be sent via  FCM. Contains payload
 * information as well as the recipient information. The recipient information must contain exactly
 * one token, to or condition parameter. Instances of this class are thread-safe and immutable.
 * Use [Message.Builder] to create new instances.
 */
class Message private constructor(builder: Builder) {

    /*

     {
  "to": "/topics/topic_name",
  "data": {
    "title": "Notification title",
    "message": "Notification message",
    "key1" : "value1",
    "key2" : "value2" //additional data you want to pass
   }
}

     */

    @SerializedName("data")
    internal val data: Map<String, String>?

    @SerializedName("token")
    internal val token: String?

    @SerializedName("to")
    internal val to: String?

    @SerializedName("condition")
    internal val condition: String?

    @SerializedName("notification")
    internal val notification: Notification?


    init {
        this.data = if (builder.data.isEmpty()) null else HashMap(builder.data)
        this.token = builder.token
        if (builder.topic != null) {
            this.to = TOPIC_PREFIX + builder.topic
        } else {
            this.to = null
        }
        this.condition = builder.condition
        this.notification = builder.notification

    }


    class Builder constructor() {

        val data = HashMap<String, String>()
        var token: String? = null
        var topic: String? = null
        var condition: String? = null
        var notification: Notification? = null


        /**
         * Sets the notification information to be included in the message.
         *
         * @param notification A [Notification] instance.
         * @return This builder.
         */
        fun setNotification(notification: Notification): Builder {
            this.notification = notification
            return this
        }

        /**
         * Sets the registration token of the device to which the message should be sent.
         *
         * @param token A valid device registration token.
         * @return This builder.
         */
        fun setToken(token: String): Builder {
            this.token = token
            return this
        }

        /**
         * Sets the name of the FCM to to which the message should be sent. Topic names may
         * contain the `/topics/` prefix.
         *
         * @param topic A valid to name.
         * @return This builder.
         */
        fun setTopic(topic: String): Builder {
            this.topic = topic
            return this
        }

        /**
         * Sets the FCM condition to which the message should be sent.
         *
         * @param condition A valid condition string (e.g. `"'foo' in topics"`).
         * @return This builder.
         */
        fun setCondition(condition: String): Builder {
            this.condition = condition
            return this
        }

        /**
         * Adds the given key-value pair to the message as a data field. Key or the value may not be
         * null.
         *
         * @param key   Name of the data field. Must not be null.
         * @param value Value of the data field. Must not be null.
         * @return This builder.
         */
        fun putData(key: String, value: String): Builder {
            this.data[key] = value
            return this
        }

        /**
         * Adds all the key-value pairs in the given map to the message as data fields. None of the
         * keys or values may be null.
         *
         * @param map A non-null map of data fields. Map must not contain null keys or values.
         * @return This builder.
         */
        fun putAllData(map: Map<String, String>): Builder {
            this.data.putAll(map)
            return this
        }

        /**
         * Creates a new [Message] instance from the parameters set on this builder.
         *
         * @return A new [Message] instance.
         * @throws IllegalArgumentException If any of the parameters set on the builder are invalid.
         */
        fun build(): Message {
            return Message(this)
        }
    }

    companion object {


        /**
         * Creates a new [Message.Builder].
         *
         * @return A [Message.Builder] instance.
         */
        fun builder(): Builder {
            return Builder()
        }

        private const val TOPIC_PREFIX = PushApiConst.TOPIC_PREFIX
    }
}