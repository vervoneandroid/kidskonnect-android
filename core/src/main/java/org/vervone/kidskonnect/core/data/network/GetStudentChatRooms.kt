package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.GenericTypeIndicator
import com.google.firebase.database.ValueEventListener
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.model.CLASS
import org.vervone.kidskonnect.core.data.network.model.FbMessageChannel
import org.vervone.kidskonnect.core.data.network.model.STUDENTS
import org.vervone.kidskonnect.core.data.network.model.USER_CHAT_DICT
import java.util.concurrent.atomic.AtomicInteger

class GetStudentChatRooms(val academicYr: String,
                          val userUid: String,
                          val className: String,
                          val callback: RequestCallback<HashMap<String, FbMessageChannel>, ApiError>?) {

    private val chatRoomIds = mutableListOf<String>()
    private val reqCount = AtomicInteger(2)

    fun execute() {
        getChatRoomsForStudent()

        object : GetClassChatRooms(listOf(className), academicYr) {
            override fun onSuccess(chatRooms: HashMap<String, Boolean>) {
                chatRoomIds.addAll(chatRooms.keys)
                if (reqCount.decrementAndGet() == 0) {
                    fetch()
                }
            }
        }
    }

    private fun getChatRoomsForStudent() {
        ConnectionHelper.getDB()!!.reference
            .child(academicYr)
            .child(CLASS)
            .child(className)
            .child(STUDENTS)
            .child(userUid)
            .child(USER_CHAT_DICT)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {
                    if (reqCount.decrementAndGet() == 0) {
                        callback?.onError(ApiError(dbError.message))
                    }
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    val typeIndicator = object : GenericTypeIndicator<HashMap<String, Boolean>>() {}
                    val allChatRooms = snapshot.getValue(typeIndicator)

                    if (allChatRooms != null) {
                        chatRoomIds.addAll(allChatRooms.keys)
                    }

                    if (reqCount.decrementAndGet() == 0) {
                        fetch()
                    } else {
                        //callback?.onSuccess(hashMapOf())
                    }
                }

            })
    }

    private fun fetch() {
        object : GetChatRooms(academicYr, chatRoomIds) {
            override fun onError(apiError: ApiError) {
                callback?.onError(apiError)
            }

            override fun onResult(result: HashMap<String, FbMessageChannel>) {
                callback?.onSuccess(result)
            }
        }
    }

}