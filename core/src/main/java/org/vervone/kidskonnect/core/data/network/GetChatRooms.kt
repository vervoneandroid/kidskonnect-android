package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.*
import org.vervone.kidskonnect.core.data.network.model.ALL_MESSAGE_CHANNELS
import org.vervone.kidskonnect.core.data.network.model.FbMessageChannel

/**
 *   Used to Get all chat rooms
 */
abstract class GetChatRooms(val academicYr: String,
                            val chatRoomIds: List<String>) {


    private val db = ConnectionHelper.getDB()

    abstract fun onResult(result: HashMap<String, FbMessageChannel>)
    abstract fun onError(apiError: ApiError)

    init {
        fetch()
    }

    private fun fetch() {
        db!!.reference.child(academicYr)
            .child(ALL_MESSAGE_CHANNELS)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val typeIndicator = object : GenericTypeIndicator<HashMap<String, FbMessageChannel>>() {}
                    val allChatRooms = snapshot.getValue(typeIndicator)

                    val filtered = allChatRooms?.filter { entry ->
                        chatRoomIds.contains(entry.key)
                    } ?: hashMapOf()

                    //DLog.v(this, "#### $filtered")
                    onResult(HashMap(filtered))
                }

                override fun onCancelled(dbError: DatabaseError) {
                    onError(ApiError(dbError.message))
                }
            })
    }

}