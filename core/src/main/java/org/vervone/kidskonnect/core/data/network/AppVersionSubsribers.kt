package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import org.vervone.kidskonnect.core.data.network.model.APP_VERSION
import org.vervone.kidskonnect.core.data.network.model.FbAppVersion
import java.util.concurrent.atomic.AtomicBoolean

object AppVersionSubsribers {

    private val subscribers = ArrayList<VersionSubscriber>()

    private val isFirst = AtomicBoolean(true)

    private fun subscribe() {
        ConnectionHelper.getDB()!!.reference
            .child(APP_VERSION)
            .addValueEventListener(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {}
                override fun onDataChange(snapShot: DataSnapshot) {
                    val appVersion = snapShot.getValue(FbAppVersion::class.java)
                    notifyCallback(appVersion)
                }
            })
    }

    private fun notifyCallback(fbAppVersion: FbAppVersion?) {
        if (fbAppVersion != null) {
            subscribers.forEach { it.onChange(fbAppVersion) }
        }
    }


    fun subscribe(subscriber: VersionSubscriber) {
        subscribers.add(subscriber)
        if (isFirst.getAndSet(false)) {
            subscribe()
        }
    }


    interface VersionSubscriber {
        fun onChange(fbAppVersion: FbAppVersion)
    }

}