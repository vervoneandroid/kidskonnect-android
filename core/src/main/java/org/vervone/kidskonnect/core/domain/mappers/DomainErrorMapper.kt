package org.vervone.kidskonnect.core.domain.mappers

import org.vervone.kidskonnect.core.Mapper
import org.vervone.kidskonnect.core.data.network.ApiError
import org.vervone.kidskonnect.core.domain.DomainError

class DomainErrorMapper : Mapper<ApiError, DomainError> {

    override fun map(input: ApiError): DomainError {
        if (input != null) {
            val domainError = DomainError(input.message)
            return domainError
        }

        return DomainError("Unknown Error")
    }

}