package org.vervone.kidskonnect.core.domain.model

data class AppVersion(var appCurrentVersion: String?,
                      var appMinimumVersion: String?,
                      var rejected: Boolean)