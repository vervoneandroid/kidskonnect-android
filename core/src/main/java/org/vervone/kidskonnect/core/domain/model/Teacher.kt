package org.vervone.kidskonnect.core.domain.model

import java.io.Serializable

class Teacher : Serializable {

    var qualification: String? = null
    var teacherName: String? = null
    var profileImageUrl: String? = null

    var subjects: List<String>? = null

    var ProfileDetails: Profile? = null


    // Academic Year details 
    var classesAndSubjects: HashMap<String, ArrayList<String>>? = null
    var isClassTeacher: String? = null
    var leavesList: HashMap<String, Boolean>? = null
    var likedNews: HashMap<String, Boolean>? = null
    var teacherDesignationTag: Int? = null
    var userChatDict: HashMap<String, Boolean>? = null

    var designation: Designation = Designation.UNKNOWN
        get() = Designation.fromTag(teacherDesignationTag)

    var teacherId: String? = null

}