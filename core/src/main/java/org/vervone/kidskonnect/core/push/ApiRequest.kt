package org.vervone.kidskonnect.core.push

import com.android.volley.NetworkResponse
import com.android.volley.ParseError
import com.android.volley.Request.Method.POST
import com.android.volley.Response
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonRequest
import org.json.JSONException
import org.vervone.kidskonnect.core.helper.JsonHelper
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset

class ApiRequest(url: String,
                 message: Message?,
                 listener: Response.Listener<String>?,
                 errorListener: Response.ErrorListener?) :
    JsonRequest<String>(POST, url, if (message == null) null else JsonHelper.toJson(message), listener, errorListener) {


    override fun parseNetworkResponse(response: NetworkResponse?): Response<String> {
        return try {
            val charSet = HttpHeaderParser.parseCharset(response!!.headers, PROTOCOL_CHARSET)!!
            val jsonString = String(response.data, Charset.forName(charSet))
            Response.success(jsonString, HttpHeaderParser.parseCacheHeaders(response))
        } catch (e: UnsupportedEncodingException) {
            Response.error(ParseError(e))
        } catch (je: JSONException) {
            Response.error(ParseError(je))
        } catch (ex: Exception) {
            Response.error(ParseError(ex))
        }
    }

    override fun getHeaders(): Map<String, String> {
        val params = HashMap<String, String>()
        params[FcmPush.HEADER_AUTH] = FcmPush.SERVER_KEY
        params[FcmPush.HEADER_CONTENT_TYPE] = FcmPush.CONTENT_TYPE
        return params
    }
}