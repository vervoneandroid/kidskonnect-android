package org.vervone.kidskonnect.core.domain.model.school

data class SchoolConfig(val name: String = "",
                        val address: String = "",
                        val district: String = "",
                        val id: String = "",
                        var projectId: String = "",
                        var dbUrl: String = "",
                        var applicationId: String = "",
                        var apiKey: String = "")
