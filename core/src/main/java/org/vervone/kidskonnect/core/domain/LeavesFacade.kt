package org.vervone.kidskonnect.core.domain

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.DataChangeListener
import org.vervone.kidskonnect.core.domain.model.Leave
import org.vervone.kidskonnect.core.domain.model.StudentParamsBm
import org.vervone.kidskonnect.core.domain.model.TeacherParamsBm

interface LeavesFacade {

    /**
     *  To get all leave requests , requested by the student to the teacher [userId].
     */
    fun getAllLeavesRequestToTeacher(academicYr: String,
                                     userId: String?,
                                     callback: RequestCallback<List<Leave>, DomainError>)

    /**
     *  To get all leaves requested by the student [userId] during the [academicYr]
     */
    fun getAllLeaveRequestsByStudent(academicYr: String,
                                     schoolClass: String,
                                     userId: String?,
                                     callback: RequestCallback<List<Leave>, DomainError>)

    fun subscribeAllLeaveRequestsByStudent(academicYr: String,
                                           schoolClass: String,
                                           userId: String?,
                                           changeListener: DataChangeListener): String

    fun unsubscribeAllLeaveRequestsByStudent(subscriberId: String)


    fun subscribeAllLeavesRequestToTeacher(academicYr: String,
                                           userId: String?,
                                           changeListener: DataChangeListener): String

    fun unsubscribeAllLeavesRequestToTeacher(subscriberId: String)
    /**
     *  Approve leave request
     */
    fun approveLeaveRequest(academicYr: String,
                            leave: Leave,
                            callback: RequestCallback<Leave, DomainError>)

    /**
     *  Reject leave request.
     */
    fun rejectLeaveRequest(academicYr: String,
                           leave: Leave,
                           callback: RequestCallback<Leave, DomainError>)

    fun subscribeLeaveRequests(teacherParamsBm: TeacherParamsBm, observer: LeaveRequestListener)

    fun subscribeLeaveStatusChange(studentParamsBm: StudentParamsBm, observer: LeaveStatusChange)

    interface LeaveRequestListener {
        fun onSnapshot(count: Int)
        fun onRequest(count: Int, leave: Leave)
    }

    interface LeaveStatusChange {
        fun onChange(leave: Leave)
    }

}