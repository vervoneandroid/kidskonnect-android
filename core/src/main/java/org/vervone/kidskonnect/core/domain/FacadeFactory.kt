package org.vervone.kidskonnect.core.domain

import android.content.Context
import org.vervone.kidskonnect.core.cloudstorage.CloudStorage
import org.vervone.kidskonnect.core.cloudstorage.aws.AwsCloudStorage

object FacadeFactory {
    val authFacade: AuthFacade = AuthFacadeImpl()
    val configFacade: ConfigFacade = ConfigFacadeImpl()
    val newsFacade: NewsFacade = NewsFacadeImpl()
    val schoolFacade: SchoolFacade = SchoolFacadeImpl()
    val messageFacade: MessageFacade = MessageFacadeImpl()
    val examsFacade: ExamsFacade = ExamsFacadeImpl()
    val leavesFacade: LeavesFacade = LeavesFacadeImpl()
    val notificationFacade: NotificationFacade = NotificationFacadeImpl()

    fun createCloudStorage(context: Context): CloudStorage {
        return AwsCloudStorage(context)
    }
}