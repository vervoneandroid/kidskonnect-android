package org.vervone.kidskonnect.core.domain.mappers

import com.google.firebase.auth.FirebaseUser
import org.vervone.kidskonnect.core.Mapper
import org.vervone.kidskonnect.core.domain.model.User

class UserMapper : Mapper<FirebaseUser?, User> {

    override fun map(input: FirebaseUser?): User {
        return User(input?.displayName, input?.photoUrl?.toString() , input?.uid)
    }
}