package org.vervone.kidskonnect.core

interface Mapper<IN, OUT> {
    fun map(input: IN): OUT
}