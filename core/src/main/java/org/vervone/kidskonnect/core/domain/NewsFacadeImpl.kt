package org.vervone.kidskonnect.core.domain

import com.google.firebase.database.DatabaseError
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.cloudstorage.CloudStorage
import org.vervone.kidskonnect.core.data.network.ApiHelper
import org.vervone.kidskonnect.core.data.network.AttachmentManager
import org.vervone.kidskonnect.core.data.network.NewsNotifications
import org.vervone.kidskonnect.core.data.network.model.FbNews
import org.vervone.kidskonnect.core.domain.mappers.CommonMapper
import org.vervone.kidskonnect.core.domain.mappers.NewsMapper
import org.vervone.kidskonnect.core.domain.mappers.ParamsMapper
import org.vervone.kidskonnect.core.domain.model.News
import org.vervone.kidskonnect.core.domain.model.StudentParamsBm
import org.vervone.kidskonnect.core.domain.model.TeacherParamsBm
import org.vervone.kidskonnect.core.helper.AppHelper
import org.vervone.kidskonnect.core.push.PushApi
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicInteger

class NewsFacadeImpl : NewsFacade {

    private val apiHelper = ApiHelper.Factory.apiHelper
    private val bgExecutor = Executors.newFixedThreadPool(3)

    override fun getNews(academicYr: String, teacherUid: String, callback: RequestCallback<List<News>, Error>) {
        bgExecutor.execute {
            apiHelper.getNews(academicYr, teacherUid, false, object : RequestCallback<List<FbNews>, DatabaseError> {
                override fun onError(error: DatabaseError?) {
                    val newsErr = Error(error?.message)
                    callback.onError(newsErr)
                }

                override fun onSuccess(res: List<FbNews>?) {
                    val news = res?.map { NewsMapper().map(it) }
                    callback.onSuccess(news)
                }
            })
        }
    }

    override fun getNewsForAllClasses(academicYr: String,
                                      teacherUid: String,
                                      callback: RequestCallback<List<News>, Error>) {
        bgExecutor.execute {
            apiHelper.getNews(academicYr, teacherUid, true, object : RequestCallback<List<FbNews>, DatabaseError> {
                override fun onError(error: DatabaseError?) {
                    val newsErr = Error(error?.message)
                    callback.onError(newsErr)
                }

                override fun onSuccess(res: List<FbNews>?) {
                    val news = res?.map { NewsMapper().map(it) }
                    callback.onSuccess(news)
                }
            })
        }
    }

    override fun postNews(
            academicYr: String,
            news: News,
            cloudStorage: CloudStorage,
            callback: RequestCallback<Void, Error>
    ) {
        bgExecutor.execute {

            val fbNews = CommonMapper.map(news)
            val attachmentMgr = AttachmentManager(news.attachments, academicYr, news.id)
            fbNews.images = attachmentMgr.asMap()

            apiHelper.postNews(academicYr, fbNews, object : RequestCallback<Void, Error> {
                override fun onSuccess(res: Void?) {

                    if (attachmentMgr.attachmentList != null && attachmentMgr.attachmentList!!.isNotEmpty()) {
                        uploadToCloud(cloudStorage, attachmentMgr, news, callback)
                    } else {
                        callback.onSuccess(null)
                        if (news.targetList != null && news.senderName != null) {
                            PushApi.Factory.pushApi.publishNewsCreated(news.senderName!!,
                                                                       news.targetList!!)
                        }
                    }
                }

                override fun onError(error: Error?) {
                    callback.onError(error)
                }
            })
        }
    }

    private fun uploadToCloud(
            cloudStorage: CloudStorage?,
            mgr: AttachmentManager,
            news: News,
            callback: RequestCallback<Void, Error>
    ) {
        bgExecutor.execute {
            UploadHelper(cloudStorage, mgr, news, callback).execute()
        }
    }

    override fun generateNewsId(academicYr: String): String {
        return apiHelper.generateNewsId(academicYr)
    }


    class UploadHelper(
            val cloudStorage: CloudStorage?,
            val mgr: AttachmentManager,
            val news: News,
            val callback: RequestCallback<Void, Error>
    ) : CloudStorage.UploadListener {


        val noOfFiles = AtomicInteger(0)

        init {

            for (attachment in mgr.attachmentList!!) {
                var multiplier = 1
                if (attachment.isVideo) {
                    // If we are uploading video, we should also upload a thumbnail image so
                    // we should count video attachment as 2
                    multiplier = 2
                }
                val cnt = noOfFiles.addAndGet(multiplier)
            }
            DLog.v(this, "noOfFiles to upload $noOfFiles")
        }


        fun execute() {
            for (attachment in mgr.attachmentList!!) {
                cloudStorage?.uploadPublic(mgr.buildKey(attachment), attachment.fileToUpload!!, this)
                if (attachment.isVideo) {
                    cloudStorage?.uploadPublic(mgr.buildKeyForThumbnail(attachment), attachment.thumbnailFile!!, this)
                }
            }
        }

        override fun onFailed() {
            if (noOfFiles.decrementAndGet() == 0) {
                DLog.v(this, "onFailed noOfFiles $noOfFiles")
                callback.onError(Error())
            }
        }

        override fun onComplete() {
            if (noOfFiles.decrementAndGet() == 0) {
                DLog.v(this, "onComplete noOfFiles $noOfFiles")
                callback.onSuccess(null)

                if (news.targetList != null && news.senderName != null) {
                    PushApi.Factory.pushApi.publishNewsCreated(news.senderName!!,
                                                               news.targetList!!)
                }
            }
        }
    }

    override fun deleteNews(academicYr: String, newsId: String, callback: RequestCallback<String, Error>) {
        apiHelper.deleteNews(academicYr, newsId, AppHelper.getCloudStorage(), object : RequestCallback<String, Error> {
            override fun onError(error: Error?) {
                callback.onError(error)
            }

            override fun onSuccess(res: String?) {
                callback.onSuccess(res)
            }
        })
    }

    override fun updateNews(
            academicYr: String, newsId: String, user: String,
            isLike: Boolean, callback: RequestCallback<String, Error>
    ) {
        apiHelper.updateLikes(academicYr, newsId, user, isLike, object : RequestCallback<String, Error> {
            override fun onError(error: Error?) {
                callback.onError(error)
            }

            override fun onSuccess(res: String?) {
                callback.onSuccess(res)
            }
        })
    }

    override fun isLikedNews(newsId: String): Boolean {
        return apiHelper.isLikedNews(newsId)
    }

    override fun getLikedNews(academicYr: String, user: String) {
        apiHelper.getLikedNews(academicYr, user)
    }

    override fun getStudentNews(
            academicYr: String,
            className: String,
            callback: RequestCallback<List<News>, Error>
    ) {
        apiHelper.getStudentNews(
                academicYr, className, object : RequestCallback<List<FbNews>, Error> {
            override fun onSuccess(res: List<FbNews>?) {
                val news = res?.map { NewsMapper().map(it) }
                callback.onSuccess(news)
            }

            override fun onError(error: Error?) {
                val newsErr = Error(error?.message)
                callback.onError(newsErr)
            }

        })
    }

    override fun getLikedNewsStudent(
            academicYr: String,
            user: String, className: String
    ) {
        apiHelper.getStudentLikedNews(academicYr, user, className)
    }

    override fun updateStudentNews(
            academicYr: String,
            newsId: String,
            user: String,
            className: String,
            isLike: Boolean,
            callback: RequestCallback<String, Error>
    ) {
        apiHelper.updateStudentNews(academicYr, newsId, user, className, isLike, callback)
    }

    override fun subscribeForNewsNotification(teacherParamsBm: TeacherParamsBm,
                                              listener: NewsFacade.NewsObserver) {
        val params = ParamsMapper().map(teacherParamsBm)
        NewsNotifications.subscribe(params, object : NewsNotifications.NewsListener {
            override fun onNewsAdded(count: Int, news: FbNews) {
                listener.onAdded(count , NewsMapper().map(news))
            }

            override fun onSnapshot(count: Int) {
                DLog.v(this, "subscribeForNewsNotification teacher $count")
                listener.onSnapshot(count)
            }
        })
    }

    override fun subscribeForNewsNotification(studentParamsBm: StudentParamsBm,
                                              listener: NewsFacade.NewsObserver) {
        val params = ParamsMapper().map(studentParamsBm)
        NewsNotifications.subscribe(params, object : NewsNotifications.NewsListener {
            override fun onNewsAdded(count: Int, news: FbNews) {
                listener.onAdded(count , NewsMapper().map(news))
            }

            override fun onSnapshot(count: Int) {
                DLog.v(this, "subscribeForNewsNotification student $count")
                listener.onSnapshot(count)
            }
        })
    }
}