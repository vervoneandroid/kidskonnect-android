package org.vervone.kidskonnect.core.domain.model

data class StudentParamsBm(val academicYr: String,
                           val className: String,
                           val studentId: String)

data class TeacherParamsBm(val academicYr: String,
                           val isAdmin: Boolean,
                           val classes: List<String>,
                           val teacherId: String)