package org.vervone.kidskonnect.core.domain

import com.google.firebase.database.DatabaseError
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.*
import org.vervone.kidskonnect.core.data.network.model.FbLeave
import org.vervone.kidskonnect.core.data.network.model.FbSchoolContact
import org.vervone.kidskonnect.core.data.network.model.FbTeacherDetails
import org.vervone.kidskonnect.core.data.network.model.student.FbAcademicReport
import org.vervone.kidskonnect.core.data.network.model.student.FbComments
import org.vervone.kidskonnect.core.data.network.model.student.FbStudent
import org.vervone.kidskonnect.core.domain.mappers.*
import org.vervone.kidskonnect.core.domain.model.*
import org.vervone.kidskonnect.core.domain.model.school.SchoolConfig
import org.vervone.kidskonnect.core.domain.model.student.AcademicReport
import org.vervone.kidskonnect.core.domain.model.student.Comment
import org.vervone.kidskonnect.core.domain.model.student.CommentType
import org.vervone.kidskonnect.core.domain.model.student.Student
import org.vervone.kidskonnect.core.push.PushApi

class SchoolFacadeImpl : SchoolFacade {

    private val apiHelper = ApiHelper.Factory.apiHelper
    private val pushApi = PushApi.Factory.pushApi
    private val schoolsApiHelper = SchoolsApiHelper.Factory.apiHelper

    override fun getClasses(academicYr: String,
                            teacherId: String,
                            callback: RequestCallback<List<String>, DomainError>) {
        apiHelper.getClasses(academicYr, teacherId, object : RequestCallback<List<String>, DatabaseError> {
            override fun onError(error: DatabaseError?) {
                callback.onError(DomainError(error?.toException()!!))
            }

            override fun onSuccess(res: List<String>?) {
                callback.onSuccess(res)
            }
        })
    }

    override fun getClasses(callback: RequestCallback<List<String>, DomainError>) {
        apiHelper.getClasses(object : RequestCallback<List<String>, DatabaseError> {
            override fun onError(error: DatabaseError?) {
                callback.onError(DomainError(error?.toException()!!))
            }

            override fun onSuccess(res: List<String>?) {
                callback.onSuccess(res)
            }
        })
    }

    override fun getSchoolContact(callback: RequestCallback<SchoolContact, DomainError>) {
        apiHelper.getSchoolContact(object : RequestCallback<FbSchoolContact, ApiError> {
            override fun onError(error: ApiError?) {
                callback.onError(DomainErrorMapper().map(error!!))
            }

            override fun onSuccess(res: FbSchoolContact?) {
                callback.onSuccess(SchoolContactMapper().map(res!!))
            }
        })
    }


    override fun getStudents(
            academicYr: String,
            query: String?,
            schoolClass: String?,
            callback: RequestCallback<List<Student>, DomainError>
    ) {

        apiHelper.getAllStudents(
                academicYr, query, schoolClass,
                object : RequestCallback<List<FbStudent>, ApiError> {
                    override fun onError(error: ApiError?) {
                        callback.onError(DomainErrorMapper().map(error!!))
                    }

                    override fun onSuccess(res: List<FbStudent>?) {
                        val list = res?.map { StudentMapper().map(it) }
                        callback.onSuccess(list)
                    }
                }
        )
    }

    override fun addCommentOnStudent(studentId: String,
                                     comment: Comment,
                                     callback: RequestCallback<Comment, DomainError>) {

        val fbComment = StudentMapper().map(comment)

        apiHelper.addComment(
                studentId, fbComment, object : RequestCallback<FbComments, ApiError> {
            override fun onSuccess(res: FbComments?) {
                callback.onSuccess(StudentMapper().map(res!!))
            }

            override fun onError(error: ApiError?) {
                callback.onError(DomainErrorMapper().map(error!!))
            }
        })
    }

    override fun getComments(
            studentId: String,
            commentType: CommentType,
            callback: RequestCallback<List<Comment>, DomainError>) {
        val comments = apiHelper.getComments(studentId, commentType.value)
        val mapper = StudentMapper()
        val mapped = comments.map { mapper.map(it) }
        callback.onSuccess(mapped)
    }

    override fun subscribeStudentByTeacher(studentId: String,
                                           subscriber: SchoolFacade.Subscriber): String {
        return apiHelper.subscribeForStudent(object : StudentsFetch.DateChangeListener {
            override fun onChange() {
                subscriber.onChange()
            }
        })
    }

    override fun getTeachersDetails(academicYr: String,
                                    className: String,
                                    callback: RequestCallback<List<Teacher>, DomainError>) {
        apiHelper.getTeacherDetails(
                academicYr, className, object : RequestCallback<Map<String, FbTeacherDetails>, ApiError> {
            override fun onSuccess(res: Map<String, FbTeacherDetails>?) {
                val list = res?.entries?.map { CommonMapper.map(it.value, it.key) }
                callback.onSuccess(list)
            }

            override fun onError(error: ApiError?) {
                callback.onError(DomainError(error?.message))
            }
        })
    }

    override fun getTeachers(academicYr: String,
                             callback: RequestCallback<List<Teacher>, DomainError>) {
        apiHelper.getAllTeacher(
                academicYr, object : RequestCallback<Map<String, FbTeacherDetails>, ApiError> {
            override fun onSuccess(res: Map<String, FbTeacherDetails>?) {
                val list = res?.entries?.map { CommonMapper.map(it.value, it.key) }
                callback.onSuccess(list)
            }

            override fun onError(error: ApiError?) {
                callback.onError(DomainError(error?.message))
            }
        })
    }

    override fun unsubscribe(subscriberId: String) {
        apiHelper.unsubscribeForStudent(subscriberId)
    }

    override fun getTeachers(academicYr: String,
                             className: String,
                             callback: RequestCallback<TeachersForTheClass, DomainError>) {

        apiHelper.getTeachers(
                academicYr, className, object : RequestCallback<HashMap<String, String>, ApiError> {
            override fun onSuccess(res: HashMap<String, String>?) {
                val map = res?.map { it.key to mapStringToTeacher(it.value) }?.toMap()
                TeachersForTheClass.setMap(map)
                callback.onSuccess(TeachersForTheClass)
            }

            override fun onError(error: ApiError?) {
                callback.onError(DomainError(error?.message))
            }
        })
    }

    private fun mapStringToTeacher(data: String): Teacher {
        val split = data.split("|")
        val teacher = Teacher()
        teacher.teacherId = split[0]
        teacher.teacherName = split[1]
        return teacher
    }

    override fun createLeaveRequest(academicYr: String,
                                    teacherId: String,
                                    leave: Leave,
                                    callback: RequestCallback<Leave, DomainError>) {
        val fbLeave = LeavesMapper().map(leave)
        apiHelper.sendLeaveRequest(
                academicYr, teacherId, fbLeave, object : RequestCallback<FbLeave, ApiError> {
            override fun onSuccess(res: FbLeave?) {
                if (res != null) {
                    callback.onSuccess(LeavesMapper().map(res))
                    if (leave.schoolClass != null && leave.studentName != null) {
                        pushApi.publishCreateLeave(teacherId, leave.studentName!!, leave.schoolClass!!)
                    }
                } else {
                    callback.onError(DomainError(""))
                }
            }

            override fun onError(error: ApiError?) {
                callback.onError(DomainError(error?.message))
            }
        })
    }


    override fun subscribeStudent(studentId: String, listener: SchoolFacade.StudentListener): String {
        return apiHelper.subscribeStudent(studentId, object : StudentSubscribe.StudentListener {
            override fun onUpdate(student: FbStudent) {
                val dStudent = StudentMapper().map(student)
                dStudent.studentId = studentId
                listener.onUpdate(dStudent)
            }
        })
    }

    override fun unsubscribeStudent(subscriberId: String) {
        apiHelper.unsubscribeStudent(subscriberId)
    }

    override fun getAcademicReport(studentId: String,
                                   callback: RequestCallback<ArrayList<AcademicReport>, DomainError>) {
        apiHelper.getAcademicReports(
                studentId, object : RequestCallback<ArrayList<FbAcademicReport>, ApiError> {
            override fun onSuccess(res: ArrayList<FbAcademicReport>?) {
                val mapper = StudentMapper()
                val reports = res?.map { mapper.map(it) } ?: emptyList()
                callback.onSuccess(ArrayList(reports))
            }

            override fun onError(error: ApiError?) {
                callback.onError(DomainError(error?.message))
            }

        })
    }

    override fun updateAlternatePhoneNumber(studentId: String,
                                            phoneNumber: String,
                                            callback: RequestCallback<String, DomainError>) {
        apiHelper.updateStudentPhoneNumber(
                studentId, phoneNumber, object : RequestCallback<String, ApiError> {
            override fun onSuccess(res: String?) {
                callback.onSuccess(res)
            }

            override fun onError(error: ApiError?) {
                callback.onError(DomainError(error?.message))
            }
        })
    }

    override fun subscribeComment(studentParams: StudentParamsBm, listener: SchoolFacade.CommentsListener) {
        val studentMapper = StudentMapper()
        CommentsSubscribe.subscribe(
                ParamsMapper().map(studentParams), object : CommentsSubscribe.CommentsListener {
            override fun onNewComment(count: Int, student: FbStudent?, comment: FbComments) {
                listener.onNewComment(count, studentMapper.map(student), StudentMapper().map(comment))
            }

            override fun onSnapshot(count: Int, student: FbStudent?) {
                listener.onSnapshot(count, studentMapper.map(student))
            }

        })
    }

    override fun subscribeComment(teacherParams: TeacherParamsBm, listener: SchoolFacade.CommentsListener) {
        val studentMapper = StudentMapper()
        CommentsSubscribe.subscribe(
                ParamsMapper().map(teacherParams), object : CommentsSubscribe.CommentsListener {
            override fun onSnapshot(count: Int, student: FbStudent?) {
                listener.onSnapshot(count, studentMapper.map(student))
            }

            override fun onNewComment(count: Int, student: FbStudent?, comment: FbComments) {
                listener.onNewComment(count, studentMapper.map(student), StudentMapper().map(comment))
            }
        })
    }

    override fun getSchoolsConfigs(callback: RequestCallback<List<SchoolConfig>, DomainError>) {
        schoolsApiHelper.getSchools(object : RequestCallback<List<SchoolConfig>, ApiError> {
            override fun onError(error: ApiError?) {
                callback.onError(DomainError(error?.message))
            }

            override fun onSuccess(res: List<SchoolConfig>?) {
                callback.onSuccess(res ?: emptyList())
            }
        })
    }

    override fun getSchoolConfigs(schoolId: String,
                                  callback: RequestCallback<SchoolConfig, DomainError>) {
        schoolsApiHelper.getSchool(schoolId, object : RequestCallback<SchoolConfig, ApiError> {
            override fun onError(error: ApiError?) {
                callback.onError(DomainError(error?.message))
            }

            override fun onSuccess(res: SchoolConfig?) {
                callback.onSuccess(res)
            }
        })
    }
}