package org.vervone.kidskonnect.core.data.network.model

/**
 *  Constants related to Firebase
 */

const val NEWS_FEEDS = "newsFeeds"
const val IMAGES = "images"
const val CLASS = "class"
const val TEACHERS = "teachers"
const val STUDENTS = "students"
const val CUR_ACAD_YEAR = "currentAccademicYear"
const val LIKED_NEWS = "likedNews"
const val ALL_MESSAGE_CHANNELS = "allMessageChannels"
const val ALL_MESSAGES = "allMessages"
const val EXAM_SCHEDULES = "examSchedules"
const val LEAVES_LIST = "leavesList"
const val NOTIFICATION = "notification"
const val CONTACT_DETAILS = "contactDetails"
const val COMMENTS = "comments"
const val USER_CHAT_DICT = "userChatDict"
const val ACADEMIC_REPORTS = "accademicReports"
const val APP_VERSION = "appVersion"
const val PROFILE_DETAILS = "ProfileDetails"