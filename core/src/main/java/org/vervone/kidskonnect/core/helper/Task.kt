package org.vervone.kidskonnect.core.helper

interface Task {
    fun cancel()
}