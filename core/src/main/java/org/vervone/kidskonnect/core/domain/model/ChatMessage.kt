package org.vervone.kidskonnect.core.domain.model

import java.io.Serializable

class ChatMessage : Serializable {
    /**
     *  Unique id for the message
     */
    var chatId: String? = null
    /**
     *  message text
     */
    var message: String? = null
    /**
     *  Status of the message
     */
    var messageStatus: Int? = null
    /**
     *  message content type
     */
    var messageType: MessageType = MessageType.UNKNOWN
    /**
     *  uid for sender
     */
    var senderId: String? = null
    /**
     *  name of the sender
     */
    var senderName: String? = null
    /**
     *  time of send
     */
    var time: Long? = null
    /**
     *  Remote url for the media messages
     */
    var attachmentsUrl: String? = null
    /**
     *  Remote url for thumbnail media message
     */
    var thumpNailImagePath: String? = null
    /**
     *  This is the local file path for uploading the media.
     */
    var localFilePath: String? = null

    /**
     *  Status of uploading files to cloud
     */
    var status: Int? = SUCCESS

    /**
     *  Media upload percentage. This is only applicable for media messages.
     */
    var uploadPercent: Int = 0

    fun isMediaMessage(): Boolean {
        return MessageType.TEXT != messageType
    }

    companion object {
        const val PENDING = 1
        const val SUCCESS = 2
        const val FAILED = 3
        const val DOWNLOADING = 4
    }
}