package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.*
import org.vervone.kidskonnect.core.data.network.model.CLASS
import org.vervone.kidskonnect.core.data.network.model.USER_CHAT_DICT
import java.util.concurrent.atomic.AtomicInteger

/**
 *  Used to get the Chat room for the classes specified.
 */
abstract class GetClassChatRooms(private val classes: List<String>,
                                 private val academicYr: String) {


    private val classChatRooms = hashMapOf<String, Boolean>()

    private val reqCount = AtomicInteger(classes.size)

    init {
        classes.forEach { getChatRoomsForClass(it) }
    }


    private fun getChatRoomsForClass(schoolClass: String) {
        // 2019-2020/class/1 A/userChatDict
        ConnectionHelper.getDB()!!.reference
            .child(academicYr)
            .child(CLASS)
            .child(schoolClass)
            .child(USER_CHAT_DICT)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {
                    if (reqCount.decrementAndGet() == 0) {
                        onSuccess(hashMapOf())
                    }
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    val typeIndicator = object : GenericTypeIndicator<HashMap<String, Boolean>>() {}
                    val allChatRooms = snapshot.getValue(typeIndicator)

                    if (allChatRooms != null) {
                        classChatRooms.putAll(allChatRooms)
                    }
                    if (reqCount.decrementAndGet() == 0) {
                        onSuccess(classChatRooms)
                    } else {
                        onSuccess(hashMapOf())
                    }
                }


            })

    }

    abstract fun onSuccess(chatRooms: HashMap<String, Boolean>)


}