package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.model.school.FbSchool
import org.vervone.kidskonnect.core.domain.mappers.CommonMapper
import org.vervone.kidskonnect.core.domain.model.school.SchoolConfig

class SchoolsApiService : SchoolsApiHelper {
    override fun getSchools(callback: RequestCallback<List<SchoolConfig>, ApiError>) {
        //Use school service
        val dbRef = FirebaseDatabase.getInstance().reference
        dbRef.child("schools")
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {
                    callback.onError(ApiError(Exception(dbError.message)))
                }

                override fun onDataChange(snapShot: DataSnapshot) {
                    val schoolsMap = snapShot.getValue(ApiHelperImpl.TYPE_INDICATOR_SCHOOLS)
                    val schools = schoolsMap?.map { CommonMapper.map(it.value, it.key) }
                    callback.onSuccess(schools)
                }
            })
    }

    override fun getSchool(schoolId: String,
                           callback: RequestCallback<SchoolConfig, ApiError>) {
        val dbRef = FirebaseDatabase.getInstance().reference
        dbRef.child("schools").child(schoolId)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {
                    callback.onError(ApiError(Exception(dbError.message)))
                }

                override fun onDataChange(snapShot: DataSnapshot) {
                    val school = snapShot.getValue(FbSchool::class.java)
                    callback.onSuccess(CommonMapper.map(school, snapShot.key))
                }
            })
    }
}