package org.vervone.kidskonnect.core.data.network

import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseError
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.UploadListener
import org.vervone.kidskonnect.core.cloudstorage.CloudStorage
import org.vervone.kidskonnect.core.data.network.model.*
import org.vervone.kidskonnect.core.data.network.model.academicyear.FbAcademicYrTeacher
import org.vervone.kidskonnect.core.data.network.model.student.FbAcademicReport
import org.vervone.kidskonnect.core.data.network.model.student.FbComments
import org.vervone.kidskonnect.core.data.network.model.student.FbStudent
import org.vervone.kidskonnect.core.helper.Task

interface ApiHelper {

    fun isLoggedIn(): Boolean

    fun doLoginCall(email: String, pwd: String, callback: RequestCallback<FirebaseUser, Exception>)

    fun fetchTeacherProfile(uid: String?, callback: RequestCallback<FbTeacher, ApiError>)


    fun fetchStudentsProfile(uid: String?, callback: RequestCallback<FbStudent, DatabaseError>)

    fun getCurrentAcademicYear(callback: RequestCallback<String, DatabaseError>)


    fun getAcademicYearData(
            uid: String?,
            academicYr: String,
            callback: RequestCallback<FbAcademicYrTeacher, ApiError>
    )


    fun getNews(
            academicYr: String, teacherUid: String, isAdmin: Boolean,
            callback: RequestCallback<List<FbNews>, DatabaseError>
    )

    fun getClasses(academicYr: String,
                   teacherUid: String,
                   callback: RequestCallback<List<String>, DatabaseError>)

    fun getClasses(callback: RequestCallback<List<String>, DatabaseError>)

    fun postNews(academicYr: String, news: FbNews, callback: RequestCallback<Void, Error>)

    fun generateNewsId(academicYr: String): String

    fun deleteNews(
            academicYr: String, newsId: String,
            cloudStorage: CloudStorage?, callback: RequestCallback<String, Error>
    )

    fun updateLikes(
            academicYr: String, newsId: String,
            user: String, isLike: Boolean, callback: RequestCallback<String, Error>
    )

    fun getLikedNews(academicYr: String, userUid: String)

    fun getAllChatRooms(
            academicYr: String, userUid: String,
            classes: List<String>,
            callback: RequestCallback<HashMap<String, FbMessageChannel>, ApiError>?
    )

    fun getChatRoomsForStudent(academicYr: String,
                               userUid: String,
                               className: String,
                               callback: RequestCallback<HashMap<String, FbMessageChannel>, ApiError>?
    )

    fun getAllMessages(
            academicYr: String, chatRoomId: String,
            callback: RequestCallback<HashMap<String, FbChatMessage>, DatabaseError>?
    ): MessagesSubscription

    fun sendMessage(
            academicYr: String, chatRoomId: String, chatMessage: FbChatMessage,
            callback: UploadListener<FbChatMessage, DatabaseError>?
    ): Task

    fun getScheduledExams(
            academicYr: String,
            classes: List<String>,
            callback: RequestCallback<List<FbExam>, ApiError>?
    )

    fun getAllLeavesRequestToTeacher(
            academicYr: String,
            userId: String?,
            callback: RequestCallback<List<FbLeave>, ApiError>
    )

    fun subscribeAllLeavesRequestToTeacher(
            academicYr: String,
            userId: String?,
            changeListener: DataChangeListener): String

    fun unsubscribeAllLeavesRequestToTeacher(subscriberId: String)

    fun getAllLeaveRequestsByStudent(
            academicYr: String,
            schoolClass: String,
            userId: String?,
            callback: RequestCallback<List<FbLeave>, ApiError>
    )

    fun subscribeAllLeaveRequestsByStudent(academicYr: String,
                                           schoolClass: String,
                                           userId: String?,
                                           changeListener: DataChangeListener): String

    fun unsubscribeAllLeaveRequestsByStudent(subscriberId: String)

    fun updateLeave(
            academicYr: String,
            leave: FbLeave, callback: RequestCallback<FbLeave, ApiError>
    )

    fun getAllNotifications(
            academicYr: String,
            callback: RequestCallback<List<FbNotification>, ApiError>
    )


    fun getSchoolContact(callback: RequestCallback<FbSchoolContact, ApiError>)


    fun getAllStudents(
            academicYr: String,
            query: String?,
            schoolClass: String?,
            callback: RequestCallback<List<FbStudent>, ApiError>
    )


    fun addComment(
            studentId: String,
            comment: FbComments,
            callback: RequestCallback<FbComments, ApiError>
    )


    fun subscribeForStudent(listener: StudentsFetch.DateChangeListener): String

    fun unsubscribeForStudent(subscriberId: String)

    fun getComments(
            studentId: String,
            commentType: Int
    ): ArrayList<FbComments>

    fun getAllTeacher(
            academicYr: String,
            callback: RequestCallback<Map<String, FbTeacherDetails>, ApiError>
    )


    fun doLogout()

    fun getStudentNews(
            academicYr: String,
            className: String,
            callback: RequestCallback<List<FbNews>, Error>
    )


    fun getStudentLikedNews(
            academicYr: String,
            userUid: String, className: String
    )

    fun updateStudentNews(
            academicYr: String, newsId: String, user: String,
            className: String, isLike: Boolean,
            callback: RequestCallback<String, Error>
    )

    fun createMessageChannelByTeacher(
            academicYr: String,
            teacherId: String,
            messageChannel: FbMessageChannel,
            callback: RequestCallback<Pair<String, FbMessageChannel>, ApiError>
    )

    fun createMessageChannelByStudent(
            academicYr: String,
            className: String,
            teacherId: String,
            messageChannel: FbMessageChannel,
            callback: RequestCallback<Pair<String, FbMessageChannel>, ApiError>
    )

    fun getLastMessage(academicYr: String, chatRoomId: String,
                       callback: RequestCallback<HashMap<String, FbChatMessage>, ApiError>?)

    fun getChatRoomIfAvailable(
            academicYr: String, targetsId: String,
            callback: RequestCallback<Pair<String, FbMessageChannel>?, DatabaseError>?
    )

    fun getTeacherDetails(academicYr: String, className: String,
                          callback: RequestCallback<Map<String, FbTeacherDetails>, ApiError>)

    fun getTeachers(academicYr: String,
                    className: String,
                    callback: RequestCallback<HashMap<String, String>, ApiError>)

    fun sendLeaveRequest(academicYr: String,
                         teacherId: String,
                         leave: FbLeave,
                         callback: RequestCallback<FbLeave, ApiError>)

    fun subscribe(topics: Topics,
                  params: TeacherParams,
                  callback: Callback): String

    fun subscribe(topics: Topics,
                  params: StudentParams,
                  callback: Callback): String

    fun unsubscribe(subscriberId: String)


    fun subscribeForMessages(params: TeacherParams,
                             callback: MessageCallback): String

    fun subscribeForMessages(params: StudentParams,
                             callback: MessageCallback): String


    /**
     *  Subscribe student for updates return subscription id
     */
    fun subscribeStudent(studentId: String,
                         listener: StudentSubscribe.StudentListener): String


    fun unsubscribeStudent(subscriptionId: String)

    fun getAcademicReports(studentId: String,
                           callback: RequestCallback<ArrayList<FbAcademicReport>, ApiError>)

    fun getRequiredAppVersions(callback: RequestCallback<FbAppVersion, ApiError>)

    fun subscribeForAppVersion(subscriber: AppVersionSubsribers.VersionSubscriber)

    fun updateStudentPhoneNumber(studentId: String, phoneNo: String,
                                 callback: RequestCallback<String, ApiError>)

    object Factory {
        var apiHelper = ApiHelperImpl()
    }

}