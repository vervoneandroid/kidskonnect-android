package org.vervone.kidskonnect.core.helper

import com.google.gson.Gson

object JsonHelper {
    fun toJson(obj: Any): String {
        return Gson().toJson(obj)
    }

    fun <T> fromJson(json: String, clazz: Class<T>): T {
        return Gson().fromJson(json, clazz)
    }
}