package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import org.vervone.kidskonnect.core.data.network.model.*
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger

/**
 *
 */
object MessageChannelsListener {

    private val subscribers = Hashtable<String, Callback>()

    private val isFirst = AtomicBoolean(true)
    private val reqCount = AtomicInteger(0)
    //private val ignore = AtomicBoolean(true)


    private fun getChatRoomsForTeacher(academicYr: String,
                                       teacherId: String) {
        ConnectionHelper.getDB()!!.reference.child(academicYr)
            .child(TEACHERS)
            .child(teacherId)
            .child(USER_CHAT_DICT)
            .addValueEventListener(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {
                    notifySubscribers()
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    notifySubscribers()
                }
            })
    }

    private fun notifySubscribers() {
        if (reqCount.decrementAndGet() <= 0) {
            subscribers.forEach { entry ->
                entry.value.onDataChange()
            }
        }
    }


    fun subscribe(params: TeacherParams,
                  callback: Callback): String {

        val uuid = ApiUtils.getUUID()
        subscribers[uuid] = callback

        if (isFirst.getAndSet(false)) {
            reqCount.set(params.classes.size + 1)
            getChatRoomsForTeacher(params.academicYr, params.teacherId)
            params.classes.forEach {
                getChatRoomsForClass(params.academicYr, it)
            }
        }

        return uuid
    }

    fun subscribe(params: StudentParams,
                  callback: Callback): String {

        val uuid = ApiUtils.getUUID()
        subscribers[uuid] = callback

        if (isFirst.getAndSet(false)) {
            reqCount.set(2)
            getChatRoomsForStudent(params.academicYr,
                                   params.studentId,
                                   params.className)
            getChatRoomsForClass(params.academicYr,
                                 params.className)
        }

        return uuid
    }

    private fun getChatRoomsForStudent(academicYr: String,
                                       studentId: String,
                                       className: String) {
        ConnectionHelper.getDB()!!.reference.child(academicYr)
            .child(CLASS)
            .child(className)
            .child(STUDENTS)
            .child(studentId)
            .child(USER_CHAT_DICT)
            .addValueEventListener(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {
                    notifySubscribers()
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    notifySubscribers()
                }
            })
    }


    private fun getChatRoomsForClass(academicYr: String,
                                     schoolClass: String) {

        ConnectionHelper.getDB()!!.reference.child(academicYr)
            .child(CLASS)
            .child(schoolClass)
            .child(USER_CHAT_DICT)
            .addValueEventListener(object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {
                    notifySubscribers()
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    notifySubscribers()
                }
            })
    }

    fun unsubscribe(subscriberId: String) {
        subscribers.remove(subscriberId)
    }

    fun unsubscribeAll() {
        isFirst.set(true)
        subscribers.clear()
    }
}