package org.vervone.kidskonnect.core.domain.model

enum class MessageType(val value: Int) {
    //(0 - text message, 1 - picture , 2 - videos )
    TEXT(0),
    PICTURE(1),
    VIDEOS(2),
    UNKNOWN(-1);

    companion object {
        fun fromInt(value: Int?): MessageType {
            return when (value) {
                0 -> TEXT
                1 -> PICTURE
                2 -> VIDEOS
                else -> UNKNOWN
            }
        }
    }
}