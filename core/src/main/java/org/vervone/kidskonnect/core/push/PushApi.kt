package org.vervone.kidskonnect.core.push

import org.vervone.kidskonnect.core.domain.model.MessageChannel
import org.vervone.kidskonnect.core.helper.LocalizedStringsImpl

interface PushApi {
    fun send(message: Message)

    fun publishCreateLeave(teacherId: String,
                           studentName: String,
                           className: String)

    fun publishApprove(teacherName: String,
                       studentId: String)

    fun publishRejected(teacherName: String,
                        studentId: String)

    fun publishNewsCreated(senderName: String,
                           classes: List<String>)

    fun publishProfileUpdated(className: String,
                              commentedPersonName: String,
                              commentType: String,
                              studentId: String,
                              studentName: String)

    fun publishMessage(channelId: String,
                       senderName: String)

    fun publishCreateChannel(senderName: String, senderId: String,
                             messageChannel: MessageChannel)

    object Factory {
        var pushApi = FcmPush(LocalizedStringsImpl())
    }
}