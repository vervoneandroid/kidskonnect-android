package org.vervone.kidskonnect.core.data.network

import org.vervone.kidskonnect.core.data.network.model.Id
import java.util.*

object ApiUtils {
    /**
     *   Mapping the id property to the Object.
     */
    fun <V : Id> mapId(entry: Map.Entry<String, V>): V {
        entry.value.id = entry.key
        return entry.value
    }

    fun getUUID(): String {
        return UUID.randomUUID().toString()
    }
}