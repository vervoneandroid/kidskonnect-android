package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.*
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.data.network.model.STUDENTS
import org.vervone.kidskonnect.core.data.network.model.student.FbStudent
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean

/**
 *    Only subscribe one student at a time .
 */
object StudentSubscribe {

    private val subscribers = Hashtable<String, StudentListener>()
    private val isFirst = AtomicBoolean(true)
    private var subscribedStudent: String? = null

    private var ref: DatabaseReference? = null


    fun subscribe(studentId: String, listener: StudentListener): String {

        if (subscribedStudent != null && !studentId.equals(subscribedStudent, false)) {
            throw IllegalStateException("Already subscribed , Call unsubscribeAll() first ")
        }

        val uuid = getUUID()
        subscribers[uuid] = listener

        if (isFirst.getAndSet(false)) {
            subscribe(studentId)
        }

        return uuid
    }


    private fun subscribe(studentId: String) {
        ref = ConnectionHelper.getDB()!!.reference
            .child(STUDENTS)
            .child(studentId)
        ref?.addValueEventListener(eventHandler)
    }

    private val eventHandler = object : ValueEventListener {
        override fun onCancelled(dbError: DatabaseError) {}
        override fun onDataChange(dataSnap: DataSnapshot) {
            val student = dataSnap.getValue(FbStudent::class.java)
            if (student != null) {
                notifyAllSubscribers(student)
            }
        }
    }

    fun unsubscribe(subscriptionId: String) {
        try {
            subscribers.remove(subscriptionId)
        } catch (ex: Exception) {
            DLog.e(this, "exception in unsubscribe", ex)
        }
    }

    fun unsubscribeAll() {
        subscribers.clear()
        isFirst.set(true)
        ref?.removeEventListener(eventHandler)
    }

    private fun getUUID(): String {
        return UUID.randomUUID().toString()
    }

    private fun notifyAllSubscribers(student: FbStudent) {
        subscribers.values.forEach { it.onUpdate(student) }
    }


    interface StudentListener {
        fun onUpdate(student: FbStudent)
    }

}