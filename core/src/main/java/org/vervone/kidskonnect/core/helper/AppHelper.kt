package org.vervone.kidskonnect.core.helper

import android.app.Activity
import android.content.Context
import org.vervone.kidskonnect.core.BuildConfig
import org.vervone.kidskonnect.core.cloudstorage.CloudStorage

object AppHelper {

    private lateinit var appDelegate: AppDelegate

    fun init(delegate: AppDelegate) {
        this.appDelegate = delegate
    }

    fun getContext(): Context {
        return appDelegate.getAppContext()
    }

    fun get(): Context {
        return appDelegate.getAppContext()
    }


    fun getActivity(): Activity? {
        return appDelegate.getActivity()
    }

    fun getCloudStorage(): CloudStorage? {
        return appDelegate.getCloudStorage()
    }

    private const val APP_VERSION = BuildConfig.VERSION_CODE

    fun getAppVersion(): Int {
        return APP_VERSION
    }

}