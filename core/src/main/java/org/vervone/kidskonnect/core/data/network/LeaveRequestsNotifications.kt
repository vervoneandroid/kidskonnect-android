package org.vervone.kidskonnect.core.data.network

import com.google.firebase.database.*
import org.vervone.kidskonnect.core.data.network.model.FbLeave
import org.vervone.kidskonnect.core.data.network.model.LEAVES_LIST
import org.vervone.kidskonnect.core.data.network.model.TEACHERS
import org.vervone.kidskonnect.core.data.network.model.TeacherParams
import java.util.concurrent.atomic.AtomicBoolean

object LeaveRequestsNotifications {

    private val subscribers = HashMap<String, LeaveRequestListener>()
    private val isFirst = AtomicBoolean(true)
    private val allLeaveIds = HashMap<String, Boolean>()

    private var childListener: ChildListener? = null


    fun subscribe(teacherParams: TeacherParams, listener: LeaveRequestListener): String {
        val uuid = ApiUtils.getUUID()
        subscribers[uuid] = listener
        if (isFirst.getAndSet(false)) {
            findRequestCount(teacherParams)
        }
        return uuid
    }


    private fun findRequestCount(teacherParams: TeacherParams) {
        val ref = ConnectionHelper.getDB()!!.reference
            .child(teacherParams.academicYr)
            .child(TEACHERS)
            .child(teacherParams.teacherId)
            .child(LEAVES_LIST)
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}
            override fun onDataChange(snapshot: DataSnapshot) {
                val typeIndicator =
                    object : GenericTypeIndicator<HashMap<String, Boolean>>() {}
                val leavesIds = snapshot.getValue(typeIndicator)
                allLeaveIds.clear()
                if (leavesIds != null) {
                    allLeaveIds.putAll(leavesIds)
                }
                subscribers.values.forEach { it.onSnapshot(allLeaveIds.size) }
                addChildListener(teacherParams)
            }
        })
    }

    private fun addChildListener(teacherParams: TeacherParams) {
        val ref = ConnectionHelper.getDB()!!.reference
            .child(teacherParams.academicYr)
            .child(TEACHERS)
            .child(teacherParams.teacherId)
            .child(LEAVES_LIST)

        this.childListener = ChildListener(teacherParams, ref)
        ref.addChildEventListener(childListener!!)
    }

    private class ChildListener(private val teacherParams: TeacherParams,
                                private val ref: DatabaseReference) : ChildEventListener {
        override fun onChildMoved(p0: DataSnapshot, p1: String?) {}
        override fun onChildChanged(p0: DataSnapshot, p1: String?) {}
        override fun onChildRemoved(p0: DataSnapshot) {}
        override fun onCancelled(dbError: DatabaseError) {}
        override fun onChildAdded(snapshot: DataSnapshot, p1: String?) {
            if (!allLeaveIds.containsKey(snapshot.key)) {
                allLeaveIds[snapshot.key!!] = true
                getLeave(teacherParams, snapshot.key!!)
            }
        }

        fun remove() {
            ref.removeEventListener(this)
        }
    }


    private fun getLeave(teacherParams: TeacherParams, leaveId: String) {
        val ref = ConnectionHelper.getDB()!!.reference
            .child(teacherParams.academicYr)
            .child(LEAVES_LIST)
            .child(leaveId)
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) {}
            override fun onDataChange(snapshot: DataSnapshot) {
                val typeIndicator =
                    object : GenericTypeIndicator<FbLeave>() {}
                val leave = snapshot.getValue(typeIndicator)
                if (leave != null) {
                    subscribers.values.forEach { it.onRequest(allLeaveIds.size, leave) }
                }
            }
        })
    }


    fun resetAll() {
        subscribers.clear()
        isFirst.set(true)
        childListener?.remove()
    }


    interface LeaveRequestListener {
        fun onSnapshot(count: Int)
        fun onRequest(count: Int, leave: FbLeave)
    }
}