package org.vervone.kidskonnect.ui.screens.exams

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.domain.model.Exam
import org.vervone.kidskonnect.ui.screens.PdfViewerActivity
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.utils.inflater

/**
 *  Show all exams scheduled
 */
class ExamsFragment : BaseFragment(), ExamsView {

    private lateinit var rvExams: RecyclerView
    private var examsList: List<Exam>? = null

    private var presenter: ExamsPresenter? = null

    override fun getLayoutId(): Int {
        return R.layout.fragment_exam_schedule
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.rvExams = view.findViewById(R.id.rv_exams)
        val layoutManager = LinearLayoutManager(activity!!)
        rvExams.layoutManager = layoutManager
        activity?.setTitle(R.string.exam_schedule)

        val itemDecorator = DividerItemDecoration(context, layoutManager?.orientation!!)
        val divider = ContextCompat.getDrawable(context!!, R.drawable.divider_1dp)
        itemDecorator.setDrawable(divider!!)
        rvExams.addItemDecoration(itemDecorator)


        presenter = ExamsPresenter()
        presenter?.onAttach(this)
        showLoading()
        presenter?.getScheduledExams()

    }


    override fun onDestroyView() {
        presenter?.onDettach()
        super.onDestroyView()
    }

    override fun setData(examList: List<Exam>) {
        showContentLayout()
        this.examsList = examList
        rvExams.adapter = ExamsAdapter()
    }

    override fun showError() {
        showErrorLayout(getString(R.string.no_exams_found))
    }

    private class VH(view: View) : RecyclerView.ViewHolder(view) {
        var tvExamTitle: TextView
        var tvExamDate: TextView
        var tvExamDesc: TextView

        init {
            tvExamTitle = view.findViewById(R.id.tv_exam_title)
            tvExamDate = view.findViewById(R.id.tv_exam_date)
            tvExamDesc = view.findViewById(R.id.tv_exam_desc)
        }

    }

    private inner class ExamsAdapter : RecyclerView.Adapter<VH>() {
        override fun onCreateViewHolder(vg: ViewGroup, pos: Int): VH {
            val view = vg.context.inflater().inflate(
                    R.layout.kc_layout_exams_row,
                    null, false
            )
            return VH(view)
        }

        override fun getItemCount(): Int {
            return examsList?.size ?: 0
        }

        override fun onBindViewHolder(vh: VH, pos: Int) {
            val exam = examsList?.get(pos)
            vh.tvExamTitle.text = exam?.title
            vh.tvExamDate.text = exam?.getFormattedDate()
            vh.tvExamDesc.text = exam?.description
            vh.itemView.setOnClickListener {
                PdfViewerActivity.startActivity(activity!!, exam?.pdfUrl)
            }
        }

    }

}