package org.vervone.kidskonnect.ui.screens.base

open class BasePresenter<VIEW : BaseView> {

    var view: VIEW? = null

    fun onAttach(view: VIEW) {
        this.view = view
    }

    fun onDettach() {
        view = null
    }
}