package org.vervone.kidskonnect.ui.utils

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager

object FragmentUtils {


    fun addFragment(fm: FragmentManager, fragment: Fragment, id: Int) {
        with(fm.beginTransaction()) {
            add(id, fragment)
            commit()
        }
    }

    fun clearBackStack(fm: FragmentManager) {
        for (i in 0 until fm.backStackEntryCount) {
            fm.popBackStackImmediate()
        }
    }

    fun replace(fm: FragmentManager?, fragment: Fragment, id: Int) {
        if (fm == null) return
        with(fm.beginTransaction()) {
            replace(id, fragment)
            commit()
        }
    }

    fun replace(act: FragmentActivity?, fragment: Fragment, id: Int) {
        if (act == null) return
        replace(act.supportFragmentManager, fragment, id)
    }

    fun replace(fm: FragmentManager?, fragment: Fragment, id: Int, addToBackStack: Boolean) {
        if (fm == null) return
        if (addToBackStack) {
            fm.beginTransaction().replace(id, fragment).addToBackStack(null).commit()
        } else {
            replace(fm, fragment, id)
        }
    }

    fun pop(activity: FragmentActivity?) {
        activity?.supportFragmentManager?.popBackStack()
    }


}