package org.vervone.kidskonnect.ui.screens.student

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.domain.model.student.Student
import org.vervone.kidskonnect.core.helper.ThreadUtils
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.utils.*

/**
 *    This search have 2 modes .
 *
 *   1. This will list out all classes and is to
 *    list out all student from the query text.
 *
 *   2. Second mode is only list out students from the class.
 */
class StudentSearchFragment : BaseFragment(), StudentSearchView, ItemClickListener {


    private lateinit var rvClasses: RecyclerView
    private lateinit var etSearchQuery: EditText

    private var presenter: StudentSearchPresenter? = null
    private var classesList = ArrayList<String>()

    private var schoolClass: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.schoolClass = arguments?.getString(SCHOOL_CLASS, null)
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_student_search
    }

    private fun isClassNameProvided(): Boolean {
        return schoolClass != null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.setTitle(R.string.students)
        this.etSearchQuery = view.findViewById(R.id.et_search_query)
        this.rvClasses = view.findViewById(R.id.rv_classes)
        rvClasses.configure()

        UiUtils.setTextViewDrawableColorFilter(etSearchQuery, resources.getColor(R.color.primaryColor))

        this.presenter = StudentSearchPresenter()
        this.presenter?.onAttach(this)

        showLoading()

        if (!isClassNameProvided()) {
            this.presenter?.getClasses()
        } else {
            activity?.setTitle(R.string.our_students)
            presenter?.queryStudents(schoolClass, null)
        }
        etSearchQuery.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {}
            override fun beforeTextChanged(charSequence: CharSequence?,
                                           p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence?,
                                       p1: Int, p2: Int, p3: Int) {
                // If there is class name provided we don't have to list
                // out all the class names . We query the student from the provided class name.
                if (!isClassNameProvided() && charSequence.isNullOrEmpty()) {
                    showClasses()
                } else {
                    presenter?.queryStudents(schoolClass, charSequence.toString())
                }
            }
        })

    }

    override fun onDestroyView() {
        this.presenter?.onDettach()
        super.onDestroyView()
    }

    override fun setSearchResult(list: List<Student>) {
        ThreadUtils.runOnUi {
            if (isClassNameProvided()) {
                showContentLayout()
            }
            rvClasses.adapter = ClassesAdapter(list, context!!, this)
        }
    }

    private fun showClasses() {
        rvClasses.adapter = ClassesAdapter(classesList, context!!, this)
    }

    override fun setClasses(list: List<String>) {
        showContentLayout()
        classesList.clear()
        classesList.addAll(list)
        rvClasses.adapter = ClassesAdapter(classesList, context!!, this)
    }

    override fun onClick(item: Any) {
        if (item is Student) {
            val fragment = StudentProfileFragment.createInstance(item)
            FragmentUtils.replace(fragmentManager,
                                  fragment, R.id.content_frame, true)

        } else {
            FragmentUtils.replace(fragmentManager, createInstance(item as String),
                                  R.id.content_frame, true)
        }
    }

    private class ClassVH(val view: View) : RecyclerView.ViewHolder(view) {
        val tvClassName: TextView = view.findViewById(R.id.tv_name)
        val tvCounter: TextView = view.findViewById(R.id.tv_counter)
    }

    private class ClassesAdapter(val dataSet: List<Any>,
                                 val context: Context,
                                 val listener: ItemClickListener) : RecyclerView.Adapter<ClassVH>() {


        private val counter: AllCommentsCount = AllCommentsCount()

        override fun onCreateViewHolder(vg: ViewGroup, pos: Int): ClassVH {
            val view = vg.context.inflater()
                .inflate(R.layout.kc_layout_text_with_right_arrow, vg, false)
            return ClassVH(view)
        }

        override fun getItemCount(): Int {
            return dataSet.size
        }

        override fun onBindViewHolder(vh: ClassVH, pos: Int) {
            if (dataSet[pos] is Student) {
                val student = dataSet[pos] as Student
                val year = AppUtils.getCurrentAcademicYear()
                val className = student.getClass(year) ?: ""
                vh.tvClassName.text = context.getString(R.string.in_bracket, student.studentName, className)
                val count = counter.getUnreadCount(student.studentId ?: "", className)
                if (count > 0) {
                    vh.tvCounter.text = count.toString()
                    vh.tvCounter.extShow()
                } else {
                    vh.tvCounter.extHide()
                }
            } else {
                val className = dataSet[pos] as String
                vh.tvClassName.text = className
                val count = counter.getCountByClassName(className)
                if (count > 0) {
                    vh.tvCounter.text = count.toString()
                    vh.tvCounter.extShow()
                } else {
                    vh.tvCounter.extHide()
                }
            }

            vh.view.setOnClickListener {
                listener.onClick(dataSet[pos])
            }
        }
    }


    companion object {

        private const val SCHOOL_CLASS = "SCHOOL_CLASS"

        fun createInstance(): StudentSearchFragment {
            return StudentSearchFragment()
        }

        fun createInstance(schoolClass: String): StudentSearchFragment {
            val args = Bundle()
            args.putString(SCHOOL_CLASS, schoolClass)

            val fragment = StudentSearchFragment()
            fragment.arguments = args
            return fragment
        }
    }
}

interface ItemClickListener {
    fun onClick(item: Any)
}