package org.vervone.kidskonnect.ui.screens.student

import org.vervone.kidskonnect.core.domain.model.student.AcademicReport
import org.vervone.kidskonnect.ui.screens.base.BaseView

interface AcademicReportsView : BaseView {
    fun setData(academicReports: ArrayList<AcademicReport>)
    fun showError()
}