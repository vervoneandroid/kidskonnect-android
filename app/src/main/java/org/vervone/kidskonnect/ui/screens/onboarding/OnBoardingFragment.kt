package org.vervone.kidskonnect.ui.screens.onboarding

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.utils.extHide
import java.io.Serializable

class OnBoardingFragment : BaseFragment() {

    private lateinit var btnAction: Button
    private lateinit var ivIcon: ImageView
    private lateinit var tvTitle: TextView
    private lateinit var tvDescription: TextView

    private var type = Type.WELCOME

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.type = arguments?.getSerializable(TYPE) as? Type ?: Type.WELCOME
    }


    override fun getLayoutId(): Int {
        return R.layout.fragment_onboarding
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.btnAction = view.findViewById(R.id.btn_action)
        this.ivIcon = view.findViewById(R.id.iv_icon)
        this.tvTitle = view.findViewById(R.id.tv_title)
        this.tvDescription = view.findViewById(R.id.tv_description)

        when (type) {
            Type.WELCOME -> {
                ivIcon.setImageResource(R.drawable.ic_onboarding_kidskonnect)
                tvTitle.text = getString(R.string.welcome_to_kidskonnect)
                tvDescription.extHide()
            }
            Type.MESSAGES -> {
                ivIcon.setImageResource(R.drawable.ic_onboarding_message)
                tvTitle.text = getString(R.string.messages)
                tvDescription.text = getString(R.string.lorem_ipsum)

            }
            Type.NEWS -> {
                ivIcon.setImageResource(R.drawable.ic_onboarding_news)
                tvTitle.text = getString(R.string.newsfeeds)
                tvDescription.text = getString(R.string.lorem_ipsum)

            }
            Type.NOTIFICATION -> {
                ivIcon.setImageResource(R.drawable.ic_onboarding_notification)
                tvTitle.text = getString(R.string.notifications)
                tvDescription.text = getString(R.string.lorem_ipsum)

            }
        }

    }

    companion object {

        private const val TYPE = "type"

        fun createInstance(type: Type): OnBoardingFragment {
            val fragment = OnBoardingFragment()
            val args = Bundle()
            args.putSerializable(TYPE, type)
            fragment.arguments = args
            return fragment
        }
    }


    enum class Type : Serializable {
        NEWS,
        MESSAGES,
        NOTIFICATION,
        WELCOME
    }


}