package org.vervone.kidskonnect.ui.screens.messages.group

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.utils.UserHelper
import org.vervone.kidskonnect.ui.utils.configure
import org.vervone.kidskonnect.ui.utils.inflate

class CategorySelectionFragment : BaseFragment() {

    private lateinit var rvCategory: RecyclerView


    override fun getLayoutId(): Int {
        return R.layout.fragment_category_selection
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.rvCategory = view.findViewById(R.id.rv_teachers)
        rvCategory.configure()


        view.findViewById<Button>(R.id.btn_cancel)
            .setOnClickListener {
                (parentFragment as ClosableWindow).closeWindow()
            }


        val categoryList = mutableListOf<String>()
        if (UserHelper.isTeacher()) {
            categoryList.addAll(listOf(
                    getString(R.string.teachers),
                    getString(R.string.students)
            ))
        } else {
            categoryList.addAll(listOf(
                    getString(R.string.teachers)))
        }


        rvCategory.adapter = CategoryAdapter(categoryList)
    }

    fun getCategoryClickListener(): CategoryClickListener? {
        if (parentFragment is CategoryClickListener) {
            return parentFragment as CategoryClickListener
        }
        return null
    }


    private class VH(view: View) : RecyclerView.ViewHolder(view) {
        val tvCategoryName: TextView = view.findViewById(R.id.tv_name)
    }

    private inner class CategoryAdapter(val list: List<String>) : RecyclerView.Adapter<VH>() {

        override fun onCreateViewHolder(vg: ViewGroup, pos: Int): VH {
            val view = vg.inflate(R.layout.kc_layout_text_with_right_arrow)
            return VH(view)
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onBindViewHolder(vh: VH, pos: Int) {
            vh.tvCategoryName.text = list[pos]

            vh.itemView.setOnClickListener {
                getCategoryClickListener()?.onCategoryClick(pos)
            }
        }
    }


    interface CategoryClickListener {
        fun onCategoryClick(pos: Int)
    }

}