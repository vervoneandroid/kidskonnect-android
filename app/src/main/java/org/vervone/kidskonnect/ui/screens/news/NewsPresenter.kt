package org.vervone.kidskonnect.ui.screens.news

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.DomainConsts
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.model.News
import org.vervone.kidskonnect.core.helper.PreferenceHelper
import org.vervone.kidskonnect.ui.screens.base.BasePresenter
import org.vervone.kidskonnect.ui.utils.AppUtils
import org.vervone.kidskonnect.ui.utils.UserHelper

class NewsPresenter : BasePresenter<NewsView>() {

    private val newsFacade = FacadeFactory.newsFacade

    fun loadNews() {

        val year = PreferenceHelper.getString(DomainConsts.KEY_CUR_ACADEMIC_YR)
        val uid = PreferenceHelper.getString(DomainConsts.KEY_CUR_USER_ID)
        if (year != null && uid != null) {
            view?.showProgressBar()
            when {
                UserHelper.isAdmin() -> loadNewsForAdmin(year, uid)
                UserHelper.isTeacher() -> loadNewsForTeacher(year, uid)
                else -> loadNewsForStudent(year)
            }
        }
    }


    private fun loadNewsForStudent(year: String) {
        val className = UserHelper.student!!.getClass(year)!!
        newsFacade.getStudentNews(year, className, object : RequestCallback<List<News>, Error> {
            override fun onError(error: Error?) {
                view?.showError()
            }

            override fun onSuccess(res: List<News>?) {
                if (res.isNullOrEmpty()) {
                    view?.showError()
                } else {
                    val sorted = res.sortedByDescending { news -> news.date }
                    view?.setNewsList(sorted)
                }
            }
        })
    }

    private fun loadNewsForTeacher(year: String, uid: String) {
        newsFacade.getNews(year, uid, object : RequestCallback<List<News>, Error> {
            override fun onError(error: Error?) {
                view?.showError()
            }

            override fun onSuccess(res: List<News>?) {
                if (res.isNullOrEmpty()) {
                    view?.showError()
                } else {
                    val sorted = res.sortedByDescending { news -> news.date }
                    view?.setNewsList(sorted)
                }
            }
        })
    }

    private fun loadNewsForAdmin(year: String, uid: String) {
        newsFacade.getNewsForAllClasses(year, uid, object : RequestCallback<List<News>, Error> {
            override fun onError(error: Error?) {
                view?.showError()
            }

            override fun onSuccess(res: List<News>?) {
                if (res.isNullOrEmpty()) {
                    view?.showError()
                } else {
                    val sorted = res.sortedByDescending { news -> news.date }
                    view?.setNewsList(sorted)
                }
            }
        })
    }


    fun deleteNews(newId: String) {
        val year = AppUtils.getCurrentAcademicYear()
        view?.showProgressBar()
        newsFacade.deleteNews(year, newId, object : RequestCallback<String, Error> {
            override fun onError(error: Error?) {
                view?.showError()
            }

            override fun onSuccess(res: String?) {
                loadNews()
            }
        })
    }

    fun isLikedNews(newsId: String): Boolean {
        return newsFacade.isLikedNews(newsId)
    }


    fun getLikedNews() {
        val year = PreferenceHelper.getSafeString(DomainConsts.KEY_CUR_ACADEMIC_YR)
        val uid = PreferenceHelper.getSafeString(DomainConsts.KEY_CUR_USER_ID)

        if (UserHelper.isTeacher()) {
            newsFacade.getLikedNews(year, uid)
        } else {
            val className = UserHelper.student!!.getClass(year)!!
            newsFacade.getLikedNewsStudent(year, uid, className)
        }
    }

    fun updateLike(newsId: String, isLike: Boolean) {
        view?.showProgressBar()
        val year = PreferenceHelper.getSafeString(DomainConsts.KEY_CUR_ACADEMIC_YR)
        val uid = PreferenceHelper.getSafeString(DomainConsts.KEY_CUR_USER_ID)

        if (UserHelper.isTeacher()) {
            newsFacade.updateNews(year, newsId, uid, isLike, object : RequestCallback<String, Error> {
                override fun onError(error: Error?) {}
                override fun onSuccess(res: String?) {
                    loadNews()
                }
            })
        } else {
            val className = UserHelper.student!!.getClass(year)!!
            newsFacade.updateStudentNews(year, newsId, uid, className, isLike,
                                         object : RequestCallback<String, Error> {
                                             override fun onError(error: Error?) {}
                                             override fun onSuccess(res: String?) {
                                                 loadNews()
                                             }
                                         })
        }

    }

}