package org.vervone.kidskonnect.ui.screens.messages

import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.UploadListener
import org.vervone.kidskonnect.core.data.network.Subscription
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.model.ChatMessage
import org.vervone.kidskonnect.core.helper.AppHelper
import org.vervone.kidskonnect.core.helper.Task
import org.vervone.kidskonnect.core.push.PushApi
import org.vervone.kidskonnect.ui.screens.base.BasePresenter
import org.vervone.kidskonnect.ui.utils.AppUtils

class MessageChatRoomPresenter : BasePresenter<MessageChatRoomView>() {


    private val messageFacde = FacadeFactory.messageFacade
    private val cloudStorage = AppHelper.getCloudStorage()
    /**
     *  Pending media messages.
     */
    private val pendingMessages = hashMapOf<ChatMessage, Task>()

    private var msgSubscription: Subscription? = null

    fun loadChatMessages(chatRoomId: String) {
        msgSubscription = messageFacde.getChatMessages(
                AppUtils.getCurrentAcademicYear(),
                chatRoomId, ChatCallback()
        )
    }

    fun stopMessageSubscription() {
        msgSubscription?.stop()
    }

    fun sendMessage(chatRoomId: String, chatMsg: ChatMessage) {
        val academicYr = AppUtils.getCurrentAcademicYear()
        val task = messageFacde.sendMessage(
                academicYr, chatRoomId,
                chatMsg, object : UploadListener<ChatMessage, Error> {
            override fun onProgress(percentage: Int) {
                chatMsg.uploadPercent = percentage
                DLog.v(this, "onProgress# : $percentage")
                //view?.reload()
            }

            override fun onError(error: Error?) {

            }

            override fun onSuccess(res: ChatMessage?) {
                PushApi.Factory.pushApi.publishMessage(chatRoomId, res?.senderName!!)
            }
        }
        )

        if (chatMsg.isMediaMessage()) {
            pendingMessages.put(chatMsg, task)
        }
    }

    /**
     *  Cancel a media message to being upload
     */
    fun cancelMessage(chatMsg: ChatMessage) {
        if (pendingMessages.containsKey(chatMsg)) {
            pendingMessages.remove(chatMsg)?.cancel()
        }
    }


    inner class ChatCallback : RequestCallback<List<ChatMessage>, Error> {

        override fun onSuccess(res: List<ChatMessage>?) {
            if (res != null) {
                val sorted = res.sortedBy { message -> message.time }
                view?.setChatMessages(sorted)
            } else {
                view?.setChatMessages(emptyList())
                //view?.showError("")
            }
        }

        override fun onError(error: Error?) {
            view?.showError("")
        }
    }

}