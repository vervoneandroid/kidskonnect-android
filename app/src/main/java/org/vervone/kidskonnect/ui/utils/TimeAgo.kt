package org.vervone.kidskonnect.ui.utils

import android.text.format.DateFormat
import java.util.*

object TimeAgo {

    private val SECOND_MILLIS = 1000
    private val MINUTE_MILLIS = 60 * SECOND_MILLIS
    private val HOUR_MILLIS = 60 * MINUTE_MILLIS

    fun getTimeAgo(timeInput: Long?): String {
        if (timeInput == null) return ""
        var time = timeInput
        if (time < 1000000000000L) {
            time *= 1000
        }

        val now = System.currentTimeMillis()
        if (time > now || time <= 0) {
            return ""
        }

        val diff = now - time
        return when {
            diff < MINUTE_MILLIS -> "just now"
            diff < 2 * MINUTE_MILLIS -> "a minute ago"
            diff < 50 * MINUTE_MILLIS -> (diff / MINUTE_MILLIS).toString() + " mins"
            diff < 90 * MINUTE_MILLIS -> "an hour ago"
            diff < 24 * HOUR_MILLIS -> (diff / HOUR_MILLIS).toString() + " hrs"
            else -> {
                val cal = Calendar.getInstance(Locale.ENGLISH)
                cal.timeInMillis = time
                DateFormat.format("dd MMM yyyy", cal).toString()
            }
        }
    }
}