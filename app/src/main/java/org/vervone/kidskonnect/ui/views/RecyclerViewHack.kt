package org.vervone.kidskonnect.ui.views

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet


/**
 *    Workaround for RecyclerView doesn't refresh the view until touch event.
 */
class RecyclerViewHack : RecyclerView {

    private var mRequestedLayout = false

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) :
            super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) :
            super(context, attrs, defStyle)

    @SuppressLint("WrongCall")
    override fun requestLayout() {
        super.requestLayout()
        if (!mRequestedLayout) {
            mRequestedLayout = true
            this.post {
                layout(left, top, right, bottom)
                onLayout(false, left, top, right, bottom)
            }
        }
    }

    fun forceRequestLayout() {
        mRequestedLayout = false
    }

}
