package org.vervone.kidskonnect.ui.screens.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.helper.SchoolConfigHelper
import org.vervone.kidskonnect.ui.utils.FragmentUtils
import org.vervone.kidskonnect.ui.views.FlipAnimation


class LoginActivity : AppCompatActivity() {

    companion object {
        private const val SHOW_TEACHER_LOGIN = "SHOW_TEACHER_LOGIN"
        fun start(context: Activity, showTeacher: Boolean = true) {
            val intent = Intent(context, LoginActivity::class.java)
            intent.putExtra(SHOW_TEACHER_LOGIN, showTeacher)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if (supportFragmentManager.findFragmentById(R.id.content_frame) == null) {

            var showTeacher = true
            if (intent.extras != null) {
                showTeacher = intent.getBooleanExtra(SHOW_TEACHER_LOGIN, true)
            }

            if (showTeacher) {
                val loginFragment = TeacherLoginFragment.newInstance(UserType.TEACHER)
                FragmentUtils.addFragment(supportFragmentManager, loginFragment, R.id.content_frame)
            } else {
                val loginFragment = StudentLoginFragment.newInstance(UserType.STUDENT)
                LoginFragment.setAnimDirection(FlipAnimation.RIGHT)
                FragmentUtils.replace(this, loginFragment, R.id.content_frame)
            }
        }

    }


    override fun onBackPressed() {
        FragmentUtils.clearBackStack(supportFragmentManager)
        super.onBackPressed()
    }


    fun flipCard() {
        if (supportFragmentManager.findFragmentById(R.id.content_frame) is StudentLoginFragment) {
            val loginFragment = TeacherLoginFragment.newInstance(UserType.TEACHER)
            LoginFragment.setAnimDirection(FlipAnimation.LEFT)
            FragmentUtils.replace(this, loginFragment, R.id.content_frame)
        } else {
            val loginFragment = StudentLoginFragment.newInstance(UserType.STUDENT)
            LoginFragment.setAnimDirection(FlipAnimation.RIGHT)
            FragmentUtils.replace(this, loginFragment, R.id.content_frame)
        }

    }

    override fun onDestroy() {
        LoginFragment.setAnim(false)
        super.onDestroy()
    }


}