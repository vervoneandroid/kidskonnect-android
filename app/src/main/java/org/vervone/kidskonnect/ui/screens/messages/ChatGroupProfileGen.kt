package org.vervone.kidskonnect.ui.screens.messages

import android.content.Context
import android.graphics.Bitmap
import org.vervone.kidskonnect.core.cloudstorage.CloudStorage
import org.vervone.kidskonnect.ui.views.MultiImageView

interface ChatGroupProfileGen {
    fun create(context: Context,
               storage: CloudStorage,
               userIdList: List<String>,
               targetId: String,
               pos: Int,
               iv: MultiImageView, observer: Observer): Result


    object Factory {
        fun getDefault(): ChatGroupProfileGen {
            return ImageProcessor()
        }
    }

    data class Result(var bitmaps: List<Bitmap>)


    interface Observer {
        fun reloadAt(pos: Int)
    }

}