package org.vervone.kidskonnect.ui.screens.messages


/**
 *
 */

object MessagesCountCache {

    private val messageCache = HashMap<String, Pair<Int, Int>>()
    private val observers = ArrayList<Observer>()


    fun put(key: String, count: Int, lastMsg: String) {
        // If count is equal to 0 , consider it as a new message channel
        // created by the current user , so ignore it .
        if (count == 0) return
        val msgCount = this.messageCache[key]
        if (msgCount == null) {
            this.messageCache[key] = Pair(count, 0)
        } else {
            val isChanged = (count - msgCount.first) > 0
            this.messageCache[key] = Pair(msgCount.first, count - msgCount.first)

            if (isChanged) {
                notifyObservers(key)
            }
        }
    }


    fun isEmpty(): Boolean {
        return this.messageCache.isEmpty()
    }


    fun clear() {
        messageCache.clear()
    }


    fun register(observer: Observer) {
        observers.add(observer)
    }

    fun unregister(observer: Observer) {
        observers.remove(observer)
    }

    private fun notifyObservers(msgChannelId: String) {
        observers.forEach { it.onChange(msgChannelId) }
    }

    private fun notifyObservers() {
        observers.forEach { it.onRemove() }
    }


    interface Observer {
        fun onChange(msgChannelId: String)
        fun onRemove()
    }

}