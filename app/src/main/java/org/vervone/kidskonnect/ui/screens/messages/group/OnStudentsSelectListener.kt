package org.vervone.kidskonnect.ui.screens.messages.group

import org.vervone.kidskonnect.core.domain.model.student.Student

interface OnStudentsSelectListener {
    fun onStudentsSelected(className: String, students: List<Student>)
}