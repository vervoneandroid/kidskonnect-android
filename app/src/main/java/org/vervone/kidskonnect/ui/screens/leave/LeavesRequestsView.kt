package org.vervone.kidskonnect.ui.screens.leave

import org.vervone.kidskonnect.core.domain.model.Leave
import org.vervone.kidskonnect.ui.screens.base.BaseView

interface LeavesRequestsView : BaseView {
    fun setData(leaves: List<Leave>)
}