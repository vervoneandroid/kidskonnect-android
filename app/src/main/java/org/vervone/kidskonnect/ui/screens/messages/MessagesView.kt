package org.vervone.kidskonnect.ui.screens.messages

import org.vervone.kidskonnect.core.domain.model.MessageChannel
import org.vervone.kidskonnect.ui.screens.base.BaseView

interface MessagesView : BaseView {
    fun setMessageChannels(list: List<MessageChannel>)
    fun refreshTable()
    fun showError()
}