package org.vervone.kidskonnect.ui.screens

interface BadgeUpdater {
    fun updateNewsCount()
    fun updateMessageCount()
    fun updateLeaveCount()
}