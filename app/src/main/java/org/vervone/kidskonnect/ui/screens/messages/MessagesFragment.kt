package org.vervone.kidskonnect.ui.screens.messages

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.data.network.model.student.capitalizeWords
import org.vervone.kidskonnect.core.domain.model.MessageChannel
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.utils.*
import org.vervone.kidskonnect.ui.views.MultiImageView


class MessagesFragment : BaseFragment(), MessagesView {


    private lateinit var rvMessageChannels: RecyclerView
    private var layoutManager: LinearLayoutManager? = null
    private lateinit var fab: FloatingActionButton
    private var msgPresenter: MessagesPresenter? = null
    private var dataSource: List<MessageChannel>? = null
    private val gene = ChatGroupProfileGen.Factory.getDefault()

    private var preselectedChannel: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.preselectedChannel = arguments?.getString(CHANNEL_ID)
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_messages
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.setTitle(R.string.messages)
        KeyboardUtil.hideSoftKeyboard(activity!!)
        this.rvMessageChannels = view.findViewById(R.id.rv_messages)
        this.fab = view.findViewById(R.id.fb_add_chat)
        layoutManager = LinearLayoutManager(activity)
        rvMessageChannels.layoutManager = layoutManager

        fab.setOnClickListener {

            FragmentUtils.replace(fragmentManager,
                                  CreateGroupFragment(),
                                  R.id.content_frame, true
            )

        }

        KeyboardUtil.setSoftInputAdjustResize(activity)

        val itemDecorator = DividerItemDecoration(context, layoutManager?.orientation!!)
        val divider = ContextCompat.getDrawable(context!!, R.drawable.divider_1dp)
        itemDecorator.setDrawable(divider!!)
        rvMessageChannels.addItemDecoration(itemDecorator)
        this.msgPresenter = MessagesPresenter()
        this.msgPresenter?.onAttach(this)
        showLoading()

        this.msgPresenter?.loadAllMessages()
    }


    override fun onResume() {
        super.onResume()
        this.msgPresenter?.subscribe()

    }

    override fun onPause() {
        this.msgPresenter?.unsubscribe()
        super.onPause()
    }


    override fun onDestroyView() {
        msgPresenter?.onDettach()
        msgPresenter?.unsubscribe()
        super.onDestroyView()
    }


    override fun setMessageChannels(list: List<MessageChannel>) {
        this.dataSource = list
        val messageAdapter = MsgAdapter()
        messageAdapter.setHasStableIds(true)
        rvMessageChannels.adapter = messageAdapter
        if (this.dataSource != null && this.dataSource!!.isEmpty()) {
            showErrorLayout(getString(R.string.no_messages_found))
        } else {
            showContentLayout()
        }


        if (preselectedChannel != null) {
            val selected = this.dataSource?.firstOrNull { it.id == this.preselectedChannel }
            if (selected != null) {
                showChatMessages(selected)
                preselectedChannel = null
            }
        }

    }

    private fun showChatMessages(msgChannel: MessageChannel) {
        val channelName = msgChannel.getChatName(UserHelper.getCurrentUser().uid!!)

        val fragment = MessageChatRoomFragment.createInstance(msgChannel.id!!,
                                                              msgChannel, channelName)
        FragmentUtils.replace(fragmentManager, fragment, R.id.content_frame, true)
    }

    override fun refreshTable() {
        rvMessageChannels.adapter?.notifyDataSetChanged()
    }

    override fun showError() {
        showErrorLayout(getString(R.string.no_messages_found))
    }

    private fun showChatMessages(chatRoomId: String, channelName: String, msgChannel: MessageChannel) {
        val fragment = MessageChatRoomFragment.createInstance(chatRoomId, msgChannel, channelName)
        FragmentUtils.replace(fragmentManager, fragment, R.id.content_frame, true)
    }

    private inner class MsgAdapter : RecyclerView.Adapter<VH>(), ChatGroupProfileGen.Observer {

        private val resultMap = hashMapOf<String, ChatGroupProfileGen.Result>()
        override fun onCreateViewHolder(parent: ViewGroup, p1: Int): VH {
            val view = LayoutInflater.from(context)
                .inflate(R.layout.kc_layout_msg_channel_row, parent, false)
            return VH(view)
        }

        override fun getItemCount(): Int {
            return dataSource?.size ?: 0
        }

        override fun reloadAt(pos: Int) {
            notifyItemChanged(pos)
        }

        override fun getItemId(position: Int): Long {
            return dataSource!![position].id.hashCode().toLong()
        }

        override fun onViewRecycled(holder: VH) {
            super.onViewRecycled(holder)
            holder.ivChatMembers.clear()
        }

        private fun createPlaceHolder(count: Int): List<Bitmap> {

            val bitmaps = arrayListOf<Bitmap>()
            for (x in 0 until count) {
                val defaultPro = BitmapFactory.decodeResource(context!!.resources, R.drawable.ic_profile_picture)
                bitmaps.add(defaultPro)
            }

            return bitmaps
        }

        override fun onBindViewHolder(vh: VH, pos: Int) {
            val messageCount = MessageCount()
            val msgChannel = dataSource!![pos]
            val channelName = msgChannel.getChatName(UserHelper.getCurrentUser().uid!!)
            vh.tvChannelName.text = channelName.capitalizeWords()
            val cloudStorage = getCloudStorage()!!
            val ctx = context!!
            val targetId = msgChannel.targetId!!
            var result = resultMap[targetId]
            if (result == null) {
                val keys = msgChannel.targets!!.keys.take(3)
                vh.ivChatMembers.tag = targetId
                vh.ivChatMembers.addImages(createPlaceHolder(keys.size))
                result = gene.create(ctx, cloudStorage, ArrayList(keys),
                                     targetId, pos, vh.ivChatMembers, this)
                resultMap[targetId] = result
            } else {
                vh.ivChatMembers.addImages(result.bitmaps)
            }
            val chatMessage = msgPresenter?.getLastMessages(msgChannel.id!!)
            if (chatMessage != null) {
                vh.tvTime.text = TimeAgo.getTimeAgo(chatMessage.time)

                if (msgChannel.isGroupMessage != null && msgChannel.isGroupMessage!!) {
                    vh.tvLastChat.text = format(chatMessage.message, chatMessage.senderName)
                } else {
                    vh.tvLastChat.text = format(chatMessage.message)
                }
            }

            vh.view.setOnClickListener {
                val channelId = msgChannel.id!!
                showChatMessages(channelId, channelName, msgChannel)
                messageCount.markAsRead(channelId)
            }
            vh.tvNotifCount.extHide()
            msgChannel.id?.let { channelId ->
                val unread = messageCount.getUnreadCount(channelId)
                if (unread > 0) {
                    vh.tvNotifCount.text = unread.toString()
                    vh.tvNotifCount.extShow()
                }
            }

        }

        private fun format(message: String?, member: String?): String {
            return "$member : $message"
        }

        private fun format(message: String?): String {
            return "$message"
        }

    }

    private class VH(var view: View) : RecyclerView.ViewHolder(view) {
        var tvChannelName: TextView = view.findViewById(R.id.tv_chat_name)
        var tvLastChat: TextView = view.findViewById(R.id.tv_last_msg)
        var tvTime: TextView = view.findViewById(R.id.tv_time)
        var ivChatMembers: MultiImageView = view.findViewById(R.id.iv_profile_pic)
        var tvNotifCount: TextView = view.findViewById(R.id.tv_notif_count)
    }


    companion object {

        private const val CHANNEL_ID = "channel_id"

        fun createInstance(channelId: String?): MessagesFragment {
            val args = Bundle()
            args.putString(CHANNEL_ID, channelId)

            val fragment = MessagesFragment()
            fragment.arguments = args

            return fragment
        }
    }

}