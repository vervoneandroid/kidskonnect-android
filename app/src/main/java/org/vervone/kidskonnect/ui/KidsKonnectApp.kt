package org.vervone.kidskonnect.ui

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.multidex.MultiDexApplication
import com.amazonaws.mobileconnectors.s3.transferutility.TransferNetworkLossHandler
import com.glidebitmappool.GlideBitmapPool
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.cloudstorage.CloudStorage
import org.vervone.kidskonnect.core.data.network.Errors
import org.vervone.kidskonnect.core.domain.ConfigFacade
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.model.AppVersion
import org.vervone.kidskonnect.core.helper.AppDelegate
import org.vervone.kidskonnect.core.helper.AppHelper
import org.vervone.kidskonnect.core.helper.cast
import org.vervone.kidskonnect.ui.screens.CloudStorageDelegate
import org.vervone.kidskonnect.ui.utils.AppUtils
import org.vervone.kidskonnect.ui.utils.DialogFactory


class KidsKonnectApp : MultiDexApplication(), AppDelegate {


    companion object {
        private val TAG = KidsKonnectApp::class.java.simpleName
    }


    private lateinit var lifeCycleDelegate: LifeCycleDelegate

    init {
        DLog.v(this, "KidsKonnectApp object created")
    }


    override fun onCreate() {
        super.onCreate()
        registerNetworkLossHandler()
        AppHelper.init(this)
        Errors.init()

        this.lifeCycleDelegate = LifeCycleDelegate()
        registerActivityLifecycleCallbacks(lifeCycleDelegate)

        GlideBitmapPool.initialize(10 * 1024 * 1024)
        //getCloudStorage()?.deleteFileFromCloud("")
    }


    private fun registerNetworkLossHandler() {
        try {
            val transferNetworkLossHandler = TransferNetworkLossHandler.getInstance(this)
            this.registerReceiver(transferNetworkLossHandler, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        } catch (iae: IllegalArgumentException) {
        } catch (ise: IllegalStateException) {
        }
    }

    /**
     *  Return the current activity
     */
    override fun getActivity(): Activity? {
        return this.lifeCycleDelegate.currentActivity
    }

    override fun getCloudStorage(): CloudStorage? {
        if (getActivity() is CloudStorageDelegate) {
            val delegate = getActivity()?.cast<CloudStorageDelegate>()
            return delegate?.getCloudStorage()
        }
        DLog.e(this,
               "getCloudStorage() return null ${getActivity()}",
               Exception("CloudStorageDelegate"))
        return null
    }

    override fun getAppContext(): Context {
        return this
    }

    private class LifeCycleDelegate : ActivityLifecycleCallbacks {

        private val resumed: Int = 0
        private val paused: Int = 0
        private val started: Int = 0
        private val stopped: Int = 0


        var currentActivity: Activity? = null
            private set

        override fun onActivityPaused(activity: Activity?) {
            this.currentActivity = activity
            paused.inc()
        }

        override fun onActivityResumed(activity: Activity?) {
            this.currentActivity = activity
            resumed.inc()
        }

        override fun onActivityStarted(activity: Activity?) {
            started.inc()
        }

        override fun onActivityDestroyed(activity: Activity?) {

        }

        override fun onActivitySaveInstanceState(activity: Activity?, p1: Bundle?) {

        }

        override fun onActivityStopped(activity: Activity?) {
            stopped.inc()
        }

        override fun onActivityCreated(activity: Activity?, p1: Bundle?) {

        }

        fun isApplicationVisible(): Boolean {
            return started > stopped
        }


        fun isApplicationInForeground(): Boolean {
            return resumed > paused
        }
    }


    fun subscribeAppVersion() {
        FacadeFactory.configFacade.subscribeAppVersionConfig(versionCallback)
    }

    private var versionCallback = object : ConfigFacade.VersionListener {
        override fun onVersionChange(appVersions: AppVersion) {
            if (appVersions.rejected && getActivity() != null) {
                val dialog = DialogFactory.createGenericErrorDialog(
                        getActivity()!!, getString(R.string.update_app),
                        DialogInterface.OnClickListener { _, _ ->
                            AppUtils.restartApp()
                        })
                dialog.setCancelable(false)
                dialog.show()
            }
        }
    }


}