package org.vervone.kidskonnect.ui.screens.messages

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.Callback
import org.vervone.kidskonnect.core.data.network.MessageCallback
import org.vervone.kidskonnect.core.domain.DomainError
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.model.ChatMessage
import org.vervone.kidskonnect.core.domain.model.MessageChannel
import org.vervone.kidskonnect.core.domain.model.StudentParamsBm
import org.vervone.kidskonnect.core.domain.model.TeacherParamsBm
import org.vervone.kidskonnect.ui.screens.base.BasePresenter
import org.vervone.kidskonnect.ui.utils.AppUtils
import org.vervone.kidskonnect.ui.utils.UserHelper
import java.util.concurrent.atomic.AtomicInteger

class MessagesPresenter : BasePresenter<MessagesView>() {

    private val schoolFacade = FacadeFactory.schoolFacade
    private val messageFacde = FacadeFactory.messageFacade
    private val lastMessageMap = hashMapOf<String, ChatMessage>()
    private val msgChannelsMap = HashMap<String, MessageChannel>()
    private val messageChannels = ArrayList<MessageChannel>()

    private var channelSubsId: String? = null
    private var isReqPending: Boolean = false
    private var messagesSubsId: String? = null


    fun subscribe() {
        val academicYr = AppUtils.getCurrentAcademicYear()
        val userId = UserHelper.getCurrentUser().uid!!
        if (UserHelper.isTeacher()) {
            val classes = UserHelper.teacher?.classesAndSubjects?.keys ?: listOf<String>()
            val teacherParamsBm = TeacherParamsBm(academicYr, UserHelper.isAdmin(), ArrayList(classes), userId)
            channelSubsId = messageFacde.subscribeForTeacherMessageChannels(teacherParamsBm, mcSubscriber)
            messagesSubsId = messageFacde.subscribeForTeacherMessages(teacherParamsBm, msgObserver)
        } else {
            val className = UserHelper.student?.getClass(academicYr)!!
            val studentParamsBm = StudentParamsBm(academicYr, className, userId)
            channelSubsId = messageFacde.subscribeForStudentMessageChannels(studentParamsBm, mcSubscriber)
            messagesSubsId = messageFacde.subscribeForStudentMessage(studentParamsBm, msgObserver)
        }
    }

    fun unsubscribe() {
        messageFacde.unsubscribe(channelSubsId)
        messageFacde.unsubscribe(messagesSubsId)
    }

    private val mcSubscriber = object : Callback {
        override fun onDataChange() {
            if (!isReqPending) {
                loadAllMessages()
            }
        }
    }


    private val msgObserver = object : MessageCallback {
        override fun onChange(msgChannelId: String, msgCount: Int) {
            val mc = msgChannelsMap[msgChannelId]
            if (mc != null) {
                Update(mc).execute()
            }
        }

        override fun onMessage(chatMessage: ChatMessage) {

        }
    }

    /*   private val msgCountObserver = object : MessagesCountCache.Observer {
           override fun onSnapshot(msgChannelId: String) {
               val mc = msgChannelsMap[msgChannelId]
               if (mc != null) {
                   Update(mc).execute()
               }
           }

           override fun onRemove() {

           }
       }*/

    private inner class Update(val msgChannel: MessageChannel) :
        RequestCallback<HashMap<String, ChatMessage>, DomainError> {

        override fun onSuccess(res: HashMap<String, ChatMessage>?) {
            lastMessageMap.putAll(res!!)
            if (res[msgChannel.id] != null) {
                msgChannel.lastMessageTime = res[msgChannel.id]?.time!!
            }
            messageChannels.sortByDescending { it.lastMessageTime }
            view?.refreshTable()
        }

        override fun onError(error: DomainError?) {}
        fun execute() {
            messageFacde.getLastMessage(AppUtils.getCurrentAcademicYear(),
                                        msgChannel.id!!, this)
        }
    }

    fun loadAllMessages() {
        val academicYr = AppUtils.getCurrentAcademicYear()
        val userId = UserHelper.getCurrentUser().uid!!
        this.isReqPending = true

        if (UserHelper.isAdmin()) {
            fetchAllClasses()
        } else if (UserHelper.isTeacher()) {
            val classes = UserHelper.teacher?.classesAndSubjects?.keys
            if (classes != null && classes.isNotEmpty()) {
                fetchMessages(classes.toList())
            } else {
                view?.showError()
            }
        } else {
            val className = UserHelper.student?.getClass(academicYr)!!
            messageFacde.getStudentChatRooms(academicYr, userId, className, MsgCallback())
        }
    }

    private fun fetchMessages(classes: List<String>) {
        val academicYr = AppUtils.getCurrentAcademicYear()
        val userId = UserHelper.getCurrentUser().uid!!
        messageFacde.getAllChatRooms(academicYr, userId
                                     , ArrayList(classes), MsgCallback())
    }


    private fun fetchAllClasses() {
        schoolFacade.getClasses(object : RequestCallback<List<String>, DomainError> {
            override fun onError(error: DomainError?) {}
            override fun onSuccess(res: List<String>?) {
                if (res != null && res.isNotEmpty()) {
                    fetchMessages(res.toList())
                } else {
                    view?.showError()
                }
            }
        })
    }


    fun getLastMessages(chatRoomId: String): ChatMessage? {
        return lastMessageMap[chatRoomId]
    }


    /**
     *  fetch last messages for the the specific channel ids .
     *
     *   For showing the last message time and message .
     *
     * */
    private inner class LastMessagesCallback(val messageChannel: MessageChannel,
                                             val callback: SyncMessages) :
        RequestCallback<HashMap<String, ChatMessage>, DomainError> {


        override fun onSuccess(res: HashMap<String, ChatMessage>?) {
            lastMessageMap.putAll(res!!)

            if (res[messageChannel.id] != null) {
                messageChannel.lastMessageTime = res[messageChannel.id]?.time!!
            }
            callback.dataReceived()

        }

        fun execute() {
            messageFacde.getLastMessage(AppUtils.getCurrentAcademicYear(),
                                        messageChannel.id!!, this)
        }


        override fun onError(error: DomainError?) {
            callback.dataReceived()
        }


    }

    private inner class SyncMessages(var messageChannels: ArrayList<MessageChannel>?) {

        val reqCnt = AtomicInteger(messageChannels?.size ?: 0)

        init {
            messageChannels?.forEach { LastMessagesCallback(it, this).execute() }
        }


        fun dataReceived() {
            if (reqCnt.decrementAndGet() == 0) {
                messageChannels?.sortByDescending { it.lastMessageTime }
                view?.setMessageChannels(messageChannels!!)
            }
        }

    }


    private inner class MsgCallback : RequestCallback<List<MessageChannel>, Error> {

        override fun onSuccess(res: List<MessageChannel>?) {
            isReqPending = false
            if (res != null) {
                if (res.isEmpty()) {
                    view?.setMessageChannels(listOf())
                } else {
                    val mc = res.associateBy { it.id ?: "" }
                    msgChannelsMap.putAll(mc)
                    messageChannels.clear()
                    messageChannels.addAll(res)
                    SyncMessages(messageChannels)
                }

            } else {
                view?.showError()
            }
        }

        override fun onError(error: Error?) {
            isReqPending = false
            view?.showError()
        }
    }
}