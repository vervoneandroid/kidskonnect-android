package org.vervone.kidskonnect.ui.utils

import android.os.Bundle
import org.vervone.kidskonnect.core.domain.model.student.CommentType
import org.vervone.kidskonnect.core.push.NotificationType
import org.vervone.kidskonnect.core.push.PushApiConst
import org.vervone.kidskonnect.ui.notification.UiNotification

class NotificationMapper {

    fun map(extras: Bundle): UiNotification {
        val type = extras.getString(type, null)
        return when (val notificationType = NotificationType.fromValue(type)) {
            NotificationType.NEW_MESSAGE_EXISTING_GROUP -> {
                val notification = UiNotification()
                notification.type = notificationType
                notification.groupId = extras.getString(groupId, null)
                notification
            }

            NotificationType.NEW_COMMENT -> {
                val notification = UiNotification()
                notification.type = notificationType
                notification.className = extras.getString(PushApiConst.CLASS_NAME, null)
                notification.studentId = extras.getString(PushApiConst.STUDENT_ID, null)
                notification.studentName = extras.getString(PushApiConst.STUDENT_NAME, null)
                val commentType = extras.getString(PushApiConst.COMMENT_TYPE, "")
                notification.commentType = CommentType.fromInt(commentType.toIntOrNull())
                notification
            }

            else -> {
                val notification = UiNotification()
                notification.type = notificationType
                notification
            }
        }
    }

    companion object {
        private const val type = "type"
        private const val groupId = "groupId"
        private const val class_name = "class_name"
        private const val student_id = "student_id"
        private const val student_name = "student_name"
        private const val comment_type = "commentType"
    }
}