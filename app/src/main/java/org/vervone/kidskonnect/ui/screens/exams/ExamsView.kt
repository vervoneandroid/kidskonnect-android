package org.vervone.kidskonnect.ui.screens.exams

import org.vervone.kidskonnect.core.domain.model.Exam
import org.vervone.kidskonnect.ui.screens.base.BaseView

interface ExamsView : BaseView {
    fun setData(examList: List<Exam>)
    fun showError()
}