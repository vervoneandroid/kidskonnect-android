package org.vervone.kidskonnect.ui.notification

import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.helper.ThreadUtils

object NotificationHelper {


    fun register(listener: OnCompleteListener) {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    DLog.w(TAG, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token
                DLog.i(TAG, "fcm token  $token")

                listener.onComplete()
            })
    }


    fun subscribe(topic: String) {

        FirebaseMessaging.getInstance().subscribeToTopic(topic)
            .addOnCompleteListener { task ->
                if (!task.isSuccessful) {
                    return@addOnCompleteListener
                }
                DLog.i(TAG, "subscribe to  $topic")
            }
    }


    fun unsubscribe(topic: String) {
        FirebaseMessaging.getInstance().unsubscribeFromTopic(topic)
            .addOnCompleteListener { task ->
                if (!task.isSuccessful) {
                }
            }
    }

    fun unsubscribeAll() {
        ThreadUtils.runOnBg {
            try {
                FirebaseInstanceId.getInstance().deleteInstanceId()
            } catch (ex: Exception) {
                DLog.w(this, "unsubscribeAll()", ex)
            }
        }
    }


    interface OnCompleteListener {
        fun onComplete()
    }

    private const val TAG = "NotificationHelper"
    // topics
    const val COMMON_NOTIFICATIONS = "commonNotifications"
    const val NOTIFICATION_TOPIC = "notificationTopic-"
    const val INITIAL_SUBSCRIPTION = "initialSubscrption-"

}