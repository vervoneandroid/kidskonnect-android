package org.vervone.kidskonnect.ui.screens.news

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.ui.screens.base.BaseDialogFragment
import org.vervone.kidskonnect.ui.utils.DialogFactory

class TargetsDialog : BaseDialogFragment(), TargetsView {

    private lateinit var rvOptions: RecyclerView
    private var targetPresenter: TargetsPresenter? = null
    var listener: Listener? = null
    private var optionList: ArrayList<CheckedItem>? = null
    private lateinit var tvSelectAll: TextView

    companion object {

        private const val ITEMS = "items"

        fun createInstance(list: ArrayList<CheckedItem>): TargetsDialog {
            val fragment = TargetsDialog()
            val bundle = Bundle()
            bundle.putSerializable(ITEMS, list)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.optionList = arguments?.get(ITEMS) as ArrayList<CheckedItem>
    }

    override fun getLayoutId(): Int {
        return R.layout.kc_dialog_targets
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window?.setLayout(width, height)
        }
    }

    private fun updateTextView() {
        val text = if (!isAllSelected()) R.string.select_all else R.string.deselect_all
        tvSelectAll.setText(text)
    }

    private fun isAllSelected(): Boolean {
        var allSelected = true
        for (item in optionList!!) {
            if (!item.isChecked) {
                allSelected = false
            }
        }
        return allSelected
    }

    private fun isAtLeastOne(): Boolean {
        if (optionList == null) return false
        for (item in optionList!!) {
            if (item.isChecked) return true
        }
        return false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.rvOptions = view.findViewById(R.id.rv_options)
        this.tvSelectAll = view.findViewById(R.id.tv_select_all)
        view.findViewById<Button>(R.id.btn_done).setOnClickListener {

            if (isAtLeastOne()) {
                listener?.onOptionSelected(ArrayList(this.optionList))
                dismiss()
            } else {
                DialogFactory.createSimpleOkErrorDialog(
                        activity!!, null,
                        getString(R.string.please_select_at_least_one_target)).show()
            }
        }
        view.findViewById<Button>(R.id.btn_cancel).setOnClickListener {
            dismiss()
        }
        updateTextView()
        tvSelectAll.setOnClickListener {

            if (isAllSelected()) {
                this.optionList?.forEach { it.isChecked = false }
            } else {
                this.optionList?.forEach { it.isChecked = true }
            }
            rvOptions.adapter?.notifyDataSetChanged()
            updateTextView()
        }

        val layoutManager = LinearLayoutManager(activity)
        rvOptions.layoutManager = layoutManager

        this.targetPresenter = TargetsPresenter()
        this.targetPresenter?.onAttach(this)
        //this.targetPresenter?.fetchSchools()

        rvOptions.adapter = Adapter(optionList!!)

    }

    override fun showProgressBar() {

    }

    override fun hideProgressBar() {

    }

    override fun showError(message: String) {

    }

    override fun setData(data: List<String>?) {
        data?.let {
            //rvOptions.adapter = Adapter(it)
        }

    }

    override fun onDestroyView() {
        this.targetPresenter?.onDettach()
        super.onDestroyView()
    }

    private inner class Adapter(val list: List<CheckedItem>) : RecyclerView.Adapter<VH>() {

        override fun onCreateViewHolder(vg: ViewGroup, pos: Int): VH {
            val inf = LayoutInflater.from(context)
            return VH(inf.inflate(R.layout.kc_checkbok_layout, vg, false))
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onBindViewHolder(vh: VH, pos: Int) {
            val item = list[pos]
            vh.tvOption.text = item.name
            vh.cbOption.isChecked = item.isChecked

            vh.cbOption.setOnCheckedChangeListener { _, b ->
                item.isChecked = b
                updateTextView()
            }
        }

    }

    private inner class VH(val view: View) : RecyclerView.ViewHolder(view) {
        var tvOption: TextView
        var cbOption: CheckBox

        init {
            tvOption = view.findViewById(R.id.tv_option)
            cbOption = view.findViewById(R.id.cb_option)

            view.setOnClickListener {
                DLog.v(this, "Option selected")
                cbOption.isChecked = !cbOption.isChecked
            }
        }

    }

    /**
     *
     */
    interface Listener {
        fun onOptionSelected(selected: List<CheckedItem>)
    }

}