package org.vervone.kidskonnect.ui.screens.leave

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.text.Spanned
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.data.network.model.student.capitalizeWords
import org.vervone.kidskonnect.core.domain.model.Leave
import org.vervone.kidskonnect.core.domain.model.LeaveStatus
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.utils.*


class LeaveRequestsFragment : BaseFragment(), LeavesRequestsView, ReplyLeaveFragment.SuccessListener {


    private lateinit var rvLeaves: RecyclerView
    private var presenter: LeavesRequestsPresenter? = null
    private var leaves: List<Leave>? = null


    override fun setData(leaves: List<Leave>) {
        this.rvLeaves.adapter = null
        this.leaves = leaves
        if (this.rvLeaves.adapter == null) {
            this.rvLeaves.adapter = Adapter()
        } else {
            this.rvLeaves.adapter?.notifyDataSetChanged()
        }

        if (leaves.isEmpty()) {
            showErrorLayout(getString(R.string.no_leaves_found))
        } else {
            showContentLayout()
        }
    }


    override fun getLayoutId(): Int {
        return R.layout.fragment_leaves
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        this.rvLeaves = view.findViewById(R.id.rv_leaves)
        val layoutManager = LinearLayoutManager(activity!!)
        rvLeaves.layoutManager = layoutManager
        layoutManager.reverseLayout = true
        layoutManager.stackFromEnd = true
        activity?.setTitle(R.string.leaves)

        val itemDecorator = DividerItemDecoration(context, layoutManager.orientation)
        val divider = ContextCompat.getDrawable(context!!, R.drawable.divider_2dp)
        itemDecorator.setDrawable(divider!!)
        rvLeaves.addItemDecoration(itemDecorator)

        this.presenter = LeavesRequestsPresenter()
        this.presenter?.onAttach(this)
        showLoading()

        this.presenter?.getLeaves()
        this.presenter?.subscribe()

        LeaveCount().markAsRead()

    }

    override fun onSuccess() {
        this.presenter?.getLeaves()
    }

    override fun onDestroyView() {
        presenter?.onDettach()
        this.presenter?.unsubscribe()
        super.onDestroyView()
    }


    // VH
    private class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var ivProfile: ImageView
        var tvStudentName: TextView
        var tvRequestDate: TextView
        var tvStartEndDate: TextView
        var tvReqStaus: TextView
        var tvReason: TextView

        var tvTeacherName: TextView
        var tvReply: TextView

        var ivCollapse: ImageView

        var btnAccept: Button
        var btnReject: Button

        var layoutPanel: View
        var layoutReplyPanel: View
        var divider: View

        init {

            ivProfile = itemView.findViewById(R.id.iv_profile_pic)
            tvStudentName = itemView.findViewById(R.id.tv_student_name)
            tvRequestDate = itemView.findViewById(R.id.tv_requested_date)
            tvStartEndDate = itemView.findViewById(R.id.tv_start_to_end_date)
            tvReqStaus = itemView.findViewById(R.id.tv_req_status)
            tvReason = itemView.findViewById(R.id.tv_leave_reason)

            tvTeacherName = itemView.findViewById(R.id.tv_teacher_name)
            tvReply = itemView.findViewById(R.id.tv_reply)

            // ivExapand = itemView.findViewById(R.id.iv_expand)
            ivCollapse = itemView.findViewById(R.id.iv_collapse)

            btnAccept = itemView.findViewById(R.id.btn_accept_leave)
            btnReject = itemView.findViewById(R.id.btn_reject_leave)

            layoutPanel = itemView.findViewById(R.id.layout_panel)
            layoutReplyPanel = itemView.findViewById(R.id.layout_reply_panel)
            divider = itemView.findViewById(R.id.divider)

        }

    }


    private inner class Adapter : RecyclerView.Adapter<VH>() {

        override fun onCreateViewHolder(vg: ViewGroup, pos: Int): VH {
            val view = vg.context.inflater().inflate(R.layout.kc_leave_details_row, vg, false)
            return VH(view)
        }

        override fun getItemCount(): Int {
            return leaves?.size ?: 0
        }

        override fun onBindViewHolder(vh: VH, pos: Int) {
            val leave = leaves?.get(pos)!!

            vh.tvStudentName.text = leave.studentName?.capitalizeWords()
            vh.tvReason.text = leave.reason
            vh.tvReqStaus.text = getStatus(leave.leaveStatus)
            vh.tvStartEndDate.text = getFormatedDate(leave)
            vh.tvRequestDate.text = leave.sendDate

            vh.tvStartEndDate.setOnClickListener {
                val selected: ArrayList<String> = ArrayList()
                if (leave.leaveDays != null) {
                    val list = leave.leaveDays!!.filterNotNull()
                    selected.addAll(list)
                }
                val datePickerDialog = LeaveCalenderFragment
                    .createInstance(selected)
                datePickerDialog.show(childFragmentManager, LeaveCalenderFragment.TAG)
            }

            toggle(leave, vh)

            vh.ivCollapse.setOnClickListener {
                leave.isExpanded = !leave.isExpanded
                toggle(leave, vh)
            }

            vh.tvTeacherName.text = getString(R.string.message_from, leave.teacherName)
            vh.tvReply.text = leave.replyString

            ImageLoader.get(
                    Urls.getProfileUrl(leave.studentId!!),
                    R.drawable.ic_profle,
                    R.drawable.ic_profle,
                    vh.ivProfile
            )

            vh.btnAccept.setOnClickListener {
                val replyFragment = ReplyLeaveFragment
                    .createInstance(leave, LeaveStatus.APPROVED)
                replyFragment.show(childFragmentManager, ReplyLeaveFragment.TAG)
            }

            vh.btnReject.setOnClickListener {
                val replyFragment = ReplyLeaveFragment
                    .createInstance(leave, LeaveStatus.REJECTED)
                replyFragment.show(childFragmentManager, ReplyLeaveFragment.TAG)
            }
        }

        fun toggle(leave: Leave, vh: VH) {
            if (leave.isExpanded) {

                vh.ivCollapse.setImageResource(R.drawable.baseline_expand_less_black_36)
                vh.tvReason.setSingleLine(false)

                if (leave.leaveStatus == LeaveStatus.PENDING) {
                    vh.layoutPanel.extShow()
                    vh.layoutReplyPanel.extHide()
                } else {
                    vh.layoutReplyPanel.extShow()
                    vh.layoutPanel.extHide()
                }


            } else {
                vh.ivCollapse.setImageResource(R.drawable.baseline_expand_more_black_36)
                vh.layoutPanel.extHide()
                vh.layoutReplyPanel.extHide()
                vh.tvReason.setSingleLine(true)
            }

            vh.divider.visibility = vh.layoutReplyPanel.visibility
        }


        fun getInt(value: String?): Int {
            if (value?.toIntOrNull() != null) {
                return value.toIntOrNull()!!
            }
            return 0
        }

        private fun getFormatedDate(leave: Leave): Spanned? {
            val suffix = if (getInt(leave.noOfDays) > 1) "s" else ""
            val startEndDate = "${leave.leaveStartsFrom} - ${leave.leaveEndsAt}"
            val styledText = "$startEndDate <font color='#3E9FB2'>( <u>${leave.noOfDays} day$suffix</u> )</font>"
            return Html.fromHtml(styledText)
        }

        private fun getStatus(status: LeaveStatus): Spanned? {
            var statusValue = ""
            var color = ""
            when (status) {
                LeaveStatus.PENDING -> {
                    statusValue = resources.getString(R.string.pending)
                    color = "#64ddf5"
                }
                LeaveStatus.APPROVED -> {
                    statusValue = resources.getString(R.string.approved)
                    color = "#4CDA65"
                }
                LeaveStatus.REJECTED -> {
                    statusValue = resources.getString(R.string.rejected)
                    color = "#FF3B2F"
                }
            }
            val styledText = "<font color='$color'>$statusValue</font>"
            return Html.fromHtml(styledText)
        }

    }

}