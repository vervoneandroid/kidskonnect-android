package org.vervone.kidskonnect.ui.screens.news

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.domain.DomainConsts
import org.vervone.kidskonnect.core.domain.model.ContentType
import org.vervone.kidskonnect.core.domain.model.News
import org.vervone.kidskonnect.core.helper.PreferenceHelper
import org.vervone.kidskonnect.ui.utils.UserHelper
import org.vervone.mediapicker.Image.ImagePicker
import org.vervone.mediapicker.Video.VideoPicker
import java.util.*


class CreateMediaNewsFragment : BaseCreateNewsFragment(), CreateMediaNewsView, TargetsDialog.Listener {

    override fun getLayoutId(): Int {
        return R.layout.kc_fragment_create_media_news
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.btnAddAttachment = view.findViewById(R.id.btn_add_attachment)
        this.btnAddAttachment.setOnClickListener {
            val fragment = MediaOptionsBottomSheet()
            fragment.show(childFragmentManager, null)
        }
        this.layoutAttachments = view.findViewById(R.id.layout_attachments)
    }

    override fun isValid(): Boolean {
        return tvAddTarget.text.isNotEmpty() && etDescription.text.trim().isNotEmpty()
    }

    override fun createNews(): News {
        val news = News()
        val uid = PreferenceHelper.getString(DomainConsts.KEY_CUR_USER_ID)
        news.date = Date().time
        news.senderId = uid
        news.description = etDescription.text.toString()
        news.contentType = ContentType.TEXT
        news.senderName = UserHelper.getCurrentUser().name
        val targets = targets?.filter { it.isChecked }!!.map { it.name }
        news.targetList = targets
        news.attachments = attachments
        return news
    }

    override fun handleOnActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {

        var handled = false

        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == AppCompatActivity.RESULT_OK) {
            val paths = data?.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH)
            if (!paths.isNullOrEmpty()) {
                handleImageUrl(paths[0])
            }

            handled = true

            DLog.v(this, "image.paths $paths")
        } else if (requestCode == VideoPicker.VIDEO_PICKER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val paths = data?.getStringArrayListExtra(VideoPicker.EXTRA_VIDEO_PATH)
            DLog.v(this, "path $paths")

            handleVideoUrl(paths?.get(0)!!)

            handled = true
        }

        return handled
    }

}