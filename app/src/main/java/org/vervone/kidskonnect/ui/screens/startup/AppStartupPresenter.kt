package org.vervone.kidskonnect.ui.screens.startup

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.ConnectionHelper
import org.vervone.kidskonnect.core.domain.DomainConsts
import org.vervone.kidskonnect.core.domain.DomainError
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.model.AppVersion
import org.vervone.kidskonnect.core.domain.model.ConfigData
import org.vervone.kidskonnect.core.domain.model.school.SchoolConfig
import org.vervone.kidskonnect.core.helper.AppHelper
import org.vervone.kidskonnect.core.helper.PreferenceHelper
import org.vervone.kidskonnect.ui.KidsKonnectApp
import org.vervone.kidskonnect.ui.screens.base.BasePresenter

class AppStartupPresenter : BasePresenter<AppStartupView>() {

    private val configFacade = FacadeFactory.configFacade
    private val schoolFacade = FacadeFactory.schoolFacade

    private fun retrieveConfig() {
        //DLog.v(this, "retrieveConfig calling ...")
        view?.showLoading()
        val schoolId = PreferenceHelper.getSafeString(DomainConsts.KEY_CURRENT_SCHOOL_ID)
        schoolFacade.getSchoolConfigs(schoolId, object : RequestCallback<SchoolConfig, DomainError> {
            override fun onError(error: DomainError?) {
                view?.dismissLoading()
                view?.showConnectionError()
            }

            override fun onSuccess(res: SchoolConfig?) {
                if (res != null) {
                    ConnectionHelper.initialize(res)
                    checkRequiredAppVersion()
                } else {
                    view?.dismissLoading()
                    view?.showConnectionError()
                }
            }
        })
    }


    fun startApp(config: SchoolConfig?) {
        if (config == null) {
            retrieveConfig()
        } else {
            view?.showLoading()
            ConnectionHelper.initialize(config)
            checkRequiredAppVersion()
        }
    }


    private fun checkRequiredAppVersion() {
        configFacade.requiredAppVersions(object : RequestCallback<AppVersion, DomainError> {
            override fun onError(error: DomainError?) {
                view?.dismissLoading()
                view?.showConnectionError()
            }

            override fun onSuccess(res: AppVersion?) {
                val needToUpdate = (res?.rejected == true)
                if (needToUpdate) {
                    view?.dismissLoading()
                    view?.showAppUpdate()
                } else {
                    checkAcademicYear()
                    subsribeAppVersion()
                }
            }
        })
    }

    private fun subsribeAppVersion() {
        val kidsKonnectApp = AppHelper.get() as KidsKonnectApp
        kidsKonnectApp.subscribeAppVersion()
    }

    private fun checkAcademicYear() {
        configFacade.syncAcademicYear(object : RequestCallback<ConfigData, Error> {
            override fun onError(error: Error?) {
                view?.dismissLoading()
            }

            override fun onSuccess(res: ConfigData?) {
                view?.dismissLoading()
                view?.showNextScreen()
            }
        })
    }
}