package org.vervone.kidskonnect.ui.screens.leave

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.DomainError
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.model.Leave
import org.vervone.kidskonnect.ui.screens.base.BasePresenter
import org.vervone.kidskonnect.ui.utils.AppUtils

class CreateLeaveRequestPresenter : BasePresenter<CreateLeaveRequestView>() {

    private val schoolFacade = FacadeFactory.schoolFacade

    fun createLeaveRequest(leave: Leave, teacherId: String) {
        view?.showProgress()
        val academicYr = AppUtils.getCurrentAcademicYear()
        schoolFacade.createLeaveRequest(academicYr, teacherId,
                                        leave, leaveRequestCallback)
    }


    private val leaveRequestCallback = object : RequestCallback<Leave, DomainError> {
        override fun onError(error: DomainError?) {
            view?.showError()
        }

        override fun onSuccess(res: Leave?) {
            view?.createLeaveRequestSuccess()
        }
    }


}