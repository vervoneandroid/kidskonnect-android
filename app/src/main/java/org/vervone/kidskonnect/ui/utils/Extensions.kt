package org.vervone.kidskonnect.ui.utils

import android.content.Context
import android.content.res.Resources
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.vervone.kidskonnect.R

fun ViewGroup.children() = object : Iterable<View> {
    override fun iterator() = object : Iterator<View> {
        var index = 0
        override fun hasNext() = index < childCount
        override fun next() = getChildAt(index++)
    }
}

fun View?.extInvisible() {
    this?.visibility = View.INVISIBLE
}

fun View?.extHide() {
    this?.visibility = View.GONE
}

fun View?.extShow() {
    this?.visibility = View.VISIBLE
}


fun Context.inflater(): LayoutInflater {
    return LayoutInflater.from(this)
}

fun RecyclerView.configure() {
    val layoutManager = LinearLayoutManager(context)
    this.layoutManager = layoutManager
    val itemDecorator = DividerItemDecoration(context, layoutManager.orientation)
    val divider = ContextCompat.getDrawable(context!!, R.drawable.divider_2dp)
    itemDecorator.setDrawable(divider!!)
    this.addItemDecoration(itemDecorator)
}

fun ViewGroup.inflate(layoutId: Int): View {
    return LayoutInflater.from(context).inflate(layoutId, this, false)
}

/**
 * Converts dp to pixel
 */
val Int.dpToPx: Int get() = (this * Resources.getSystem().displayMetrics.density).toInt()

/**
 * Converts pixel to dp
 */
val Int.pxToDp: Int get() = (this / Resources.getSystem().displayMetrics.density).toInt()

