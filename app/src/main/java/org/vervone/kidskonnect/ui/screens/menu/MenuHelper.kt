package org.vervone.kidskonnect.ui.screens.menu

import org.vervone.kidskonnect.R

class MenuHelper {

    companion object {

        const val ID_HOME = 1
        const val ID_NEWS = 2
        const val ID_MESSAGES = 3
        const val ID_EXAMS = 4
        const val ID_LEAVES = 5
        const val ID_NOTIFICATIONS = 6

        const val ID_CONTACTS = 7
        const val ID_STUDENTS = 8
        const val ID_TEACHERS = 9
        const val ID_LOGOUT = 10

        private val HOME = NavMenuItem("Home", R.drawable.ic_home, ID_HOME)
        private val NEWS = NavMenuItem("News feeds", R.drawable.ic_news, ID_NEWS)
        private val MESSAGES = NavMenuItem("Messages", R.drawable.ic_message, ID_MESSAGES)
        private val EXAMS = NavMenuItem("Exam Schedules", R.drawable.ic_exams, ID_EXAMS)
        private val LEAVES = NavMenuItem("Leaves", R.drawable.ic_leave, ID_LEAVES)
        private val NOTIFICATIONS = NavMenuItem("Notifications", R.drawable.ic_notification, ID_NOTIFICATIONS)
        private val CONTACTS = NavMenuItem("Contact School", R.drawable.ic_contacts, ID_CONTACTS)
        private val STUDENTS = NavMenuItem("Students", R.drawable.ic_student, ID_STUDENTS)
        private val TEACHERS = NavMenuItem("Teachers", R.drawable.ic_teacher, ID_TEACHERS)
        private val LOGOUT = NavMenuItem("Logout", R.drawable.ic_logout, ID_LOGOUT)


        val STUDENT_MENU = mutableListOf(
            HOME, NEWS, MESSAGES, EXAMS, LEAVES, NOTIFICATIONS,
            CONTACTS, TEACHERS, LOGOUT
        )
        val TEACHERS_MENU = mutableListOf(
            HOME, NEWS, MESSAGES, EXAMS, LEAVES, NOTIFICATIONS,
            CONTACTS, STUDENTS, LOGOUT
        )

        val ADMIN_MENU = mutableListOf(
            HOME, NEWS, MESSAGES, EXAMS, LEAVES, NOTIFICATIONS,
            CONTACTS, STUDENTS, TEACHERS, LOGOUT
        )

    }

}