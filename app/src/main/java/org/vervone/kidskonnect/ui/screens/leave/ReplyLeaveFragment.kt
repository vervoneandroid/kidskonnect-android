package org.vervone.kidskonnect.ui.screens.leave

import android.os.Bundle
import android.view.View
import android.widget.*
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.DomainError
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.LeavesFacade
import org.vervone.kidskonnect.core.domain.model.Leave
import org.vervone.kidskonnect.core.domain.model.LeaveStatus
import org.vervone.kidskonnect.core.helper.cast
import org.vervone.kidskonnect.ui.screens.base.BaseDialogFragment
import org.vervone.kidskonnect.ui.utils.AppUtils
import org.vervone.kidskonnect.ui.utils.UiUtils

class ReplyLeaveFragment : BaseDialogFragment() {

    private lateinit var btnOk: Button
    private lateinit var btnCancel: Button
    private lateinit var tvContent: TextView
    private lateinit var etReplyComment: EditText

    private val leavesFacade: LeavesFacade = FacadeFactory.leavesFacade

    private var leave: Leave? = null
    private var status: LeaveStatus? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.leave = arguments?.getSerializable(ARG_LEAVE) as Leave?
        this.status = arguments?.getSerializable(ARG_ACTION) as LeaveStatus?
    }

    override fun getLayoutId(): Int {
        return R.layout.dialog_leave_reply
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.etReplyComment = view.findViewById(R.id.et_reply_comment)
        this.tvContent = view.findViewById(R.id.tv_content)
        this.btnCancel = view.findViewById(R.id.btn_cancel)
        this.btnOk = view.findViewById(R.id.btn_ok)

        val contentText = if (status == LeaveStatus.APPROVED) R.string.leave_approve_confirmation_request else
            R.string.leave_reject_confirmation_request
        tvContent.setText(contentText)

        val color = if (status == LeaveStatus.APPROVED) R.color.dark_green else
            R.color.dark_red

        tvContent.setTextColor(resources.getColor(color))

        btnOk.setOnClickListener {
            leave?.let {
                val academicYear = AppUtils.getCurrentAcademicYear()
                it.leaveStatus = status!!
                it.replyString = UiUtils.getString(etReplyComment)
                if (status == LeaveStatus.APPROVED) {
                    leavesFacade.approveLeaveRequest(academicYear, it, leaveRequestCallback)
                } else {
                    leavesFacade.rejectLeaveRequest(academicYear, it, leaveRequestCallback)
                }
            }
        }

        btnCancel.setOnClickListener {
            dismiss()
        }
    }


    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                                  LinearLayout.LayoutParams.WRAP_CONTENT)

    }

    /* callback */
    private val leaveRequestCallback = object : RequestCallback<Leave, DomainError> {

        override fun onError(error: DomainError?) {
            Toast.makeText(context, R.string.operation_failed, Toast.LENGTH_SHORT).show()
            dismiss()
        }

        override fun onSuccess(res: Leave?) {
            getListener()?.onSuccess()
            dismiss()
        }
    }


    private fun getListener(): SuccessListener? {
        return if (parentFragment is SuccessListener) this.parentFragment?.cast()
        else null
    }


    companion object {

        const val TAG = "ReplyLeaveFragment"
        private const val ARG_LEAVE = "Leave"
        private const val ARG_ACTION = "action"

        fun createInstance(leave: Leave, leaveStatus: LeaveStatus): ReplyLeaveFragment {
            val fragment = ReplyLeaveFragment()
            val args = Bundle()
            args.putSerializable(ARG_LEAVE, Leave(leave))
            args.putSerializable(ARG_ACTION, leaveStatus)
            fragment.arguments = args
            return fragment
        }
    }


    interface SuccessListener {
        fun onSuccess()
    }

}