package org.vervone.kidskonnect.ui.screens.student

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.domain.model.student.ProfileDetails
import org.vervone.kidskonnect.core.domain.model.student.Student
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.screens.contacts.ContactsFragment
import org.vervone.kidskonnect.ui.utils.PermissionHelper
import org.vervone.kidskonnect.ui.utils.UserHelper
import org.vervone.kidskonnect.ui.utils.inflate

class StudentPersonalDetailsFragment : BaseFragment(), PermissionHelper.PermissionsListener,
    EditPhoneNumberFragment.UpdateListener {


    private lateinit var rvPersonalDetails: RecyclerView
    private var student: Student? = null
    private var personalDetails: ProfileDetails? = null
    private var dataSet = ArrayList<Row>()
    private var permissionHelper: PermissionHelper? = null
    private var phoneNo: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.student = arguments?.getSerializable(ARG_STUDENT) as Student?
        this.personalDetails = student?.ProfileDetails
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_st_personal_details
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.rvPersonalDetails = view.findViewById(R.id.rv_personal_details)

        val layoutManager = LinearLayoutManager(context)
        rvPersonalDetails.layoutManager = layoutManager
        dataSet.clear()
        configure()
        this.permissionHelper = PermissionHelper(this)
        this.permissionHelper?.setListener(this)

    }

    private fun openEmailClient(emailId: String) {
        /* Create the Intent */
        val emailIntent = Intent(Intent.ACTION_SEND)

        /* Fill it with Data */
        emailIntent.type = "plain/text";
        emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(emailId));
        context?.startActivity(Intent.createChooser(emailIntent, ""))
    }

    override fun onPermissionGranted(requestCode: Int) {
        if (ContactsFragment.CALL_PHONE_REQ_CODE == requestCode && phoneNo != null) {
            makeCall()
        }
    }

    override fun onPermissionRejectedManyTimes(rejectedPerms: MutableList<String>, request_code: Int) {

    }

    override fun onUpdate(newPhoneNo: String) {
        dataSet.clear()
        personalDetails?.localGuardianNumber = newPhoneNo
        configure()
    }


    private fun configure() {

        if (personalDetails != null) {

            with(personalDetails!!)
            {

                val alternatePhone: Row = if (UserHelper.getCurrentUser().uid == student?.studentId) {
                    Row(getString(R.string.alternative_contact), localGuardianNumber
                        ?: "", R.drawable.baseline_edit_black_18, PHONE_EDIT)
                } else {
                    Row(getString(R.string.alternative_contact), localGuardianNumber
                        ?: "", R.drawable.baseline_local_phone_black_18, PHONE)
                }
                val list = arrayListOf(
                        Row(getString(R.string.parent), parentName ?: "", 0),
                        Row(getString(R.string.address), parentAdderss ?: "", 0),
                        Row(getString(R.string.mail), ParentEmail
                            ?: "", R.drawable.baseline_email_black_18, EMAIL),
                        Row(getString(R.string.local_guardian), LocalGuardian ?: "", 0),
                        Row(getString(R.string.address), LocalGuardianAddress ?: "", 0),
                        Row(getString(R.string.mobile), ParentNumber
                            ?: "", R.drawable.baseline_local_phone_black_18, PHONE),
                        alternatePhone
                )
                dataSet.addAll(list)
            }
        }


        rvPersonalDetails.adapter = PersonalDetailsAdapter(dataSet, actionListener)
    }

    private var actionListener = object : PersonalDetailsAdapter.ActionListener {
        override fun onAction(rowData: Row) {
            if (rowData.action == PHONE) {
                phoneNo = rowData.value
                permissionHelper?.requestPermission(arrayOf(ContactsFragment.CALL_PHONE),
                                                    ContactsFragment.CALL_PHONE_REQ_CODE)
            } else if (rowData.action == EMAIL) {
                openEmailClient(rowData.value)
            } else if (rowData.action == PHONE_EDIT) {
                if (student != null && student?.studentId != null) {
                    val editPhoneNumber = EditPhoneNumberFragment
                        .createInstance(student?.studentId!!, rowData.value)
                    editPhoneNumber.show(childFragmentManager, "")
                }
            }
        }
    }


    private fun makeCall() {
        try {
            val callIntent = Intent(Intent.ACTION_CALL)
            callIntent.data = Uri.parse("tel:$phoneNo")
            activity?.startActivity(callIntent)
        } catch (ex: Exception) {

        }
    }


    private class VH(view: View) : RecyclerView.ViewHolder(view) {
        val tvKey: TextView = view.findViewById(R.id.tv_key)
        val tvValue: TextView = view.findViewById(R.id.tv_value)
    }

    private class Row(val key: String,
                      val value: String, val drawableId: Int, val action: Int = NO_ACTION)

    private class PersonalDetailsAdapter(val list: List<Row>,
                                         val actListener: ActionListener) : RecyclerView.Adapter<VH>() {

        override fun onCreateViewHolder(vg: ViewGroup, pos: Int): VH {
            return VH(vg.inflate(R.layout.kc_layout_table_row))
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onBindViewHolder(vh: VH, pos: Int) {
            val rowData = list[pos]
            vh.tvKey.text = rowData.key
            vh.tvValue.text = rowData.value
            vh.tvValue.setCompoundDrawablesWithIntrinsicBounds(0, 0, rowData.drawableId, 0)


            vh.tvValue.setOnClickListener {
                if (rowData.action == EMAIL || rowData.action == PHONE || rowData.action == PHONE_EDIT) {
                    actListener.onAction(rowData)
                }
            }
        }


        interface ActionListener {
            fun onAction(rowData: Row)
        }

    }


    companion object {

        private const val NO_ACTION = 0
        private const val PHONE = 1
        private const val PHONE_EDIT = 3
        private const val EMAIL = 2

        private const val ARG_STUDENT = "argsStudent"

        fun createInstance(student: Student?): StudentPersonalDetailsFragment {
            val args = Bundle()
            args.putSerializable(ARG_STUDENT, student)

            val fragment = StudentPersonalDetailsFragment()
            fragment.arguments = args
            return fragment
        }
    }

}