package org.vervone.kidskonnect.ui

import org.vervone.kidskonnect.core.domain.model.student.Comment
import org.vervone.kidskonnect.core.domain.model.student.CommentType
import org.vervone.kidskonnect.core.domain.model.student.Student
import org.vervone.kidskonnect.ui.screens.base.BaseView

interface MainActivityView : BaseView {
    fun updateMessageCount(count: Int)
    fun showMessageNotification(count: Int, channelName: String, channelId: String)
    fun showNewsNotification(count: Int)
    fun showLeaveRequestNotification(studentName: String)
    fun updateNewsCount(count: Int)
    fun updateCommentsCount(count: Int)
    fun updateStudentCommentsCount(count: Int)
    fun updateRequestsLeaveCount(count: Int)
    fun showProfileUpdatedNotification(comment: Comment, student: Student)
    fun navigateToStudentProfile(commentType: CommentType)
    fun showLeaveApprovedNotification()
    fun showLeaveRejectedNotification()
}