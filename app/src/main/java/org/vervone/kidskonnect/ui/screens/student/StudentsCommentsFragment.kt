package org.vervone.kidskonnect.ui.screens.student

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.DomainError
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.SchoolFacade
import org.vervone.kidskonnect.core.domain.model.student.Comment
import org.vervone.kidskonnect.core.domain.model.student.CommentType
import org.vervone.kidskonnect.core.domain.model.student.Student
import org.vervone.kidskonnect.core.helper.Formatter
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.utils.*

class StudentsCommentsFragment : BaseFragment(), AddCommentFragment.SuccessListener, SchoolFacade.Subscriber,
    SchoolFacade.StudentListener {

    private lateinit var rvComments: RecyclerView
    private lateinit var fabAddComment: FloatingActionButton
    private var comments: ArrayList<Comment>? = null
    private var studentId: String? = null
    private var commentType = CommentType.UNKNOWN
    private var studentName: String? = null
    private var className: String? = null
    private val schoolFacade = FacadeFactory.schoolFacade
    private var subscriberId: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val argsComments = arguments?.getSerializable(COMMENTS) as? ArrayList<Comment>
        this.comments = ArrayList(argsComments ?: emptyList())
        this.studentId = arguments?.getString(STUDENT_ID, null)
        this.studentName = arguments?.getString(STUDENT_NAME, null)
        this.className = arguments?.getString(CLASS_NAME, null)
        this.commentType = arguments!!.getSerializable(ARG_COMMENT_TYPE) as CommentType
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_st_personal_details
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.rvComments = view.findViewById(R.id.rv_personal_details)
        this.fabAddComment = view.findViewById(R.id.fb_add_comment)
        rvComments.configure()

        this.fabAddComment.extShow()

        if (comments.isNullOrEmpty()) {
            showErrorLayout(getString(R.string.no_comments_found))
        } else {
            bindData()
        }

        fabAddComment.setOnClickListener {
            val addCommentFragment = AddCommentFragment
                .createInstance(studentId!!, studentName!!, className!!, commentType)
            addCommentFragment.show(childFragmentManager, AddCommentFragment.TAG)
        }
    }

    override fun onResume() {
        super.onResume()

        if (UserHelper.isTeacher()) {
            subscriberId = schoolFacade.subscribeStudentByTeacher(studentId!!, this)
            schoolFacade.getComments(studentId!!, commentType, commentListener)
        } else {
            subscriberId = schoolFacade.subscribeStudent(studentId!!, this)
        }
    }

    override fun onUpdate(student: Student) {
        this.comments?.clear()
        student.comments?.let { this.comments?.addAll(it) }
        bindData()
    }

    override fun onChange() {
        if (UserHelper.isTeacher()) {
            schoolFacade.getComments(studentId!!, commentType, commentListener)
        }
    }


    override fun onPause() {
        super.onPause()

        subscriberId?.let {
            if (UserHelper.isTeacher()) {
                schoolFacade.unsubscribe(it)
            } else {
                schoolFacade.unsubscribeStudent(it)
            }
        }
        this.subscriberId = null
    }

    private val commentListener = object : RequestCallback<List<Comment>, DomainError> {
        override fun onError(error: DomainError?) {}
        override fun onSuccess(res: List<Comment>?) {
            runOnUi {
                if (res.isNullOrEmpty() && comments.isNullOrEmpty()) {
                    showErrorLayout(getString(R.string.no_comments_found))
                } else {
                    comments = ArrayList(res)
                    if (comments.isNullOrEmpty()) {
                        //showErrorLayout(getString(R.string.no_comments_found))
                    } else {
                        bindData()
                    }
                }
            }
        }
    }

    private fun bindData() {
        showContentLayout()
        comments?.sortByDescending { it.postTime }
        rvComments.adapter = Adapter(comments!!)
    }

    override fun onSuccess() {
        //schoolFacade.getComments(studentId!!, commentType, commentListener)
    }


    private class VH(view: View) : RecyclerView.ViewHolder(view) {
        val tvPersonName: TextView = view.findViewById(R.id.tv_person_name)
        val tvComment: TextView = view.findViewById(R.id.tv_comment)
        val tvCommentDate: TextView = view.findViewById(R.id.tv_comment_date)
        val ivProfilePic: ImageView = view.findViewById(R.id.iv_profile_pic)
    }

    private class Adapter(val comments: List<Comment>) : RecyclerView.Adapter<VH>() {

        override fun onCreateViewHolder(vg: ViewGroup, position: Int): VH {
            return VH(vg.inflate(R.layout.kc_student_comments))
        }

        override fun getItemCount(): Int {
            return comments.size
        }

        override fun onBindViewHolder(vh: VH, position: Int) {
            val comment = comments[position]
            vh.tvPersonName.text = comment.commentedPersonName
            vh.tvComment.text = comment.comment
            vh.tvCommentDate.text = Formatter.getDate(comment.postTime)
            if (comment.commentedPersonId != null) {
                ImageLoader.get(
                        Urls.getProfileUrl(comment.commentedPersonId!!),
                        R.drawable.ic_profle,
                        R.drawable.ic_profle,
                        vh.ivProfilePic
                )
            }
        }

    }


    companion object {

        private const val COMMENTS = "student.comments"
        private const val STUDENT_ID = "student.id"
        private const val ARG_COMMENT_TYPE = "commentType"
        private const val STUDENT_NAME = "student_name"
        private const val CLASS_NAME = "class_name"

        fun createInstance(
                studentId: String,
                studentName: String,
                className: String,
                commentType: CommentType,
                comments: ArrayList<Comment>
        ): StudentsCommentsFragment {
            val args = Bundle()
            args.putSerializable(COMMENTS, comments)
            args.putSerializable(ARG_COMMENT_TYPE, commentType)
            args.putString(STUDENT_ID, studentId)
            args.putString(STUDENT_NAME, studentName)
            args.putString(CLASS_NAME, className)

            val fragment = StudentsCommentsFragment()
            fragment.arguments = args
            return fragment
        }

    }

}