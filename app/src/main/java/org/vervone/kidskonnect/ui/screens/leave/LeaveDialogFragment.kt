package org.vervone.kidskonnect.ui.screens.leave

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.prolificinteractive.materialcalendarview.CalendarDay
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.helper.cast
import org.vervone.kidskonnect.ui.screens.base.BaseDialogFragment

class LeaveDialogFragment : BaseDialogFragment() {

    private lateinit var btnOk: Button
    private lateinit var btnCancel: Button
    private lateinit var tvContent: TextView
    private var calendarDay: CalendarDay? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.calendarDay = arguments?.getParcelable(CALENDER_DAY)
    }

    override fun getLayoutId(): Int {
        return R.layout.dialog_leave_warning
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.tvContent = view.findViewById(R.id.tv_content)
        this.btnCancel = view.findViewById(R.id.btn_cancel)
        this.btnOk = view.findViewById(R.id.btn_continue)
        tvContent.setText(R.string.selected_date_is_over)

        btnOk.setOnClickListener {
            getListener()?.onContinueClick(calendarDay)
            dismiss()
        }

        btnCancel.setOnClickListener {
            getListener()?.onCancelClick(calendarDay)
            dismiss()
        }
    }


    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                                  LinearLayout.LayoutParams.WRAP_CONTENT)

    }


    private fun getListener(): OnButtonClickListener? {
        return if (parentFragment is OnButtonClickListener) this.parentFragment?.cast()
        else null
    }


    companion object {
        const val TAG = "LeaveDialogFragment"
        private const val CALENDER_DAY = "CALENDER_DAY"
        fun createInstance(calendarDay: CalendarDay): LeaveDialogFragment {
            val fragment = LeaveDialogFragment()
            val args = Bundle()
            args.putParcelable(CALENDER_DAY, calendarDay)
            fragment.arguments = args
            return fragment
        }
    }


    interface OnButtonClickListener {
        fun onCancelClick(calendarDay: CalendarDay?) {}
        fun onContinueClick(calendarDay: CalendarDay?) {}
    }

}