package org.vervone.kidskonnect.ui.screens.messages

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.*
import com.glidebitmappool.GlideBitmapFactory
import com.iceteck.silicompressorr.SiliCompressor
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.cloudstorage.CloudStorage
import org.vervone.kidskonnect.core.domain.model.ChatMessage
import org.vervone.kidskonnect.core.domain.model.MessageChannel
import org.vervone.kidskonnect.core.domain.model.MessageType
import org.vervone.kidskonnect.core.helper.Formatter
import org.vervone.kidskonnect.core.helper.ThreadUtils
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.screens.news.FullScreenImageViewer
import org.vervone.kidskonnect.ui.screens.news.FullScreenVideoPlayer
import org.vervone.kidskonnect.ui.screens.news.MediaOptionsBottomSheet
import org.vervone.kidskonnect.ui.utils.*
import org.vervone.kidskonnect.ui.views.RecyclerViewHack
import org.vervone.mediapicker.Image.ImagePicker
import org.vervone.mediapicker.Video.VideoPicker
import java.io.File


class MessageChatRoomFragment : BaseFragment(), MessageChatRoomView {


    private lateinit var rvChatMessages: RecyclerViewHack
    private lateinit var layoutMembers: LinearLayout
    private lateinit var ivAttachFile: ImageView
    private lateinit var etChatMessage: EditText
    private lateinit var btnSend: Button
    private var presenter: MessageChatRoomPresenter? = null
    private var chatRoomId: String? = null
    private var chatRoomName: String? = null
    private var chatMessages: MutableList<ChatMessage>? = null
    // Media message type , getting from onActivity result .
    private var pendingMessage: ChatMessage? = null
    // ChatMessage from createChatGroup Page
    // default mode , This will be null
    private var argsChatMessage: ChatMessage? = null

    private var chatRoom: MessageChannel? = null
    private var progressDialog: DialogFragment? = null

    private var isCompressing = false

    override fun getLayoutId(): Int {
        return R.layout.fragment_message_chat
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.chatRoomId = arguments?.getString(CHAT_ROOM_ID, null)
        this.chatRoomName = arguments?.getString(CHAT_ROOM_NAME, "")
        this.argsChatMessage = arguments?.getSerializable(CHAT_MESSAGE) as? ChatMessage
        this.chatRoom = arguments?.getSerializable(CHAT_ROOM) as? MessageChannel
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        chatRoomId?.let {
            MessageCount().markAsRead(it)
            MessageCount.setActiveChatRoomId(it)
        }
        activity?.title = chatRoomName
        this.rvChatMessages = view.findViewById(R.id.rv_chat_messages)
        this.layoutMembers = view.findViewById(R.id.layout_members)
        this.ivAttachFile = view.findViewById(R.id.iv_attach_file)
        this.etChatMessage = view.findViewById(R.id.et_chat_message)
        this.btnSend = view.findViewById(R.id.btn_send)
        presenter = MessageChatRoomPresenter()
        presenter?.onAttach(this)


        val layoutManager = LinearLayoutManager(activity!!)
        rvChatMessages.layoutManager = layoutManager
        presenter?.loadChatMessages(this.chatRoomId!!)
        layoutManager.stackFromEnd = true
        ivAttachFile.setOnClickListener {
            val fragment = MediaOptionsBottomSheet()
            fragment.show(childFragmentManager, null)
        }

        etChatMessage.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {}
            override fun beforeTextChanged(charSequence: CharSequence?,
                                           p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence?,
                                       p1: Int, p2: Int, p3: Int) {
                btnSend.isEnabled = !(charSequence.isNullOrBlank() || charSequence.isNullOrEmpty())
            }
        })
        btnSend.isEnabled = false
        this.btnSend.setOnClickListener {
            if (UiUtils.getString(etChatMessage).isEmpty()) return@setOnClickListener
            sendMessage(MessageType.TEXT, null, getMessage(MessageType.TEXT))
        }
    }

    fun canShowNotification(chatRoomId: String): Boolean {
        return chatRoomId != this.chatRoomId
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        if (chatRoom?.isGroupMessage == true) {
            inflater?.inflate(R.menu.menu_chat_room, menu)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item != null) {
            when (item.itemId) {
                R.id.members -> showMembers()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun showMembers() {
        if (chatRoom != null && chatRoom?.targets != null) {
            val fragment = ChatMembersFragment.createInstance(chatRoom?.targets!!)
            fragment.show(childFragmentManager, "")
        }
    }


    private fun showProgressBar() {
        hideProgressBar()
        progressDialog = DialogFactory.createProgressDialog(activity!!)
        progressDialog?.show(childFragmentManager, null)
    }

    private fun hideProgressBar() {
        progressDialog?.dismissAllowingStateLoss()
        progressDialog = null
    }

    override fun onResume() {
        super.onResume()
        val chatMessage = pendingMessage
        if (chatMessage != null) {
            val messageType = chatMessage.messageType
            if (messageType == MessageType.VIDEOS && !isCompressing) {
                showProgressBar()
                compressVideo()
            } else if (messageType == MessageType.PICTURE) {
                sendMessage(messageType, chatMessage.localFilePath, getMessage(messageType))
                pendingMessage = null
            }
        }
    }


    private fun compressVideo() {
        this.isCompressing = true
        ThreadUtils.runOnBg {
            val destinationDirectory = File(context!!.cacheDir.path)
            val filePath = SiliCompressor.with(context?.applicationContext)
                .compressVideo(pendingMessage?.localFilePath, destinationDirectory.path)
            pendingMessage?.localFilePath = filePath
            DLog.v(this, "runOnBg $filePath")

            runOnUi {
                try {
                    sendMessage(MessageType.VIDEOS,
                                pendingMessage?.localFilePath,
                                getMessage(MessageType.VIDEOS))
                    pendingMessage = null
                    isCompressing = false
                } catch (ex: Exception) {
                    pendingMessage = null
                    isCompressing = false
                }

                hideProgressBar()
            }
        }
    }

    override fun handleOnActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        var handled = false
        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE &&
            resultCode == AppCompatActivity.RESULT_OK
        ) {

            val paths = data?.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH)
            if (!paths.isNullOrEmpty()) {
                pendingMessage = ChatMessage()
                pendingMessage?.messageType = MessageType.PICTURE
                pendingMessage?.localFilePath = paths.get(0)!!
                DLog.v(this, "image.paths $paths")
            }
            handled = true

        } else if (requestCode == VideoPicker.VIDEO_PICKER_REQUEST_CODE &&
            resultCode == Activity.RESULT_OK
        ) {
            val paths = data?.getStringArrayListExtra(VideoPicker.EXTRA_VIDEO_PATH)
            DLog.v(this, "path $paths")
            pendingMessage = ChatMessage()
            pendingMessage?.messageType = MessageType.VIDEOS
            pendingMessage?.localFilePath = paths?.get(0)!!
            handled = true
        }
        return handled
    }

    override fun reload() {
        rvChatMessages.adapter?.notifyDataSetChanged()
    }

    private fun getMessage(messageType: MessageType): String {
        return when (messageType) {
            MessageType.TEXT -> UiUtils.getString(etChatMessage)
            MessageType.PICTURE -> "image"
            MessageType.VIDEOS -> "video"
            else -> ""
        }
    }

    private fun sendMessage(messageType: MessageType,
                            path: String?, message: String?) {
        val chatMessage = ChatMessage()
        chatMessage.messageType = messageType
        chatMessage.message = message
        chatMessage.time = System.currentTimeMillis()
        chatMessage.senderName = UserHelper.getCurrentUser().name
        chatMessage.senderId = UserHelper.getCurrentUser().uid
        chatMessage.messageStatus = 0
        chatMessage.localFilePath = path
        chatMessage.status = ChatMessage.PENDING

        this.chatMessages?.add(chatMessage)

        presenter?.sendMessage(chatRoomId!!, chatMessage)
        etChatMessage.setText("")

        rvChatMessages.adapter?.notifyDataSetChanged()
        scrollToBottom()
    }

    override fun setChatMessages(chatList: List<ChatMessage>) {
        this.chatMessages = ArrayList(chatList)
        if (rvChatMessages.adapter == null) {
            rvChatMessages.adapter = ChatMessagesAdapter()
        }
        rvChatMessages.adapter?.notifyDataSetChanged()
        chatRoomId?.let {
            MessageCount().markAsRead(it)
        }
        if (argsChatMessage != null) {
            sendMessage(argsChatMessage?.messageType!!,
                        argsChatMessage?.localFilePath,
                        argsChatMessage?.message)
            argsChatMessage = null
        }

        scrollToBottom()
    }

    private fun scrollToBottom() {
        val itemCount = this.chatMessages?.size ?: 0
        rvChatMessages.layoutManager?.smoothScrollToPosition(rvChatMessages, null, itemCount)
    }

    override fun showError(message: String?) {

    }

    override fun onDestroyView() {
        chatRoomId?.let {
            MessageCount().markAsRead(it)
            MessageCount.setActiveChatRoomId(null)
        }
        presenter?.stopMessageSubscription()
        super.onDestroyView()
        presenter?.onDettach()
    }


    private inner class ChatMessagesAdapter : RecyclerView.Adapter<BaseVH>() {


        private val drawableMap = hashMapOf<String, Drawable>()
        private val profileMap = hashMapOf<String, String>()

        init {
            rvChatMessages.setRecyclerListener {
                if (it is MediaVH) {
                    it.ivMediaMessage.setImageBitmap(null)
                }
            }
        }

        override fun getItemViewType(position: Int): Int {
            val chatMessage = chatMessages!!.get(position)
            return if (UserHelper.getCurrentUser().uid == chatMessage.senderId) {
                when (chatMessage.messageType) {
                    MessageType.VIDEOS -> CURRENT_USER_VIDEO
                    MessageType.PICTURE -> CURRENT_USER_IMAGE
                    else -> CURRENT_USER
                }

            } else {
                when (chatMessage.messageType) {
                    MessageType.VIDEOS -> OTHER_USER_VIDEO
                    MessageType.PICTURE -> OTHER_USER_IMAGE
                    else -> OTHER_USER
                }
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewtype: Int): BaseVH {

            when (viewtype) {
                CURRENT_USER -> {
                    val view = parent.inflate(R.layout.kc_user_layout_msg_chat)
                    return TextVH(view)
                }

                CURRENT_USER_VIDEO -> {
                    val view = parent.inflate(R.layout.kc_user_layout_msg_chat_video)
                    return VideoVH(view)
                }

                CURRENT_USER_IMAGE -> {
                    val view = parent.inflate(R.layout.kc_user_layout_msg_chat_video)
                    return ImageVH(view)
                }

                OTHER_USER_IMAGE -> {
                    val view = parent.inflate(R.layout.kc_layout_msg_chat_video)
                    return ImageVH(view)
                }

                OTHER_USER_VIDEO -> {
                    val view = parent.inflate(R.layout.kc_layout_msg_chat_video)
                    return VideoVH(view)
                }

                else -> {
                    val view = parent.inflate(R.layout.kc_layout_msg_chat)
                    return TextVH(view)
                }
            }

        }

        override fun getItemCount(): Int {
            return chatMessages?.size ?: 0
        }


        private fun bindVH(textVh: TextVH, chatMessage: ChatMessage) {
            textVh.tvChatTime.text = Formatter.getTime(chatMessage.time)
            textVh.tvChatMsg.text = chatMessage.message
        }

        private fun bindVH(vh: MediaVH, chatMessage: ChatMessage, pos: Int) {

            vh.ivPlay.setOnClickListener {
                val attachmentFile = File(context?.cacheDir, chatMessage.attachmentsUrl)
                val player = FullScreenVideoPlayer
                    .createInstance(chatMessage.attachmentsUrl, true, attachmentFile.path)
                player.show(childFragmentManager, null)
            }
            vh.tvMediaChatTime.text = Formatter.getTime(chatMessage.time)

            vh.ivMediaMessage.setOnClickListener {
                if (chatMessage.messageType == MessageType.VIDEOS) return@setOnClickListener
                val imageUrls = listOfNotNull(chatMessage.attachmentsUrl)
                val fragment = FullScreenImageViewer.createInstance(imageUrls)
                fragment.show(childFragmentManager, null)
            }

            vh.ivDownload.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    chatMessage.status = ChatMessage.DOWNLOADING
                    vh.showDownloadingMode()
                    rvChatMessages.forceRequestLayout()
                    notifyItemChanged(pos)
                    if (chatMessage.messageType == MessageType.VIDEOS) {
                        downloadVideo(chatMessage, vh, pos)
                    } else {
                        downloadImage(chatMessage, vh, pos)
                    }

                }
            })

            vh.ivCancel?.setOnClickListener {
                presenter?.cancelMessage(chatMessage)
                try {
                    chatMessages?.remove(chatMessage)
                } catch (ex: Exception) {
                    DLog.w(this, "onBindViewHolder()", ex)
                } finally {
                    reload()
                }
            }


            if (chatMessage.thumpNailImagePath != null) {
                val attachmentFile = File(context?.cacheDir, chatMessage.attachmentsUrl)
                val thumpNailFile = File(context?.cacheDir, chatMessage.thumpNailImagePath)
                if (attachmentFile.exists()) {
                    vh.showDownloadedMode()
                    if (chatMessage.messageType == MessageType.VIDEOS) {
                        bindImage(thumpNailFile, vh, chatMessage, pos)
                        vh.ivPlay.extShow()
                    } else {
                        vh.ivPlay.extHide()
                        bindImage(attachmentFile, vh, chatMessage, pos)
                    }
                } else {
                    vh.showNormalMode()
                    // Bind thumbnail image to the ui
                    if (thumpNailFile.exists()) {
                        loadThumbnailImage(thumpNailFile, vh)
                    } else {
                        getCloudStorage()?.download(
                                chatMessage.thumpNailImagePath,
                                thumpNailFile,
                                object : CloudStorage.DownloadListener {
                                    override fun onError(error: Error) {}
                                    override fun onComplete(downloadedPath: String?) {
                                        loadThumbnailImage(thumpNailFile, vh)
                                    }
                                })
                    }
                }

            }

            when {
                chatMessage.status == ChatMessage.PENDING -> vh.showPendingMode()
                chatMessage.status == ChatMessage.DOWNLOADING -> vh.showDownloadingMode()
            }

        }

        override fun onBindViewHolder(vh: BaseVH, pos: Int) {
            val chatMessages = chatMessages!!
            val chatMessage = chatMessages[pos]
            val date = Formatter.getDate(chatMessage.time)

            val showHdr = if (pos == 0) {
                true
            } else {
                val preIndex = pos - 1
                val prevMessage = chatMessages[preIndex].time
                val prevDate = Formatter.getDate(prevMessage)
                (date != prevDate)
            }

            if (showHdr) {
                vh.tvMsgDate.extShow()
                vh.tvMsgDate.text = date
            } else {
                vh.tvMsgDate.extHide()
            }

            if (vh is TextVH) {
                bindVH(vh, chatMessage)
            } else if (vh is MediaVH) {
                bindVH(vh, chatMessage, pos)
            }


            if (chatRoom != null && chatRoom?.isGroupMessage == true) {
                vh.tvMemberName.text = chatMessage.senderName
                vh.tvMemberName.extShow()
            } else {
                vh.tvMemberName.extInvisible()
            }

            val key = Urls.getProfileKey(chatMessage.senderId!!)
            val path = profileMap[key]
            if (path == null || !File(path).exists()) {
                val profilePic = File(context?.cacheDir, key)
                getCloudStorage()?.downloadIfNeeded(
                        key, profilePic, object : CloudStorage.DownloadListener {
                    override fun onComplete(downloadedPath: String?) {
                        profileMap[key] = downloadedPath!!
                        val bitmap = GlideBitmapFactory.decodeFile(downloadedPath)
                        vh.ivChatProfile.setImageBitmap(bitmap)
                    }

                    override fun onError(error: Error) {
                        profileMap[key] = ""
                    }
                })
            } else {
                if (path.isNotEmpty()) {
                    try {
                        val options = BitmapFactory.Options()
                        options.inSampleSize = 8
                        val bitmap = BitmapFactory.decodeFile(path, options)
                        if (bitmap == null) {
                            vh.ivChatProfile.setImageResource(R.drawable.ic_profle)
                        } else {
                            vh.ivChatProfile.setImageBitmap(bitmap)
                        }
                    } catch (ex: Exception) {
                        vh.ivChatProfile.setImageResource(R.drawable.ic_profle)
                    }
                } else {
                    vh.ivChatProfile.setImageResource(R.drawable.ic_profle)
                }
            }

        }


        private fun downloadVideo(chatMessage: ChatMessage, vh: MediaVH, pos: Int) {
            val file = File(context?.cacheDir, chatMessage.attachmentsUrl!!)
            getCloudStorage()?.download(
                    chatMessage.attachmentsUrl, file, object : CloudStorage.DownloadListener {
                override fun onError(error: Error) {}
                override fun onComplete(downloadedPath: String?) {
                    vh.showDownloadedMode()
                    rvChatMessages.forceRequestLayout()
                    notifyItemChanged(pos)
                }
            })
        }


        private fun downloadImage(chatMessage: ChatMessage, vh: MediaVH, pos: Int) {
            val file = File(context?.cacheDir, chatMessage.attachmentsUrl!!)
            if (file.exists()) {
                bindImage(file, vh, chatMessage, pos, true)
            } else {
                getCloudStorage()?.download(
                        chatMessage.attachmentsUrl, file, object : CloudStorage.DownloadListener {
                    override fun onError(error: Error) {}
                    override fun onComplete(downloadedPath: String?) {
                        bindImage(file, vh, chatMessage, pos, true)
                    }
                })
            }
        }

        private fun bindImage(file: File, vh: MediaVH,
                              chatMessage: ChatMessage, pos: Int, reload: Boolean = false) {
            val iv = vh.ivMediaMessage
            val placeHolder = R.drawable.transparent_place_holder
            ImageLoader.load(file, placeHolder, placeHolder, iv, object : ImageLoader.Callback {
                override fun onLoad(bmp: Bitmap, uniqueId: String, path: String) {
                    if (activity == null) return
                    var dra = drawableMap[path]
                    if (dra == null) {
                        dra = createDrawable(bmp)
                        drawableMap[path] = dra
                    }
                    chatMessage.status = ChatMessage.SUCCESS
                    iv.setImageDrawable(dra)

                    if (reload) {
                        rvChatMessages.post {
                            rvChatMessages.forceRequestLayout()
                            notifyItemChanged(pos)
                        }
                    }
                }
            })
        }


        private fun loadThumbnailImage(file: File?, vh: MediaVH) {
            val iv = vh.ivMediaMessage
            if (file == null) {
                iv.setImageResource(R.drawable.transparent_place_holder)
            } else {
                val placeHolder = R.drawable.transparent_place_holder
                ImageLoader.load(file, placeHolder, placeHolder, iv, object : ImageLoader.Callback {
                    override fun onLoad(bmp: Bitmap, uniqueId: String, path: String) {
                        if (uniqueId == iv.tag && activity != null) {
                            var dra = drawableMap[path]
                            if (dra == null) {
                                dra = createDrawable(bmp)
                                drawableMap[path] = dra
                            }
                            iv.setImageDrawable(dra)
                            vh.showNormalMode()
                        }
                    }
                })

            }
        }

        private fun createDrawable(bmp: Bitmap): Drawable {
            val dra = MaskedDrawablePorterDuffSrcIn()
            dra.setMaskBitmap(BitmapFactory.decodeResource(activity!!.resources,
                                                           R.drawable.user_bubble_mask))
            dra.setPictureBitmap(bmp)
            return dra
        }
    }


    private abstract class BaseVH(view: View) : RecyclerView.ViewHolder(view) {
        abstract val tvMsgDate: TextView
        abstract val tvMemberName: TextView
        abstract val ivChatProfile: ImageView
    }

    private class VideoVH(view: View) : MediaVH(view) {
        override fun showDownloadedMode() {
            reset()
            ivPlay.extShow()
        }

        override fun showDownloadingMode() {
            reset()
            pbDownload.extShow()
        }

        override fun showNormalMode() {
            reset()
            ivDownload.extShow()
            ivVideoIcon.extShow()
        }
    }

    private class ImageVH(view: View) : MediaVH(view) {
        override fun showDownloadedMode() {
            reset()
        }

        override fun showDownloadingMode() {
            reset()
            pbDownload.extShow()
        }

        override fun showNormalMode() {
            reset()
            ivDownload.extShow()
        }
    }

    private abstract class MediaVH(view: View) : BaseVH(view) {
        override val tvMsgDate: TextView = view.findViewById(R.id.tv_msg_date)
        override val ivChatProfile: ImageView = view.findViewById(R.id.iv_chat_profile)
        override val tvMemberName: TextView = view.findViewById(R.id.tv_member_name)
        val ivMediaMessage: ImageView = view.findViewById(R.id.iv_media_message)
        val ivPlay: ImageView = view.findViewById(R.id.iv_play)
        val tvMediaChatTime: TextView = view.findViewById(R.id.tv_media_chat_time)
        var pbUploadPercentage: ProgressBar = view.findViewById(R.id.pb_upload)
        var ivCancel: ImageView? = view.findViewById(R.id.iv_cancel)
        var ivDownload: ImageView = view.findViewById(R.id.iv_download)
        var ivVideoIcon: ImageView = view.findViewById(R.id.iv_video_icon)
        var pbDownload: ProgressBar = view.findViewById(R.id.pb_download)

        init {
            reset()
        }

        protected fun reset() {
            ivPlay.extHide()
            pbUploadPercentage.extHide()
            ivCancel.extHide()
            ivDownload.extHide()
            ivVideoIcon.extHide()
            pbDownload.extHide()
        }

        /**
         *  Used to show a upload in progress mode for the
         *  media message.
         */
        fun showPendingMode() {
            reset()
            pbUploadPercentage.extShow()
            ivCancel.extShow()
        }

        abstract fun showDownloadedMode()
        abstract fun showNormalMode()
        abstract fun showDownloadingMode()
    }


    private class TextVH(view: View) : BaseVH(view) {
        override val tvMsgDate: TextView = view.findViewById(R.id.tv_msg_date)
        override val tvMemberName: TextView = view.findViewById(R.id.tv_member_name)
        override val ivChatProfile: ImageView = view.findViewById(R.id.iv_chat_profile)
        val tvChatMsg: TextView = view.findViewById(R.id.tv_chat_message)
        val tvChatTime: TextView = view.findViewById(R.id.tv_chat_time)
    }


    companion object {

        private const val CURRENT_USER = 1
        private const val OTHER_USER = 2
        private const val OTHER_USER_VIDEO = 3
        private const val OTHER_USER_IMAGE = 4
        private const val CURRENT_USER_IMAGE = 5
        private const val CURRENT_USER_VIDEO = 6

        private const val CHAT_ROOM_ID = "CHAT_ROOM_ID"
        private const val CHAT_MESSAGE = "CHAT_MESSAGE"
        private const val CHAT_ROOM_NAME = "CHAT_ROOM_NAME"
        private const val CHAT_ROOM = "CHAT_ROOM"

        fun createInstance(chatRoomId: String,
                           chatRoom: MessageChannel?,
                           channelName: String? = null,
                           chatMessage: ChatMessage? = null): MessageChatRoomFragment {

            val chatRoomFragment = MessageChatRoomFragment()
            val args = Bundle()
            args.putString(CHAT_ROOM_ID, chatRoomId)
            if (chatMessage != null) {
                args.putSerializable(CHAT_MESSAGE, chatMessage)
            }

            if (chatRoom != null) {
                args.putSerializable(CHAT_ROOM, chatRoom)
            }

            if (channelName != null) {
                args.putString(CHAT_ROOM_NAME, channelName)
            }
            chatRoomFragment.arguments = args
            return chatRoomFragment
        }
    }


}