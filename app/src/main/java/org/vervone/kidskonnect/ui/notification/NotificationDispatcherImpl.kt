package org.vervone.kidskonnect.ui.notification

import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.push.NotificationType
import org.vervone.kidskonnect.core.push.PushApiConst
import org.vervone.kidskonnect.ui.utils.SubscriptionHelper
import org.vervone.kidskonnect.ui.utils.UserHelper

class NotificationDispatcherImpl : NotificationDisPatcher {

    override fun dispatch(topic: String?, data: Map<String, String>) {
        when (val type = NotificationType.fromValue(data[PushApiConst.TYPE])) {
            NotificationType.NEW_MESSAGE_EXISTING_GROUP -> {
                if (isNewMessage(topic)) {
                    handleMessage(data)
                }
            }
            else -> DLog.v(this, type.name)
        }
    }


    /**
     *  If NotificationType is NEW_MESSAGE_EXISTING_GROUP && TOPIC is
     *  /topics/notificationTopic-<uid> , we consider it as a new message channel notification.
     */
    private fun isNewMessage(topic: String?): Boolean {
        return topic == (PushApiConst.TOPIC_PREFIX
                + PushApiConst.NOTIFICATION_TOPIC
                + UserHelper.getCurrentUser().uid)
    }

    private fun handleMessage(data: Map<String, String>) {
        val groupId = data[PushApiConst.GROUP_ID]

        if (groupId != null) {
            SubscriptionHelper.subscribeMessageChannel(groupId)
        }
    }
}