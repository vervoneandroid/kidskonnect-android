package org.vervone.kidskonnect.ui.screens.student

import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.DomainError
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.ui.screens.base.BaseDialogFragment
import org.vervone.kidskonnect.ui.utils.DialogFactory
import org.vervone.kidskonnect.ui.utils.KeyboardUtil
import org.vervone.kidskonnect.ui.utils.UiUtils

class EditPhoneNumberFragment : BaseDialogFragment() {

    private val schoolFacade = FacadeFactory.schoolFacade

    private lateinit var etPhone: EditText
    private var studentId: String? = null
    private var phoneNumber: String? = null
    private var progressDialog: DialogFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.studentId = arguments?.getString(STUDENT_ID, null)
        this.phoneNumber = arguments?.getString(STUDENT_ALTERNATE_PH_NO, "")
    }

    override fun getLayoutId(): Int {
        return R.layout.dialog_edit_phone_number
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                                  LinearLayout.LayoutParams.WRAP_CONTENT)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        etPhone = view.findViewById(R.id.et_phone_number)
        etPhone.setText(phoneNumber)

        val btnCancel = view.findViewById<Button>(R.id.btn_cancel)
        val btnDone = view.findViewById<Button>(R.id.btn_done)

        btnCancel.setOnClickListener {
            dismiss()
        }

        btnDone.setOnClickListener {
            if (validate() && studentId != null) {
                showProgressBar()
                KeyboardUtil.hideSoftKeyboard(activity!!)
                schoolFacade.updateAlternatePhoneNumber(this.studentId!!,
                                                        UiUtils.getString(etPhone),
                                                        updateCallback)
            }
        }
    }


    private fun showProgressBar() {
        progressDialog = DialogFactory.createProgressDialog(activity!!)
        progressDialog?.show(childFragmentManager, null)
    }

    private fun hideProgressBar() {
        progressDialog?.dismiss()
    }


    private val updateCallback = object : RequestCallback<String, DomainError> {
        override fun onSuccess(res: String?) {
            hideProgressBar()
            if (parentFragment is UpdateListener) {
                val listener = parentFragment as UpdateListener
                listener.onUpdate(res!!)
            }
            dismiss()
        }

        override fun onError(error: DomainError?) {
            val dialog = DialogFactory.createGenericErrorDialog(
                    activity!!, getString(R.string.failed_to_phone_number),
                    DialogInterface.OnClickListener { dialog, _ ->
                        dialog.dismiss()
                    })
            dialog.setCancelable(false)
            dialog.show()
        }
    }

    private fun validate(): Boolean {
        if (UiUtils.getString(etPhone).isEmpty()) {
            DialogFactory.createSimpleOkErrorDialog(
                    activity!!, getString(R.string.app_name),
                    getString(R.string.invalid_phone_number)).show()
            return false
        } else if (UiUtils.getString(etPhone) == phoneNumber) {
            DialogFactory.createSimpleOkErrorDialog(
                    activity!!, getString(R.string.app_name),
                    getString(R.string.phone_number_up_to_date)).show()
            return false
        }
        return true
    }


    companion object {
        private const val STUDENT_ID = "student.id"
        private const val STUDENT_ALTERNATE_PH_NO = "student.alternate.ph.no"
        fun createInstance(studentId: String, currentPhoneNo: String): EditPhoneNumberFragment {
            val args = Bundle()
            args.putString(STUDENT_ID, studentId)
            args.putString(STUDENT_ALTERNATE_PH_NO, currentPhoneNo)

            val fragment = EditPhoneNumberFragment()
            fragment.arguments = args
            return fragment
        }
    }


    interface UpdateListener {
        fun onUpdate(newPhoneNo: String)
    }

}