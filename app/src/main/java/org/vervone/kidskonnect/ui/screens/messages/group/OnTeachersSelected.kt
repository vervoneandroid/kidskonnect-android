package org.vervone.kidskonnect.ui.screens.messages.group

import org.vervone.kidskonnect.core.domain.model.Teacher

interface OnTeachersSelected {
    fun onTeachersSelected(selected: ArrayList<Teacher>)
}