package org.vervone.kidskonnect.ui.screens.messages

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.cloudstorage.CloudStorage
import org.vervone.kidskonnect.core.helper.ThreadUtils
import org.vervone.kidskonnect.ui.utils.ImageLoader
import org.vervone.kidskonnect.ui.utils.Urls
import org.vervone.kidskonnect.ui.views.MultiImageView
import java.io.File
import java.util.concurrent.atomic.AtomicInteger
import kotlin.math.min

class ImageProcessor : ChatGroupProfileGen {


    override fun create(context: Context,
                        storage: CloudStorage,
                        userIdList: List<String>,
                        targetId: String,
                        pos: Int,
                        iv: MultiImageView,
                        observer: ChatGroupProfileGen.Observer): ChatGroupProfileGen.Result {
        val result = ChatGroupProfileGen.Result(listOf())
        ThreadUtils.runOnBg {
            Downloader(context, storage, userIdList, targetId, pos, iv, observer, result).create()
        }
        return result
    }


    private class Downloader(private val context: Context,
                             private val storage: CloudStorage,
                             private val userIdList: List<String>,
                             private val targetId: String,
                             private val pos: Int,
                             private val iv: MultiImageView,
                             private val observer: ChatGroupProfileGen.Observer,
                             private val result: ChatGroupProfileGen.Result) {

        private val reqCount = AtomicInteger(userIdList.size)

        private val files = ArrayList<File>()

        fun create() {
            userIdList.forEach {
                val key = Urls.getProfileKey(it)
                val file = File(context.cacheDir, key)
                files.add(file)
                download(storage, file, key)
            }

            if (reqCount.get() == 0) {
                bindImage()
            }
        }

        private fun download(storage: CloudStorage, file: File, key: String) {
            storage.downloadIfNeeded(key, file, object : CloudStorage.DownloadListener {
                override fun onComplete(downloadedPath: String?) {
                    DLog.v(this, "bindImage ds $userIdList")
                    if (reqCount.decrementAndGet() == 0) {
                        bindImage()
                    }
                }

                override fun onError(error: Error) {
                    DLog.v(this, "bindImage de $key $userIdList")
                    if (reqCount.decrementAndGet() == 0) {
                        bindImage()
                    }
                }
            })
        }


        private fun bindImage() {
            ThreadUtils.runOnBg {
                val bmp = getBitmaps(context.resources)
                result.bitmaps = bmp
                if (targetId == iv.tag) {
                    ThreadUtils.runOnUi {
                        observer.reloadAt(pos)
                        iv.addImages(bmp)
                    }
                }
            }
        }

        private fun getBitmaps(res: Resources): List<Bitmap> {
            val maxCount = min(userIdList.size, MAX_IMAGE_COUNT)
            val parts = files.take(maxCount)
            val existingFiles = parts.filter { it.exists() }
            val bitmaps = existingFiles.mapNotNull { ImageLoader.load(it) }.toMutableList()
            val loop = (maxCount - bitmaps.size)
            for (i in 1..loop) {
                val defaultPro = BitmapFactory.decodeResource(res, R.drawable.ic_profile_picture)
                bitmaps.add(defaultPro)
            }

            return bitmaps
        }
    }

    companion object {
        const val MAX_IMAGE_COUNT = 3
    }


}