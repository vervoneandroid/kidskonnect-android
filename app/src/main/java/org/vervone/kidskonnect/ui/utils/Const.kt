package org.vervone.kidskonnect.ui.utils

object Const {
    const val PREF_IS_FIRST = "pref_is_first"

    // notification count

    const val NEWS_COUNT = "news.count"

    const val ACTIONS_NOTIFICATIONS = "ACTIONS_NOTIFICATIONS"
    const val ACTIONS_PROFILE_PIC = "ACTIONS_PROFILE_PIC"

    const val MESSAGE = "message"

    const val LAST_LOGIN_UID = "LAST_LOGIN_UID"

    // Used to save the unread comment type for navigation.
    const val UNREAD_COMMENT_TYPE = "unread_comment_type"

}