package org.vervone.kidskonnect.ui.screens.startup

import org.vervone.kidskonnect.ui.screens.base.BaseView

interface AppStartupView : BaseView {
    fun showAppUpdate()
    fun showNextScreen()
    fun showConnectionError()
    fun showLoading()
    fun dismissLoading()
}