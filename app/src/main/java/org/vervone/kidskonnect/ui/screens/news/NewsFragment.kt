package org.vervone.kidskonnect.ui.screens.news

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Paint
import android.graphics.PorterDuff
import android.graphics.Rect
import android.net.Uri
import android.os.Bundle
import android.support.customtabs.CustomTabsIntent
import android.support.design.widget.FloatingActionButton
import android.support.v4.content.ContextCompat
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.SparseBooleanArray
import android.util.SparseIntArray
import android.view.*
import android.widget.*
import com.squareup.picasso.Cache
import com.squareup.picasso.LruCache
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.cloudstorage.CloudStorage
import org.vervone.kidskonnect.core.domain.model.ContentType
import org.vervone.kidskonnect.core.domain.model.News
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.utils.*
import org.vervone.kidskonnect.ui.views.CirclePageIndicator
import org.vervone.kidskonnect.ui.views.LinkPreview
import java.io.File


class NewsFragment : BaseFragment(), NewsView, PlayButtonDelegate {


    private lateinit var rvNewsList: RecyclerView
    //private var fbAddNews: FloatingActionButton? = null
    private var newsPresenter: NewsPresenter? = null
    private var layoutManager: LinearLayoutManager? = null
    private var fab: FloatingActionButton? = null
    //Current scroll position
    private var scrollPos = 0

    private var cache: Cache? = null

    override fun getLayoutId(): Int {
        return R.layout.fragment_news
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.setTitle(R.string.news_feeds_title)
        cache = LruCache(50 * 1024 * 1024)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.setTitle(R.string.news_feeds_title)
        this.rvNewsList = view.findViewById(R.id.rv_news)
        //this.fbAddNews = view.findViewById(R.id.fb_add_news) as FloatingActionButton
        layoutManager = LinearLayoutManager(activity)
        //layoutManager?.reverseLayout = true
        //layoutManager?.stackFromEnd = true

        fab = view.findViewById(R.id.fb_add_news)

        fab?.setOnClickListener {
            FragmentUtils.replace(
                    fragmentManager, CreateNewsFragment()
                    , R.id.content_frame, true
            )
        }

        KeyboardUtil.hideSoftKeyboard(activity!!)

        //var tab : TabLayout

        val itemDecorator = DividerItemDecoration(
                context,
                layoutManager?.orientation!!
        )

        val divider = ContextCompat.getDrawable(context!!, R.drawable.news_divider)
        itemDecorator.setDrawable(divider!!)


        rvNewsList.addItemDecoration(itemDecorator)
        rvNewsList.layoutManager = layoutManager
        rvNewsList.clearOnScrollListeners()
        //rvNewsList.setItemViewCacheSize(6)


        if (!UserHelper.isTeacher()) {
            fab.extHide()
        } else {
            rvNewsList.addOnScrollListener(scrollListener)
        }

        newsPresenter = NewsPresenter()
        newsPresenter?.onAttach(this)
        newsPresenter?.loadNews()

        newsPresenter?.getLikedNews()
    }


    private var scrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            when (newState) {
                RecyclerView.SCROLL_STATE_IDLE -> fab?.show()
                else -> fab?.hide()
            }
            super.onScrollStateChanged(recyclerView, newState)
        }
    }


    override fun onResume() {
        super.onResume()
        NewsCount().markAsRead()
    }


    private fun showPopup(view: View, newsId: String) {

        val context = ContextThemeWrapper(activity, R.style.PopupMenu)

        val popupMenu = PopupMenu(context, view)
        val inflater = popupMenu.menuInflater
        inflater.inflate(R.menu.menu_news, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener { menuItem ->
            when (menuItem!!.itemId) {
                R.id.news_delete -> handleDeleteClick(newsId)
                else -> popupMenu.dismiss()
            }
            true
        }
        popupMenu.show()
    }


    private fun handleDeleteClick(newsId: String) {
        this.scrollPos = layoutManager!!.findFirstVisibleItemPosition()
        newsPresenter?.deleteNews(newsId)
    }


    override fun onPause() {
        super.onPause()
    }


    override fun onDestroyView() {
        newsPresenter?.onDettach()
        cache?.clear()
        super.onDestroyView()
    }

    override fun showProgressBar() {
        super.showLoading()
    }

    override fun onPlayClicked(url: String) {
        val player = FullScreenVideoPlayer.createInstance(url, true)
        player.show(childFragmentManager, null)
    }

    override fun onImageClicked(urls: List<String>) {
        val imageUrls = urls.filter { Urls.isImageUrl(it) }
        val fragment = FullScreenImageViewer.createInstance(imageUrls)
        fragment.show(childFragmentManager, null)
    }


    override fun setNewsList(list: List<News>) {
        if (rvNewsList.adapter == null) {
            rvNewsList.adapter = NewsAdapter(activity!!, list, getCloudStorage(), this)
            rvNewsList.scrollToPosition(scrollPos)
        } else {
            (rvNewsList.adapter as NewsAdapter).list = list
            rvNewsList.adapter?.notifyDataSetChanged()
        }
        super.showContentLayout()
    }

    override fun reloadNews() {
        rvNewsList.adapter?.notifyDataSetChanged()
        super.showContentLayout()
    }

    override fun showError() {
        super.showErrorLayout(getString(R.string.no_data_available))
    }

    private class NewsViewVH(view: View) : RecyclerView.ViewHolder(view) {

        var ivProfile: ImageView? = null
        var tvAuthor: TextView? = null
        var tvTime: TextView? = null
        var ibMore: ImageButton? = null

        var viewPager: ViewPager? = null
        var indicator: CirclePageIndicator? = null
        var tvNews: TextView? = null
        var tvReadMore: TextView? = null

        var ivLikes: ImageButton? = null
        var tvLikeCount: TextView? = null

        var layoutMedia: ViewGroup? = null
        var richLinkView: LinkPreview? = null

        init {

            ivProfile = view.findViewById(R.id.iv_profile_pic)
            tvAuthor = view.findViewById(R.id.tv_author)
            tvTime = view.findViewById(R.id.tv_time)
            ibMore = view.findViewById(R.id.ib_more)

            viewPager = view.findViewById(R.id.vp_attachments)
            indicator = view.findViewById(R.id.indicator)
            tvNews = view.findViewById(R.id.tv_news)
            tvReadMore = view.findViewById(R.id.tv_read_more)

            ivLikes = view.findViewById(R.id.iv_likes)
            tvLikeCount = view.findViewById(R.id.tv_like_count)

            layoutMedia = view.findViewById(R.id.layout_media)
            richLinkView = view.findViewById(R.id.rich_link_preview)


        }

    }

    private inner class NewsAdapter(
            val context: Context, var list: List<News>,
            val cloudStorage: CloudStorage?,
            val delegate: PlayButtonDelegate
    ) : RecyclerView.Adapter<NewsViewVH>() {

        // ViewPager last visible currentItem
        private val viewPagerState = SparseIntArray()
        private val expanded = SparseBooleanArray()

        var textLayoutWidth = -1


        override fun onCreateViewHolder(parent: ViewGroup, position: Int): NewsViewVH {
            val view = LayoutInflater.from(context).inflate(R.layout.kc_layout_news_row, parent, false)

            if (textLayoutWidth == -1) {
                val text = view.findViewById<LinearLayout>(R.id.layout_container)
                text.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                    override fun onGlobalLayout() {
                        val width = text.measuredWidth
                        // DLog.v(this, "width : $width")
                        text.viewTreeObserver.removeOnGlobalLayoutListener(this)
                        if (textLayoutWidth == -1 && width > 0) {
                            textLayoutWidth = width
                            notifyDataSetChanged()
                        }
                    }
                })
            }

            return NewsViewVH(view)
        }

        override fun onViewRecycled(holder: NewsViewVH) {
            holder.viewPager?.let {
                viewPagerState.put(holder.adapterPosition, it.currentItem)
            }
            super.onViewRecycled(holder)
        }


        override fun getItemCount(): Int {
            return list.size
        }

        private fun hasMoreButton(senderId: String?): Boolean {
            if (UserHelper.isTeacher() &&
                senderId.equals(UserHelper.getCurrentUser().uid, false)
            ) {
                return true
            } else if (UserHelper.isAdmin()) {
                return true
            }
            return false
        }

        override fun onBindViewHolder(vh: NewsViewVH, pos: Int) {
            val news = list[pos]
            vh.tvReadMore?.paintFlags = vh.tvReadMore!!.paintFlags or Paint.UNDERLINE_TEXT_FLAG
            vh.tvAuthor?.text = news.senderName
            if (hasMoreButton(news.senderId)) {
                vh.ibMore?.extShow()
            } else {
                vh.ibMore?.extHide()
            }
            vh.ibMore?.setOnClickListener(View.OnClickListener {
                showPopup(vh.ibMore!!, news.id!!)
            })
            vh.tvTime?.text = TimeAgo.getTimeAgo(news.date)
            vh.tvNews?.text = news.description
            vh.tvLikeCount?.text = news.likes?.toString() ?: "0"
            if (news.senderId != null)
                ImageLoader.get(
                        Urls.getProfileUrl(news.senderId!!),
                        R.drawable.ic_profle,
                        R.drawable.ic_profle,
                        vh.ivProfile!!
                )

            when (news.contentType) {
                ContentType.TEXT -> vh.layoutMedia.extHide()
                ContentType.MULTI_MEDIA -> setupMediaRow(news, vh, pos)
                else -> {
                }
            }

            val isLike = newsPresenter != null && newsPresenter!!.isLikedNews(news.id!!)
            if (isLike) {
                vh.ivLikes?.setImageResource(R.drawable.baseline_favorite_red_18)
            } else {
                val drawable = context.resources.getDrawable(R.drawable.baseline_favorite_border_white_18)
                drawable.setColorFilter(context.resources.getColor(R.color.blue_1), PorterDuff.Mode.SRC_ATOP)
                vh.ivLikes?.setImageDrawable(drawable)
            }

            toggleReadMoreButton(pos, vh)

            vh.tvReadMore?.setOnClickListener {
                if (expanded.get(pos, false)) {
                    expanded.delete(pos)
                } else {
                    expanded.put(pos, true)
                }
                toggleReadMoreButton(pos, vh)
            }

            if (vh.tvReadMore != null && vh.tvNews != null) {
                getLineCount(vh.tvNews!!, news.description ?: "", vh.tvReadMore!!)
            }

            vh.ivLikes?.setOnClickListener {
                scrollPos = layoutManager!!.findFirstVisibleItemPosition()
                newsPresenter?.updateLike(news.id!!, !isLike)
            }

            if (news.webLink != null && news.contentType == ContentType.WEB_LINKS) {
                vh.richLinkView?.extShow()
                vh.layoutMedia.extHide()
                vh.richLinkView?.setListener { preview ->
                    linkPreviewMap[news.webLink] = preview?.metaData
                }

                vh.richLinkView?.setOnClickListener {
                    openBrowser(news.webLink)
                }

                if (linkPreviewMap.get(news.webLink) == null) {
                    vh.richLinkView?.setData(news.webLink)
                } else {
                    val md = linkPreviewMap.get(news.webLink)
                    vh.richLinkView?.metaData = md
                }


            } else {
                vh.richLinkView?.extHide()
            }
        }

        private val bounds = Rect()

        private fun getLineCount(tv: TextView, text: String, tvMore: TextView) {
            bounds.setEmpty()
            tv.paint?.getTextBounds(text, 0, text.length, bounds)
            // DLog.v(this, "text : $text | ${bounds.width()} | ${textLayoutWidth}")

            val numLines = Math.ceil((bounds.width() / textLayoutWidth).toDouble())
            if (numLines > 1) {
                tvMore.visibility = View.VISIBLE
            } else {
                tvMore.visibility = View.GONE
            }

        }


        private fun openBrowser(url: String?) {
            val builder = CustomTabsIntent.Builder()
            val customTabsIntent = builder.build()
            customTabsIntent.launchUrl(activity, Uri.parse(url))
        }

        val linkPreviewMap = mutableMapOf<String?, LinkPreview.MetaData?>()

        private fun toggleReadMoreButton(pos: Int, vh: NewsViewVH) {

            if (expanded.get(pos, false)) {
                vh.tvNews?.maxLines = Integer.MAX_VALUE
                vh.tvNews?.ellipsize = null
                vh.tvReadMore?.setText(R.string.view_less)

            } else {
                vh.tvReadMore?.setText(R.string.view_more)
                vh.tvNews?.maxLines = 1
                vh.tvNews?.ellipsize = TextUtils.TruncateAt.END
            }
        }

        private fun setupMediaRow(news: News, vh: NewsViewVH, pos: Int) {
            vh.layoutMedia.extShow()
            vh.viewPager?.adapter = MediaAdapter(context, news.attachments, cloudStorage, delegate)
            vh.indicator?.setViewPager(vh.viewPager)
            vh.viewPager?.setCurrentItem(viewPagerState.get(pos, 0), false)
            if (news.attachments.size > 1) {
                vh.indicator.extShow()
            } else {
                vh.indicator.extHide()
            }
        }

        override fun getItemViewType(position: Int): Int {
            return 1
        }

    }

    private inner class MediaAdapter(
            val context: Context, private val keys:
            List<String>, val cloudStorage: CloudStorage?,
            val delegate: PlayButtonDelegate
    ) : PagerAdapter() {


        fun getThumbnail(path: String): String {
            var thumbnailsPath = ""
            if (path.endsWith(".MOV")) {
                thumbnailsPath = path.replace(".MOV", "_thumpnail.png")
            } else if (path.endsWith(".mp4")) {
                thumbnailsPath = path.replace(".mp4", "_thumpnail.png")
            }

            return thumbnailsPath
        }


        override fun instantiateItem(collection: ViewGroup, position: Int): Any {
            val inflater = LayoutInflater.from(context)
            val layout = inflater.inflate(R.layout.kc_layout_img_with_play, collection, false) as ViewGroup
            val imageView = layout.findViewById<ImageView>(R.id.ivSlinding)
            val ivPlay = layout.findViewById<ImageView>(R.id.iv_play)
            val key = keys[position]

            val isImage = Urls.isImageUrl(key)

            // If the the media type is video , we have to change the path to
            // corresponding  thumbnails path
            val url = if (isImage) keys[position] else getThumbnail(key)

            val bmp = cache?.get(url)
            if (bmp == null) {
                val file = File(context.filesDir, url)
                DLog.v(this, "file# $isImage ${file.name} - ${file.exists()}")
                if (file.exists()) {
                    execute(file, imageView, url)
                } else {
                    cloudStorage?.download(url, file, object : CloudStorage.DownloadListener {
                        override fun onComplete(downloadedPath: String?) {
                            execute(file, imageView, url)
                        }

                        override fun onError(error: Error) {
                            DLog.v(this, "Download Error ${error.message} ")
                            DLog.v(this, "Download Error Url ${url} ")
                        }
                    })
                }
            } else {
                imageView.setImageBitmap(bmp)
            }


            if (!isImage) {
                ivPlay.extShow()
                ivPlay.setOnClickListener {
                    delegate.onPlayClicked(key)
                }
            } else {
                ivPlay.extHide()
            }

            if (isImage) {
                imageView.setOnClickListener {
                    delegate.onImageClicked(keys)
                }
            }

            collection.addView(layout)
            return layout
        }

        private fun execute(file: File?, iv: ImageView, url: String) {
            if (file == null) {
                iv.setImageResource(R.drawable.transparent_place_holder)
            } else {
                ImageLoader.load(file, R.drawable.transparent_place_holder,
                                 R.drawable.ic_student, iv, object : ImageLoader.Callback {
                    override fun onLoad(bmp: Bitmap, uniqueId: String, path: String) {
                        cache?.let {
                            it[url] = bmp
                        }
                        iv.setImageBitmap(bmp)
                    }
                }
                )
            }
        }


        override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
            collection.removeView(view as View)
        }

        override fun getCount(): Int {
            return keys.size
        }

        override fun isViewFromObject(view: View, aObject: Any): Boolean {
            return view === aObject
        }

    }


}

interface PlayButtonDelegate {
    fun onPlayClicked(url: String)
    fun onImageClicked(urls: List<String>)
}