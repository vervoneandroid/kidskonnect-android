package org.vervone.kidskonnect.ui.utils

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import org.vervone.kidskonnect.core.helper.AppHelper
import java.io.File
import java.util.concurrent.atomic.AtomicLong

object ImageLoader {

    private val targets = arrayListOf<TargetCallback>()
    private val uniqReqId = AtomicLong()


    fun load(url: String, placeholder: Int, errorResId: Int, iv: ImageView) {
        val picassoBuilder = Picasso.Builder(AppHelper.getContext())
        val picasso = picassoBuilder.build()
        picasso.load(url)
            .networkPolicy(NetworkPolicy.NO_CACHE)
            .placeholder(placeholder)
            .error(errorResId)
            .into(iv)
    }


    fun get(url: String, placeholder: Int, errorResId: Int, iv: ImageView) {
        Picasso.get()
            .load(url)
            .placeholder(placeholder)
            .error(errorResId)
            .into(iv)
    }

    fun invalidate(url: String) {

    }

    fun get(file: File, placeholder: Int?, errorResId: Int, iv: ImageView) {
        val picasso = Picasso.get()
        //  picasso.setIndicatorsEnabled(true)
        picasso
            .load(file)
            .networkPolicy(NetworkPolicy.OFFLINE).noFade()
            //.placeholder(placeholder)
            .error(errorResId)
            .into(iv)
    }

    fun loadX(file: File,
              placeholder: Int?,
              errorResId: Int,
              iv: ImageView,
              callback: Callback): String {
        val uniqReqId = uniqReqId.incrementAndGet().toString()
        if (iv.tag != null) {
           // throw IllegalArgumentException("Dont set tag for imageview ")
        }
        iv.tag = uniqReqId
        val target = TargetCallback(callback, uniqReqId, file.name)
        targets.add(target)

        val picasso = Picasso.get()
            .load(file)
            .networkPolicy(NetworkPolicy.OFFLINE, NetworkPolicy.NO_CACHE).noFade()
            .error(errorResId)
            .into(target)

        return uniqReqId
    }

    fun load(file: File,
             placeholder: Int?,
             errorResId: Int,
             iv: ImageView,
             callback: Callback): String {
        val uniqReqId = uniqReqId.incrementAndGet().toString()
        if (iv.tag != null) {
//            throw IllegalArgumentException("Dont set tag for imageview ")
        }
        iv.tag = uniqReqId
        val target = TargetCallback(callback, uniqReqId, file.name)
        targets.add(target)

        val picasso = Picasso.get()
        picasso
            .load(file)
            .networkPolicy(NetworkPolicy.OFFLINE, NetworkPolicy.NO_CACHE).noFade()
            .error(errorResId)
            .into(target)

        return uniqReqId
    }

    fun load(file: File): Bitmap? {
        return try {
            val picasso = Picasso.get()
            picasso.load(file)
                .networkPolicy(NetworkPolicy.OFFLINE, NetworkPolicy.NO_CACHE).noFade().get()
        } catch (ex: Exception) {
            null
        }
    }


    private class DefaultCallback(val callback: Callback, val path: String) : Target {
        override fun onPrepareLoad(placeHolderDrawable: Drawable?) {

        }

        override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {

        }

        override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
            callback.onLoad(bitmap!!, "", path)
        }
    }


    private class TargetCallback(
            val callback: Callback,
            val uniqueId: String,
            val path: String
    ) : Target {

        override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
        }

        override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {
            targets.remove(this)
        }

        override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
            callback.onLoad(bitmap!!, uniqueId, path)
            targets.remove(this)
        }
    }


    interface Callback {
        fun onLoad(bmp: Bitmap, uniqueId: String, path: String)
    }

}