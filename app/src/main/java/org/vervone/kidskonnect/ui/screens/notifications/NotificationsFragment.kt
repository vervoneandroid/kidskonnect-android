package org.vervone.kidskonnect.ui.screens.notifications

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.domain.model.Notification
import org.vervone.kidskonnect.core.helper.Formatter
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.utils.configure
import org.vervone.kidskonnect.ui.utils.inflater

class NotificationsFragment : BaseFragment(), NotificationsView {


    private lateinit var rvNotification: RecyclerView
    private var notifications: List<Notification>? = null
    private var presenter: NotificationsPresenter? = null

    override fun getLayoutId(): Int {
        return R.layout.fragment_notifications
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.rvNotification = view.findViewById(R.id.rv_notifications)
        rvNotification.configure()
        activity?.setTitle(R.string.notifications)
        this.presenter = NotificationsPresenter()
        this.presenter?.onAttach(this)
        showLoading()
        this.presenter?.getNotifications()
    }

    override fun onDestroyView() {
        this.presenter?.onDettach()
        super.onDestroyView()
    }

    override fun setNotifications(notifications: List<Notification>) {
        this.notifications = notifications
        if (rvNotification.adapter == null) {
            rvNotification.adapter = Adapter()
        } else {
            rvNotification.adapter?.notifyDataSetChanged()
        }

        showContentLayout()
    }


    private class VH(view: View) : RecyclerView.ViewHolder(view) {
        val tvTitle: TextView
        val tvDate: TextView
        val tvContent: TextView

        init {
            tvContent = view.findViewById(R.id.tv_notification_content)
            tvDate = view.findViewById(R.id.tv_notification_date)
            tvTitle = view.findViewById(R.id.tv_notification_title)
        }
    }

    private inner class Adapter : RecyclerView.Adapter<VH>() {

        override fun onCreateViewHolder(vg: ViewGroup, pos: Int): VH {
            val view = vg.context.inflater().inflate(R.layout.kc_layout_notification_row, vg, false)
            return VH(view)
        }

        override fun getItemCount(): Int {
            return notifications?.size ?: 0
        }

        override fun onBindViewHolder(vh: VH, pos: Int) {
            val notification = notifications?.get(pos)!!
            vh.tvDate.text = Formatter.getDate(notification.date)
            vh.tvContent.text = notification.description
            vh.tvTitle.text = notification.title
        }
    }

}