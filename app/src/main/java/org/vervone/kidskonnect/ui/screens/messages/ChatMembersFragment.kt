package org.vervone.kidskonnect.ui.screens.messages

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.ui.screens.base.BaseFullScreenWindow
import org.vervone.kidskonnect.ui.utils.ImageLoader
import org.vervone.kidskonnect.ui.utils.Urls
import org.vervone.kidskonnect.ui.utils.configure
import org.vervone.kidskonnect.ui.utils.inflate

class ChatMembersFragment : BaseFullScreenWindow() {

    private lateinit var rvMembers: RecyclerView
    private var memberList: List<Pair<String, String>>? = null

    override fun getLayoutId(): Int {
        return R.layout.fragment_chat_members
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val targets = arguments?.getSerializable(TARGETS) as? HashMap<String, String>
        this.memberList = targets?.toList() ?: emptyList()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.rvMembers = view.findViewById(R.id.rv_chat_members)
        rvMembers.configure()

        val ivClose = view.findViewById<ImageView>(R.id.iv_close)
        ivClose.setOnClickListener {
            dismiss()
        }

        rvMembers.adapter = Adapter()
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)
    }


    private class VH(view: View) : RecyclerView.ViewHolder(view) {
        val tvPersonName: TextView = view.findViewById(R.id.tv_member_name)
        val ivProfilePic: ImageView = view.findViewById(R.id.iv_profile_pic)
    }

    private inner class Adapter : RecyclerView.Adapter<VH>() {

        override fun onCreateViewHolder(vg: ViewGroup, pos: Int): VH {
            return VH(vg.inflate(R.layout.kc_layout_chat_member))
        }

        override fun getItemCount(): Int {
            return memberList?.size ?: 0
        }

        override fun onBindViewHolder(vh: VH, pos: Int) {
            val member = memberList!![pos]
            vh.tvPersonName.text = member.second

            ImageLoader.get(
                    Urls.getProfileUrl(member.first),
                    R.drawable.ic_profle,
                    R.drawable.ic_profle,
                    vh.ivProfilePic
            )
        }
    }

    companion object {
        private const val TARGETS = "targets"
        fun createInstance(targets: HashMap<String, String>): ChatMembersFragment {
            val args = Bundle()
            args.putSerializable(TARGETS, targets)
            val fragment = ChatMembersFragment()
            fragment.arguments = args
            return fragment
        }
    }
}