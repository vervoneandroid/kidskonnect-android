package org.vervone.kidskonnect.ui.screens.leave

import android.graphics.Color
import android.graphics.Rect
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.text.Spanned
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.domain.model.Leave
import org.vervone.kidskonnect.core.domain.model.LeaveStatus
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.utils.*

class LeavesFragment : BaseFragment(), LeavesView, CreateLeaveRequestFragment.Listener {


    private lateinit var rvLeaves: RecyclerView
    private var presenter: LeavesPresenter? = null
    private var leaves: List<Leave>? = null
    private var layoutManager: LinearLayoutManager? = null

    override fun setData(leaves: List<Leave>) {
        this.leaves = leaves

        if (this.rvLeaves.adapter == null) {
            this.rvLeaves.adapter = Adapter()
        } else {
            this.rvLeaves.adapter?.notifyDataSetChanged()
        }
        if (leaves.isEmpty()) {
            showErrorLayout(getString(R.string.no_leaves_found))
        } else {
            showContentLayout()
        }
    }

    override fun onSuccess() {
    }

    override fun showError(message: String) {
        showErrorLayout(getString(R.string.unknown_backend_error))
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_leaves
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.rvLeaves = view.findViewById(R.id.rv_leaves)
        this.rvLeaves.isSaveEnabled = false

        val fbRequestLeave = view.findViewById<View>(R.id.fb_request_leave)
        fbRequestLeave.visibility = View.VISIBLE
        fbRequestLeave.setOnClickListener {
            val fragment = CreateLeaveRequestFragment()
            fragment.setTargetFragment(this, 0)
            navHelper?.navToScreen(fragment, true)
        }

        this.layoutManager = LinearLayoutManager(activity!!)
        rvLeaves.layoutManager = layoutManager
        layoutManager?.reverseLayout = true
        layoutManager?.stackFromEnd = true
        layoutManager?.recycleChildrenOnDetach = true
        activity?.setTitle(R.string.leaves)

        val itemDecorator = DividerItemDecoration(context, layoutManager!!.orientation)
        val divider = ContextCompat.getDrawable(context!!, R.drawable.divider_2dp)
        itemDecorator.setDrawable(divider!!)
        rvLeaves.addItemDecoration(itemDecorator)

        this.presenter = LeavesPresenter()
        this.presenter?.onAttach(this)
        showLoading()

        this.presenter?.getLeaves()
        this.presenter?.subscribe()

    }


    override fun onDestroyView() {
        presenter?.unsubscribe()
        presenter?.onDettach()
        super.onDestroyView()
    }

    // VH
    private class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvRequestDate: TextView
        var tvStartEndDate: TextView
        var tvReqStaus: TextView
        var tvReason: TextView

        var tvTeacherName: TextView
        var tvReply: TextView

        var ivCollapse: ImageView
        var ivExpand: ImageView

        var layoutReplyPanel: View
        var layoutComment: View
        var divider: View

        init {

            tvRequestDate = itemView.findViewById(R.id.tv_requested_date)
            tvStartEndDate = itemView.findViewById(R.id.tv_leave_dates)
            tvReqStaus = itemView.findViewById(R.id.tv_leave_status)
            tvReason = itemView.findViewById(R.id.tv_leave_reason)

            tvTeacherName = itemView.findViewById(R.id.tv_teacher_name)
            tvReply = itemView.findViewById(R.id.tv_reply)

            ivCollapse = itemView.findViewById(R.id.iv_collapse)
            ivExpand = itemView.findViewById(R.id.iv_expand)

            layoutReplyPanel = itemView.findViewById(R.id.layout_reply_panel)
            layoutComment = itemView.findViewById(R.id.layout_comment)
            divider = itemView.findViewById(R.id.divider)

        }

    }


    private inner class Adapter : RecyclerView.Adapter<VH>() {

        private var textLayoutWidth = -1
        private val bounds = Rect()

        override fun onCreateViewHolder(vg: ViewGroup, pos: Int): VH {
            val view = vg.context.inflater().inflate(R.layout.kc_leave_row, vg, false)
            if (textLayoutWidth == -1) {
                val text = view.findViewById<LinearLayout>(R.id.layout_reason)
                text.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                    override fun onGlobalLayout() {
                        val width = text.measuredWidth
                        //DLog.v(this, "width : $width")
                        text.viewTreeObserver.removeOnGlobalLayoutListener(this)
                        if (textLayoutWidth == -1 && width > 0) {
                            textLayoutWidth = width
                            notifyDataSetChanged()
                        }
                    }
                })
            }
            return VH(view)
        }

        override fun getItemCount(): Int {
            return leaves?.size ?: 0
        }


        private fun getLineCount(tv: TextView, text: String, tvMore: View) {
            bounds.setEmpty()
            tv.paint?.getTextBounds(text, 0, text.length, bounds)
            val numLines = bounds.width() / textLayoutWidth.toDouble()

            //DLog.v(this, "text : $text | ${bounds.width()} | $textLayoutWidth | $numLines")
            if (numLines > 1) {
                // tvMore.visibility = View.VISIBLE
            } else {
                // tvMore.visibility = View.GONE
            }

        }

        override fun onBindViewHolder(vh: VH, pos: Int) {
            val leave = leaves?.get(pos)!!

            vh.tvReason.text = leave.reason
            getStatus(leave.leaveStatus, vh.tvReqStaus)
            vh.tvStartEndDate.text = getFormattedDate(leave)
            vh.tvRequestDate.text = leave.sendDate

            vh.tvStartEndDate.setOnClickListener {
                val selected: ArrayList<String> = ArrayList()
                if (leave.leaveDays != null) {
                    val list = leave.leaveDays!!.filterNotNull()
                    selected.addAll(list)
                }
                val datePickerDialog = LeaveCalenderFragment
                    .createInstance(selected)
                datePickerDialog.show(childFragmentManager, LeaveCalenderFragment.TAG)
            }

            toggle(leave, vh)

            vh.ivCollapse.setOnClickListener {
                leave.isExpanded = !leave.isExpanded
                toggle(leave, vh)
            }

            vh.ivExpand.setOnClickListener {
                leave.isExpanded = !leave.isExpanded
                toggle(leave, vh)
            }

            vh.tvTeacherName.text = getString(R.string.message_from, leave.teacherName)
            vh.tvReply.text = leave.replyString

            if (leave.reason != null) {
                getLineCount(vh.tvReason, leave.reason!!, vh.ivExpand)
            }
        }

        fun toggle(leave: Leave, vh: VH) {
            if (leave.isExpanded) {
                vh.tvReason.setSingleLine(false)
                if (leave.leaveStatus == LeaveStatus.PENDING) {
                    vh.layoutReplyPanel.extHide()
                } else {
                    vh.layoutReplyPanel.extShow()
                }
                vh.ivExpand.extHide()
                vh.ivCollapse.extShow()
                vh.layoutComment.extShow()

            } else {
                vh.layoutReplyPanel.extHide()
                vh.tvReason.setSingleLine(true)
                vh.ivExpand.extShow()
                vh.ivCollapse.extHide()
                vh.layoutComment.extHide()
            }

            vh.divider.visibility = vh.layoutReplyPanel.visibility
        }


        fun getInt(value: String?): Int {
            if (value?.toIntOrNull() != null) {
                return value.toIntOrNull()!!
            }
            return 0
        }

        private fun getFormattedDate(leave: Leave): Spanned? {
            val suffix = if (getInt(leave.noOfDays) > 1) "s" else ""
            val startEndDate = "${leave.leaveStartsFrom} - ${leave.leaveEndsAt}"
            val styledText =
                "$startEndDate <font color='#3E9FB2'>( <u>${leave.noOfDays} day$suffix</u> )</font>"
            return Html.fromHtml(styledText)
        }

        private fun getStatus(status: LeaveStatus, tvStatus: TextView) {
            var statusValue = ""
            var color = ""
            var drawable = 0

            when (status) {
                LeaveStatus.PENDING -> {
                    statusValue = resources.getString(R.string.pending)
                    color = "#64ddf5"
                    drawable = R.drawable.ic_pending
                }
                LeaveStatus.APPROVED -> {
                    statusValue = resources.getString(R.string.approved)
                    color = "#4CDA65"
                    drawable = R.drawable.ic_accepted
                }
                LeaveStatus.REJECTED -> {
                    statusValue = resources.getString(R.string.rejected)
                    color = "#FF3B2F"
                    drawable = R.drawable.ic_rejected
                }
            }
            val styledText = "<font color='$color'>$statusValue</font>"
            tvStatus.text = Html.fromHtml(styledText)

            val icon = resources.getDrawable(drawable)

            val tHeight = 14.dpToPx
            icon.setBounds(0,
                           0,
                           icon.intrinsicWidth * tHeight / icon.intrinsicHeight,
                           tHeight)
            tvStatus.setCompoundDrawables(icon, null, null, null)
            UiUtils.setTextViewDrawableColorFilter(tvStatus, Color.parseColor(color))
        }

    }

}