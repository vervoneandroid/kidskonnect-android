package org.vervone.kidskonnect.ui.screens

import android.content.DialogInterface
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.DialogFragment
import android.support.v7.app.AppCompatActivity
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.ApiError
import org.vervone.kidskonnect.core.data.network.NoDataError
import org.vervone.kidskonnect.core.domain.DomainError
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.TeachersForTheClass
import org.vervone.kidskonnect.core.domain.model.Teacher
import org.vervone.kidskonnect.core.domain.model.student.Student
import org.vervone.kidskonnect.core.helper.GServicesUtils
import org.vervone.kidskonnect.core.helper.PreferenceHelper
import org.vervone.kidskonnect.core.helper.SchoolConfigHelper
import org.vervone.kidskonnect.ui.screens.login.LoginActivity
import org.vervone.kidskonnect.ui.screens.onboarding.OnboardingActivity
import org.vervone.kidskonnect.ui.screens.startup.AppStartupPresenter
import org.vervone.kidskonnect.ui.screens.startup.AppStartupView
import org.vervone.kidskonnect.ui.utils.*


class SplashActivity : AppCompatActivity(), AppStartupView {

    companion object {
        private const val SPLASH_DURATION = 200L
    }

    private val authFacade = FacadeFactory.authFacade
    private val schoolFacade = FacadeFactory.schoolFacade

    private var presenter: AppStartupPresenter? = null
    private var progressDialog: DialogFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        DLog.v(this, "#SCREEN DENSITY :" + UiUtils.determineScreenDensityCode(resources))

        val maxMemory = (Runtime.getRuntime().maxMemory() / 1024).toInt()
        DLog.v(this, "#maxMemory :$maxMemory")

        this.presenter = AppStartupPresenter()
        this.presenter?.onAttach(this)
    }


    override fun onDestroy() {
        this.presenter?.onDettach()
        super.onDestroy()
    }

    override fun showAppUpdate() {
        val dialog = DialogFactory.createGenericErrorDialog(
                this@SplashActivity, getString(R.string.update_app),
                DialogInterface.OnClickListener { _, _ ->
                    SchoolConfigHelper.invalidateConfig()
                    AppUtils.restartApp()
                })
        dialog.setCancelable(false)
        dialog.show()
    }

    private fun navigateToSchoolSelection() {
        SchoolSelectionActivity.start(this)
        finish()
    }

    override fun showConnectionError() {
        val dialog = DialogFactory.createGenericErrorDialog(
                this, getString(R.string.unknown_backend_error),
                DialogInterface.OnClickListener { _, _ ->
                    moveTaskToBack(true);
                    finish();
                })
        dialog.setCancelable(false)
        dialog.show()
    }


    override fun showLoading() {
        progressDialog = DialogFactory.createProgressDialog(this)
        progressDialog?.show(supportFragmentManager, null)
    }

    override fun dismissLoading() {
        progressDialog?.dismissAllowingStateLoss()
    }

    override fun showNextScreen() {
        Handler().postDelayed({ navigateTo() }, 0)
    }

    private fun navigateTo() {
        if (PreferenceHelper.getBoolean(Const.PREF_IS_FIRST, true)) {
            OnboardingActivity.start(this)
            finish()
        } else {
            val userData = UserHelper.getUserData()
            if (authFacade.isLoggedIn() && userData != null) {
                if (userData.isTeacher) {
                    authFacade.getTeacherAllDetails(userData.uid, teacherCallback)
                } else {
                    authFacade.fetchStudentProfile(userData.uid, studentCallback)
                }
            } else {
                SubscriptionHelper.unsubscribeAll()
                LoginActivity.start(this)
                finish()
            }
        }
    }

    private val studentCallback = object : RequestCallback<Student, Error> {
        override fun onSuccess(res: Student?) {
            if (res != null) {
                UserHelper.setStudent(res)
                UserHelper.saveUserData(isTeacher = false)
                val academicYr = AppUtils.getCurrentAcademicYear()
                val className = res.getClass(academicYr) ?: ""
                val callback = TeachersCallback(res)
                schoolFacade.getTeachers(academicYr, className, callback)
            }
        }

        override fun onError(error: Error?) {
            //TODO : Splash screen error handling
        }
    }

    private val teacherCallback = object : RequestCallback<Teacher, ApiError> {

        override fun onSuccess(res: Teacher?) {
            UserHelper.setTeacher(res!!)
            UserHelper.saveUserData(isTeacher = true)
            val extras = intent.extras
            if (extras != null) {
                val data = NotificationMapper().map(extras)
                MainActivity.start(this@SplashActivity, MainActivity.createExtras(res, data))
            } else {
                MainActivity.start(this@SplashActivity, MainActivity.createExtras(res))
            }
            finish()
        }

        override fun onError(error: ApiError?) {
            var errorMsg = getString(R.string.unknown_backend_error)
            if (error?.cause is NoDataError) {
                errorMsg = getString(R.string.user_not_found_in_the_academic_year)
            }
            val dialog = DialogFactory.createGenericErrorDialog(
                    this@SplashActivity, errorMsg,
                    DialogInterface.OnClickListener { _, _ ->
                        moveTaskToBack(true);
                        finish()
                    })
            dialog.setCancelable(false)
            dialog.show()
        }
    }


    override fun onResume() {
        super.onResume()
        if (GServicesUtils.checkPlayServices(this)) {
            when {
                SchoolConfigHelper.hasValidConfig() -> {
                    SchoolConfigHelper.load()
                    presenter?.startApp(SchoolConfigHelper.getConfig())
                }
                PreferenceHelper.getBoolean(Const.PREF_IS_FIRST, true) -> {
                    showOnBoarding()
                }
                else -> {
                    navigateToSchoolSelection()
                }
            }
        }
    }

    private fun showOnBoarding() {
        Handler().postDelayed(
                {
                    OnboardingActivity.start(this)
                    finish()
                }, SPLASH_DURATION)
    }

    // Fetch teachers for the class.
    private inner class TeachersCallback(val student: Student) : RequestCallback<TeachersForTheClass, DomainError> {
        override fun onError(error: DomainError?) {}
        override fun onSuccess(res: TeachersForTheClass?) {
            finish()
            val extras = intent.extras
            if (extras != null) {
                val data = NotificationMapper().map(extras)
                MainActivity.start(this@SplashActivity, MainActivity.createExtras(student, data))
            } else {
                MainActivity.start(this@SplashActivity, MainActivity.createExtras(student))
            }
        }
    }

}
