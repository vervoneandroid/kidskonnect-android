package org.vervone.kidskonnect.ui.screens.news

import android.os.Bundle
import android.view.View
import android.widget.Button
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.ui.screens.base.BaseBottomSheetFragment
import org.vervone.mediapicker.Image.ImagePicker
import org.vervone.mediapicker.Video.VideoPicker


class MediaOptionsBottomSheet : BaseBottomSheetFragment() {


    override fun getLayoutId(): Int {
        return R.layout.kc_bs_layout_upload_option
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<Button>(R.id.btn_add_photo).setOnClickListener {
            ImagePicker.Builder(activity)
                .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                .directory(ImagePicker.Directory.DEFAULT)
                .extension(ImagePicker.Extension.PNG)
                .scale(600, 600)
                .allowMultipleImages(false)
                .enableDebuggingMode(false)
                .build()

            dismiss()
        }

        view.findViewById<Button>(R.id.btn_add_video).setOnClickListener {
            VideoPicker.Builder(activity)
                .mode(VideoPicker.Mode.CAMERA_AND_GALLERY)
                .directory(VideoPicker.Directory.DEFAULT)
                .extension(VideoPicker.Extension.MP4)
                .enableDebuggingMode(false)
                .build()

            dismiss()
        }

    }

}