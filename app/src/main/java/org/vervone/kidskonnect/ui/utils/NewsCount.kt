package org.vervone.kidskonnect.ui.utils

import org.vervone.kidskonnect.core.helper.PreferenceHelper

class NewsCount {

    fun getUnreadCount(): Int {
        val counts = PreferenceHelper.getString(NEWS_COUNT)
        val split = counts?.split("|")

        if (split != null) {
            return split[1].toInt() - split[0].toInt()
        }
        return 0
    }

    fun resetCount() {
        PreferenceHelper.remove(NEWS_COUNT)
    }

    fun markAsRead() {
        val counts = PreferenceHelper.getString(NEWS_COUNT)
        val split = counts?.split("|")
        if (split != null) {
            val count = split[1].toInt()
            PreferenceHelper.save("$count|$count", NEWS_COUNT)
        }
    }

    fun setUnreadCount(newCount: Int): Int {
        val counts = PreferenceHelper.getString(NEWS_COUNT)
        val split = counts?.split("|")
        val unread: Int
        if (split != null) {
            val readCount = split[0].toInt()
            if (readCount > newCount) {
                PreferenceHelper.save("$newCount|$newCount", NEWS_COUNT)
                unread = 0
            } else {
                PreferenceHelper.save("$readCount|$newCount", NEWS_COUNT)
                unread = newCount - readCount
            }

        } else {
            unread = 0
            PreferenceHelper.save("$newCount|$newCount", NEWS_COUNT)
        }

        return if (unread > 0) unread else 0
    }

    companion object {
        private const val NEWS_COUNT = Const.NEWS_COUNT
    }
}