package org.vervone.kidskonnect.ui.utils

import org.vervone.kidskonnect.core.helper.PreferenceHelper

class LeaveCount {

    fun getUnreadCount(): Int {
        val counts = PreferenceHelper.getString(LEAVE_COUNT)
        val split = counts?.split("|")

        if (split != null) {
            return split[1].toInt() - split[0].toInt()
        }
        return 0
    }

    fun resetCount() {
        PreferenceHelper.remove(LEAVE_COUNT)
    }

    fun markAsRead() {
        val counts = PreferenceHelper.getString(LEAVE_COUNT)
        val split = counts?.split("|")
        if (split != null) {
            val count = split[1].toInt()
            PreferenceHelper.save("$count|$count", LEAVE_COUNT)
        }
    }

    fun setUnreadCount(newCount: Int): Int {
        val counts = PreferenceHelper.getString(LEAVE_COUNT)
        val split = counts?.split("|")
        val unread: Int
        if (split != null) {
            val readCount = split[0].toInt()
            PreferenceHelper.save("$readCount|$newCount", LEAVE_COUNT)
            unread = newCount - readCount
        } else {
            PreferenceHelper.save("$newCount|$newCount", LEAVE_COUNT)
            unread = 0
        }

        return if (unread > 0) unread else 0
    }

    companion object {
        private const val LEAVE_COUNT = "LEAVE_COUNT"
    }

}