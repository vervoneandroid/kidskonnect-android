package org.vervone.kidskonnect.ui.screens.leave

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import com.prolificinteractive.materialcalendarview.MaterialCalendarView.SELECTION_MODE_MULTIPLE
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.domain.TeachersForTheClass
import org.vervone.kidskonnect.core.domain.model.Leave
import org.vervone.kidskonnect.core.domain.model.LeaveStatus
import org.vervone.kidskonnect.core.helper.Formatter
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.utils.*
import java.util.*

class CreateLeaveRequestFragment : BaseFragment(), CreateLeaveRequestView,
    LeaveDialogFragment.OnButtonClickListener {

    private lateinit var calenderView: MaterialCalendarView
    private lateinit var tvNumberOfDays: TextView
    private lateinit var etLeaveReason: EditText
    private lateinit var btnApply: Button
    private var createLeavesPresenter: CreateLeaveRequestPresenter? = null


    override fun getLayoutId(): Int {
        return R.layout.fragment_create_leave_request
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.calenderView = view.findViewById(R.id.calendarView)
        calenderView.selectionMode = SELECTION_MODE_MULTIPLE
        this.tvNumberOfDays = view.findViewById(R.id.tv_number_of_days)
        this.etLeaveReason = view.findViewById(R.id.et_leave_reason)
        this.btnApply = view.findViewById(R.id.btn_apply)

        createLeavesPresenter = CreateLeaveRequestPresenter()
        createLeavesPresenter?.onAttach(this)
        btnApply.setOnClickListener {
            createLeaveRequest()
        }

        calenderView.setOnDateChangedListener { _, date, selected ->

            if (selected && isWeekend(date)) {
                calenderView.setDateSelected(date, false)
                updateDays()
                return@setOnDateChangedListener
            }

            if (selected && isSelectedDayIsOver(date)) {

                updateDays()
                val leaveDialog = LeaveDialogFragment.createInstance(date)
                leaveDialog.show(childFragmentManager, LeaveDialogFragment.TAG)
            } else {
                updateDays()
            }
        }
    }

    override fun onCancelClick(calendarDay: CalendarDay?) {
        calenderView.setDateSelected(calendarDay, false)
        updateDays()
    }


    private fun updateDays() {
        val noOfDays = calenderView.selectedDates.size.toString()
        tvNumberOfDays.text = noOfDays
    }


    private fun isWeekend(calendarDay: CalendarDay): Boolean {
        val calender = calendarDay.calendar
        return calender.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY ||
                calender.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY
    }

    private fun isSelectedDayIsOver(calendarDay: CalendarDay): Boolean {
        return calendarDay.isBefore(CalendarDay.today())
    }


    private fun getSelectedDates(): List<Date> {
        return calenderView.selectedDates.sortedBy { cd -> cd.date }.map { it.date }
    }

    private fun createLeaveRequest() {


        val selectedDates = getSelectedDates()
        val text = UiUtils.getString(etLeaveReason)


        if (selectedDates.isEmpty() || text.isBlank()) {
            DialogFactory.createSimpleOkErrorDialog(activity!!,
                                                    R.string.app_name,
                                                    R.string.pls_fill_all_fields_continue).show()
            return
        }


        val leave = Leave()
        val academicYr = AppUtils.getCurrentAcademicYear()
        val className = UserHelper.student?.getClass(academicYr)!!
        leave.schoolClass = className
        leave.leaveStatus = LeaveStatus.PENDING


        leave.leaveDays = selectedDates.map { Formatter.getFormattedDate(it) }

        leave.noOfDays = selectedDates.size.toString()

        leave.leaveStartsFrom = Formatter.getFormatted1Date(selectedDates.first())
        leave.leaveEndsAt = Formatter.getFormatted1Date(selectedDates.last())

        leave.reason = text
        leave.sendDate = Formatter.getFormatted1Date(Calendar.getInstance().time)

        val currentStudent = UserHelper.student
        if (currentStudent != null) {
            leave.studentId = currentStudent.studentId
            leave.studentName = currentStudent.studentName
        }

        val classTeacher = TeachersForTheClass.getClassTeacher()
        if (classTeacher?.teacherId != null) {
            leave.teacherName = classTeacher.teacherName
            createLeavesPresenter?.createLeaveRequest(leave, classTeacher.teacherId!!)
        }

    }

    override fun showError() {
        showContentLayout()
        //TODO : Leave Error handling
    }

    override fun showProgress() {
        showLoading()
    }

    override fun createLeaveRequestSuccess() {
        if (targetFragment is Listener) {
            val listener = targetFragment as Listener
            listener.onSuccess()
        }
        FragmentUtils.pop(activity)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        createLeavesPresenter?.onDettach()
    }


    interface Listener {
        fun onSuccess()
    }

}