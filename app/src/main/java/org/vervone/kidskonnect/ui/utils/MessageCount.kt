package org.vervone.kidskonnect.ui.utils

import org.vervone.kidskonnect.core.helper.PreferenceHelper

class MessageCount {

    fun getUnreadCount(channelId: String): Int {
        return unreadCount(buildKey(channelId))
    }

    fun markAsRead(channelId: String) {
        val counts = PreferenceHelper.getString(buildKey(channelId))
        val split = counts?.split("|")
        if (split != null) {
            PreferenceHelper.save("${split[1]}|${split[1]}", buildKey(channelId))
        }
    }


    fun setUnreadCount(channelId: String, msgCount: Int): Int {

        val channelIdKey = buildKey(channelId)
        if (channelId == activeChatRoomId) {
            PreferenceHelper.save("$msgCount|$msgCount", channelIdKey)
            return msgCount
        }
        val counts = PreferenceHelper.getString(channelIdKey)
        val split = counts?.split("|")
        val unread: Int

        if (split != null) {
            val readCount = split[0].toInt()
            PreferenceHelper.save("$readCount|$msgCount", channelIdKey)
            unread = msgCount - readCount
        } else {
            //val adjCount = msgCount - 1
            PreferenceHelper.save("$msgCount|$msgCount", channelIdKey)
            unread = 0
        }
        return if (unread > 0) unread else 1
    }

    private fun buildKey(channelId: String): String {
        return "$PREFIX_MC$channelId"
    }


    private fun unreadCount(key: String): Int {
        val counts = PreferenceHelper.getString(key)
        val split = counts?.split("|")
        if (split != null) {
            return split[1].toInt() - split[0].toInt()
        }
        return 0
    }

    fun getTotalUnread(): Int {
        val msgcountKeys = PreferenceHelper.getAllKeys().filter {
            it.startsWith(PREFIX_MC)
        }
        return msgcountKeys.map { unreadCount(it) }.fold(0) { sum, i -> sum + i }
    }

    fun resetCount() {
        val msgcountKeys = PreferenceHelper.getAllKeys().filter {
            it.startsWith(PREFIX_MC)
        }
        msgcountKeys.forEach { PreferenceHelper.remove(it) }
    }

    companion object {
        private const val PREFIX_MC = "MC|"
        private var activeChatRoomId: String? = null
        fun setActiveChatRoomId(chatRoomId: String?) {
            this.activeChatRoomId = chatRoomId
        }
    }

}