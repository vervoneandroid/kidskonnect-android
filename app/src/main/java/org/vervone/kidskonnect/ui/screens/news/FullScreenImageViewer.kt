package org.vervone.kidskonnect.ui.screens.news

import android.content.Context
import android.os.Bundle
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.ProgressBar
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.cloudstorage.CloudStorage
import org.vervone.kidskonnect.core.helper.ThreadUtils
import org.vervone.kidskonnect.ui.screens.CloudStorageDelegate
import org.vervone.kidskonnect.ui.screens.base.BaseFullScreenWindow
import org.vervone.kidskonnect.ui.utils.ImageLoader
import org.vervone.kidskonnect.ui.utils.extHide
import org.vervone.kidskonnect.ui.utils.extShow
import org.vervone.kidskonnect.ui.views.CirclePageIndicator

class FullScreenImageViewer : BaseFullScreenWindow() {

    private var viewPager: ViewPager? = null
    private var indicator: CirclePageIndicator? = null
    private var cloudStorage: CloudStorage? = null
    //private var imageUrl: String? = null
    private var imageKeys: ArrayList<String>? = null
    //private var progressBar: ProgressBar? = null

    companion object {
        private const val IMAGE_KEYS = "image_keys"
        //private const val NEED_PRE_SIGN = "need_pre_sign"

        fun createInstance(keys: List<String>?): FullScreenImageViewer {
            val videoFragment = FullScreenImageViewer()
            val bundle = Bundle()
            bundle.putStringArrayList(IMAGE_KEYS, ArrayList(keys))
            videoFragment.arguments = bundle
            return videoFragment
        }


    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (activity is CloudStorageDelegate) {
            val delegate = activity as CloudStorageDelegate
            cloudStorage = delegate.getCloudStorage()
        }

    }

    override fun getLayoutId(): Int {
        return R.layout.kc_layout_fs_img_viewer
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.imageKeys = arguments?.getStringArrayList(IMAGE_KEYS)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewPager = view.findViewById(R.id.v_pager)
        indicator = view.findViewById(R.id.indicator)
        viewPager?.adapter = VPAdapter(activity!!, imageKeys!!)
        indicator?.setViewPager(viewPager)

        view.findViewById<ImageButton>(R.id.ib_close).setOnClickListener {
            dismiss()
        }

    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setBackgroundDrawableResource(android.R.color.transparent);
    }


    inner class VPAdapter(private val mContext: Context, private val urls: ArrayList<String>) : PagerAdapter() {

        override fun instantiateItem(collection: ViewGroup, position: Int): Any {
            val inflater = LayoutInflater.from(mContext)
            val layout = inflater.inflate(R.layout.kc_layout_image_viewer, collection, false) as ViewGroup
            val imageView = layout.findViewById<ImageView>(R.id.iv_slider)

            val progressBar = layout.findViewById<ProgressBar>(R.id.pb_loading)
            progressBar.extShow()

            val url = urls[position]
            // Since The aws s3 url is not public so , we have to get the pre-signed image url.
            cloudStorage?.getSignedUrl(url, object : CloudStorage.Callback {
                override fun onResult(url: String?) {
                    DLog.v(this, "#url# $url")
                    execute(url, imageView, progressBar)
                }
            })


            collection.addView(layout)
            return layout
        }

        private fun execute(url: String?, iv: ImageView, pb: ProgressBar) {
            ThreadUtils.runOnUi(Runnable {
                pb.extHide()
                if (url == null) {
                    iv.setImageResource(R.drawable.transparent_place_holder)
                } else {
                    ImageLoader.get(
                        url, R.drawable.transparent_place_holder, R.drawable.transparent_place_holder, iv
                    )
                }
            })
        }

        override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
            collection.removeView(view as View)
        }

        override fun getCount(): Int {
            return urls.size
        }

        override fun isViewFromObject(view: View, aObject: Any): Boolean {
            return view === aObject
        }

    }

}