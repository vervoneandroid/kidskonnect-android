package org.vervone.kidskonnect.ui.screens.menu

data class NavMenuItem(var name: String, var icon: Int, var id: Int)