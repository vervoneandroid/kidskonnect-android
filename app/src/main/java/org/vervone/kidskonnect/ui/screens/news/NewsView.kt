package org.vervone.kidskonnect.ui.screens.news

import org.vervone.kidskonnect.core.domain.model.News
import org.vervone.kidskonnect.ui.screens.base.BaseView

interface NewsView : BaseView {
    fun showProgressBar()
    //fun hideProgressBar()
    fun setNewsList(list: List<News>)

    fun showError()

    fun reloadNews()
}