package org.vervone.kidskonnect.ui.screens.messages.group

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.buildware.widget.indeterm.IndeterminateCheckBox
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.DomainError
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.model.student.Student
import org.vervone.kidskonnect.core.helper.cast
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.utils.*

class ClassSelectionFragment : BaseFragment(), OnStudentsSelectListener {


    private lateinit var rvClasses: RecyclerView
    private lateinit var tvSelectAll: TextView


    private val schoolFacade = FacadeFactory.schoolFacade
    // Selected students
    private var selectedStudent = HashMap<String, List<Student>>()
    // Original data set
    private val studentsGroupBy = HashMap<String, List<Student>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.selectedStudent = arguments!!.getSerializable(SELECTED_STUDENTS).cast()
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_search_classes
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.rvClasses = view.findViewById(R.id.rv_classes)
        rvClasses.configure()

        tvSelectAll = view.findViewById(R.id.tv_select_all)

        view.findViewById<Button>(R.id.btn_done).setOnClickListener {
            val adapter = (rvClasses.adapter as ClassesAdapter)

            val selectedMap = HashMap<String, List<Student>>()
            adapter.selectedClasses.forEach {
                selectedMap.put(it, studentsGroupBy[it]!!)
            }

            var isAtLeastOne = selectedStudent.isNotEmpty()

            if (isAtLeastOne) {
                for (entry in selectedStudent.entries) {
                    isAtLeastOne = entry.value.any()
                    if (isAtLeastOne) break
                }
            }

            if (!isAtLeastOne && adapter.selectedClasses.isEmpty()) {
                DialogFactory.createSimpleOkErrorDialog(
                        activity!!,
                        null,
                        getString(R.string.please_select_at_least_one_member)).show()
                return@setOnClickListener
            }

            getListener().onClassesSelected(selectedStudent, adapter.selectedClasses)
        }


        tvSelectAll.setOnClickListener {
            val adapter = (rvClasses.adapter as ClassesAdapter)
            adapter.toggle()
        }

        view.findViewById<Button>(R.id.btn_cancel)
            .setOnClickListener {
                (parentFragment as ClosableWindow).closeWindow()
            }


        showLoading()
        val year = AppUtils.getCurrentAcademicYear()
        schoolFacade.getStudents(year, null, null, classCallback)
    }


    private fun getListener(): OnClassesSelected {
        return parentFragment as OnClassesSelected
    }


    private val classCallback = object : RequestCallback<List<Student>, DomainError> {
        override fun onError(error: DomainError?) {
            runOnUi {
                showErrorLayout("")
            }
        }

        override fun onSuccess(res: List<Student>?) {
            runOnUi {
                showContentLayout()

                val year = AppUtils.getCurrentAcademicYear()
                val studentByClass = res!!.groupBy { it.getClass(year)!! }

                studentsGroupBy.clear()
                studentsGroupBy.putAll(studentByClass)

                val allSelectedClasses = ArrayList<String>()

                selectedStudent.entries.forEach { entry ->
                    // If all students in the class is selected , Class check box will be checked.
                    val students = studentByClass.get(entry.key)?.map { student -> student.studentId!! }
                    val selectedStudents = entry.value.map { it.studentId!! }

                    if (students!!.size == selectedStudents.size &&
                        students.containsAll(selectedStudents)
                    ) {
                        allSelectedClasses.add(entry.key)
                    }
                }


                val adapter = ClassesAdapter(ArrayList(studentByClass.keys))
                adapter.selectedClasses = ArrayList(allSelectedClasses)
                rvClasses.adapter = adapter
            }
        }
    }


    private class VH(view: View) : RecyclerView.ViewHolder(view) {
        val tvClassName: TextView = view.findViewById(R.id.tv_class_name)
        val cbClass: IndeterminateCheckBox = view.findViewById(R.id.cb_class)
    }

    private inner class ClassesAdapter(val list: List<String>) : RecyclerView.Adapter<VH>() {

        var selectedClasses = ArrayList<String>()

        override fun onCreateViewHolder(vg: ViewGroup, pos: Int): VH {
            val view = vg.inflate(R.layout.kc_layout_classes_row)
            return VH(view)
        }

        override fun getItemCount(): Int {
            return list.size
        }

        fun toggle() {
            if (studentsGroupBy.keys.size == selectedClasses.size) {
                selectedClasses.clear()
                tvSelectAll.setText(R.string.select_all)
            } else {
                selectedClasses.clear()
                selectedClasses.addAll(studentsGroupBy.keys)
                tvSelectAll.setText(R.string.deselect_all)
            }
            notifyDataSetChanged()
        }

        fun updateUi() {
            if (studentsGroupBy.keys.size == selectedClasses.size) {
                tvSelectAll.setText(R.string.deselect_all)
            } else {
                tvSelectAll.setText(R.string.select_all)
            }
        }

        override fun onBindViewHolder(vh: VH, pos: Int) {
            val className = list[pos]
            vh.tvClassName.text = className

            if (selectedClasses.contains(className)) {
                vh.cbClass.isChecked = true
            } else if (selectedStudent[className] != null && selectedStudent[className]!!.isNotEmpty()) {
                vh.cbClass.isIndeterminate = true
                //vh.cbClass.isChecked = false
            } else {
                vh.cbClass.isChecked = false
                vh.cbClass.isIndeterminate = false
            }

            vh.cbClass.setOnClickListener {
                if (vh.cbClass.isChecked) {
                    if (!selectedClasses.contains(className)) {
                        selectedClasses.add(className)
                    }
                    val students = studentsGroupBy[className]!!
                    selectedStudent.put(className, students)
                } else {
                    selectedClasses.remove(className)
                    selectedStudent.remove(className)
                }
                updateUi()
                notifyDataSetChanged()
            }

            vh.itemView.setOnClickListener {
                val list = studentsGroupBy.get(className) ?: emptyList()
                val selected = selectedStudent[className]?.map { it.studentId!! } ?: emptyList()
                navigateTo(ArrayList(list), ArrayList(selected), className)
            }
        }
    }

    private fun navigateTo(
            studentList: ArrayList<Student>,
            selectedStudents: ArrayList<String>, className: String
    ) {
        val fragment = StudentSearchFragment.createInstance(
                selectedStudents,
                studentList, className
        )
        fragment.setTargetFragment(this, 0)
        FragmentUtils.replace(
                fragmentManager,
                fragment,
                R.id.fragment_container, true
        )
    }

    override fun onStudentsSelected(className: String, students: List<Student>) {
        selectedStudent.put(className, students)
    }


    companion object {
        private const val SELECTED_STUDENTS = "SELECTED_STUDENTS"

        fun createInstance(map: HashMap<String, List<Student>>): ClassSelectionFragment {
            val args = Bundle()
            args.putSerializable(SELECTED_STUDENTS, map)
            val fragment = ClassSelectionFragment()
            fragment.arguments = args
            return fragment
        }
    }

}