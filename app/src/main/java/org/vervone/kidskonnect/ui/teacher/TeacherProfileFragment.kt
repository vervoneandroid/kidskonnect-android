package org.vervone.kidskonnect.ui.teacher

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.cloudstorage.CloudStorage
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.utils.*
import java.io.File


class TeacherProfileFragment : BaseFragment() {

    private lateinit var ivProfilePic: ImageView

    private var croppedImgUri: Uri? = null

    override fun getLayoutId(): Int {
        return R.layout.fragment_teacher_profile
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.setTitle(R.string.profile)
        val tvTeacherName = view.findViewById<TextView>(R.id.tv_teacher_name)
        val tvTeacherDesignation = view.findViewById<TextView>(R.id.tv_designation)
        this.ivProfilePic = view.findViewById(R.id.iv_profile_pic)

        view.findViewById<View>(R.id.edit_profile_layout)
            .setOnClickListener {
                val shape =
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) CropImageView.CropShape.RECTANGLE else CropImageView.CropShape.OVAL
                CropImage.activity().setFixAspectRatio(true)
                    .setCropShape(CropImageView.CropShape.OVAL)
                    .start(context!!, this)
            }


        if (UserHelper.getCurrentUser().uid != null) {
            val url = Urls.getProfileUrl(UserHelper.getCurrentUser().uid!!)
            ImageLoader.load(url,
                             R.drawable.ic_profle,
                             R.drawable.ic_profle,
                             ivProfilePic
            )
        }

        tvTeacherName.text = UserHelper.teacher?.teacherName
        tvTeacherDesignation.text = AppUtils.getDesignation(UserHelper.teacher)
    }

    override fun onResume() {
        super.onResume()

        if (croppedImgUri != null) {
            showLoading()
            ivProfilePic.setImageURI(croppedImgUri)
            val file = File(croppedImgUri!!.path)
            val key = Urls.getProfileKey(UserHelper.getCurrentUser().uid!!)
            getCloudStorage()?.uploadPublic(key, file, object : CloudStorage.UploadListener {
                override fun onComplete() {
                    // Thread.sleep(2000)
                    val intent = Intent(Const.ACTIONS_PROFILE_PIC)
                    context?.sendBroadcast(intent)
                    showContentLayout()
                }

                override fun onFailed() {
                    Toast.makeText(context, R.string.operation_failed, Toast.LENGTH_SHORT).show()
                }
            })

            croppedImgUri = null
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == RESULT_OK) {
                this.croppedImgUri = result.uri
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
            }
        }
    }
}