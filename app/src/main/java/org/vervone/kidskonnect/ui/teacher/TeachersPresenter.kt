package org.vervone.kidskonnect.ui.teacher

import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.DomainError
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.model.Designation
import org.vervone.kidskonnect.core.domain.model.MessageChannel
import org.vervone.kidskonnect.core.domain.model.Teacher
import org.vervone.kidskonnect.ui.screens.base.BasePresenter
import org.vervone.kidskonnect.ui.screens.messages.MessagesUtils
import org.vervone.kidskonnect.ui.utils.AppUtils
import org.vervone.kidskonnect.ui.utils.UserHelper

class TeachersPresenter(val academicYr: String) : BasePresenter<TeachersView>() {

    private val schoolFacade = FacadeFactory.schoolFacade
    private val messageFacade = FacadeFactory.messageFacade

    fun getTeachers() {
        schoolFacade.getTeachers(academicYr, object : RequestCallback<List<Teacher>, DomainError> {
            override fun onError(error: DomainError?) {}
            override fun onSuccess(res: List<Teacher>?) {
                val filtered = res?.filterNot {
                    it.designation == Designation.ADMIN ||
                            it.teacherId == UserHelper.getCurrentUser().uid
                }?.sortedWith(compareBy { it.teacherName })
                view?.setData(filtered!!)
            }
        })
    }

    fun getTeacher(className: String) {
        schoolFacade.getTeachersDetails(academicYr, className, object : RequestCallback<List<Teacher>, DomainError> {
            override fun onError(error: DomainError?) {}
            override fun onSuccess(res: List<Teacher>?) {
                val filtered = res?.filterNot {
                    it.designation == Designation.ADMIN ||
                            it.teacherId == UserHelper.getCurrentUser().uid
                }?.sortedWith(compareBy { it.teacherName })
                view?.setData(filtered!!)
            }
        })
    }


    fun navigateToChat(teacher: Teacher) {

        val targets = listOfNotNull(teacher.teacherId,
                                    UserHelper.getCurrentUser().uid)

        val targetId = MessagesUtils.buildTargetId(targets)
        messageFacade.getChatRoomIfAvailable(
                AppUtils.getCurrentAcademicYear(),
                targetId, object : RequestCallback<MessageChannel?, DomainError> {

            override fun onSuccess(res: MessageChannel?) {
                if (res == null) {
                    view?.navigateToCreateChatGroup(teacher)
                    DLog.v(this, "Chat room doesn't exists")
                } else {
                    DLog.v(this, "Chat room exists")
                    view?.navigateToChatRoom(res)
                }
            }

            override fun onError(error: DomainError?) {
                //TODO : Message error handling
            }
        })
    }

}