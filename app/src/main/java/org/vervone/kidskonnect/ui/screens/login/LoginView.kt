package org.vervone.kidskonnect.ui.screens.login

import org.vervone.kidskonnect.core.domain.model.Teacher
import org.vervone.kidskonnect.core.domain.model.student.Student
import org.vervone.kidskonnect.ui.screens.base.BaseView

interface LoginView : BaseView {
    fun showProgressBar()
    fun hideProgressBar()
    fun showError(message: String)
    fun showUnknownError()
    fun loginSuccess(student: Student)
    fun loginSuccess(teacher: Teacher)
}