package org.vervone.kidskonnect.ui.screens.notifications

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.DomainError
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.model.Notification
import org.vervone.kidskonnect.ui.screens.base.BasePresenter
import org.vervone.kidskonnect.ui.utils.AppUtils

class NotificationsPresenter : BasePresenter<NotificationsView>() {

    private val notificationFacade = FacadeFactory.notificationFacade

    fun getNotifications() {
        notificationFacade.getNotification(AppUtils.getCurrentAcademicYear()!!,
                notificationCallback)
    }


    private val notificationCallback =
        object : RequestCallback<List<Notification>, DomainError> {
            override fun onError(error: DomainError?) {
                //TODO : Notification Error handling
            }

            override fun onSuccess(res: List<Notification>?) {
                view?.setNotifications(res!!)
            }
        }

}