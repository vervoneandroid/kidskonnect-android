package org.vervone.kidskonnect.ui.screens.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.cloudstorage.CloudStorage
import org.vervone.kidskonnect.ui.screens.CloudStorageDelegate
import org.vervone.kidskonnect.ui.utils.NavigationHelper
import org.vervone.kidskonnect.ui.views.ContentLayout

abstract class BaseFragment : Fragment() {

    abstract fun getLayoutId(): Int

    var navHelper: NavigationHelper? = null
        private set

    private var contentLayout: ContentLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DLog.v(this, "onCreate")
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        DLog.v(this, "onAttach")

        navHelper = NavigationHelper(activity!!)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        DLog.v(this, "onCreateView")
        this.contentLayout = ContentLayout(context!!, getLayoutId(), inflater)
        return contentLayout
    }


    override fun onResume() {
        super.onResume()
        DLog.v(this, "onResume")
    }

    override fun onPause() {
        super.onPause()
        DLog.v(this, "onPause")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        DLog.v(this, "onDestroyView")
    }

    override fun onDestroy() {
        super.onDestroy()
        DLog.v(this, "onDestroy")
    }

    override fun onDetach() {
        super.onDetach()
        DLog.v(this, "onDetach")
    }

    fun showLoading() {
        this.contentLayout?.showProgressBar()
    }

    fun showErrorLayout(message: String) {
        this.contentLayout?.showError(message)
    }

    fun showContentLayout() {
        this.contentLayout?.showContent()
    }


    protected fun getCloudStorage(): CloudStorage? {
        if (activity is CloudStorageDelegate) {
            val delegate = activity as CloudStorageDelegate
            return delegate.getCloudStorage()
        }

        return null
    }

    open fun handleOnActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        val childFrags = childFragmentManager.fragments
        for (fragment in childFrags) {
            if (fragment is BaseFragment) {
                if (fragment.handleOnActivityResult(requestCode, resultCode, data)) {
                    return true
                }
            }
        }
        return false
    }


    protected fun runOnUi(runnable: () -> Unit) {
        activity?.runOnUiThread(runnable)
    }

}