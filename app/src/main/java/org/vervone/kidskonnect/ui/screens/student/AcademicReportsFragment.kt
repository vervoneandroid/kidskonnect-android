package org.vervone.kidskonnect.ui.screens.student

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.domain.model.student.AcademicReport
import org.vervone.kidskonnect.core.helper.Formatter
import org.vervone.kidskonnect.ui.screens.PdfViewerActivity
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.utils.inflater

/**
 *  Show academic report for the specified student
 */
class AcademicReportsFragment : BaseFragment(), AcademicReportsView {

    private lateinit var rvExams: RecyclerView
    private var academicReports: ArrayList<AcademicReport>? = null

    private var presenter: AcademicReportsPresenter? = null
    private var studentId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.studentId = arguments?.getString(STUDENT_ID)
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_exam_schedule
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.rvExams = view.findViewById(R.id.rv_exams)
        val layoutManager = LinearLayoutManager(activity!!)
        rvExams.layoutManager = layoutManager
        activity?.setTitle(R.string.academic_reports)

        val itemDecorator = DividerItemDecoration(context, layoutManager?.orientation!!)
        val divider = ContextCompat.getDrawable(context!!, R.drawable.divider_1dp)
        itemDecorator.setDrawable(divider!!)
        rvExams.addItemDecoration(itemDecorator)

        presenter = AcademicReportsPresenter(studentId!!)
        presenter?.onAttach(this)
        showLoading()
        presenter?.getAcademicReports()

    }


    override fun onDestroyView() {
        presenter?.onDettach()
        super.onDestroyView()
    }

    override fun setData(academicReports: ArrayList<AcademicReport>) {
        showContentLayout()
        this.academicReports = academicReports
        rvExams.adapter = ExamsAdapter()
    }

    override fun showError() {

    }

    private class VH(view: View) : RecyclerView.ViewHolder(view) {
        var tvTitle: TextView = view.findViewById(R.id.tv_report_name)
        var tvDate: TextView = view.findViewById(R.id.tv_report_date)
    }

    private inner class ExamsAdapter : RecyclerView.Adapter<VH>() {
        override fun onCreateViewHolder(vg: ViewGroup, pos: Int): VH {
            val view = vg.context.inflater().inflate(
                    R.layout.kc_layout_reports,
                    null, false
            )
            return VH(view)
        }

        override fun getItemCount(): Int {
            return academicReports?.size ?: 0
        }

        override fun onBindViewHolder(vh: VH, pos: Int) {
            val report = academicReports?.get(pos)
            vh.tvTitle.text = report?.reportName
            vh.tvDate.text = Formatter.getDate(report?.date)

            vh.itemView.setOnClickListener {
                PdfViewerActivity.startActivity(activity!!, report?.reportUrl,
                                                getString(R.string.academic_reports))
            }
        }

    }


    companion object {
        private const val STUDENT_ID = "student_id"
        fun createInstance(studentId: String): AcademicReportsFragment {
            val fragment = AcademicReportsFragment()
            val args = Bundle()
            args.putString(STUDENT_ID, studentId)
            fragment.arguments = args
            return fragment
        }
    }

}