package org.vervone.kidskonnect.ui.utils

import org.vervone.kidskonnect.core.domain.DomainConsts
import org.vervone.kidskonnect.core.domain.model.Designation
import org.vervone.kidskonnect.core.domain.model.Teacher
import org.vervone.kidskonnect.core.domain.model.User
import org.vervone.kidskonnect.core.domain.model.UserData
import org.vervone.kidskonnect.core.domain.model.student.Student
import org.vervone.kidskonnect.core.helper.JsonHelper
import org.vervone.kidskonnect.core.helper.PreferenceHelper

object UserHelper {

    var teacher: Teacher? = null
        private set
    var student: Student? = null
        private set
    private var user: User? = null
    private var userData: UserData? = null

    fun getCurrentUser(): User {

        if (user == null) {
            user = when {
                teacher != null -> {
                    val unwrap = teacher!!
                    val uid = PreferenceHelper.getString(DomainConsts.KEY_CUR_USER_ID)
                    User(unwrap.teacherName, unwrap.profileImageUrl, uid, true)
                }
                student != null -> {
                    val unwrap = student!!
                    val uid = PreferenceHelper.getString(DomainConsts.KEY_CUR_USER_ID)
                    User(unwrap.studentName, unwrap.profileImageUrl, uid, false)
                }
                else -> User("", "", "")
            }
        }

        return user!!
    }

    fun clear() {
        PreferenceHelper.remove(DomainConsts.KEY_CUR_USER_ID)
        PreferenceHelper.remove(DomainConsts.KEY_CURRENT_USER)
        this.teacher = null
        this.student = null
        this.userData = null
        this.user = null
    }

    fun isAdmin(): Boolean {
        loadUserData()

        if (teacher != null) {
            val teacher = teacher!!
            return teacher.designation == Designation.ADMIN ||
                    teacher.designation == Designation.PRINCIPAL
        }

        return false
    }


    fun isTeacher(): Boolean {
        loadUserData()
        return userData?.isTeacher != null
                && userData!!.isTeacher
    }

    fun setTeacher(teacher: Teacher?) {
        this.teacher = teacher
        this.student = null
    }

    fun setStudent(student: Student?) {
        this.student = student
        this.teacher = null
    }

    fun saveUserData(isTeacher: Boolean) {
        val uid = PreferenceHelper.getString(DomainConsts.KEY_CUR_USER_ID)
        val userData = UserData(uid!!, isTeacher)
        PreferenceHelper.save(JsonHelper.toJson(userData), DomainConsts.KEY_CURRENT_USER)
    }


    fun getUserData(): UserData? {
        if (userData == null) {
            loadUserData()
        }
        return userData
    }


    private fun loadUserData() {
        val json = PreferenceHelper.getString(DomainConsts.KEY_CURRENT_USER)
        if (json != null) {
            userData = JsonHelper.fromJson(json, UserData::class.java)
        }
    }

}