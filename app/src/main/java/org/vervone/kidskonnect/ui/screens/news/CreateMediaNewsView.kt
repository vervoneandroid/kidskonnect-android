package org.vervone.kidskonnect.ui.screens.news

import org.vervone.kidskonnect.ui.screens.base.BaseView

interface CreateMediaNewsView : BaseView {
    fun showProgressBar()
    fun setClasses(list: List<CheckedItem>)
    fun showError()
    fun closeWindow()
}