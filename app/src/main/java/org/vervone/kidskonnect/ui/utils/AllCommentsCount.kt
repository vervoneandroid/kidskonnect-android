package org.vervone.kidskonnect.ui.utils

import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import org.vervone.kidskonnect.core.helper.AppHelper

/**
 *  This class used to get all the unread comments added for students.
 *  This is class only used for teachers.
 */
class AllCommentsCount {

    private val commentCountPref: SharedPreferences

    init {
        val context = AppHelper.getContext()
        this.commentCountPref = context.getSharedPreferences(COMMENTS_COUNT_FILE, MODE_PRIVATE)
    }

    private fun buildKey(studentId: String, className: String): String {
        return "$className|$studentId"
    }

    private fun findStudent(studentId: String): String {
        return commentCountPref.all.keys.firstOrNull { it.contains(studentId, false) } ?: ""
    }

    fun markAsRead(studentId: String, className: String) {
        val key = buildKey(studentId, className)
        val counts = commentCountPref.getString(key, null)
        val split = counts?.split("|")
        if (split != null) {
            val count = split[1].toInt()
            val countString = "$count|$count"
            save(key, countString)
        }
    }

    fun setUnreadCount(newCount: Int, studentId: String, className: String) {
        val key = buildKey(studentId, className)
        val counts = commentCountPref.getString(key, null)
        val split = counts?.split("|")
        if (split != null) {
            val readCount = split[0].toInt()
            val countString = "$readCount|$newCount"
            save(key, countString)
        } else {
            val countString = "$newCount|$newCount"
            save(key, countString)
        }
    }

    fun getUnreadCount(studentId: String, className: String): Int {
        return getUnreadCountForKey(buildKey(studentId, className))
    }

    private fun getUnreadCountForKey(key: String): Int {
        val counts = commentCountPref.getString(key, null)
        val split = counts?.split("|")
        if (split != null) {
            return split[1].toInt() - split[0].toInt()
        }
        return 0
    }

    private fun save(key: String, value: String) {
        commentCountPref.edit().putString(key, value).apply()
    }

    fun resetAll() {
        commentCountPref.edit().clear().apply()
    }

    fun getTotalCount(): Int {
        var totalCount = 0
        for (key in commentCountPref.all.keys) {
            totalCount += getUnreadCountForKey(key)
        }
        return totalCount
    }

    fun getCountByClassName(className: String): Int {
        val classKeys = commentCountPref.all.keys.filter { it.startsWith("$className|") }
        var totalCount = 0
        for (key in classKeys) {
            val unreadCnt = getUnreadCountForKey(key)
            if (unreadCnt > 0) {
                totalCount++
            }
        }
        return totalCount
    }

    companion object {
        private const val COMMENTS_COUNT_FILE = "COMMENTS_COUNT_FILE"
    }
}