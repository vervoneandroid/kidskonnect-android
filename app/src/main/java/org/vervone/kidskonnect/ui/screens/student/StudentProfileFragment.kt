package org.vervone.kidskonnect.ui.screens.student

import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.text.SpannableString
import android.text.style.StyleSpan
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.DomainError
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.model.MessageChannel
import org.vervone.kidskonnect.core.domain.model.student.CommentType
import org.vervone.kidskonnect.core.domain.model.student.Student
import org.vervone.kidskonnect.core.helper.PreferenceHelper
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.screens.messages.CreateGroupFragment
import org.vervone.kidskonnect.ui.screens.messages.MessageChatRoomFragment
import org.vervone.kidskonnect.ui.screens.messages.MessagesUtils
import org.vervone.kidskonnect.ui.utils.*
import org.vervone.kidskonnect.ui.views.RecyclerTabLayout

class StudentProfileFragment : BaseFragment(), ViewPager.OnPageChangeListener {

    private lateinit var mViewPager: ViewPager
    private lateinit var recyclerTabLayout: RecyclerTabLayout
    private lateinit var tvStudentName: TextView
    private lateinit var tvSchoolClass: TextView
    private lateinit var ivChat: ImageView
    private lateinit var ivProfilePic: ImageView
    private lateinit var tvAcademicReport: TextView

    private var mAdapter: StudentDetailsAdapter? = null

    private var student: Student? = null
    private var studentId: String? = null
    private var commentType: CommentType = CommentType.PROFILE

    private val messageFacade = FacadeFactory.messageFacade
    private val authFacade = FacadeFactory.authFacade


    override fun getLayoutId(): Int {
        return R.layout.fragment_student_profile
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.student = arguments?.getSerializable(ARG_STUDENT) as Student?
        this.studentId = arguments?.getString(ARG_STUDENT_ID)
        val type = arguments?.getSerializable(ARG_COMMENT_TYPE) as? CommentType
        this.commentType = type ?: CommentType.PROFILE
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.setTitle(R.string.profile)
        KeyboardUtil.hideSoftKeyboard(activity!!)
        // Top Panel
        tvStudentName = view.findViewById(R.id.tv_student_name)
        tvSchoolClass = view.findViewById(R.id.tv_school_class)
        tvAcademicReport = view.findViewById(R.id.tv_academic_report)
        ivChat = view.findViewById(R.id.iv_chat)
        ivProfilePic = view.findViewById(R.id.iv_profile_pic)
        this.mViewPager = view.findViewById(R.id.viewPager)
        this.recyclerTabLayout = view.findViewById(R.id.recycler_tab_layout)

        if (student != null) {
            renderUi()
        } else {
            showLoading()
            authFacade.fetchStudentProfile(studentId, studentCallback)
        }
    }


    private val studentCallback = object : RequestCallback<Student, Error> {
        override fun onError(error: Error?) {}
        override fun onSuccess(res: Student?) {
            if (res?.studentId.isNullOrBlank()) {
                showErrorLayout("")
            } else {
                student = res
                showContentLayout()
                renderUi()
            }
        }
    }

    private fun renderUi() {
        tvStudentName.text = student?.studentName
        val className = student?.getClass(AppUtils.getCurrentAcademicYear()) ?: ""
        tvSchoolClass.text = className

        if (student?.studentId != null) {
            ImageLoader.get(
                    Urls.getProfileUrl(student?.studentId!!),
                    R.drawable.ic_profle,
                    R.drawable.ic_profle,
                    ivProfilePic
            )

            if (student?.studentId == UserHelper.getCurrentUser().uid) {
                CommentsCount().markAsRead()
                PreferenceHelper.remove(Const.UNREAD_COMMENT_TYPE)
            }
        }

        val studentId = student?.studentId
        if (UserHelper.isTeacher() && studentId != null) {
            AllCommentsCount().markAsRead(studentId, className)
        }

        if (!UserHelper.isTeacher()) {
            ivChat.extHide()
        }

        ivChat.setOnClickListener {
            handleChatClick()
        }

        tvAcademicReport.setOnClickListener {
            FragmentUtils.replace(fragmentManager,
                                  AcademicReportsFragment.createInstance(student?.studentId!!),
                                  R.id.content_frame, true
            )
        }

        val student = student
        if (student != null) {
            val studentAdapter = StudentDetailsAdapter(childFragmentManager, resources, student)
            this.mAdapter = studentAdapter
            mViewPager.adapter = mAdapter
            val pageIndex = PAGE_INDEX_MAP[commentType] ?: 1
            mViewPager.currentItem = studentAdapter.getCenterPosition(pageIndex)
            mViewPager.addOnPageChangeListener(this)
            recyclerTabLayout.setUpWithAdapter(TabStyleAdapter(mViewPager))
            recyclerTabLayout.setAutoSelectionMode(true)
        }
    }

    private fun handleChatClick() {
        val targets = listOfNotNull(student?.studentId, UserHelper.getCurrentUser().uid)
        val targetId = MessagesUtils.buildTargetId(targets)
        messageFacade.getChatRoomIfAvailable(
                AppUtils.getCurrentAcademicYear(), targetId, object : RequestCallback<MessageChannel?, DomainError> {
            override fun onSuccess(res: MessageChannel?) {
                if (res == null) {
                    navigateToCreateChatGroup()
                    DLog.v(this, "Chat room doesn't exists")
                } else {
                    DLog.v(this, "Chat room exists")
                    navigateToChatRoom(res)
                }
            }

            override fun onError(error: DomainError?) {
                //TODO : Message error handling
            }
        })
    }

    private fun navigateToCreateChatGroup() {
        student?.getClass(AppUtils.getCurrentAcademicYear())?.let {
            val fragment = CreateGroupFragment.createInstance(it, student!!)
            FragmentUtils.replace(activity?.supportFragmentManager,
                                  fragment, R.id.content_frame, true)
        }
    }

    private fun navigateToChatRoom(messageChannel: MessageChannel) {
        val channelName = messageChannel.getChatName(UserHelper.getCurrentUser().uid!!)
        val fragment = MessageChatRoomFragment.createInstance(messageChannel.id!!,
                                                              chatRoom = messageChannel,
                                                              channelName = channelName,
                                                              chatMessage = null)
        FragmentUtils.replace(activity?.supportFragmentManager,
                              fragment, R.id.content_frame, true)
    }


    override fun onPageScrollStateChanged(p0: Int) {}

    override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {}

    override fun onPageSelected(position: Int) {
        //got to center
        val size = mAdapter!!.getRealPageCount()
        val nearLeftEdge = position <= size
        val nearRightEdge = position >= mAdapter!!.getCount() - size
        if (nearLeftEdge || nearRightEdge) {
            mViewPager.setCurrentItem(mAdapter!!.getCenterPosition(0), false)
        }
    }


    private class TabStyleAdapter(viewPager: ViewPager) : RecyclerTabLayout.Adapter<TabVH>(viewPager) {
        private var adapter: StudentDetailsAdapter? = null

        init {
            adapter = viewPager.adapter as StudentDetailsAdapter?
        }

        override fun onCreateViewHolder(vg: ViewGroup, p1: Int): TabVH {
            val view = vg.context.inflater()
                .inflate(R.layout.kc_layout_tab, vg, false)
            return TabVH(view, viewPager)
        }

        override fun getItemCount(): Int {
            return adapter?.count ?: 0
        }

        override fun onBindViewHolder(vh: TabVH, pos: Int) {
            val title = adapter?.getPageTitle(pos)

            val res = vh.tvTitle.context.resources
            val name = SpannableString(title)
            if (pos == currentIndicatorPosition) {
                vh.tvTitle.setTextColor(res.getColor(R.color.white))
                vh.tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16f)
                name.setSpan(StyleSpan(Typeface.BOLD), 0, name.length, 0)
            } else {
                vh.tvTitle.setTextColor(res.getColor(R.color.white_2))
                vh.tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14f)
            }
            vh.tvTitle.text = name
        }
    }

    private class TabVH(view: View, viewPager: ViewPager) : RecyclerView.ViewHolder(view) {
        val tvTitle: TextView = view.findViewById(R.id.tv_tab_name)

        init {
            itemView.setOnClickListener { viewPager.currentItem = adapterPosition }
        }
    }


    companion object {
        private const val ARG_STUDENT = "argsStudent"
        private const val ARG_STUDENT_ID = "argsStudentId"
        private const val ARG_COMMENT_TYPE = "argsCommentType"

        private val PAGE_INDEX_MAP = mapOf(Pair(CommentType.UNKNOWN, 1),
                                           Pair(CommentType.PROFILE, 1),
                                           Pair(CommentType.TALENTS_ACHIEVEMENT, 2),
                                           Pair(CommentType.HEALTH_MATTERS, 3),
                                           Pair(CommentType.FAMILY_MATTERS, 0))

        fun createInstance(studentId: String, commentType: CommentType = CommentType.UNKNOWN):
                StudentProfileFragment {
            val args = Bundle()
            args.putString(ARG_STUDENT_ID, studentId)
            args.putSerializable(ARG_COMMENT_TYPE, commentType)
            val fragment = StudentProfileFragment()
            fragment.arguments = args
            return fragment
        }

        fun createInstance(student: Student, commentType: CommentType = CommentType.UNKNOWN):
                StudentProfileFragment {
            val args = Bundle()
            args.putSerializable(ARG_STUDENT, student)
            args.putSerializable(ARG_COMMENT_TYPE, commentType)
            val fragment = StudentProfileFragment()
            fragment.arguments = args
            return fragment
        }
    }

}