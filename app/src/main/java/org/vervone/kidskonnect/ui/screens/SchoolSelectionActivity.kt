package org.vervone.kidskonnect.ui.screens

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.DomainError
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.model.school.SchoolConfig
import org.vervone.kidskonnect.core.helper.PreferenceHelper
import org.vervone.kidskonnect.core.helper.SchoolConfigHelper
import org.vervone.kidskonnect.ui.screens.login.LoginActivity
import org.vervone.kidskonnect.ui.screens.onboarding.OnboardingActivity
import org.vervone.kidskonnect.ui.screens.startup.AppStartupPresenter
import org.vervone.kidskonnect.ui.screens.startup.AppStartupView
import org.vervone.kidskonnect.ui.utils.*

class SchoolSelectionActivity : AppCompatActivity(), ProgressBarFragment.OnBackButtonListener {

    private lateinit var recyclerView: RecyclerView
    private lateinit var etSearchSchool: EditText
    private lateinit var tvError: TextView
    private lateinit var progressBar: ProgressBar
    private val schoolsList = mutableListOf<SchoolConfig>()
    private val schoolFacade = FacadeFactory.schoolFacade
    private var presenter: AppStartupPresenter? = null
    private var progressDialog: DialogFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_school_selection)
        this.recyclerView = findViewById(R.id.rvSchoolList)
        this.progressBar = findViewById(R.id.pb_loading)
        this.tvError = findViewById(R.id.tv_fallback)

        this.presenter = AppStartupPresenter()
        this.presenter?.onAttach(StartupViewHandler(this))

        this.recyclerView.configure()
        this.recyclerView.adapter = SchoolAdapter()
        this.etSearchSchool = findViewById(R.id.et_search_query)

        this.etSearchSchool.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                (recyclerView.adapter as? SchoolAdapter)?.filter(s)
            }
        })
    }

    override fun onResume() {
        super.onResume()
        showProgressbar()
        schoolFacade.getSchoolsConfigs(object : RequestCallback<List<SchoolConfig>, DomainError> {
            override fun onError(error: DomainError?) {
                showFallback(getString(R.string.unknown_backend_error))
            }

            override fun onSuccess(res: List<SchoolConfig>?) {
                schoolsList.clear()
                if (res != null) {
                    schoolsList.addAll(res)
                }
                (recyclerView.adapter as? SchoolAdapter)?.filter(null)
                showContent()
            }
        })
    }

    private fun showProgressbar() {
        progressBar.visibility = View.VISIBLE
    }

    private fun showContent() {
        progressBar.visibility = View.GONE
        tvError.visibility = View.GONE
    }

    private fun showFallback(message: String) {
        tvError.text = message
        tvError.visibility = View.VISIBLE
    }


    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvName: TextView = itemView.findViewById(R.id.tvName)
        val tvAddress: TextView = itemView.findViewById(R.id.tvAddress)
    }

    inner class SchoolAdapter : RecyclerView.Adapter<VH>() {
        private val dataSet = mutableListOf<SchoolConfig>()
        override fun onCreateViewHolder(vg: ViewGroup, pos: Int): VH {
            return VH(vg.inflate(R.layout.layout_school_info))
        }

        override fun getItemCount(): Int {
            return dataSet.size
        }

        override fun onBindViewHolder(vh: VH, pos: Int) {
            val school = dataSet[pos]
            vh.tvName.text = school.name
            vh.tvAddress.text = school.address
            vh.itemView.setOnClickListener {
                onSchoolClicked(school)
            }
        }

        fun filter(query: CharSequence?) {
            dataSet.clear()
            if (query.isNullOrBlank()) {
                dataSet.addAll(schoolsList)
            } else {
                val filtered = schoolsList.filter { school ->
                    school.name.contains(query, true)
                }
                dataSet.addAll(filtered)
            }
            if (dataSet.isEmpty()) {
                showFallback(getString(R.string.no_school_found))
            } else {
                showContent()
            }
            notifyDataSetChanged()
        }
    }

    inner class StartupViewHandler(val activity: SchoolSelectionActivity) : AppStartupView {
        override fun showAppUpdate() {
            val dialog = DialogFactory.createGenericErrorDialog(
                    activity, getString(R.string.update_app),
                    DialogInterface.OnClickListener { _, _ ->
                        SchoolConfigHelper.invalidateConfig()
                        AppUtils.restartApp()
                        finish()
                    })
            dialog.setCancelable(false)
            dialog.show()
        }

        override fun showNextScreen() {
            if (PreferenceHelper.getBoolean(Const.PREF_IS_FIRST, true)) {
                OnboardingActivity.start(activity)
                finish()
            } else {
                SubscriptionHelper.unsubscribeAll()
                LoginActivity.start(activity)
            }
        }

        override fun showLoading() {
            activity.showLoading()
        }

        override fun dismissLoading() {
            activity.dismissLoading()
        }

        override fun showConnectionError() {
            val dialog = DialogFactory.createGenericErrorDialog(
                    activity, getString(R.string.unknown_backend_error),
                    DialogInterface.OnClickListener { _, _ ->
                        moveTaskToBack(true);
                        finish()
                        //exitProcess(0)
                    })
            dialog.setCancelable(false)
            dialog.show()
        }
    }

    override fun onBack() {
        moveTaskToBack(true);
        finish()
    }

    private fun showLoading() {
        progressDialog = DialogFactory.createProgressDialog(this)
        progressDialog?.show(supportFragmentManager, null)
    }

    private fun dismissLoading() {
        progressDialog?.dismissAllowingStateLoss()
    }

    private fun onSchoolClicked(schoolConfig: SchoolConfig) {
        SchoolConfigHelper.set(schoolConfig)
        presenter?.startApp(schoolConfig)
    }

    override fun onDestroy() {
        this.presenter?.onDettach()
        this.presenter = null
        super.onDestroy()
    }


    companion object {
        fun start(context: Context) {
            val intent = Intent(context, SchoolSelectionActivity::class.java)
            context.startActivity(intent)
        }
    }
}