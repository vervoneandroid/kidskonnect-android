package org.vervone.kidskonnect.ui.screens.news

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ThumbnailUtils
import android.os.Bundle
import android.provider.MediaStore
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.*
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.domain.model.News
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.utils.DialogFactory
import org.vervone.kidskonnect.ui.utils.FragmentUtils

abstract class BaseCreateNewsFragment : BaseFragment(), CreateMediaNewsView, TargetsDialog.Listener {

    protected lateinit var btnAddAttachment: Button
    protected lateinit var btnPost: Button
    protected lateinit var layoutAttachments: LinearLayout
    protected lateinit var tvAddTarget: TextView
    protected lateinit var etDescription: EditText
    protected lateinit var etUrl: EditText

    private var targetDialog: TargetsDialog? = null

    protected var targets: List<CheckedItem>? = null

    private var presenter: CreateMediaNewsPresenter? = null
    protected var attachments = ArrayList<String>()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.etDescription = view.findViewById(R.id.etDescription)

        this.tvAddTarget = view.findViewById(R.id.tv_add_targets)
        this.tvAddTarget.setOnClickListener {

            val clonedList = targets?.map { CheckedItem(it.name, it.isChecked) }
            targetDialog = TargetsDialog.createInstance(ArrayList(clonedList))
            targetDialog?.listener = this
            targetDialog?.show(childFragmentManager, "")
        }

        this.btnPost = view.findViewById(R.id.btn_post)
        btnPost.setOnClickListener {

            if (isValid()) {
                presenter?.postNews(createNews(), getCloudStorage()!!)
            } else {
                DialogFactory.createSimpleOkErrorDialog(
                    activity!!,
                    R.string.app_name, R.string.fill_required_fields
                ).show()
            }
        }

        this.presenter = CreateMediaNewsPresenter()
        this.presenter?.onAttach(this)
        this.presenter?.fetchClasses()
    }

    protected open fun isValid(): Boolean {
        return false
    }

    abstract fun createNews(): News


    protected fun handleImageUrl(imageUrl: String) {
        DLog.v(this, "path $imageUrl")
        val bitmap =
            BitmapFactory.decodeFile(imageUrl)
        addThumbnail(bitmap, imageUrl)
    }

    protected fun handleVideoUrl(url: String) {
        val thumbnail = ThumbnailUtils.createVideoThumbnail(url, MediaStore.Images.Thumbnails.MICRO_KIND);
        addThumbnail(thumbnail, url)
    }

    private fun addThumbnail(thumbnail: Bitmap, attachment: String) {

        attachments.add(attachment)


        val ivParent = FrameLayout(context)
        ivParent.tag = attachment
        ivParent.setBackgroundColor(context!!.resources.getColor(R.color.black))

        val ivThumbnail = ImageView(context)
        ivThumbnail.scaleType = ImageView.ScaleType.CENTER_CROP

        val ivDelete = ImageView(context)
        val bmDelete = BitmapFactory.decodeResource(resources, R.drawable.baseline_close_red_36)
        ivDelete.setImageBitmap(bmDelete)

        val side = context!!.resources.getDimensionPixelSize(R.dimen.vspace_6)

        val thumbnailParams = FrameLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        ivParent.addView(ivThumbnail, thumbnailParams)

        val removeIvParams = FrameLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        removeIvParams.gravity = Gravity.CENTER
        ivParent.addView(ivDelete, removeIvParams)

        ivThumbnail.setImageBitmap(thumbnail)
        ivParent.setOnClickListener { view ->
            layoutAttachments.removeView(view)
            if (attachments.remove(view.tag)) {
                DLog.v(this, "attachment removed !!")
            }
        }
        val params = LinearLayout.LayoutParams(side, side)
        params.leftMargin = context!!.resources.getDimensionPixelSize(R.dimen.dp_3)
        layoutAttachments.addView(ivParent, params)
    }


    override fun onDestroyView() {
        presenter?.onDettach()
        this.targetDialog?.dismiss()
        super.onDestroyView()
    }

    override fun showProgressBar() {
        showLoading()
    }


    override fun setClasses(list: List<CheckedItem>) {
        this.targets = list
        showContentLayout()
    }

    override fun showError() {
        showContentLayout()
        Toast.makeText(activity, R.string.uploading_news_failed, Toast.LENGTH_LONG).show()
    }

    override fun closeWindow() {
        FragmentUtils.pop(activity)
    }

    override fun onOptionSelected(selected: List<CheckedItem>) {
        this.targets = selected
        tvAddTarget.text = format()
    }

    private fun format(): String {
        return if (targets != null) {
            val filtered = targets!!.filter { it.isChecked }
            val commaSeparatedString = filtered.joinToString { it.name }
            commaSeparatedString
        } else {
            ""
        }
    }

}