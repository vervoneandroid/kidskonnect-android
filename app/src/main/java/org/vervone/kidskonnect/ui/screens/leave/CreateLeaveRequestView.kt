package org.vervone.kidskonnect.ui.screens.leave

import org.vervone.kidskonnect.ui.screens.base.BaseView

interface CreateLeaveRequestView : BaseView {
    fun showProgress()
    fun createLeaveRequestSuccess()
    fun showError()
}