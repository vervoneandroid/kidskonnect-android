package org.vervone.kidskonnect.ui.screens.news

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.DomainError
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.ui.screens.base.BasePresenter

class TargetsPresenter : BasePresenter<TargetsView>() {

    private val schoolFacade = FacadeFactory.schoolFacade

    fun fetchSchools() {
        view?.showProgressBar()
        schoolFacade.getClasses(object : RequestCallback<List<String>, DomainError> {
            override fun onError(error: DomainError?) {
                view?.showError(error?.cause?.localizedMessage!!)
            }

            override fun onSuccess(res: List<String>?) {
                view?.hideProgressBar()
                view?.setData(res)
            }
        })
    }

}