package org.vervone.kidskonnect.ui.screens.base

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.View
import org.vervone.kidskonnect.ui.utils.configure

abstract class BaseListFragment : BaseFragment() {
    protected abstract var recyclerView: RecyclerView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.configure()

    }


}