package org.vervone.kidskonnect.ui.notification

interface NotificationDisPatcher {
    fun dispatch(topic: String?, data: Map<String, String>)
}