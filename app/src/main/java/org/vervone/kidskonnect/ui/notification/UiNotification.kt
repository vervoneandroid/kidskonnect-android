package org.vervone.kidskonnect.ui.notification

import org.vervone.kidskonnect.core.domain.model.student.CommentType
import org.vervone.kidskonnect.core.push.NotificationType
import java.io.Serializable

class UiNotification : Serializable {
    var type: NotificationType? = null
    var groupId: String? = null
    var studentId: String? = null
    var className: String? = null
    var commentType: CommentType? = null
    var studentName: String? = null
}