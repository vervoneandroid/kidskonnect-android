package org.vervone.kidskonnect.ui.utils

import android.content.Intent
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.domain.DomainConsts
import org.vervone.kidskonnect.core.domain.model.Designation
import org.vervone.kidskonnect.core.domain.model.Teacher
import org.vervone.kidskonnect.core.helper.AppHelper
import org.vervone.kidskonnect.core.helper.PreferenceHelper


object AppUtils {
    fun getCurrentAcademicYear(): String {
        return PreferenceHelper.getSafeString(DomainConsts.KEY_CUR_ACADEMIC_YR)
    }

    /** Return subjects of teachers separated by comma */
    fun getFormattedSubjects(teacher: Teacher): String {
        teacher.classesAndSubjects?.let {
            return it.values.flatten().distinct().joinToString(" , ")
        }
        return ""
    }

    fun restartApp() {
        val ctx = AppHelper.getContext()
        val intent = ctx.packageManager.getLaunchIntentForPackage(ctx.packageName)
        intent?.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent?.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        ctx.startActivity(intent)
    }

    fun getDesignation(teacher: Teacher?): String {
        if (teacher != null) {
            val ctx = AppHelper.getContext()
            return when (teacher.designation) {
                Designation.PRINCIPAL -> ctx.getString(R.string.principal)
                Designation.FACULTY -> ctx.getString(R.string.faculty)
                Designation.ADMIN -> ctx.getString(R.string.admin)
                else -> ""
            }
        }
        return ""
    }
}