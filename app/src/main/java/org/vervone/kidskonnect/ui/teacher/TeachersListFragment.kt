package org.vervone.kidskonnect.ui.teacher

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.domain.TeachersForTheClass
import org.vervone.kidskonnect.core.domain.model.Designation
import org.vervone.kidskonnect.core.domain.model.MessageChannel
import org.vervone.kidskonnect.core.domain.model.Teacher
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.screens.messages.CreateGroupFragment
import org.vervone.kidskonnect.ui.screens.messages.MessageChatRoomFragment
import org.vervone.kidskonnect.ui.utils.*

class TeachersListFragment : BaseFragment(), TeachersView {


    private lateinit var rvTeachers: RecyclerView
    private var presenter: TeachersPresenter? = null


    override fun getLayoutId(): Int {
        return R.layout.fragment_teachers
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (UserHelper.isAdmin()) {
            activity?.setTitle(R.string.teachers)
        } else if (!UserHelper.isTeacher()) {
            activity?.setTitle(R.string.my_teachers)
        }

        KeyboardUtil.hideSoftKeyboard(activity!!)

        rvTeachers = view.findViewById(R.id.rv_teachers_list)
        rvTeachers.configure()
        val year = AppUtils.getCurrentAcademicYear()
        presenter = TeachersPresenter(year)
        presenter?.onAttach(this)

        showLoading()

        if (!UserHelper.isTeacher()) {
            val className = UserHelper.student?.getClass(year) ?: ""
            presenter?.getTeacher(className)
        } else {
            presenter?.getTeachers()
        }
    }

    override fun setData(teachers: List<Teacher>) {
        rvTeachers.adapter = TeacherListAdapter(teachers)
        showContentLayout()
    }


    override fun onDestroyView() {
        super.onDestroyView()
        presenter?.onDettach()
    }

    private class VH(view: View) : RecyclerView.ViewHolder(view) {
        val ivProfile: ImageView = view.findViewById(R.id.iv_profile_pic)
        val tvTeacherName: TextView = view.findViewById(R.id.tv_teacher_name)
        val tvQualification: TextView = view.findViewById(R.id.tv_qualification)
        val tvSubject: TextView = view.findViewById(R.id.tv_subjects)
        val ivChat: ImageView = view.findViewById(R.id.iv_chat)
    }

    private inner class TeacherListAdapter(val teachers: List<Teacher>) :
        RecyclerView.Adapter<VH>() {

        override fun onCreateViewHolder(vg: ViewGroup, pos: Int): VH {
            return VH(vg.inflate(R.layout.kc_layout_teacher_row))
        }

        override fun getItemCount(): Int {
            return teachers.size
        }

        override fun onBindViewHolder(vh: VH, pos: Int) {
            val teacher = teachers[pos]
            vh.tvTeacherName.text = teacher.teacherName
            vh.tvQualification.text = teacher.qualification
            vh.tvSubject.text = AppUtils.getFormattedSubjects(teacher)

            if (!UserHelper.isTeacher()) {
                val classTeacher = TeachersForTheClass.getClassTeacher()
                if (teacher.teacherId == classTeacher?.teacherId) {
                    vh.tvTeacherName.text = "${teacher.teacherName} (Class incharge)"
                }

                if (teacher.designation == Designation.PRINCIPAL) {
                    vh.tvTeacherName.text = "${teacher.teacherName} (Principal)"
                }
            }

            if (teacher.teacherId != null) {
                ImageLoader.load(
                        Urls.getProfileUrl(teacher.teacherId!!),
                        R.drawable.ic_profle,
                        R.drawable.ic_profle,
                        vh.ivProfile
                )
            }

            vh.ivChat.setOnClickListener {
                handleChatClick(teachers[pos])
            }
        }

    }


    private fun handleChatClick(teacher: Teacher) {
        presenter?.navigateToChat(teacher)
    }

    override fun navigateToCreateChatGroup(teacher: Teacher) {
        val fragment = CreateGroupFragment.createInstance(teacher)
        FragmentUtils.replace(activity?.supportFragmentManager,
                              fragment, R.id.content_frame, true)

    }

    override fun navigateToChatRoom(messageChannel: MessageChannel) {

        val channelName = messageChannel.getChatName(UserHelper.getCurrentUser().uid!!)
        val fragment = MessageChatRoomFragment.createInstance(messageChannel.id!!,
                                                              messageChannel,
                                                              channelName = channelName,
                                                              chatMessage = null)
        FragmentUtils.replace(activity?.supportFragmentManager,
                              fragment, R.id.content_frame, true)
    }

}