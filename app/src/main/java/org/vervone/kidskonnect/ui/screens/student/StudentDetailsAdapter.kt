package org.vervone.kidskonnect.ui.screens.student

import android.content.res.Resources
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.domain.model.student.Comment
import org.vervone.kidskonnect.core.domain.model.student.CommentType
import org.vervone.kidskonnect.core.domain.model.student.Student
import org.vervone.kidskonnect.ui.utils.AppUtils

class StudentDetailsAdapter(fm: FragmentManager,
                            val resources: Resources,
                            val student: Student) : FragmentStatePagerAdapter(fm) {


    override fun getItem(position: Int): Fragment {
        return when (adjustIndex(position)) {
            0 -> StudentsCommentsFragment.createInstance(
                    student.studentId!!,
                    student.studentName!!,
                    student.getClass(AppUtils.getCurrentAcademicYear())!!,
                    CommentType.FAMILY_MATTERS,
                    filter(CommentType.FAMILY_MATTERS)
            )

            1 -> StudentPersonalDetailsFragment.createInstance(student)

            2 -> StudentsCommentsFragment.createInstance(
                    student.studentId!!, student.studentName!!,
                    student.getClass(AppUtils.getCurrentAcademicYear())!!,
                    CommentType.TALENTS_ACHIEVEMENT,
                    filter(CommentType.TALENTS_ACHIEVEMENT)
            )
            3 -> StudentsCommentsFragment.createInstance(
                    student.studentId!!, student.studentName!!,
                    student.getClass(AppUtils.getCurrentAcademicYear())!!,
                    CommentType.HEALTH_MATTERS,
                    filter(CommentType.HEALTH_MATTERS)
            )

            else -> Fragment()
        }

    }

    private fun filter(commentType: CommentType): ArrayList<Comment> {
        val comments = ArrayList<Comment>()

        val filtered = student.comments?.filter { comment ->
            comment.commentType == commentType
        }

        if (filtered != null) {
            comments.addAll(filtered)
        }

        return comments
    }


    private fun adjustIndex(position: Int): Int {
        return position % NUMBER_OF_PAGES
    }

    override fun getCount(): Int {
        return NUMBER_OF_PAGES * NUMBER_OF_LOOPS
    }

    fun getCenterPosition(position: Int): Int {
        return NUMBER_OF_PAGES * NUMBER_OF_LOOPS / 2 + position
    }

    fun getRealPageCount(): Int {
        return NUMBER_OF_PAGES
    }

    override fun getPageTitle(position: Int): String? {
        return when (adjustIndex(position)) {
            0 -> resources.getString(R.string.family_matters).toUpperCase()
            1 -> resources.getString(R.string.personal_details).toUpperCase()
            2 -> resources.getString(R.string.talents_and_achievements).toUpperCase()
            3 -> resources.getString(R.string.health_matters).toUpperCase()
            else -> ""
        }
    }

    companion object {
        //Dirty hack for infinite loop for the view pager.
        private const val NUMBER_OF_LOOPS = 1000
        private const val NUMBER_OF_PAGES = 4
    }

}