package org.vervone.kidskonnect.ui.screens.messages.group

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.*
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.domain.model.student.Student
import org.vervone.kidskonnect.core.helper.cast
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.utils.configure
import org.vervone.kidskonnect.ui.utils.inflate

class StudentSearchFragment : BaseFragment() {

    private lateinit var rvStudents: RecyclerView
    private lateinit var tvSelectAll: TextView
    private lateinit var etSearchQuery: EditText

    private var selectedStudentId = ArrayList<String>()
    private var studentList: ArrayList<Student>? = null
    private var className: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val selected = arguments?.getStringArrayList(SELECTED_IDS)
        if (selected != null) {
            selectedStudentId.addAll(selected)
        }

        this.studentList = arguments?.getSerializable(STUDENTS)?.cast()
        this.className = arguments?.getString(STUDENT_NAME)
    }


    override fun getLayoutId(): Int {
        return R.layout.fragment_messages_student_search
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.rvStudents = view.findViewById(R.id.rv_students)
        rvStudents.configure()

        tvSelectAll = view.findViewById(R.id.tv_select_all)
        view.findViewById<View>(R.id.iv_back).setOnClickListener {
            fragmentManager?.popBackStack()
        }


        tvSelectAll.setOnClickListener {
            (rvStudents.adapter as StudentAdapter).toggle()
        }
        val adapter = StudentAdapter()
        rvStudents.adapter = adapter
        adapter.updateUi()


        etSearchQuery = view.findViewById(R.id.et_search_query)
        etSearchQuery.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {}
            override fun beforeTextChanged(charSequence: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(
                    charSequence: CharSequence?,
                    p1: Int, p2: Int, p3: Int
            ) {
                if (rvStudents.adapter != null) {
                    (rvStudents.adapter as StudentAdapter).filter.filter(charSequence)
                }
            }
        })

        view.findViewById<Button>(R.id.btn_done)
            .setOnClickListener {
                val adapter = (rvStudents.adapter as StudentAdapter)

                val selected = studentList?.filter { selectedStudentId.contains(it.studentId) }
                getListener().onStudentsSelected(className!!, selected!!)

                fragmentManager?.popBackStack()
            }
    }


    private fun getListener(): OnStudentsSelectListener {
        return targetFragment as OnStudentsSelectListener
    }


    private class VH(view: View) : RecyclerView.ViewHolder(view) {
        val tvStudentName: TextView = view.findViewById(R.id.tv_option)
        val cbOption: CheckBox = view.findViewById(R.id.cb_option)
    }

    private inner class StudentAdapter : RecyclerView.Adapter<VH>(), Filterable {

        private var filteredData = ArrayList<Student>(studentList)

        override fun onCreateViewHolder(vg: ViewGroup, pos: Int): VH {
            val view = vg.inflate(R.layout.kc_checkbok_layout)
            return VH(view)
        }

        override fun getItemCount(): Int {
            return filteredData.size
        }

        fun toggle() {

            if (selectedStudentId.size == filteredData.size) {
                tvSelectAll.setText(R.string.select_all)
                selectedStudentId.clear()

            } else {
                tvSelectAll.setText(R.string.deselect_all)
                selectedStudentId.clear()
                selectedStudentId.addAll(filteredData.map { it.studentId!! })
            }

            notifyDataSetChanged()
        }

        fun updateUi() {
            if (selectedStudentId.size == filteredData.size) {
                tvSelectAll.setText(R.string.deselect_all)
            } else {
                tvSelectAll.setText(R.string.select_all)
            }
        }

        override fun onBindViewHolder(vh: VH, pos: Int) {
            val student = filteredData[pos]
            vh.tvStudentName.text = student.studentName
            vh.cbOption.isChecked = selectedStudentId.contains(student.studentId)
            vh.cbOption.setOnClickListener {
                if (vh.cbOption.isChecked) {
                    selectedStudentId.add(student.studentId!!)
                } else {
                    selectedStudentId.remove(student.studentId)
                }
                updateUi()
                notifyDataSetChanged()
            }

            vh.itemView.setOnClickListener {

            }
        }

        override fun getFilter(): Filter {
            return studentFilter
        }


        private fun studentMatches(student: Student, search: String): Boolean {
            return student.studentName != null && student.studentName!!.contains(search, ignoreCase = true)
        }

        private val studentFilter = TeacherFilter()

        inner class TeacherFilter : Filter() {
            override fun performFiltering(charSequence: CharSequence?): FilterResults {
                val charString = charSequence.toString()
                filteredData = if (charString.isEmpty()) {
                    ArrayList(studentList)
                } else {
                    val filteredList = studentList?.filter { studentMatches(it, charString) }
                    ArrayList(filteredList)
                }

                val filterResults = FilterResults()
                filterResults.values = filteredData
                return filterResults
            }

            override fun publishResults(charSeq: CharSequence?, resultSet: FilterResults?) {
                filteredData = resultSet?.values as ArrayList<Student>
                updateUi()
                notifyDataSetChanged()
            }
        }
    }

    companion object {

        private const val SELECTED_IDS = "SELECTED_IDS"
        private const val STUDENTS = "STUDENTS"
        private const val STUDENT_NAME = "STUDENT_NAME"

        fun createInstance(
                selectedIds: ArrayList<String>,
                studentList: ArrayList<Student>,
                className: String
        ): StudentSearchFragment {
            val args = Bundle()
            args.putStringArrayList(SELECTED_IDS, selectedIds)
            args.putSerializable(STUDENTS, studentList)

            args.putString(STUDENT_NAME, className)

            val studentFragment = StudentSearchFragment()
            studentFragment.arguments = args
            return studentFragment
        }

    }
}