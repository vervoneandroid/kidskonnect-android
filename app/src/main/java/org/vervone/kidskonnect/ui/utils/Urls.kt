package org.vervone.kidskonnect.ui.utils

object Urls {

    private const val BASE_URL = "https://s3-ap-south-1.amazonaws.com/com.vervone.kidskonnect/"
    private const val PROFILE_PICTURES = BASE_URL + "profilePictures/"
    private const val PROFILE_PIC_KEY_PREFIX = "profilePictures/"

    private const val NEWS_MEDIA = BASE_URL

    fun getProfileUrl(senderId: String): String {
        return "$PROFILE_PICTURES$senderId.jpeg"
    }

    fun getProfileKey(senderId: String): String {
        return "$PROFILE_PIC_KEY_PREFIX$senderId.jpeg"
    }

    fun getNewsMediaUrl(attachment: String): String {
        return "$NEWS_MEDIA$attachment"
    }

    fun isImageUrl(path: String): Boolean {
        return (path.endsWith(".png") or path.endsWith(".jpg")) or path.endsWith(".jpeg")
    }
}