package org.vervone.kidskonnect.ui.screens.leave

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import com.prolificinteractive.materialcalendarview.MaterialCalendarView.SELECTION_MODE_NONE
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.helper.Formatter
import org.vervone.kidskonnect.ui.screens.base.BaseDialogFragment

/**
 *  Showing the calender for the specified leave
 */
class LeaveCalenderFragment : BaseDialogFragment() {


    private lateinit var calenderView: MaterialCalendarView
    private var selectedDates: List<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.selectedDates = arguments?.getStringArrayList(SELECTED_DATE)
    }

    override fun getLayoutId(): Int {
        return R.layout.dialog_leave_calender
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT)

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.calenderView = view.findViewById(R.id.calendarView)
        view.findViewById<Button>(R.id.btnClose).setOnClickListener {
            dismiss()
        }

        calenderView.selectionMode = SELECTION_MODE_NONE

        var startDate = false
        if (selectedDates != null) {
            selectedDates!!.forEach {
                val date = Formatter.getDate(it)
                if (date != null) {
                    this.calenderView.setDateSelected(date, true)
                }

                if (!startDate) {
                    startDate = true
                    calenderView.setCurrentDate(date)
                }
            }
        }
    }


    companion object {
        private const val SELECTED_DATE = "SELECTED_DATE"
        const val TAG = "LeaveCalenderFragment"

        fun createInstance(selectedDates: ArrayList<String>?): LeaveCalenderFragment {
            val args = Bundle()
            args.putStringArrayList(SELECTED_DATE, selectedDates)

            val fragment = LeaveCalenderFragment()
            fragment.arguments = args
            return fragment
        }
    }

}