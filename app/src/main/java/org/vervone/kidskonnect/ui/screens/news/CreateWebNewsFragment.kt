package org.vervone.kidskonnect.ui.screens.news

import android.os.Bundle
import android.util.Patterns
import android.view.View
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.domain.DomainConsts
import org.vervone.kidskonnect.core.domain.model.ContentType
import org.vervone.kidskonnect.core.domain.model.News
import org.vervone.kidskonnect.core.helper.PreferenceHelper
import org.vervone.kidskonnect.ui.utils.UserHelper
import java.util.*

class CreateWebNewsFragment : BaseCreateNewsFragment() {


    override fun getLayoutId(): Int {
        return R.layout.kc_fragment_web_news
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.etUrl = view.findViewById(R.id.etUrl)

    }

    override fun isValid(): Boolean {
        return Patterns.WEB_URL.matcher(etUrl.text).matches()
                && etDescription.text.isNotEmpty()
    }

    override fun createNews(): News {
        val news = News()
        val uid = PreferenceHelper.getString(DomainConsts.KEY_CUR_USER_ID)
        news.date = Date().time
        news.senderId = uid
        news.description = etDescription.text.toString()
        news.webLink = etUrl.text.toString()
        news.contentType = ContentType.WEB_LINKS
        news.senderName = UserHelper.getCurrentUser().name
        val targets = targets?.filter { it.isChecked }!!.map { it.name }
        news.targetList = targets
        return news
    }


}