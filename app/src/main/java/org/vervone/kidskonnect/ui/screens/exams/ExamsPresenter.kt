package org.vervone.kidskonnect.ui.screens.exams

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.DomainError
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.model.Exam
import org.vervone.kidskonnect.ui.screens.base.BasePresenter
import org.vervone.kidskonnect.ui.utils.AppUtils
import org.vervone.kidskonnect.ui.utils.UserHelper

class ExamsPresenter : BasePresenter<ExamsView>() {

    private val examsFacade = FacadeFactory.examsFacade
    private val schoolFacade = FacadeFactory.schoolFacade

    fun getScheduledExams() {

        if (UserHelper.isAdmin()) {
            getAllClasses()
        } else if (UserHelper.isTeacher() && UserHelper.teacher != null) {
            val teacher = UserHelper.teacher!!
            val classes = teacher.classesAndSubjects?.keys
            if (classes != null && classes.isNotEmpty()) {
                getExams(classes.toList())
            } else {
                view?.showError()
            }

        } else if (UserHelper.student != null) {
            UserHelper.student?.getClass(AppUtils.getCurrentAcademicYear())?.let {
                getExams(listOf(it))
            }
        }
    }

    private fun getExams(classes: List<String>) {
        examsFacade.getScheduledExams(AppUtils.getCurrentAcademicYear(), classes, ExamsCallback())
    }


    private fun getAllClasses() {
        schoolFacade.getClasses(object : RequestCallback<List<String>, DomainError> {
            override fun onError(error: DomainError?) {}
            override fun onSuccess(res: List<String>?) {
                if (res != null && res.isNotEmpty()) {
                    getExams(res)
                }
            }
        })
    }


    inner class ExamsCallback : RequestCallback<List<Exam>, DomainError> {
        override fun onSuccess(res: List<Exam>?) {
            if (res != null && res.isNotEmpty()) {
                view?.setData(res)
            } else {
                view?.showError()
            }
        }

        override fun onError(error: DomainError?) {
            view?.showError()
        }

    }
}