package org.vervone.kidskonnect.ui.screens.notifications

import org.vervone.kidskonnect.core.domain.model.Notification
import org.vervone.kidskonnect.ui.screens.base.BaseView

interface NotificationsView : BaseView {
    fun setNotifications(notifications: List<Notification>)
}