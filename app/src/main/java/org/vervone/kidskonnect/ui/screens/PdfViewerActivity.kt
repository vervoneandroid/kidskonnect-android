package org.vervone.kidskonnect.ui.screens

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ProgressBar
import android.widget.TextView
import com.github.barteksc.pdfviewer.PDFView
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.helper.ThreadUtils
import org.vervone.kidskonnect.ui.utils.extHide
import org.vervone.kidskonnect.ui.utils.extShow
import java.io.File
import java.io.FileOutputStream
import java.net.URL


class PdfViewerActivity : AppCompatActivity() {


    private lateinit var pdfView: PDFView
    private lateinit var pbPdfLoading: ProgressBar
    private lateinit var tvError: TextView
    private var file: File? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pdf_viewer)
        this.pdfView = findViewById(R.id.pdfView)
        this.pbPdfLoading = findViewById(R.id.pb_pdf_loading)
        this.tvError = findViewById(R.id.tv_pdf_error)
        this.tvError.setText(R.string.pdf_download_failed)
        this.file = File.createTempFile("exams_schedule", ".pdf", cacheDir)
        val pdfUrl = intent.getStringExtra(PDF_URL)

        val title = intent.getStringExtra(TITLE)
        if (title != null) {
            setTitle(title)
        } else {
            setTitle(R.string.exam_schedule)
        }

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        if (pdfUrl != null) {
            ThreadUtils.runOnBg {
                try {
                    download(pdfUrl, file!!)
                    openPdf()
                } catch (ex: Exception) {
                    showFallback()
                }
            }
        } else {
            tvError.extShow()
        }
    }

    private fun showFallback() {
        ThreadUtils.runOnUi {
            tvError.extShow()
            pbPdfLoading.extHide()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onDestroy() {
        file?.delete()
        super.onDestroy()
    }

    private fun openPdf() {
        ThreadUtils.runOnUi(Runnable {
            pdfView.fromFile(file)
                .defaultPage(0)
                .enableSwipe(true)
                .swipeHorizontal(false)
                //.onPageChange(this)
                .enableAnnotationRendering(true)
                //.onLoad(this)
                .scrollHandle(DefaultScrollHandle(this))
                .load()

            pbPdfLoading.extHide()
        })
    }

    private fun download(link: String, file: File) {
        URL(link).openStream().use { input ->
            FileOutputStream(file).use { output ->
                input.copyTo(output)
            }
        }
    }

    companion object {
        private const val PDF_URL = "PDF_URL"
        private const val TITLE = "TITLE"
        fun startActivity(context: Context, pdfUrl: String?, title: String? = null) {
            val intent = Intent(context, PdfViewerActivity::class.java)
            intent.putExtra(PDF_URL, pdfUrl)
            if (title != null) {
                intent.putExtra(TITLE, title)
            }
            context.startActivity(intent)
        }
    }


}