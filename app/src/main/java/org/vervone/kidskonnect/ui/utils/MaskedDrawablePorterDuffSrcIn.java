package org.vervone.kidskonnect.ui.utils;

import android.graphics.*;
import android.graphics.drawable.Drawable;

public class MaskedDrawablePorterDuffSrcIn extends Drawable {

    private Bitmap mPictureBitmap;
    private Bitmap mMaskBitmap;
    private Bitmap mBufferBitmap;
    private Canvas mBufferCanvas;
    private final Paint mPaintSrcIn = new Paint();


    public MaskedDrawablePorterDuffSrcIn() {
        mPaintSrcIn.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
    }


    public void setPictureBitmap(Bitmap pictureBitmap) {
        mPictureBitmap = pictureBitmap;
        redrawBufferCanvas();
    }


    public void setMaskBitmap(Bitmap maskBitmap) {
        mMaskBitmap = maskBitmap;
        redrawBufferCanvas();
    }

    @Override
    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        final int width = bounds.width();
        final int height = bounds.height();

        if (width <= 0 || height <= 0) {
            return;
        }

        if (mBufferBitmap != null
                && mBufferBitmap.getWidth() == width
                && mBufferBitmap.getHeight() == height) {
            return;
        }

        mBufferBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888); //that's too bad
        mBufferCanvas = new Canvas(mBufferBitmap);
        redrawBufferCanvas();
    }

    private void redrawBufferCanvas() {
        if (mPictureBitmap == null || mMaskBitmap == null || mBufferCanvas == null) {
            return;
        }

        mBufferCanvas.drawBitmap(mMaskBitmap, 0, 0, null);
        //Rect frameToDraw = new Rect(0, 0, mPictureBitmap.getWidth(), mPictureBitmap.getHeight());
        RectF whereToDraw = new RectF(0, 0, mMaskBitmap.getWidth(), mMaskBitmap.getHeight());

        Bitmap adjusted = scaleCenterCrop(mPictureBitmap, mMaskBitmap.getHeight(), mMaskBitmap.getWidth());
        mBufferCanvas.drawBitmap(adjusted, null, whereToDraw, mPaintSrcIn);
    }

    private static Bitmap scaleCenterCrop(Bitmap source, int newHeight,
                                          int newWidth) {
        int sourceWidth = source.getWidth();
        int sourceHeight = source.getHeight();

        float xScale = (float) newWidth / sourceWidth;
        float yScale = (float) newHeight / sourceHeight;
        float scale = Math.max(xScale, yScale);

        // Now get the size of the source bitmap when scaled
        float scaledWidth = scale * sourceWidth;
        float scaledHeight = scale * sourceHeight;

        float left = (newWidth - scaledWidth) / 2;
        float top = (newHeight - scaledHeight) / 2;

        RectF targetRect = new RectF(left, top, left + scaledWidth, top
                + scaledHeight);

        Bitmap dest = Bitmap.createBitmap(newWidth, newHeight,
                source.getConfig());
        Canvas canvas = new Canvas(dest);
        canvas.drawBitmap(source, null, targetRect, null);

        return dest;
    }

    @Override
    public void draw(Canvas canvas) {
        //dump the buffer
        canvas.drawBitmap(mBufferBitmap, 0, 0, null);
    }

    @Override
    public void setAlpha(int alpha) {
        mPaintSrcIn.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        //Not implemented
    }

    @Override
    public int getOpacity() {
        return PixelFormat.UNKNOWN;
    }

    @Override
    public int getIntrinsicWidth() {
        return mMaskBitmap != null ? mMaskBitmap.getWidth() : super.getIntrinsicWidth();
    }

    @Override
    public int getIntrinsicHeight() {
        return mMaskBitmap != null ? mMaskBitmap.getHeight() : super.getIntrinsicHeight();
    }
}