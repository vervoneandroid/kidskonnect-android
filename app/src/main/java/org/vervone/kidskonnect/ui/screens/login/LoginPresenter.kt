package org.vervone.kidskonnect.ui.screens.login

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.DomainError
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.TeachersForTheClass
import org.vervone.kidskonnect.core.domain.model.Teacher
import org.vervone.kidskonnect.core.domain.model.User
import org.vervone.kidskonnect.core.domain.model.student.Student
import org.vervone.kidskonnect.core.helper.PreferenceHelper
import org.vervone.kidskonnect.ui.screens.base.BasePresenter
import org.vervone.kidskonnect.ui.utils.*

class LoginPresenter(private val userType: UserType) : BasePresenter<LoginView>() {


    private val authFacade = FacadeFactory.authFacade
    private val schoolFacade = FacadeFactory.schoolFacade


    fun performLogin(email: String, pwd: String) {
        view?.showProgressBar()
        authFacade.doLogin(
                email,
                pwd,
                AuthCallback()
        )
    }

    private fun fetchProfile(uid: String?) {
        if (userType == UserType.STUDENT) {
            authFacade.fetchStudentProfile(uid!!, SProfileCallback(uid))
        } else {
            authFacade.fetchTeacherProfile(uid!!, TProfileCallback(uid))
        }
    }


    /**  Authentication callback   */
    inner class AuthCallback : RequestCallback<User, Error> {
        override fun onSuccess(res: User?) {
            fetchProfile(res?.uid)
        }

        override fun onError(error: Error?) {
            view?.hideProgressBar()
            view?.showError(error?.message!!)
        }
    }

    /**  Teacher **/
    inner class TProfileCallback(val uid: String) : RequestCallback<Teacher, Error> {
        override fun onSuccess(res: Teacher?) {
            res?.let {
                UserHelper.setTeacher(res)
                UserHelper.saveUserData(isTeacher = true)
                authFacade.getAcademicYearData(uid, TAcademicCallback(uid, res))
            }
        }

        override fun onError(error: Error?) {
            view?.hideProgressBar()
            view?.showError(error?.message!!)
        }
    }

    /**  Student **/
    inner class SProfileCallback(val uid: String) : RequestCallback<Student, Error> {
        override fun onSuccess(res: Student?) {
            res?.let {
                UserHelper.setStudent(res)
                UserHelper.saveUserData(isTeacher = false)
                fetchTeacherList(res)
            }
        }

        override fun onError(error: Error?) {
            view?.hideProgressBar()
            view?.showError(error?.message!!)
        }
    }


    private fun fetchTeacherList(student: Student) {
        val academicYr = AppUtils.getCurrentAcademicYear()
        val className = student.getClass(academicYr) ?: ""
        val callback = TeachersCallback(student)
        schoolFacade.getTeachers(academicYr, className, callback)
    }

    private inner class TeachersCallback(val student: Student) :
        RequestCallback<TeachersForTheClass, DomainError> {
        override fun onError(error: DomainError?) {
            view?.hideProgressBar()
            view?.showError(error?.message!!)
        }

        override fun onSuccess(res: TeachersForTheClass?) {
            view?.hideProgressBar()
            cleanAllUserData(student.studentId!!)
            view?.loginSuccess(student)
        }
    }


    private fun cleanAllUserData(currentUid: String) {
        val uid = PreferenceHelper.getString(Const.LAST_LOGIN_UID)
        if (uid != null && uid != currentUid) {
            NewsCount().resetCount()
            MessageCount().resetCount()
            CommentsCount().resetCount()
            LeaveCount().resetCount()
            AllCommentsCount().resetAll()
        }
        PreferenceHelper.save(key = Const.LAST_LOGIN_UID, value = currentUid)
    }

    /**
     *   Teachers academic year Data callback
     */
    inner class TAcademicCallback(val uid: String, val teacher: Teacher) : RequestCallback<Teacher, Error> {
        override fun onSuccess(res: Teacher?) {

            teacher.userChatDict = res?.userChatDict
            teacher.teacherDesignationTag = res?.teacherDesignationTag
            teacher.likedNews = res?.likedNews
            teacher.leavesList = res?.leavesList
            teacher.isClassTeacher = res?.isClassTeacher
            teacher.classesAndSubjects = res?.classesAndSubjects

            res?.let {
                cleanAllUserData(uid)
                view?.loginSuccess(teacher)
                view?.hideProgressBar()
            }
        }

        override fun onError(error: Error?) {
            view?.hideProgressBar()
            val msg = error?.message
            if (msg != null) {
                view?.showError(msg)
            } else {
                view?.showUnknownError()
            }
        }
    }

}