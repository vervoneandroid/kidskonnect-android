package org.vervone.kidskonnect.ui.screens.home

import android.content.Context
import android.os.Bundle
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.widget.GridLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.ui.screens.BadgeUpdater
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.utils.*
import org.vervone.kidskonnect.ui.utils.UiUtils.formatCount
import org.vervone.kidskonnect.ui.views.CirclePageIndicator

class HomeFragment : BaseFragment(), BadgeUpdater {

    private var viewPager: ViewPager? = null
    private var indicator: CirclePageIndicator? = null
    private var gridLayout: GridLayout? = null


    companion object {
        private val DRAWABLES: IntArray = intArrayOf(
                R.drawable.img_slider_1
                , R.drawable.img_slider_2, R.drawable.img_slider_3
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }


    override fun getLayoutId(): Int {
        return R.layout.fragment_home
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.setTitle(R.string.home_title)
        gridLayout = view.findViewById(R.id.grid_layout)
        indicator = view.findViewById(R.id.indicator)

        viewPager = view.findViewById(R.id.pager)
        viewPager?.adapter = VPAdapter(activity!!)

        indicator?.setViewPager(viewPager)

        findAllMenus()
    }


    override fun onResume() {
        super.onResume()
        updateNewsCount()
        updateMessageCount()
        updateLeaveCount()
    }


    private fun getCountView(tag: String, resId: Int): TextView? {
        return gridLayout?.findViewWithTag<View>(tag)?.findViewById(resId)
    }

    override fun updateNewsCount() {
        val tvCount = getCountView("news", R.id.tv_news_count)
        if (tvCount != null) {
            val count = NewsCount().getUnreadCount()
            if (count > 0) {
                tvCount.text = formatCount(count)
                tvCount.extShow()
            } else {
                tvCount.extHide()
            }
        }
    }

    override fun updateMessageCount() {
        val tvCount = getCountView("messages", R.id.tv_msg_count)
        if (tvCount != null) {
            val count = MessageCount().getTotalUnread()
            if (count > 0) {
                tvCount.text = formatCount(count)
                tvCount.extShow()
            } else {
                tvCount.extHide()
            }
        }
    }

    override fun updateLeaveCount() {
        val tvCount = getCountView("leave", R.id.tv_leave_count)
        if (tvCount != null) {
            val count = LeaveCount().getUnreadCount()
            if (count > 0) {
                tvCount.text = formatCount(count)
                tvCount.extShow()
            } else {
                tvCount.extHide()
            }
        }
    }



    private fun findAllMenus() {
        for (menu in gridLayout?.children()!!) {
            menu.setOnClickListener { view -> handleClick(view?.tag as String) }
        }
    }

    private fun handleClick(tag: String) {

        when (tag) {
            "messages" -> navHelper?.navToMessages()
            "news" -> navHelper?.navToNewsFeeds()
            "contacts" -> navHelper?.navToContact()
            "exams" -> navHelper?.navToExams()
            "leave" -> navHelper?.navToLeave()
            "notifications" -> navHelper?.navToNotifications()
        }

    }


    inner class VPAdapter(private val mContext: Context) : PagerAdapter() {

        override fun instantiateItem(collection: ViewGroup, position: Int): Any {
            val inflater = LayoutInflater.from(mContext)
            val layout = inflater.inflate(R.layout.layout_image, collection, false) as ViewGroup
            val imageView = layout.findViewById<ImageView>(R.id.ivSlinding)
            imageView?.setImageResource(DRAWABLES[position])
            collection.addView(layout)
            return layout
        }

        override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
            collection.removeView(view as View)
        }

        override fun getCount(): Int {
            return DRAWABLES.size
        }

        override fun isViewFromObject(view: View, aObject: Any): Boolean {
            return view === aObject
        }

    }


}