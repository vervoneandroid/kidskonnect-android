package org.vervone.kidskonnect.ui.screens.messages

import org.vervone.kidskonnect.core.domain.model.MessageChannel
import org.vervone.kidskonnect.ui.screens.base.BaseView

interface CreateGroupView : BaseView {
    fun navigateToChatRoom(messageChannel: MessageChannel)
    fun showProgress()
}