package org.vervone.kidskonnect.ui.views

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import android.widget.ProgressBar
import android.widget.TextView
import org.vervone.kidskonnect.R

@SuppressLint("ViewConstructor")
class ContentLayout(context: Context, layoutId: Int, inf: LayoutInflater) : FrameLayout(context) {

    private var progressLayout: View? = null
    private var progressBar: ProgressBar? = null
    private var tvFallbackText: TextView? = null
    var userTouchEnabled = true

    init {
        inf.inflate(layoutId, this, true)
        this.progressLayout = inf.inflate(R.layout.kc_layout_content, this, true)
        initializeViews()
    }


    private fun initializeViews() {
        this.progressBar = progressLayout?.findViewById(R.id.pb_cl_loading)
        this.tvFallbackText = progressLayout?.findViewById(R.id.tv_cl_fallback)
    }


    fun showProgressBar() {
        userTouchEnabled = false
        this.progressBar?.visibility = View.VISIBLE
        this.tvFallbackText?.visibility = View.GONE
    }

    private fun hideProgressBar() {
        userTouchEnabled = true
        this.progressBar?.visibility = View.GONE
    }


    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        if (!userTouchEnabled) return true
        return super.onInterceptTouchEvent(event)
    }

    fun showError(message: String) {
        this.tvFallbackText?.text = message
        this.tvFallbackText?.visibility = View.VISIBLE
        hideProgressBar()
    }

    private fun hideError() {
        this.tvFallbackText?.visibility = View.GONE
    }

    fun showContent() {
        hideProgressBar()
        hideError()
    }


}