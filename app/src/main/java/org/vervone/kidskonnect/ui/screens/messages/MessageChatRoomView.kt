package org.vervone.kidskonnect.ui.screens.messages

import org.vervone.kidskonnect.core.domain.model.ChatMessage
import org.vervone.kidskonnect.ui.screens.base.BaseView

interface MessageChatRoomView : BaseView {
    fun setChatMessages(chatList: List<ChatMessage>)
    fun showError(message: String?)
    fun reload()
}