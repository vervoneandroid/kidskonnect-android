package org.vervone.kidskonnect.ui.utils

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.TeachersForTheClass
import org.vervone.kidskonnect.core.domain.model.student.CommentType
import org.vervone.kidskonnect.ui.screens.contacts.ContactsFragment
import org.vervone.kidskonnect.ui.screens.exams.ExamsFragment
import org.vervone.kidskonnect.ui.screens.home.HomeFragment
import org.vervone.kidskonnect.ui.screens.leave.LeaveRequestsFragment
import org.vervone.kidskonnect.ui.screens.leave.LeavesFragment
import org.vervone.kidskonnect.ui.screens.messages.MessagesFragment
import org.vervone.kidskonnect.ui.screens.news.NewsFragment
import org.vervone.kidskonnect.ui.screens.notifications.NotificationsFragment
import org.vervone.kidskonnect.ui.screens.student.StudentProfileFragment
import org.vervone.kidskonnect.ui.screens.student.StudentSearchFragment
import org.vervone.kidskonnect.ui.teacher.TeachersListFragment

class NavigationHelper {


    private var fm: FragmentManager? = null

    constructor(activity: FragmentActivity) {
        this.fm = activity.supportFragmentManager
    }

    fun navToNewsFeeds() {
        FragmentUtils.replace(fm, NewsFragment(), R.id.content_frame)
    }


    fun navToMessages(channeId: String? = null) {
        FragmentUtils.replace(fm, MessagesFragment.createInstance(channeId), R.id.content_frame)
    }

    fun navToExams() {
        FragmentUtils.replace(fm, ExamsFragment(), R.id.content_frame)
    }

    fun navToLeave() {
        if (UserHelper.isTeacher()) {
            FragmentUtils.replace(fm, LeaveRequestsFragment(), R.id.content_frame)
        } else {
            FragmentUtils.replace(fm, LeavesFragment(), R.id.content_frame)
        }
    }

    fun navToNotifications() {
        FragmentUtils.replace(fm, NotificationsFragment(), R.id.content_frame)
    }

    fun navToContact() {
        FragmentUtils.replace(fm, ContactsFragment.createInstance(), R.id.content_frame)
    }

    fun navToHome() {
        FragmentUtils.replace(fm, HomeFragment(), R.id.content_frame)
    }

    fun isHomePage(): Boolean {
        return fm?.findFragmentById(R.id.content_frame) is HomeFragment
    }

    fun navToStudents() {
        FragmentUtils.replace(
                fm, StudentSearchFragment.createInstance()
                , R.id.content_frame
        )
    }

    fun navToTeachers() {
        FragmentUtils.replace(fm, TeachersListFragment(), R.id.content_frame)
    }

    fun navToStudentProfile(studentId: String, commentType: CommentType) {
        val fragment = StudentProfileFragment.createInstance(studentId, commentType)
        FragmentUtils.replace(fm, fragment, R.id.content_frame)
    }

    fun navToScreen(fragment: Fragment, addToBack: Boolean = false) {
        FragmentUtils.replace(fm, fragment, R.id.content_frame, addToBack)
    }

    fun logout() {
        FacadeFactory.authFacade.logout()
        UserHelper.clear()
        TeachersForTheClass.clear()
        SubscriptionHelper.unsubscribeAll()
        //MessagesCountCache.clear()
    }
}