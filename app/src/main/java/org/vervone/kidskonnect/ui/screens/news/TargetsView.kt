package org.vervone.kidskonnect.ui.screens.news

import org.vervone.kidskonnect.ui.screens.base.BaseView

interface TargetsView : BaseView {
    fun showProgressBar()
    fun hideProgressBar()
    fun showError(message: String)
    fun setData(data: List<String>?)
}