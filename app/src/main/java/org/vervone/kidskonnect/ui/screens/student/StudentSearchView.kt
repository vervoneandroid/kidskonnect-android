package org.vervone.kidskonnect.ui.screens.student

import org.vervone.kidskonnect.core.domain.model.student.Student
import org.vervone.kidskonnect.ui.screens.base.BaseView

interface StudentSearchView : BaseView {
    fun setSearchResult(list: List<Student>)
    fun setClasses(list: List<String>)
}