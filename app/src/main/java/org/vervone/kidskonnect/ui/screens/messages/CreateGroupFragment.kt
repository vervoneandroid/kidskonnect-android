package org.vervone.kidskonnect.ui.screens.messages

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.iceteck.silicompressorr.SiliCompressor
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.domain.model.ChatMessage
import org.vervone.kidskonnect.core.domain.model.MessageChannel
import org.vervone.kidskonnect.core.domain.model.MessageType
import org.vervone.kidskonnect.core.domain.model.Teacher
import org.vervone.kidskonnect.core.domain.model.student.Student
import org.vervone.kidskonnect.core.helper.ThreadUtils
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.screens.messages.group.OnClassesSelected
import org.vervone.kidskonnect.ui.screens.messages.group.OnTeachersSelected
import org.vervone.kidskonnect.ui.screens.messages.group.StudentTeacherListFragment
import org.vervone.kidskonnect.ui.screens.news.MediaOptionsBottomSheet
import org.vervone.kidskonnect.ui.utils.*
import org.vervone.mediapicker.Image.ImagePicker
import org.vervone.mediapicker.Video.VideoPicker
import java.io.File


class CreateGroupFragment : BaseFragment(), CreateGroupView,
    OnTeachersSelected, OnClassesSelected {


    private lateinit var rvGroupMembers: RecyclerView
    private lateinit var ivAddMember: ImageView
    private lateinit var btnSend: Button
    private lateinit var etMessage: EditText
    private lateinit var ivAttachFile: ImageView

    private var selectedTeachers = ArrayList<String>()
    private var selectedStudent = HashMap<String, List<Student>>()
    private var selectedTeachersList = ArrayList<Teacher>()
    private var allSelectedClasses = ArrayList<String>()

    private var createGroupPresenter: CreateGroupPresenter? = null

    // Media message type , getting from onActivity result .
    private var pendingMessage: ChatMessage? = null
    // True if coming from student profile.
    private var preStudentSelected = false

    // True only if coming from teachers list
    private var preTeacherSelected = false

    private var addMember = true
    private var progressDialog: DialogFragment? = null

    override fun getLayoutId(): Int {
        return R.layout.fragment_create_group
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val className = arguments?.getString(CLASS_NAME, "")
        val student = arguments?.getSerializable(STUDENT) as? Student
        this.preStudentSelected = (className != null && student != null)
        val teacher = arguments?.getSerializable(TEACHER) as? Teacher
        this.preTeacherSelected = (teacher != null)
        if (preStudentSelected) {
            selectedStudent[className!!] = arrayListOf(student!!)
        }
        if (arguments != null) {
            addMember = arguments!!.getBoolean(ADD_MEMBER, true)
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.rvGroupMembers = view.findViewById(R.id.rv_group_members)
        this.ivAddMember = view.findViewById(R.id.iv_add_member)
        this.btnSend = view.findViewById(R.id.btn_send)
        this.etMessage = view.findViewById(R.id.et_chat_message)
        this.ivAttachFile = view.findViewById(R.id.iv_attach_file)

        if (!addMember) {
            ivAddMember.extHide()
        }


        val layoutMgr = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        rvGroupMembers.layoutManager = layoutMgr

        this.createGroupPresenter = CreateGroupPresenter()
        this.createGroupPresenter?.onAttach(this)


        ivAddMember.setOnClickListener {
            StudentTeacherListFragment
                .createInstance(selectedTeachers, HashMap(selectedStudent))
                .show(childFragmentManager, "")
        }

        etMessage.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {}
            override fun beforeTextChanged(charSequence: CharSequence?,
                                           p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence?,
                                       p1: Int, p2: Int, p3: Int) {
                btnSend.isEnabled = !(charSequence.isNullOrBlank() || charSequence.isNullOrEmpty())
            }
        })
        btnSend.isEnabled = false

        this.btnSend.setOnClickListener {
            if (validate()) {
                createGroupPresenter?.sendMessage(
                        selectedStudent,
                        selectedTeachersList,
                        allSelectedClasses
                )
            } else {
                DialogFactory.createSimpleOkErrorDialog(activity!!,
                                                        R.string.app_name,
                                                        R.string.select_recipient_to_send_message).show()
            }
        }

        this.ivAttachFile.setOnClickListener {

            if (rvGroupMembers.adapter != null &&
                rvGroupMembers.adapter!!.itemCount > 0
            ) {
                val fragment = MediaOptionsBottomSheet()
                fragment.show(childFragmentManager, null)
            }

        }

        if (preStudentSelected) {
            val dataSet = mapToChip()
            rvGroupMembers.adapter = ChipAdapter(dataSet)
        }

        if (preTeacherSelected) {
            val teacher = arguments?.getSerializable(TEACHER) as? Teacher
            onTeachersSelected(arrayListOf(teacher!!))
        }
    }

    private fun validate(): Boolean {
        return !UiUtils.isEmpty(etMessage)
                && rvGroupMembers.adapter != null
                && rvGroupMembers.adapter!!.itemCount > 0
    }

    override fun showProgress() {
        showLoading()
    }


    private fun showProgressBar() {
        progressDialog = DialogFactory.createProgressDialog(activity!!)
        progressDialog?.show(childFragmentManager, null)
    }

    private fun hideProgressBar() {
        progressDialog?.dismiss()
    }

    override fun onResume() {
        super.onResume()

        val chatMessage = pendingMessage
        if (chatMessage != null) {
            val messageType = chatMessage.messageType
            if (messageType == MessageType.VIDEOS) {
                showProgressBar()
                compressVideo()
            } else {
                sendMessage()
            }
        }
    }

    private fun sendMessage() {
        createGroupPresenter?.sendMessage(
                selectedStudent,
                selectedTeachersList,
                allSelectedClasses
        )
    }

    private fun compressVideo() {
        ThreadUtils.runOnBg {
            val context = context
            if (context != null) {
                val destinationDirectory = File(context.cacheDir.path)
                val filePath =
                    SiliCompressor.with(context).compressVideo(pendingMessage?.localFilePath, destinationDirectory.path)
                pendingMessage?.localFilePath = filePath
                DLog.v(this, "runOnBg $filePath")

                runOnUi {
                    sendMessage()
                    hideProgressBar()
                }
            }
        }
    }

    override fun handleOnActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        var handled = false
        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == AppCompatActivity.RESULT_OK) {
            val paths = data?.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH)
            if (!paths.isNullOrEmpty()) {
                pendingMessage = ChatMessage()
                pendingMessage?.messageType = MessageType.PICTURE
                pendingMessage?.localFilePath = paths.get(0)!!
                DLog.v(this, "image.paths $paths")
            }
            handled = true

        } else if (requestCode == VideoPicker.VIDEO_PICKER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val paths = data?.getStringArrayListExtra(VideoPicker.EXTRA_VIDEO_PATH)
            DLog.v(this, "path $paths")
            pendingMessage = ChatMessage()
            pendingMessage?.messageType = MessageType.VIDEOS
            pendingMessage?.localFilePath = paths?.get(0)!!

            handled = true
        }
        return handled
    }

    override fun navigateToChatRoom(messageChannel: MessageChannel) {
        val activityTemp = activity
        FragmentUtils.pop(activity)


        var msgType = MessageType.TEXT
        var localFilePath: String? = null
        if (pendingMessage != null) {
            msgType = pendingMessage?.messageType!!
            localFilePath = pendingMessage?.localFilePath
            this.pendingMessage = null
        }

        val chatMessage = ChatMessage()

        val message = when (msgType) {
            MessageType.TEXT -> UiUtils.getString(etMessage)
            MessageType.PICTURE -> "image"
            MessageType.VIDEOS -> "video"
            else -> ""
        }
        chatMessage.messageType = msgType
        chatMessage.message = message
        chatMessage.localFilePath = localFilePath

        val channelName = messageChannel.getChatName(UserHelper.getCurrentUser().uid!!)
        val fragment = MessageChatRoomFragment.createInstance(messageChannel.id!!,
                                                              chatRoom = messageChannel,
                                                              channelName = channelName,
                                                              chatMessage = chatMessage)
        FragmentUtils.replace(activityTemp!!.supportFragmentManager,
                              fragment, R.id.content_frame, true)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        this.createGroupPresenter?.onDettach()
    }

    override fun onClassesSelected(
            selectedStudent: Map<String, List<Student>>,
            allSelectedClasses: List<String>
    ) {
        this.selectedStudent.clear()
        this.selectedStudent.putAll(selectedStudent)
        this.allSelectedClasses.clear()
        this.allSelectedClasses.addAll(allSelectedClasses)

        val dataSet = mapToChip()
        rvGroupMembers.adapter = ChipAdapter(dataSet)

    }

    override fun onTeachersSelected(selected: ArrayList<Teacher>) {
        selectedTeachersList.clear()
        selectedTeachersList.addAll(selected)
        selectedTeachers.clear()
        selectedTeachers.addAll(selected.map { it.teacherId!! })

        val dataSet = mapToChip()
        rvGroupMembers.adapter = ChipAdapter(dataSet)

        if (!UserHelper.isTeacher() && dataSet.isNotEmpty()) {
            ivAddMember.extHide()
        }
    }


    private fun mapToChip(): List<String?> {

        val teachers = selectedTeachersList.map { teacher -> teacher.teacherName }
        val classes = allSelectedClasses
        val studentNames = ArrayList<String>()

        selectedStudent.entries.forEach { entry ->
            if (!classes.contains(entry.key)) {
                val names = entry.value.map { student -> formatStudentName(student) }
                studentNames.addAll(names)
            }
        }
        return teachers + classes + studentNames
    }


    // Used to append class name with student name.
    private fun formatStudentName(student: Student): String {
        return "${student.studentName}(${student.getClass(AppUtils.getCurrentAcademicYear())})"
    }


    private class VH(view: View) : RecyclerView.ViewHolder(view) {
        val tvMemberName: TextView = view.findViewById(R.id.tv_chip_member_name)
    }

    private class ChipAdapter(val chipsList: List<String?>) : RecyclerView.Adapter<VH>() {

        override fun onCreateViewHolder(vg: ViewGroup, pos: Int): VH {
            return VH(vg.inflate(R.layout.kc_layout_chip))
        }

        override fun getItemCount(): Int {
            return chipsList.size
        }

        override fun onBindViewHolder(vh: VH, pos: Int) {
            vh.tvMemberName.text = chipsList[pos]
        }

    }

    companion object {

        private const val CLASS_NAME = "class_name"
        private const val ADD_MEMBER = "add_member"
        private const val STUDENT = "student"
        private const val TEACHER = "teacher"
        fun createInstance(className: String,
                           student: Student): CreateGroupFragment {
            val bundle = Bundle()
            bundle.putSerializable(STUDENT, student)
            bundle.putString(CLASS_NAME, className)

            bundle.putBoolean(ADD_MEMBER, false)

            val fragment = CreateGroupFragment()
            fragment.arguments = bundle
            return fragment
        }

        fun createInstance(teacher: Teacher): CreateGroupFragment {
            val bundle = Bundle()
            bundle.putSerializable(TEACHER, teacher)
            bundle.putBoolean(ADD_MEMBER, false)
            val fragment = CreateGroupFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

}