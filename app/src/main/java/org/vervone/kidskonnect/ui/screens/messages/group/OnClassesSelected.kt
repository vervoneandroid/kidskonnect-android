package org.vervone.kidskonnect.ui.screens.messages.group

import org.vervone.kidskonnect.core.domain.model.student.Student

interface OnClassesSelected {
    fun onClassesSelected(
        selectedStudent: Map<String, List<Student>>,
        allSelectedClasses: List<String>
    )
}