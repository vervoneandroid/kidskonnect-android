package org.vervone.kidskonnect.ui.screens.leave

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.DataChangeListener
import org.vervone.kidskonnect.core.domain.DomainError
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.model.Leave
import org.vervone.kidskonnect.ui.screens.base.BasePresenter
import org.vervone.kidskonnect.ui.utils.AppUtils
import org.vervone.kidskonnect.ui.utils.UserHelper

class LeavesPresenter : BasePresenter<LeavesView>() {

    private val leavesFacade = FacadeFactory.leavesFacade

    private var subscriberId: String? = null

    fun subscribe() {
        val academicYr = AppUtils.getCurrentAcademicYear()
        val className = UserHelper.student?.getClass(academicYr)!!
        this.subscriberId = leavesFacade.subscribeAllLeaveRequestsByStudent(
                academicYr, className, UserHelper.getCurrentUser().uid, object : DataChangeListener {
            override fun onChange() {
                getLeaves()
            }
        })
    }

    fun unsubscribe() {
        subscriberId?.let {
            leavesFacade.unsubscribeAllLeaveRequestsByStudent(it)
        }
        this.subscriberId = null
    }

    fun getLeaves() {
        val academicYr = AppUtils.getCurrentAcademicYear()
        val className = UserHelper.student?.getClass(academicYr)!!
        leavesFacade.getAllLeaveRequestsByStudent(
                academicYr, className,
                UserHelper.getCurrentUser().uid,
                Callback())
    }

    private inner class Callback : RequestCallback<List<Leave>, DomainError> {
        override fun onSuccess(res: List<Leave>?) {
            val sorted: List<Leave> = res?.sortedBy { it.id } ?: emptyList()
            view?.setData(sorted)
        }

        override fun onError(error: DomainError?) {
            view?.showError(error?.message ?: "")
        }
    }

}