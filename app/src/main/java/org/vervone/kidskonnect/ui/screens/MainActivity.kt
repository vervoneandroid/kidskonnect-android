package org.vervone.kidskonnect.ui.screens

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.sun.easysnackbar.EasySnackBar
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.cloudstorage.CloudStorage
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.model.Designation
import org.vervone.kidskonnect.core.domain.model.Teacher
import org.vervone.kidskonnect.core.domain.model.student.Comment
import org.vervone.kidskonnect.core.domain.model.student.CommentType
import org.vervone.kidskonnect.core.domain.model.student.Student
import org.vervone.kidskonnect.core.push.NotificationType
import org.vervone.kidskonnect.ui.MainActivityView
import org.vervone.kidskonnect.ui.notification.UiNotification
import org.vervone.kidskonnect.ui.screens.base.BaseActivity
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.screens.home.HomeFragment
import org.vervone.kidskonnect.ui.screens.leave.LeaveRequestsFragment
import org.vervone.kidskonnect.ui.screens.login.LoginActivity
import org.vervone.kidskonnect.ui.screens.menu.MenuHelper
import org.vervone.kidskonnect.ui.screens.menu.MenuHelper.Companion.ID_LEAVES
import org.vervone.kidskonnect.ui.screens.menu.MenuHelper.Companion.ID_MESSAGES
import org.vervone.kidskonnect.ui.screens.menu.MenuHelper.Companion.ID_NEWS
import org.vervone.kidskonnect.ui.screens.menu.MenuHelper.Companion.ID_STUDENTS
import org.vervone.kidskonnect.ui.screens.menu.NavMenuItem
import org.vervone.kidskonnect.ui.screens.messages.MessageChatRoomFragment
import org.vervone.kidskonnect.ui.screens.news.NewsFragment
import org.vervone.kidskonnect.ui.screens.student.StudentProfileFragment
import org.vervone.kidskonnect.ui.teacher.TeacherProfileFragment
import org.vervone.kidskonnect.ui.utils.*
import org.vervone.kidskonnect.ui.utils.Const.ACTIONS_PROFILE_PIC


/**
 *  Main Screen for the app .
 */
class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener,
    CloudStorageDelegate, MainActivityView {


    private var teacher: Teacher? = null
    private var student: Student? = null
    private var navView: NavigationView? = null
    private var drawerLayout: DrawerLayout? = null
    private var mDrawerToggle: ActionBarDrawerToggle? = null
    private var cloudStorage: CloudStorage? = null
    private var navigationHelper: NavigationHelper? = null
    private var mainActivityPresenter: MainActivityPresenter? = null
    private var isPaused = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.navigationHelper = NavigationHelper(this)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)


        this.drawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)
        mDrawerToggle = object : ActionBarDrawerToggle(
                this,
                drawerLayout,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close) {
            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                mainActivityPresenter?.syncNotificationCount()
            }
        }
        this.drawerLayout?.addDrawerListener(mDrawerToggle!!)
        mDrawerToggle?.syncState()
        navView?.itemIconTintList = null
        navView?.setNavigationItemSelectedListener(this)

        var notification: UiNotification? = null
        if (intent?.extras != null) {
            val bundle = intent?.extras!!
            if (bundle.containsKey(TEACHER)) {
                this.teacher = bundle.getSerializable(TEACHER) as Teacher?
                when (teacher?.designation) {
                    Designation.FACULTY -> setupMenuItems(MenuHelper.TEACHERS_MENU)
                    Designation.ADMIN, Designation.PRINCIPAL -> setupMenuItems(MenuHelper.ADMIN_MENU)
                    Designation.UNKNOWN -> return
                }
            } else if (bundle.containsKey(STUDENT)) {
                this.student = bundle.getSerializable(STUDENT) as Student?
                setupMenuItems(MenuHelper.STUDENT_MENU)
            }

            if (bundle.containsKey(NOTIFICATION_DATA)) {
                notification = bundle.getSerializable(NOTIFICATION_DATA) as? UiNotification?
            }
        }
        this.cloudStorage = FacadeFactory.createCloudStorage(this)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        if (supportFragmentManager.findFragmentById(R.id.content_frame) == null && notification == null) {
            val homeFragment = HomeFragment()
            FragmentUtils.addFragment(supportFragmentManager, homeFragment, R.id.content_frame)
        }

        if (notification != null) {
            when (notification.type) {
                NotificationType.NEW_MESSAGE_EXISTING_GROUP -> navigationHelper?.navToMessages(
                        notification.groupId)
                NotificationType.NEWS_FEEDS -> navigationHelper?.navToNewsFeeds()
                NotificationType.LEAVE_APPROVAL -> navigationHelper?.navToLeave()
                NotificationType.NEW_COMMENT -> {
                    val type = notification.commentType ?: CommentType.UNKNOWN
                    navigationHelper?.navToStudentProfile(notification.studentId ?: "", type)
                }
                else -> {
                    DLog.v(this, "Unhandled.")
                    val homeFragment = HomeFragment()
                    FragmentUtils.addFragment(supportFragmentManager,
                                              homeFragment,
                                              R.id.content_frame)
                }
            }
        }

        supportFragmentManager.addOnBackStackChangedListener {
            if (supportFragmentManager.backStackEntryCount == 0) {
                showUpArrow(false)
            } else {
                showUpArrow(true)
            }
        }
        mainActivityPresenter = MainActivityPresenter(FacadeFactory.messageFacade)
        mainActivityPresenter?.onAttach(this)
        mainActivityPresenter?.initialize()

    }

    override fun onResume() {
        super.onResume()
        this.isPaused = false
        updateProfilePic()
        this.registerReceiver(profilePicUpdater, IntentFilter(ACTIONS_PROFILE_PIC))
    }


    private val profilePicUpdater = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            updateProfilePic()
        }
    }

    private fun updateProfilePic() {
        val headerLayout = navView?.getHeaderView(0)
        val ivProfilePic = headerLayout?.findViewById<ImageView>(R.id.ivProfilePic)
        ivProfilePic?.setImageDrawable(null)
        if (UserHelper.getCurrentUser().uid != null) {
            val url = Urls.getProfileUrl(UserHelper.getCurrentUser().uid!!)
            ImageLoader.load(url, R.drawable.ic_profle, R.drawable.ic_profle, ivProfilePic!!)
        }
    }

    override fun onPause() {
        this.isPaused = true
        unregisterReceiver(profilePicUpdater)
        super.onPause()
    }


    override fun showProfileUpdatedNotification(comment: Comment, student: Student) {
        val commented = comment.commentedPersonName
        val studentName = student.studentName
        val msg = if (student.studentId == UserHelper.getCurrentUser().uid) {
            getString(R.string.added_new_comment_to_your, commented)
        } else {
            getString(R.string.added_new_comment, commented, studentName)
        }
        showSnackBar(msg) {
            val type = comment.commentType ?: CommentType.UNKNOWN
            navigationHelper?.navToStudentProfile(student.studentId ?: "", type)
        }
    }

    override fun showNewsNotification(count: Int) {
        val fragment = supportFragmentManager.findFragmentById(R.id.content_frame)
        val canShow = fragment !is NewsFragment
        if (canShow && count > 0) {
            val message = getString(R.string.new_news_received, count.toString())
            showSnackBar(message) {
                navigationHelper?.navToNewsFeeds()
            }
        }
    }

    @SuppressLint("WrongConstant")
    override fun showMessageNotification(count: Int, channelName: String, channelId: String) {

        if (isPaused) {
            return
        }

        val fragment = supportFragmentManager.findFragmentById(R.id.content_frame)
        var canShow = true
        if (fragment is MessageChatRoomFragment) {
            canShow = fragment.canShowNotification(channelId)
        }

        if (canShow) {
            val message = getString(R.string.new_messages_from, count.toString(), channelName)
            showSnackBar(message) {
                navigationHelper?.navToMessages(channelId)
            }
        }
    }

    private fun getBadgeUpdate(): BadgeUpdater? {
        val currentFragment = supportFragmentManager.findFragmentById(R.id.content_frame)
        if (currentFragment is BadgeUpdater) {
            return currentFragment
        }
        return null
    }

    override fun updateRequestsLeaveCount(count: Int) {
        val fragment = supportFragmentManager.findFragmentById(R.id.content_frame)
        if (fragment !is LeaveRequestsFragment) {
            val view = navView?.menu?.findItem(ID_LEAVES)?.actionView as LinearLayout
            if (count != 0) {
                view.findViewById<TextView>(R.id.tv_counter).text = UiUtils.formatCount(count)
                view.findViewById<TextView>(R.id.tv_counter).extShow()
            } else {
                view.findViewById<TextView>(R.id.tv_counter).extHide()
            }
        } else {
            val view = navView?.menu?.findItem(ID_LEAVES)?.actionView as LinearLayout
            view.findViewById<TextView>(R.id.tv_counter).extHide()
        }

        getBadgeUpdate()?.updateLeaveCount()
    }

    override fun showLeaveRequestNotification(studentName: String) {
        val message = getString(R.string.new_leave_request_from, 1.toString(), studentName)
        showSnackBar(message) {
            navigationHelper?.navToLeave()
        }
    }

    override fun updateStudentCommentsCount(count: Int) {
        val view = navView?.menu?.findItem(ID_STUDENTS)?.actionView as LinearLayout
        if (count != 0) {
            view.findViewById<TextView>(R.id.tv_counter).text = UiUtils.formatCount(count)
            view.findViewById<TextView>(R.id.tv_counter).extShow()
        } else {
            view.findViewById<TextView>(R.id.tv_counter).extHide()
        }

    }

    override fun updateCommentsCount(count: Int) {
        val headerLayout = navView?.getHeaderView(0)
        if (headerLayout != null) {
            val tvCommentCount = headerLayout.findViewById<TextView>(R.id.tv_comments_count)
            if (count != 0) {
                tvCommentCount.text = UiUtils.formatCount(count)
                tvCommentCount.extShow()
            } else {
                tvCommentCount.extHide()
            }
        }
    }

    override fun updateNewsCount(count: Int) {
        val view = navView?.menu?.findItem(ID_NEWS)?.actionView as LinearLayout
        if (count != 0) {
            view.findViewById<TextView>(R.id.tv_counter).text = UiUtils.formatCount(count)
            view.findViewById<TextView>(R.id.tv_counter).extShow()
        } else {
            view.findViewById<TextView>(R.id.tv_counter).extHide()
        }

        getBadgeUpdate()?.updateNewsCount()
    }

    override fun showLeaveApprovedNotification() {
        val message = getString(R.string.your_leave_request_has_been_approved)
        showSnackBar(message) {
            navigationHelper?.navToLeave()
        }
    }

    @SuppressLint("WrongConstant")
    private fun showSnackBar(message: String, clickHandler: () -> Unit) {
        if (isPaused) {
            return
        }
        val anchor = findViewById<View>(R.id.snackbar_anchor)
        val contentView = EasySnackBar.convertToContentView(anchor, R.layout.kc_layout_snackbar)
        contentView.findViewById<TextView>(R.id.tv_snack_text).text = message
        contentView.setOnClickListener {
            clickHandler.invoke()
        }
        EasySnackBar.make(anchor, contentView, EasySnackBar.LENGTH_SHORT, true).show()
    }


    override fun showLeaveRejectedNotification() {
        val message = getString(R.string.your_leave_request_has_been_rejected)
        showSnackBar(message) {
            navigationHelper?.navToLeave()
        }
    }

    override fun updateMessageCount(count: Int) {
        val view = navView?.menu?.findItem(ID_MESSAGES)?.actionView as LinearLayout
        if (count != 0) {
            view.findViewById<TextView>(R.id.tv_counter).text = UiUtils.formatCount(count)
            view.findViewById<TextView>(R.id.tv_counter).extShow()
        } else {
            view.findViewById<TextView>(R.id.tv_counter).extHide()
        }

        getBadgeUpdate()?.updateMessageCount()
    }

    private fun hideActionView(view: View) {
        view.findViewById<TextView>(R.id.tv_counter).extHide()
    }

    override fun onDestroy() {
        mainActivityPresenter?.finalize()
        mainActivityPresenter?.onDettach()
        super.onDestroy()
    }

    override fun navigateToStudentProfile(commentType: CommentType) {
        val studentId = UserHelper.student?.studentId
        if (studentId != null) {
            val fragment = StudentProfileFragment.createInstance(studentId, commentType)
            FragmentUtils.replace(supportFragmentManager,
                                  fragment, R.id.content_frame, true)
        }
    }

    private fun setupMenuItems(list: List<NavMenuItem>) {
        list.forEach {
            val menuItem = navView?.menu?.add(0, it.id, 0, it.name)
            menuItem?.setIcon(it.icon)
            menuItem?.setActionView(R.layout.kc_layout_count)
            hideActionView(menuItem?.actionView!!)
        }

        val headerLayout = navView?.getHeaderView(0)
        val ivProfilePic = headerLayout?.findViewById<ImageView>(R.id.ivProfilePic)
        val tvUsername = headerLayout?.findViewById<TextView>(R.id.tvUsername)
        val tvSub2 = headerLayout?.findViewById<TextView>(R.id.tvClassOrDesignation)

        if (!UserHelper.isTeacher() && UserHelper.student != null) {
            ivProfilePic?.setOnClickListener {
                closeDrawer()
                mainActivityPresenter?.navigateToSpecificCommentPage()
            }
        } else {
            ivProfilePic?.setOnClickListener {
                closeDrawer()
                val fragment = TeacherProfileFragment()
                FragmentUtils.replace(supportFragmentManager,
                                      fragment, R.id.content_frame, true)
            }
        }

        teacher?.let {
            tvUsername?.text = it.teacherName
            tvSub2?.text = AppUtils.getDesignation(it)
        }

        student?.let {
            tvUsername?.text = it.studentName
            tvSub2?.text = it.getClass(AppUtils.getCurrentAcademicYear())
        }

        if (UserHelper.getCurrentUser().uid != null) {
            ImageLoader.load(
                    Urls.getProfileUrl(UserHelper.getCurrentUser().uid!!),
                    R.drawable.ic_profle,
                    R.drawable.ic_profle,
                    ivProfilePic!!
            )
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        mainActivityPresenter?.syncNotificationCount()
        return if (mDrawerToggle!!.onOptionsItemSelected(item)) {
            true
        } else super.onOptionsItemSelected(item)

    }

    override fun onSupportNavigateUp(): Boolean {
        supportFragmentManager.popBackStackImmediate()
        return super.onSupportNavigateUp()
    }


    private fun showUpArrow(enable: Boolean) {
        if (enable) {
            supportActionBar?.setDisplayHomeAsUpEnabled(enable)
            mDrawerToggle?.isDrawerIndicatorEnabled = false
        } else {
            mDrawerToggle?.isDrawerIndicatorEnabled = true
            supportActionBar?.setHomeButtonEnabled(true)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
    }


    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        closeDrawer()

        when (menuItem.itemId) {
            MenuHelper.ID_HOME -> navigationHelper?.navToHome()
            MenuHelper.ID_NEWS -> navigationHelper?.navToNewsFeeds()
            MenuHelper.ID_MESSAGES -> navigationHelper?.navToMessages()
            MenuHelper.ID_EXAMS -> navigationHelper?.navToExams()
            MenuHelper.ID_LEAVES -> navigationHelper?.navToLeave()
            MenuHelper.ID_NOTIFICATIONS -> navigationHelper?.navToNotifications()
            MenuHelper.ID_CONTACTS -> navigationHelper?.navToContact()
            MenuHelper.ID_STUDENTS -> navigationHelper?.navToStudents()
            MenuHelper.ID_TEACHERS -> navigationHelper?.navToTeachers()
            MenuHelper.ID_LOGOUT -> {
                val isTeacher = UserHelper.isTeacher()
                navigationHelper?.logout()
                finish()
                LoginActivity.start(this, isTeacher)
            }
        }
        return true
    }

    private fun closeDrawer() {
        drawerLayout?.closeDrawer(GravityCompat.START)
    }

    override fun onBackPressed() {
        if (drawerLayout?.isDrawerOpen(GravityCompat.START) == true) {
            closeDrawer()
        } else {
            if (supportFragmentManager.backStackEntryCount != 0 || navigationHelper!!.isHomePage()) {
                super.onBackPressed()
            } else {
                FragmentUtils.clearBackStack(supportFragmentManager)
                navigationHelper?.navToHome()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val fragments = supportFragmentManager.fragments
        for (fragment in fragments) {
            if (fragment != null && fragment is BaseFragment) {
                val handled = fragment.handleOnActivityResult(requestCode, resultCode, data)
                if (handled) break
            }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    companion object {
        const val STUDENT = "STUDENT"
        const val TEACHER = "TEACHER"
        const val ADMIN = "ADMIN"
        const val NOTIFICATION_DATA = "NOTIFICATION_DATA"

        fun start(context: Context, extras: Bundle) {
            val intent = Intent(context, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            intent.putExtras(extras)
            context.startActivity(intent)
        }

        fun createExtras(student: Student, notification: UiNotification? = null): Bundle {
            val bundle = Bundle()
            bundle.putSerializable(STUDENT, student)
            bundle.putSerializable(NOTIFICATION_DATA, notification)
            return bundle
        }

        fun createExtras(teacher: Teacher, notification: UiNotification? = null): Bundle {
            val bundle = Bundle()
            bundle.putSerializable(TEACHER, teacher)
            bundle.putSerializable(NOTIFICATION_DATA, notification)
            return bundle
        }

    }

    override fun getCloudStorage(): CloudStorage? {
        return cloudStorage
    }


}