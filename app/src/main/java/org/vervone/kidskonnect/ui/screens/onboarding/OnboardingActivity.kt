package org.vervone.kidskonnect.ui.screens.onboarding

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.ImageView
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.helper.PreferenceHelper
import org.vervone.kidskonnect.ui.screens.SchoolSelectionActivity
import org.vervone.kidskonnect.ui.screens.login.LoginActivity
import org.vervone.kidskonnect.ui.utils.Const
import org.vervone.kidskonnect.ui.utils.UiUtils
import org.vervone.kidskonnect.ui.views.CirclePageIndicator

class OnboardingActivity : AppCompatActivity() {


    private lateinit var viewPager: ViewPager
    private lateinit var btnNext: Button
    private lateinit var btnPrev: Button
    private lateinit var pageIndicator: CirclePageIndicator
    private lateinit var btnSingle: Button
    private lateinit var layout: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding)

        findViewById<ImageView>(R.id.ibSkip).setOnClickListener {
            PreferenceHelper.save(false, Const.PREF_IS_FIRST)
            //LoginActivity.start(this)
           // finish()
            navigateToSchoolSelection()
        }

        this.btnNext = findViewById(R.id.btn_next)
        this.btnPrev = findViewById(R.id.btn_prev)
        this.pageIndicator = findViewById(R.id.page_indicator)
        this.viewPager = findViewById(R.id.viewPager)
        this.btnSingle = findViewById(R.id.btn_single)
        this.layout = findViewById(R.id.btn_layout)
        btnSingle.text = getString(R.string.lets_start)

        btnSingle.setOnClickListener {
            PreferenceHelper.save(false, Const.PREF_IS_FIRST)
           // LoginActivity.start(this)
            navigateToSchoolSelection()
           // finish()
        }

        this.viewPager.adapter = VPAdapter()
        pageIndicator.setViewPager(viewPager)

        btnPrev.setOnClickListener {
            if (viewPager.currentItem != 0) {
                viewPager.currentItem = viewPager.currentItem - 1
            }
        }

        btnNext.setOnClickListener {
            viewPager.currentItem = viewPager.currentItem + 1
        }

        btnPrev.isEnabled = (viewPager.currentItem != 0)
        updateButton()

        pageIndicator.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {}
            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {}
            override fun onPageSelected(position: Int) {
                btnPrev.isEnabled = position != 0
                updateButton()
                if (position == 3) {
                    btnSingle.visibility = View.VISIBLE
                    layout.visibility = View.INVISIBLE
                } else {
                    btnSingle.visibility = View.GONE
                    layout.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun navigateToSchoolSelection() {
        SchoolSelectionActivity.start(this)
        finish()
    }

    private fun updateButton() {
        if (!btnPrev.isEnabled) {
            val grey = resources.getColor(R.color.grey)
            btnPrev.setTextColor(grey)
            UiUtils.setTextViewDrawableColorFilter(btnPrev, grey)
        } else {
            val primary = resources.getColor(R.color.primaryColor)
            btnPrev.setTextColor(primary)
            UiUtils.setTextViewDrawableColorFilter(btnPrev, primary)

        }
    }


    private inner class VPAdapter : FragmentPagerAdapter(supportFragmentManager) {

        private val TYPES = listOf(OnBoardingFragment.Type.NEWS,
                                   OnBoardingFragment.Type.NOTIFICATION,
                                   OnBoardingFragment.Type.MESSAGES,
                                   OnBoardingFragment.Type.WELCOME)


        override fun getItem(position: Int): Fragment {
            return OnBoardingFragment.createInstance(TYPES[position])
        }

        override fun getCount(): Int {
            return TYPES.size
        }

    }


    companion object {
        fun start(context: Context) {
            val intent = Intent(context, OnboardingActivity::class.java)
            context.startActivity(intent)
        }
    }


}