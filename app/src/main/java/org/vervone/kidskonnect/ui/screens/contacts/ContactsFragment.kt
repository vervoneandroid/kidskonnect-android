package org.vervone.kidskonnect.ui.screens.contacts

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.TextView
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.DomainError
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.model.SchoolContact
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.utils.PermissionHelper
import org.vervone.kidskonnect.ui.utils.UiUtils


class ContactsFragment : BaseFragment(), PermissionHelper.PermissionsListener {


    private val schoolFacade = FacadeFactory.schoolFacade

    private lateinit var tvSchoolName: TextView
    private lateinit var tvPhoneNumber: TextView
    private lateinit var tvEmail: TextView
    private lateinit var tvAddress: TextView
    private lateinit var tvPlace: TextView

    private var permissionHelper: PermissionHelper? = null
    private var schoolContact: SchoolContact? = null

    override fun getLayoutId(): Int {
        return R.layout.fragment_contact_page
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.setTitle(R.string.contact_school)

        tvSchoolName = view.findViewById(R.id.tv_sc_name)
        tvPlace = view.findViewById(R.id.tv_sc_place)
        tvEmail = view.findViewById(R.id.tv_sc_email)
        tvPhoneNumber = view.findViewById(R.id.tv_sc_phone)
        tvAddress = view.findViewById(R.id.tv_sc_address)

        val tintColor = resources.getColor(R.color.blue_1)

        UiUtils.setTextViewDrawableColorFilter(tvEmail, tintColor)
        UiUtils.setTextViewDrawableColorFilter(tvPhoneNumber, tintColor)

        showLoading()

        schoolFacade.getSchoolContact(callback)

        this.permissionHelper = PermissionHelper(this)
        this.permissionHelper?.setListener(this)


        tvPhoneNumber.setOnClickListener {
            this.permissionHelper?.requestPermission(arrayOf(CALL_PHONE), CALL_PHONE_REQ_CODE)
        }

        tvEmail.setOnClickListener {
            openEmailClient()
        }
    }

    private fun openEmailClient() {
        /* Create the Intent */
        val emailIntent = Intent(Intent.ACTION_SEND)

        /* Fill it with Data */
        emailIntent.type = "plain/text";
        emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(schoolContact?.email));
        context?.startActivity(Intent.createChooser(emailIntent, ""));
    }

    override fun onDestroyView() {
        permissionHelper?.onDestroy()
        super.onDestroyView()
    }

    override fun onPermissionGranted(requestCode: Int) {
        if (CALL_PHONE_REQ_CODE == requestCode && schoolContact?.contactNo != null) {
            makeCall()
        }
    }

    private fun makeCall() {
        try {
            val callIntent = Intent(Intent.ACTION_CALL)
            callIntent.data = Uri.parse("tel:${schoolContact?.contactNo}")
            activity?.startActivity(callIntent)
        } catch (ex: Exception) {

        }
    }

    override fun onPermissionRejectedManyTimes(rejectedPerms: MutableList<String>, request_code: Int) {

    }

    private val callback = object : RequestCallback<SchoolContact, DomainError> {
        override fun onError(error: DomainError?) {
            showErrorLayout(getString(R.string.operation_failed))
        }

        override fun onSuccess(res: SchoolContact?) {
            updateUi(res)
        }
    }

    private fun updateUi(contact: SchoolContact?) {
        showContentLayout()
        this.schoolContact = contact
        if (contact != null) {
            val schoolContact = contact!!
            tvSchoolName.text = schoolContact.schoolName ?: ""
            tvPhoneNumber.text = schoolContact.contactNo ?: ""
            tvEmail.text = schoolContact.email ?: ""
            tvPlace.text = schoolContact.place ?: ""
            tvAddress.text = schoolContact.address ?: ""
        }
    }

    companion object {
        const val CALL_PHONE = android.Manifest.permission.CALL_PHONE
        const val CALL_PHONE_REQ_CODE = 11
        fun createInstance(): ContactsFragment {
            return ContactsFragment()
        }
    }


}