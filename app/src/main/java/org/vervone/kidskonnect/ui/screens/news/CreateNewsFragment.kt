package org.vervone.kidskonnect.ui.screens.news

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.ui.screens.base.BaseFragment

class CreateNewsFragment : BaseFragment() {

    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager: ViewPager

    override fun getLayoutId(): Int {
        return R.layout.fragment_create_news
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.setTitle(R.string.news_feeds_title)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.tabLayout = view.findViewById(R.id.tab_layout)
        this.viewPager = view.findViewById(R.id.pager)

        viewPager.adapter = CreateNewsAdapter()
        tabLayout.setupWithViewPager(viewPager)
    }


    inner class CreateNewsAdapter() : FragmentPagerAdapter(childFragmentManager) {

        override fun getItem(pos: Int): Fragment {
            return when (pos) {
                0 -> CreateMediaNewsFragment()
                1 -> CreateWebNewsFragment()
                else -> Fragment()
            }
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return when (position) {
                0 -> getString(R.string.create_news)
                1 -> getString(R.string.paste_a_url)
                else -> ""
            }
        }

        override fun getCount(): Int {
            return 2
        }

    }


}