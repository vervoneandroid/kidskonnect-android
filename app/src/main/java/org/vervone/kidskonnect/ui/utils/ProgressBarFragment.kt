package org.vervone.kidskonnect.ui.utils

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.*
import org.vervone.kidskonnect.R


class ProgressBarFragment : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_progress, container, false)
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setOnKeyListener { _, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK
                && event.action == KeyEvent.ACTION_UP
            ) {
                getListener()?.onBack()
                return@setOnKeyListener true
            }
            false
        }
        return dialog
    }


    private fun getListener(): OnBackButtonListener? {
        return activity as? OnBackButtonListener
    }

    interface OnBackButtonListener {
        fun onBack()
    }

    companion object {
        fun newInstance(): ProgressBarFragment {
            val args = Bundle()
            val fragment = ProgressBarFragment()
            fragment.arguments = args
            return fragment
        }
    }
}