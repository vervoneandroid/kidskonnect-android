package org.vervone.kidskonnect.ui.screens.student

import android.os.Bundle
import android.view.View
import android.widget.*
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.DomainError
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.model.student.Comment
import org.vervone.kidskonnect.core.domain.model.student.CommentType
import org.vervone.kidskonnect.core.helper.cast
import org.vervone.kidskonnect.core.push.PushApi
import org.vervone.kidskonnect.ui.screens.base.BaseDialogFragment
import org.vervone.kidskonnect.ui.utils.UiUtils
import org.vervone.kidskonnect.ui.utils.UserHelper

class AddCommentFragment : BaseDialogFragment() {

    private lateinit var btnOk: Button
    private lateinit var btnCancel: Button
    private lateinit var tvContent: TextView
    private lateinit var tvAlertTitle: TextView
    private lateinit var etReplyComment: EditText

    private val schoolFacade = FacadeFactory.schoolFacade

    private var commentType = CommentType.UNKNOWN
    private var studentId: String? = null
    private var studentName: String? = null
    private var className: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null) {
            this.commentType = arguments!!.getSerializable(ARG_COMMENT_TYPE) as CommentType
            this.studentId = arguments!!.getString(ARG_STUDENT_ID, null)
            this.studentName = arguments!!.getString(ARG_STUDENT_NAME, null)
            this.className = arguments!!.getString(ARG_CLASS_NAME, null)
        }

    }

    override fun getLayoutId(): Int {
        return R.layout.dialog_leave_reply
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.etReplyComment = view.findViewById(R.id.et_reply_comment)
        this.tvContent = view.findViewById(R.id.tv_content)
        this.btnCancel = view.findViewById(R.id.btn_cancel)
        this.btnOk = view.findViewById(R.id.btn_ok)
        this.tvAlertTitle = view.findViewById(R.id.tv_dialog_title)

        when (commentType) {
            CommentType.TALENTS_ACHIEVEMENT -> {
                tvContent.setText(R.string.add_kid_talents_and_achievements)
                tvAlertTitle.setText(R.string.talents_and_achievements)
            }

            CommentType.FAMILY_MATTERS -> {
                tvContent.setText(R.string.add_family_matters)
                tvAlertTitle.setText(R.string.family_matters)
            }

            CommentType.HEALTH_MATTERS -> {
                tvContent.setText(R.string.add_health_matters)
                tvAlertTitle.setText(R.string.health_matters)
            }

            CommentType.UNKNOWN -> {
                tvContent.setText("")
                tvAlertTitle.setText("")
            }
        }

        btnOk.setOnClickListener {

            val commentString = UiUtils.getString(etReplyComment)
            if (commentString.isNotEmpty() && commentString.isNotBlank()) {
                val comment = Comment()
                comment.comment = commentString
                comment.commentType = commentType
                comment.commentedPersonId = UserHelper.getCurrentUser().uid!!
                comment.commentedPersonName = UserHelper.getCurrentUser().name
                comment.postTime = System.currentTimeMillis()
                schoolFacade.addCommentOnStudent(
                        studentId!!,
                        comment,
                        LeaveRequestCallback(comment)
                )
            }
        }

        btnCancel.setOnClickListener {
            dismiss()
        }
    }


    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        )

    }

    private inner class LeaveRequestCallback(val comment: Comment) : RequestCallback<Comment, DomainError> {

        override fun onError(error: DomainError?) {
            Toast.makeText(context, R.string.operation_failed, Toast.LENGTH_SHORT).show()
            dismiss()
        }

        override fun onSuccess(res: Comment?) {
            getListener()?.onSuccess()
            publish()
            dismiss()
        }

        private fun publish() {
            PushApi.Factory.pushApi.publishProfileUpdated(className!!,
                                                          comment.commentedPersonName!!,
                                                          comment.commentType!!.value.toString(),
                                                          studentId!!, studentName!!)
        }
    }


    private fun getListener(): SuccessListener? {
        return if (parentFragment is SuccessListener) this.parentFragment?.cast()
        else null
    }


    companion object {

        const val TAG = "AddCommentFragment"
        private const val ARG_STUDENT_ID = "student_id"
        private const val ARG_COMMENT_TYPE = "comment_type"
        private const val ARG_STUDENT_NAME = "student_name"
        private const val ARG_CLASS_NAME = "class_name"


        fun createInstance(studentId: String,
                           studentName: String,
                           className: String,
                           commentType: CommentType): AddCommentFragment {
            val fragment = AddCommentFragment()
            val args = Bundle()
            args.putString(ARG_STUDENT_ID, studentId)
            args.putString(ARG_STUDENT_NAME, studentName)
            args.putString(ARG_CLASS_NAME, className)
            args.putSerializable(ARG_COMMENT_TYPE, commentType)
            fragment.arguments = args
            return fragment
        }
    }


    interface SuccessListener {
        fun onSuccess()
    }

}