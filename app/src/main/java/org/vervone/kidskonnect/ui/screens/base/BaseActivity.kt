package org.vervone.kidskonnect.ui.screens.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import org.vervone.kidskonnect.core.DLog

abstract class BaseActivity : AppCompatActivity() {

    //protected var tag = javaClass.simpleName


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DLog.v(this, "onCreate $this")
    }


    override fun onResume() {
        super.onResume()
        DLog.v(this, "onResume $this")
    }

    override fun onPause() {
        super.onPause()
        DLog.v(this, "onPause $this")
    }

    override fun onDestroy() {
        super.onDestroy()
        DLog.v(this, "onDestroy $this")
    }

}