package org.vervone.kidskonnect.ui.screens.login

import android.os.Bundle

class TeacherLoginFragment : LoginFragment() {

    companion object {
        fun newInstance(userType: UserType): TeacherLoginFragment {
            val loginFragment = TeacherLoginFragment()
            val args = Bundle()
            args.putInt(ARG_USER_TYPE, userType.value)
            loginFragment.arguments = args
            return loginFragment
        }
    }

}