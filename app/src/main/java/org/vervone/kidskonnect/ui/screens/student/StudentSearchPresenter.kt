package org.vervone.kidskonnect.ui.screens.student

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.DomainError
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.model.student.Student
import org.vervone.kidskonnect.ui.screens.base.BasePresenter
import org.vervone.kidskonnect.ui.utils.AppUtils
import org.vervone.kidskonnect.ui.utils.UserHelper

class StudentSearchPresenter : BasePresenter<StudentSearchView>() {
    private val schoolFacade = FacadeFactory.schoolFacade

    fun queryStudents(schoolClass: String?, query: String?) {
        val year = AppUtils.getCurrentAcademicYear()
        schoolFacade.getStudents(year, query, schoolClass, searchCallback)
    }

    fun getClasses() {
        schoolFacade.getClasses(object : RequestCallback<List<String>, DomainError> {
            override fun onError(error: DomainError?) {

            }

            override fun onSuccess(res: List<String>?) {
                view?.setClasses(filter(res ?: listOf()))
            }
        })
    }

    private fun filter(list: List<String>): List<String> {
        if (UserHelper.isAdmin()) {
            return list
        } else if (UserHelper.isTeacher() && UserHelper.teacher?.classesAndSubjects != null) {
            val keys = UserHelper.teacher?.classesAndSubjects?.keys
            if (keys != null) {
                return list.filter { keys.contains(it) }
            }
        }
        return list
    }


    private val searchCallback = object : RequestCallback<List<Student>, DomainError> {
        override fun onError(error: DomainError?) {

        }

        override fun onSuccess(res: List<Student>?) {
            view?.setSearchResult(res!!)
        }
    }

}