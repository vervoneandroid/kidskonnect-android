package org.vervone.kidskonnect.ui.screens.login

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.View
import android.view.animation.Animation
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.domain.model.Teacher
import org.vervone.kidskonnect.core.domain.model.student.Student
import org.vervone.kidskonnect.core.helper.SchoolConfigHelper
import org.vervone.kidskonnect.ui.screens.MainActivity
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.utils.DialogFactory
import org.vervone.kidskonnect.ui.utils.UiUtils
import org.vervone.kidskonnect.ui.views.FlipAnimation


/**
 *  Login screen fragment , used for both student and teacher login.
 */
open class LoginFragment : BaseFragment(), LoginView {


    private var loginPresenter: LoginPresenter? = null
    private var etEmailId: EditText? = null
    private var etPwd: EditText? = null
    private var btnSignIn: Button? = null
    private var txtSignInAs: TextView? = null
    private var userType: Int = -1


    companion object {
        private const val DURATION: Long = 500

        const val ARG_USER_TYPE = "userType"
        const val ARG_ANIM = "anim_direction"

        // Startup Don't show animation
        private var needAnim = false

        private var animDirection = FlipAnimation.RIGHT

        fun setAnimDirection(direction: Int) {
            animDirection = direction
        }

        fun setAnim(value: Boolean) {
            needAnim = value
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.userType = arguments?.getInt(ARG_USER_TYPE, -1)!!
    }


    override fun getLayoutId(): Int {
        return R.layout.fragment_login
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // view initializing
        this.btnSignIn = view.findViewById(R.id.btnSignIn)
        this.etEmailId = view.findViewById(R.id.etName)
        this.etPwd = view.findViewById(R.id.etPwd)
        this.txtSignInAs = view.findViewById(R.id.tvSignInAs)

        if (userType == UserType.STUDENT.value) {
            this.txtSignInAs?.setText(R.string.sign_in_teacher)
            //etEmailId?.setText("anu@vervone.com")
            //etPwd?.setText("sonysunny")

        } else {
            this.txtSignInAs?.setText(R.string.sign_in_student)
            etEmailId?.setText("admin@vervone.com")
            etPwd?.setText("vervone@123")
        }

        txtSignInAs?.setOnClickListener {
            val activity = activity as LoginActivity
            activity.flipCard()
        }

        loginPresenter = LoginPresenter(UserType.fromValue(userType))
        loginPresenter?.onAttach(this)

        btnSignIn?.setOnClickListener {

            if (validate()) {
                loginPresenter?.performLogin(
                        UiUtils.getString(etEmailId),
                        UiUtils.getString(etPwd)
                )
            } else {
                DialogFactory.createSimpleOkErrorDialog(
                        activity!!, R.string.error,
                        R.string.pls_fill_all_fields
                ).show()
            }
        }
    }


    private fun validate(): Boolean {
        if (UiUtils.isEmpty(etEmailId) || UiUtils.isEmpty(etPwd)) {
            return false
        }
        return true
    }

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        return if (needAnim) {
            FlipAnimation.create(animDirection, enter, DURATION)
        } else {
            // If this is app startup we dont show animation.
            needAnim = true
            super.onCreateAnimation(transit, enter, nextAnim)
        }
    }

    override fun showError(message: String) {
        DialogFactory.createSimpleOkErrorDialog(
                activity!!, getString(R.string.error),
                message
        ).show()
    }

    override fun showUnknownError() {
        DialogFactory.createSimpleOkErrorDialog(
                activity!!, getString(R.string.error),
                getString(R.string.unknown_backend_error)
        ).show()
    }

    override fun onDestroyView() {
        loginPresenter?.onDettach()
        super.onDestroyView()
    }

    override fun loginSuccess(student: Student) {
        SchoolConfigHelper.saveExisting()
        MainActivity.start(activity!!, MainActivity.createExtras(student))
        activity?.finish()
    }

    override fun loginSuccess(teacher: Teacher) {
        SchoolConfigHelper.saveExisting()
        MainActivity.start(activity!!, MainActivity.createExtras(teacher))
        activity?.finish()
    }

    private var progressDialog: DialogFragment? = null
    override fun showProgressBar() {
        progressDialog = DialogFactory.createProgressDialog(activity!!)
        progressDialog?.show(childFragmentManager, null)
    }

    override fun hideProgressBar() {
        progressDialog?.dismiss()
    }

}