package org.vervone.kidskonnect.ui.screens.messages.group

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.*
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.DomainError
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.TeachersForTheClass
import org.vervone.kidskonnect.core.domain.model.Designation
import org.vervone.kidskonnect.core.domain.model.Teacher
import org.vervone.kidskonnect.ui.screens.base.BaseFragment
import org.vervone.kidskonnect.ui.utils.*


class TeachersSelectionFragment : BaseFragment() {


    private lateinit var rvTeachers: RecyclerView
    private lateinit var tvSelectAll: TextView
    private val facade = FacadeFactory.schoolFacade
    private lateinit var etSearchQuery: EditText
    private var selected = ArrayList<String>()


    override fun getLayoutId(): Int {
        return R.layout.fragment_teacher_selection
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.selected = (arguments!!.getSerializable(SELECTED_TEACHERS) as ArrayList<String>)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvTeachers = view.findViewById(R.id.rv_teachers)
        tvSelectAll = view.findViewById(R.id.tv_select_all)
        rvTeachers.configure()

        view.findViewById<Button>(R.id.btn_done)
            .setOnClickListener {
                val adapter = (rvTeachers.adapter as Adapter)
                val selectedTeachers = adapter.list.filter { adapter.selected.contains(it.teacherId) }
                if (selectedTeachers.isEmpty()) {
                    DialogFactory.createSimpleOkErrorDialog(
                            activity!!,
                            null,
                            getString(R.string.please_select_at_least_one_teacher)).show()
                } else {
                    (parentFragment as OnTeachersSelected).onTeachersSelected(ArrayList(selectedTeachers))
                }
            }


        if (!UserHelper.isTeacher()) {
            tvSelectAll.extHide()
        }

        tvSelectAll.setOnClickListener {
            val adapter = (rvTeachers.adapter as Adapter)
            adapter.toggleSelection()
        }

        view.findViewById<Button>(R.id.btn_cancel)
            .setOnClickListener {
                (parentFragment as ClosableWindow).closeWindow()
            }

        etSearchQuery = view.findViewById(R.id.et_search_query)
        etSearchQuery.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {}
            override fun beforeTextChanged(
                    charSequence: CharSequence?,
                    p1: Int, p2: Int, p3: Int
            ) {
            }

            override fun onTextChanged(
                    charSequence: CharSequence?,
                    p1: Int, p2: Int, p3: Int
            ) {
                if (rvTeachers.adapter != null) {
                    //(rvTeachers.adapter as Adapter).selected.clear()
                    (rvTeachers.adapter as Adapter).filter.filter(charSequence)
                }
            }
        })

        val year = AppUtils.getCurrentAcademicYear()
        showLoading()
        facade.getTeachers(year, teachersCallback)
    }


    private val teachersCallback = object : RequestCallback<List<Teacher>, DomainError> {

        override fun onError(error: DomainError?) {
            activity?.runOnUiThread {
                showErrorLayout("")
            }
        }

        override fun onSuccess(res: List<Teacher>?) {
            val filtered = res!!.filterNot {
                it.designation == Designation.ADMIN ||
                        it.teacherId == UserHelper.getCurrentUser().uid ||
                        it.designation == Designation.UNKNOWN
            }.sortedWith(compareBy { it.teacherName })
            activity?.runOnUiThread {
                val adapter = Adapter(filtered)
                adapter.selected = selected
                selectionStatus(filtered)
                rvTeachers.adapter = adapter
                showContentLayout()
            }
        }
    }


    private fun selectionStatus(teachers: List<Teacher>) {
        val status = isAllSelected(teachers)
        tvSelectAll.setText(if (status) R.string.deselect_all else R.string.select_all)
    }

    private fun isAllSelected(teachers: List<Teacher>): Boolean {
        if (selected.size == teachers.size) {
            return teachers.map { it.teacherId }.containsAll(selected)
        } else if (selected.size > teachers.size) {
            return selected.containsAll(teachers.map { it.teacherId })
        }
        return false
    }

    private class VH(view: View) : RecyclerView.ViewHolder(view) {
        val tvTeacherName: TextView = view.findViewById(R.id.tv_teacher_name)
        val tvSubject: TextView = view.findViewById(R.id.tv_teacher_subject)
        val cbOption: CheckBox = view.findViewById(R.id.cb_option)
        val rootView: View = view.findViewById(R.id.root_layout)
    }

    private inner class Adapter(val list: List<Teacher>) :
        RecyclerView.Adapter<VH>(), Filterable {


        private var filteredData = ArrayList<Teacher>(list)
        var selected = ArrayList<String>()


        override fun onCreateViewHolder(vg: ViewGroup, pos: Int): VH {
            val view = vg.inflate(R.layout.kc_teacher_search_row)
            return VH(view)
        }

        override fun getItemCount(): Int {
            return filteredData.size
        }

        fun toggleSelection() {
            if (filteredData.isEmpty()) return
            if (!isAllSelected(filteredData)) {
                selected.clear()
                val selectedIds = filteredData.map { it.teacherId!! }
                selected.addAll(selectedIds)
            } else {
                selected.clear()
            }
            selectionStatus(filteredData)
            notifyDataSetChanged()
        }

        override fun onBindViewHolder(vh: VH, pos: Int) {
            val teacher = filteredData[pos]
            vh.tvTeacherName.text = teacher.teacherName
            if (!UserHelper.isTeacher()) {
                val classTeacher = TeachersForTheClass.getClassTeacher()
                if (teacher.teacherId == classTeacher?.teacherId) {
                    vh.tvTeacherName.text = "${teacher.teacherName} (Class incharge)"
                }

                if (teacher.designation == Designation.PRINCIPAL) {
                    vh.tvTeacherName.text = "${teacher.teacherName} (Principal)"
                }
            }

            vh.tvSubject.text = AppUtils.getFormattedSubjects(teacher)
            vh.cbOption.isChecked = selected.contains(teacher.teacherId)
            vh.cbOption.setOnClickListener {


                if (!UserHelper.isTeacher()) {
                    selected.clear()
                    if (vh.cbOption.isChecked) {
                        selected.add(teacher.teacherId!!)
                    } else {
                        selected.remove(teacher.teacherId)
                    }
                } else {

                    if (vh.cbOption.isChecked) {
                        selected.add(teacher.teacherId!!)
                    } else {
                        selected.remove(teacher.teacherId)
                    }
                }

                selectionStatus(filteredData)
                notifyDataSetChanged()
            }

        }


        override fun getFilter(): Filter {
            return teacherFilter
        }

        private val teacherFilter = TeacherFilter()

        inner class TeacherFilter : Filter() {
            override fun performFiltering(charSequence: CharSequence?): FilterResults {
                val charString = charSequence.toString()
                filteredData = if (charString.isEmpty()) {
                    ArrayList(list)
                } else {
                    val filteredList = list.filter { teacher ->
                        teacher.teacherName != null && teacher.teacherName!!.contains(charString, ignoreCase = true)
                    }
                    ArrayList(filteredList)
                }

                val filterResults = FilterResults()
                filterResults.values = filteredData
                return filterResults
            }

            override fun publishResults(charSeq: CharSequence?, resultSet: FilterResults?) {
                filteredData = resultSet?.values as ArrayList<Teacher>
                selectionStatus(filteredData)
                notifyDataSetChanged()
            }
        }

    }


    companion object {
        private const val SELECTED_TEACHERS = "SELECTED_TEACHERS"
        fun createInstance(list: ArrayList<String>): TeachersSelectionFragment {
            val args = Bundle()
            args.putSerializable(SELECTED_TEACHERS, list)
            val fragment = TeachersSelectionFragment()
            fragment.arguments = args
            return fragment
        }
    }

}