package org.vervone.kidskonnect.ui.screens.login

enum class UserType(val value: Int) {
    TEACHER(1),
    STUDENT(2);

    companion object {
        fun fromValue(value: Int): UserType {
            return if (value == 2) STUDENT else TEACHER
        }
    }
}