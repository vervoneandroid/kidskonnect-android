package org.vervone.kidskonnect.ui.teacher

import org.vervone.kidskonnect.core.domain.model.MessageChannel
import org.vervone.kidskonnect.core.domain.model.Teacher
import org.vervone.kidskonnect.ui.screens.base.BaseView

interface TeachersView : BaseView {
    fun setData(teachers: List<Teacher>)
    fun navigateToCreateChatGroup(teacher: Teacher)
    fun navigateToChatRoom(messageChannel: MessageChannel)
}