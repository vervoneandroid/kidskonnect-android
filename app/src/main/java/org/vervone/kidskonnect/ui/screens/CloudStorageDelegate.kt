package org.vervone.kidskonnect.ui.screens

import org.vervone.kidskonnect.core.cloudstorage.CloudStorage

interface CloudStorageDelegate {
    fun getCloudStorage(): CloudStorage?
}