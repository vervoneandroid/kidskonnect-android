package org.vervone.kidskonnect.ui.utils

import android.content.res.Resources
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.os.Build
import android.support.annotation.ColorInt
import android.util.DisplayMetrics
import android.widget.TextView

object UiUtils {
    fun determineScreenDensityCode(resources: Resources): String {
        return when (resources.displayMetrics.densityDpi) {
            DisplayMetrics.DENSITY_LOW -> "ldpi"
            DisplayMetrics.DENSITY_MEDIUM -> "mdpi"
            DisplayMetrics.DENSITY_HIGH -> "hdpi"
            DisplayMetrics.DENSITY_XHIGH, DisplayMetrics.DENSITY_280 -> "xhdpi"
            DisplayMetrics.DENSITY_XXHIGH, DisplayMetrics.DENSITY_360, DisplayMetrics.DENSITY_400, DisplayMetrics.DENSITY_420 -> "xxhdpi"
            DisplayMetrics.DENSITY_XXXHIGH, DisplayMetrics.DENSITY_560 -> "xxxhdpi"
            else -> "Unknown code ${resources.displayMetrics.densityDpi}"
        }
    }

    fun isEmpty(tv: TextView?): Boolean {
        tv?.let {
            val input = tv.text.toString().trim { it <= ' ' }
            return input.isEmpty()
        }
        return false
    }

    fun getString(tv: TextView?): String {
        tv?.let {
            return tv.text.toString().trim { it <= ' ' }
        }
        return ""
    }

    fun setTextViewDrawableColorFilter(textView: TextView, @ColorInt color: Int) {
        for (drawable in textView.compoundDrawables) {
            drawable?.mutate()?.colorFilter = PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            for (drawable in textView.compoundDrawablesRelative)
                drawable?.mutate()?.colorFilter = PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP)
        }
    }

    fun formatCount(count: Int): String {
        return if (count > 99) "99+" else count.toString()
    }

}
