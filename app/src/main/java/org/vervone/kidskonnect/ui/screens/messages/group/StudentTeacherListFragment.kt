package org.vervone.kidskonnect.ui.screens.messages.group

import android.app.Dialog
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.domain.model.Teacher
import org.vervone.kidskonnect.core.domain.model.student.Student
import org.vervone.kidskonnect.core.helper.cast
import org.vervone.kidskonnect.ui.screens.base.BaseDialogFragment
import org.vervone.kidskonnect.ui.utils.FragmentUtils
import org.vervone.kidskonnect.ui.utils.UserHelper


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class StudentTeacherListFragment : BaseDialogFragment(),
    CategorySelectionFragment.CategoryClickListener,
    OnTeachersSelected, ClosableWindow, OnClassesSelected {

    private var selected = ArrayList<String>()
    private var selectedStudent = HashMap<String, List<Student>>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FSDialog)
        arguments?.let { args ->
            this.selected = args.getSerializable(SELECTED_TEACHERS).cast()
            this.selectedStudent = args.getSerializable(SELECTED_STUDENTS).cast()
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_content
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!UserHelper.isTeacher()) {
            onCategoryClick(0)
        } /*else if (UserHelper.isTeacher() && !UserHelper.isAdmin()) {
            onCategoryClick(1)
        }*/ else {
            FragmentUtils.replace(
                    childFragmentManager,
                    CategorySelectionFragment(),
                    R.id.fragment_container
            )
        }
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setCancelable(false)
        dialog.setOnKeyListener { _, keyCode, keyEvent ->
            if (keyCode == KeyEvent.KEYCODE_BACK &&
                keyEvent?.action == KeyEvent.ACTION_UP
            ) {
                handleBack()
            } else {
                false
            }
        }
        return dialog
    }

    private fun handleBack(): Boolean {
        val handled = childFragmentManager.backStackEntryCount > 0
        if (handled) {
            childFragmentManager.popBackStack()
        }

        return handled
    }


    override fun onCategoryClick(pos: Int) {
        if (pos == 0) {
            FragmentUtils.replace(
                    childFragmentManager,
                    TeachersSelectionFragment.createInstance(selected),
                    R.id.fragment_container
            )
        } else {
            FragmentUtils.replace(
                    childFragmentManager,
                    ClassSelectionFragment.createInstance(selectedStudent),
                    R.id.fragment_container
            )
        }
    }

    override fun onTeachersSelected(selected: ArrayList<Teacher>) {
        if (parentFragment is OnTeachersSelected) {
            (parentFragment as OnTeachersSelected).onTeachersSelected(selected)
        }
        dismiss()
    }

    override fun closeWindow() {
        dismiss()
    }


    override fun onClassesSelected(
            selectedStudent: Map<String, List<Student>>,
            allSelectedClasses: List<String>
    ) {
        if (parentFragment is OnClassesSelected) {
            (parentFragment as OnClassesSelected).onClassesSelected(
                    selectedStudent,
                    allSelectedClasses
            )
        }
        dismiss()
    }

    companion object {
        private const val SELECTED_TEACHERS = "SELECTED_TEACHERS"
        private const val SELECTED_STUDENTS = "SELECTED_STUDENTS"
        fun createInstance(
                list: ArrayList<String>,
                selectedStudent: HashMap<String, List<Student>>
        ): StudentTeacherListFragment {
            val args = Bundle()
            args.putSerializable(SELECTED_TEACHERS, ArrayList(list))
            args.putSerializable(SELECTED_STUDENTS, selectedStudent)
            val fragment = StudentTeacherListFragment()
            fragment.arguments = args
            return fragment
        }
    }


}