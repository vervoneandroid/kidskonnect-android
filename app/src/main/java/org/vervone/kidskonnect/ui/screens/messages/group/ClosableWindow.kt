package org.vervone.kidskonnect.ui.screens.messages.group

interface ClosableWindow {
    fun closeWindow()
}