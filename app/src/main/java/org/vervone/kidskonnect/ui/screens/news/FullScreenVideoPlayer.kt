package org.vervone.kidskonnect.ui.screens.news

import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.ImageButton
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.upstream.FileDataSourceFactory
import org.vervone.kidskonnect.R
import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.cloudstorage.CloudStorage
import org.vervone.kidskonnect.core.helper.ThreadUtils
import org.vervone.kidskonnect.ui.screens.CloudStorageDelegate
import org.vervone.kidskonnect.ui.screens.base.BaseFullScreenWindow
import java.io.File


class FullScreenVideoPlayer : BaseFullScreenWindow() {

    private var playerView: PlayerView? = null
    private var exoPlayer: SimpleExoPlayer? = null
    private var videoUrl: String? = null
    private var videoKey: String? = null
    // Video local path .
    private var videoPath: String? = null
    private var needPreSign: Boolean = false
    private var cloudStorage: CloudStorage? = null

    companion object {
        private const val VIDEO_KEY = "video_key"
        private const val NEED_PRE_SIGN = "need_pre_sign"
        private const val VIDEO_PATH = "video_path"
        private const val USER_AGENT = "exoplayer"

        fun createInstance(key: String?, needPreSign: Boolean = false
                           , videoPath: String? = null): FullScreenVideoPlayer {
            val videoFragment = FullScreenVideoPlayer()
            val bundle = Bundle()
            bundle.putString(VIDEO_KEY, key)
            bundle.putBoolean(NEED_PRE_SIGN, needPreSign)
            bundle.putString(VIDEO_PATH, videoPath)
            videoFragment.arguments = bundle
            return videoFragment
        }


    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (activity is CloudStorageDelegate) {
            val delegate = activity as CloudStorageDelegate
            cloudStorage = delegate.getCloudStorage()
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.videoKey = arguments?.getString(VIDEO_KEY)
        this.needPreSign = arguments?.getBoolean(NEED_PRE_SIGN, false)!!
        this.videoPath = arguments?.getString(VIDEO_PATH)


        if (videoPath != null && File(videoPath).exists()) {
        } else if (needPreSign && videoKey != null) {
            videoPath = null
            cloudStorage?.getSignedUrl(videoKey!!, object : CloudStorage.Callback {
                override fun onResult(url: String?) {
                    DLog.v(this, "#url# $url")
                    playIfViewVideoAvailable(url)
                }
            })
        }
    }


    private fun playIfViewVideoAvailable(videoUrl: String?) {
        ThreadUtils.runOnUi(Runnable {
            this.videoUrl = videoUrl
            if (this.playerView != null) {
                playVideo()
            }
        })

    }

    private fun buildMediaSourceFile(uri: Uri): MediaSource {
        val dataSourceFactory = FileDataSourceFactory()
        return ExtractorMediaSource(uri, dataSourceFactory,
                                    DefaultExtractorsFactory(), null, null)
    }

    private fun playLocalVideo() {
        if (videoPath != null) {
            val uri = Uri.parse(videoPath)
            val mediaSource = buildMediaSourceFile(uri)
            exoPlayer?.prepare(mediaSource)
        }
    }

    private fun playVideo() {
        if (videoUrl != null) {
            val uri = Uri.parse(videoUrl)
            val mediaSource = buildMediaSource(uri)
            exoPlayer?.prepare(mediaSource, true, false)
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.kc_layout_full_screen_vplayer
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.playerView = view.findViewById(R.id.video_view)
        view.findViewById<ImageButton>(R.id.ib_close).setOnClickListener {
            dismiss()
        }

    }

    private fun initializePlayer() {
        exoPlayer = ExoPlayerFactory.newSimpleInstance(
                DefaultRenderersFactory(activity),
                DefaultTrackSelector(), DefaultLoadControl()
        )

        playerView?.player = exoPlayer

        exoPlayer?.playWhenReady = true
        exoPlayer?.seekTo(0, 0)

        if (videoPath == null) {
            playVideo()
        } else {
            playLocalVideo()
        }

    }

    private fun buildMediaSource(uri: Uri): MediaSource {
        return ExtractorMediaSource.Factory(DefaultHttpDataSourceFactory("exoplayer"))
            .createMediaSource(uri)
    }

    override fun onStart() {
        super.onStart()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            initializePlayer()
            if (playerView != null) {
                // playerView?.onResume()
            }
        }
    }

    override fun onResume() {
        super.onResume()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = dialog.window
            window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window?.statusBarColor = Color.BLACK
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N || exoPlayer == null) {
            initializePlayer()
            if (playerView != null) {
                //playerView.onResume()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            releasePlayer()
        }
    }

    override fun onStop() {
        super.onStop()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            releasePlayer()
        }
    }

    private fun releasePlayer() {
        exoPlayer?.stop()
        exoPlayer?.release()
    }

}