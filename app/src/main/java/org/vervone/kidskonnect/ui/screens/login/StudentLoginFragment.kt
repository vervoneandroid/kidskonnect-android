package org.vervone.kidskonnect.ui.screens.login

import android.os.Bundle

class StudentLoginFragment : LoginFragment() {

    companion object {

        fun newInstance(userType: UserType): StudentLoginFragment {
            val loginFragment = StudentLoginFragment()
            val args = Bundle()
            args.putInt(ARG_USER_TYPE, userType.value)
            loginFragment.arguments = args
            return loginFragment
        }


    }

}