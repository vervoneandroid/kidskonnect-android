package org.vervone.kidskonnect.ui.screens.messages

object MessagesUtils {

    fun buildTargetId(targets: List<String>): String {
        val sortedIds = targets.sorted()
        return sortedIds.joinToString(separator = "/", postfix = "/")
    }

}