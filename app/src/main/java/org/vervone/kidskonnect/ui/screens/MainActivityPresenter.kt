package org.vervone.kidskonnect.ui.screens

import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.data.network.MessageCallback
import org.vervone.kidskonnect.core.domain.*
import org.vervone.kidskonnect.core.domain.model.*
import org.vervone.kidskonnect.core.domain.model.student.Comment
import org.vervone.kidskonnect.core.domain.model.student.CommentType
import org.vervone.kidskonnect.core.domain.model.student.Student
import org.vervone.kidskonnect.core.helper.PreferenceHelper
import org.vervone.kidskonnect.ui.MainActivityView
import org.vervone.kidskonnect.ui.screens.base.BasePresenter
import org.vervone.kidskonnect.ui.screens.messages.MessagesCountCache
import org.vervone.kidskonnect.ui.utils.*

class MainActivityPresenter(private val messageFacade: MessageFacade) : BasePresenter<MainActivityView>() {

    private var newsFacade = FacadeFactory.newsFacade
    private var schoolFacade = FacadeFactory.schoolFacade
    private var leavesFacade = FacadeFactory.leavesFacade

    fun initialize() {
        val year = AppUtils.getCurrentAcademicYear()

        if (UserHelper.isTeacher() && UserHelper.teacher != null
            && UserHelper.teacher?.classesAndSubjects?.keys != null
        ) {


            schoolFacade.getStudents(
                    year, null, null, object : RequestCallback<List<Student>, DomainError> {
                override fun onError(error: DomainError?) {}
                override fun onSuccess(res: List<Student>?) {

                }
            })

            val teacher = UserHelper.teacher!!
            val classes = ArrayList(teacher.classesAndSubjects!!.keys).toList()

            val params = TeacherParamsBm(year, UserHelper.isAdmin(), classes, teacher.teacherId!!)
            messageFacade.subscribeForTeacherMessages(
                    params, object : MessageCallback {
                override fun onChange(msgChannelId: String, msgCount: Int) {
                    MessagesCountCache.put(msgChannelId, msgCount, "")
                }
            })

            messageFacade.subscribeTeacherMessageNotifications(params, object :
                MessageFacade.NotificationListener {
                override fun onSnapshot(messageChannel: MessageChannel?, msgCount: Int) {
                    if (messageChannel != null) {
                        handleMessageSnapshotEvent(messageChannel, msgCount)
                    }
                }

                override fun onMessage(fbChatMessage: ChatMessage, msgCount: Int, messageChannel: MessageChannel?) {
                    if (messageChannel != null) {
                        handleMessageCount(messageChannel, msgCount, fbChatMessage)
                    }
                }
            })

            newsFacade.subscribeForNewsNotification(params, object : NewsFacade.NewsObserver {
                override fun onAdded(count: Int, news: News) {
                    handleNewsAddEvent(count, news)
                }

                override fun onSnapshot(newsCount: Int) {
                    handleNewsSnapshot(newsCount)
                }
            })

            leavesFacade.subscribeLeaveRequests(params, object : LeavesFacade.LeaveRequestListener {
                override fun onRequest(count: Int, leave: Leave) {
                    handleLeaveCount(count)
                    view?.showLeaveRequestNotification(leave.studentName ?: "")
                }

                override fun onSnapshot(count: Int) {
                    handleLeaveCount(count)
                }
            })

            val allCommentsCount = AllCommentsCount()
            schoolFacade.subscribeComment(params, object : SchoolFacade.CommentsListener {
                override fun onNewComment(count: Int, student: Student, comment: Comment) {
                    DLog.v(this, "onNewComment")
                    if (comment.commentedPersonId != UserHelper.getCurrentUser().uid) {
                        val className = student.getClass(year)
                        val studentId = student.studentId
                        if (studentId != null && className != null) {
                            allCommentsCount.setUnreadCount(count, studentId, className)
                            view?.updateStudentCommentsCount(allCommentsCount.getTotalCount())
                        }
                        view?.showProfileUpdatedNotification(comment, student)
                    }
                }

                override fun onSnapshot(count: Int, student: Student) {
                    DLog.v(this, "onSnapshot ${student.studentName} $count")
                    val className = student.getClass(year)
                    val studentId = student.studentId
                    if (studentId != null && className != null) {
                        allCommentsCount.setUnreadCount(count, studentId, className)
                        view?.updateStudentCommentsCount(allCommentsCount.getTotalCount())
                    }


                }
            })


        } else if (!UserHelper.isTeacher() && UserHelper.student != null) {

            val student = UserHelper.student!!
            val studentId = student.studentId!!
            val params = StudentParamsBm(year, student.getClass(year)!!, studentId)
            messageFacade.subscribeForStudentMessage(
                    params, object : MessageCallback {
                override fun onChange(msgChannelId: String, msgCount: Int) {}
            })

            messageFacade.subscribeStudentMessageNotifications(params, object :
                MessageFacade.NotificationListener {
                override fun onMessage(fbChatMessage: ChatMessage, msgCount: Int, messageChannel: MessageChannel?) {
                    if (messageChannel != null) {
                        handleMessageCount(messageChannel, msgCount, fbChatMessage)
                    }
                }

                override fun onSnapshot(messageChannel: MessageChannel?, msgCount: Int) {
                    if (messageChannel != null) {
                        handleMessageSnapshotEvent(messageChannel, msgCount)
                    }
                }
            })

            newsFacade.subscribeForNewsNotification(params, object : NewsFacade.NewsObserver {
                override fun onAdded(count: Int, news: News) {
                    handleNewsAddEvent(count, news)
                }

                override fun onSnapshot(newsCount: Int) {
                    handleNewsSnapshot(newsCount)
                }
            })

            schoolFacade.subscribeStudent(studentId, object : SchoolFacade.StudentListener {
                override fun onUpdate(student: Student) {
                }
            })

            schoolFacade.subscribeComment(params, object : SchoolFacade.CommentsListener {
                override fun onNewComment(count: Int, student: Student, comment: Comment) {
                    DLog.v(this, "onNewComment")
                    if (comment.commentedPersonId != UserHelper.getCurrentUser().uid) {
                        handleCommentCount(count)
                        view?.showProfileUpdatedNotification(comment, student)
                        val commentType = comment.commentType?.name ?: CommentType.UNKNOWN.name
                        val commentTypes = PreferenceHelper.getStringSet(UNREAD_COMMENT_TYPE).toMutableSet()
                        commentTypes.add(commentType)
                        PreferenceHelper.save(commentTypes, UNREAD_COMMENT_TYPE)
                    } else {
                        val counter = CommentsCount()
                        counter.setUnreadCount(count)
                        counter.markAsRead()
                    }
                }

                override fun onSnapshot(count: Int, student: Student) {
                    handleCommentCount(count)
                }
            })

            leavesFacade.subscribeLeaveStatusChange(params, object : LeavesFacade.LeaveStatusChange {
                override fun onChange(leave: Leave) {
                    if (leave.leaveStatus == LeaveStatus.APPROVED) {
                        view?.showLeaveApprovedNotification()
                    } else if (leave.leaveStatus == LeaveStatus.REJECTED) {
                        view?.showLeaveRejectedNotification()
                    }
                }
            })
        }

        SubscriptionHelper.register()
    }

    fun navigateToSpecificCommentPage() {
        val unreadComment = PreferenceHelper.getStringSet(UNREAD_COMMENT_TYPE)
        val commentType = unreadComment.lastOrNull()
        if (commentType != null) {
            view?.navigateToStudentProfile(CommentType.valueOf(commentType))
        } else {
            view?.navigateToStudentProfile(CommentType.PROFILE)
        }
    }


    private fun handleLeaveCount(count: Int) {
        val leaveCount = LeaveCount().setUnreadCount(count)
        view?.updateRequestsLeaveCount(leaveCount)
    }

    private fun handleCommentCount(count: Int) {
        val commentCount = CommentsCount().setUnreadCount(count)
        view?.updateCommentsCount(commentCount)
    }

    private fun handleMessageCount(msgChannel: MessageChannel, msgCount: Int, message: ChatMessage) {
        val channelId = msgChannel.id!!
        val unread = MessageCount().setUnreadCount(channelId, msgCount)

        if (message.senderId != UserHelper.getCurrentUser().uid) {
            view?.showMessageNotification(
                    unread, msgChannel.getChatName(UserHelper.getUserData()?.uid!!), channelId)
            val count = MessageCount().getTotalUnread()
            view?.updateMessageCount(count)
        }
    }

    private fun handleMessageSnapshotEvent(msgChannel: MessageChannel, msgCount: Int) {
        val channelId = msgChannel.id!!
        MessageCount().setUnreadCount(channelId, msgCount)
        val count = MessageCount().getTotalUnread()
        view?.updateMessageCount(count)
    }

    private fun handleNewsAddEvent(count: Int, news: News) {
        if (news.senderId == UserHelper.getUserData()?.uid) {
            NewsCount().markAsRead()
        } else {
            val unread = NewsCount().setUnreadCount(count)
            if (unread > 0) {
                view?.showNewsNotification(unread)
                view?.updateNewsCount(unread)
            }
        }
    }

    private fun updateStudentCommentCount() {
        view?.updateStudentCommentsCount(AllCommentsCount().getTotalCount())
    }

    private fun handleNewsSnapshot(count: Int) {
        val unread = NewsCount().setUnreadCount(count)
        if (unread > 0) {
            view?.showNewsNotification(unread)
            view?.updateNewsCount(unread)
        }
    }


    fun syncNotificationCount() {
        updateMessageCount()
        updateNewsCount()

        if (!UserHelper.isTeacher()) {
            updateCommentsCount()
        }

        if (UserHelper.isTeacher()) {
            updateLeaveRequestsCount()
            updateStudentCommentCount()
        }
    }

    private fun updateLeaveRequestsCount() {
        val count = LeaveCount().getUnreadCount()
        view?.updateRequestsLeaveCount(count)
    }

    private fun updateCommentsCount() {
        val count = CommentsCount().getUnreadCount()
        view?.updateCommentsCount(count)
    }

    private fun updateMessageCount() {
        val count = MessageCount().getTotalUnread()
        view?.updateMessageCount(count)
    }

    private fun updateNewsCount() {
        val count = NewsCount().getUnreadCount()
        view?.updateNewsCount(count)

    }

    fun finalize() {
        //MessagesCountCache.unregister(this)
    }

    companion object {
        // Used to save the unread comment type for navigation.
        private const val UNREAD_COMMENT_TYPE = Const.UNREAD_COMMENT_TYPE
    }

}