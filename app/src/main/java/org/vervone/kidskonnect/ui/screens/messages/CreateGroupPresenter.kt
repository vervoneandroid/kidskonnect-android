package org.vervone.kidskonnect.ui.screens.messages

import org.vervone.kidskonnect.core.DLog
import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.DomainError
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.model.MessageChannel
import org.vervone.kidskonnect.core.domain.model.Teacher
import org.vervone.kidskonnect.core.domain.model.student.Student
import org.vervone.kidskonnect.core.push.PushApi
import org.vervone.kidskonnect.ui.screens.base.BasePresenter
import org.vervone.kidskonnect.ui.utils.AppUtils
import org.vervone.kidskonnect.ui.utils.UserHelper

class CreateGroupPresenter : BasePresenter<CreateGroupView>() {

    private val messageFacade = FacadeFactory.messageFacade

    private fun createChatGroup(messageChannel: MessageChannel) {

        val academicYr = AppUtils.getCurrentAcademicYear()
        val uid = UserHelper.getCurrentUser().uid!!
        val name = UserHelper.getCurrentUser().name ?: ""
        if (UserHelper.isTeacher()) {
            messageFacade.createMessageChannelByTeacher(
                    academicYr, uid, messageChannel,
                    object : RequestCallback<MessageChannel, DomainError> {
                        override fun onSuccess(res: MessageChannel?) {
                            view?.navigateToChatRoom(res!!)
                            messageChannel.id = res?.id
                            PushApi.Factory.pushApi.publishCreateChannel(name, uid, messageChannel)
                        }

                        override fun onError(error: DomainError?) {
                            //TODO : Message error handling

                        }
                    })
        } else {
            val className = UserHelper.student?.getClass(academicYr)!!
            messageFacade.createMessageChannelByStudent(
                    academicYr, className, uid, messageChannel,
                    object : RequestCallback<MessageChannel, DomainError> {
                        override fun onSuccess(res: MessageChannel?) {
                            view?.navigateToChatRoom(res!!)
                            messageChannel.id = res?.id
                            PushApi.Factory.pushApi.publishCreateChannel(name, uid, messageChannel)
                        }

                        override fun onError(error: DomainError?) {
                            //TODO : Message error handling

                        }
                    })
        }

    }

    private fun buildTargetId(targetsMap: Map<String, String>): String {
        return MessagesUtils.buildTargetId(targetsMap.map { it.key })
    }

    private fun buildMessageChannel(targetsMap: Map<String, String>,
                                    targetId: String): MessageChannel {
        val messageChannel = MessageChannel()
        messageChannel.targets = HashMap(targetsMap)
        messageChannel.targetId = targetId

        return messageChannel
    }


    private fun generateGroupName(targetNames: List<String>): String {

        val targetList = targetNames.sorted()
        val list = mutableListOf(UserHelper.getCurrentUser().name)
        list.addAll(targetList)
        return list.joinToString()
    }


    fun sendMessage(selectedStudent: HashMap<String, List<Student>>,
                    selectedTeachersList: ArrayList<Teacher>,
                    allSelectedClasses: ArrayList<String>) {
        view?.showProgress()
        val targets = buildTargetsMap(
                selectedStudent,
                selectedTeachersList, allSelectedClasses
        )


        val teacherIds = selectedTeachersList.map { it.teacherId!! }
        val classes = allSelectedClasses.map { it }

        val studentsMap = HashMap<String, String>()

        selectedStudent.entries.forEach { entry ->
            // Avoid students from selected classes
            if (!allSelectedClasses.contains(entry.key)) {
                val classStudents = entry.value.associateBy({ it.studentId!! }, { entry.key })
                studentsMap.putAll(classStudents)
            }
        }


        val isGroup = classes.isNotEmpty() || targets.size > 2

        val targetId = buildTargetId(targets)
        messageFacade.getChatRoomIfAvailable(AppUtils.getCurrentAcademicYear(),
                                             targetId, object : RequestCallback<MessageChannel?, DomainError> {
            override fun onSuccess(res: MessageChannel?) {
                if (res == null) {
                    val mc = buildMessageChannel(targets, targetId)
                    mc.isGroupMessage = isGroup
                    mc.teacherIds = teacherIds
                    mc.classes = classes
                    mc.studentIdsAndClass = studentsMap

                    if (isGroup && mc.targets != null) {
                        val targetsNames = HashMap(mc.targets!!.toMap())
                        targetsNames.remove(UserHelper.getCurrentUser().uid)
                        val groupName = generateGroupName(targetsNames.values.toList())
                        mc.groupName = groupName
                    }

                    createChatGroup(mc)
                    DLog.v(this, "Chat room doesn't exists")
                } else {
                    view?.navigateToChatRoom(res)
                }
            }

            override fun onError(error: DomainError?) {
                //TODO : Message error handling
            }
        })

    }

    private fun buildTargetsMap(selectedStudent: HashMap<String, List<Student>>,
                                selectedTeachersList: ArrayList<Teacher>,
                                allSelectedClasses: ArrayList<String>): HashMap<String, String> {
        val teacherIds = selectedTeachersList.associateBy({ it.teacherId!! }, { it.teacherName!! })
        val classes = allSelectedClasses.associateBy({ it }, { it })

        val studentsMap = HashMap<String, String>()

        selectedStudent.entries.forEach { entry ->
            // Avoid students from selected classes
            if (!allSelectedClasses.contains(entry.key)) {
                val classStudents = entry.value.associateBy({ it.studentId!! }, { it.studentName!! })
                studentsMap.putAll(classStudents)
            }
        }

        val targets = HashMap(studentsMap)
        targets.putAll(classes)
        targets.putAll(teacherIds)
        targets.put(UserHelper.getCurrentUser().uid,
                    UserHelper.getCurrentUser().name)
        return targets
    }

}