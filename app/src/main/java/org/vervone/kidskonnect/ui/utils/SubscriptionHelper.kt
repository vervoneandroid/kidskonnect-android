package org.vervone.kidskonnect.ui.utils

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.model.MessageChannel
import org.vervone.kidskonnect.core.helper.ThreadUtils
import org.vervone.kidskonnect.ui.notification.NotificationHelper

object SubscriptionHelper {

    private const val COMMON_NOTIFICATIONS = "commonNotifications"
    private const val NOTIFICATION_TOPIC = "notificationTopic-"
    private const val INITIAL_SUBSCRPTION = "initialSubscrption-"

    private val messageFacade = FacadeFactory.messageFacade


    fun register() {
        NotificationHelper.register(notificationHelper)
    }

    private val notificationHelper = object : NotificationHelper.OnCompleteListener {
        override fun onComplete() {
            subscribe()
        }
    }


    private fun subscribe() {
        ThreadUtils.runOnBg {
            subscribeCommonTopics()
            if (UserHelper.isTeacher()) {
                subscribeTeacherTopics()
                subscribeToTeacherMessages()
            } else {
                subscribeToStudentMessages()
            }
        }
    }


    private fun subscribeCommonTopics() {
        val uid = UserHelper.getCurrentUser().uid!!
        NotificationHelper.subscribe(NOTIFICATION_TOPIC + uid)
        NotificationHelper.subscribe(COMMON_NOTIFICATIONS)
    }

    private fun subscribeTeacherTopics() {
        NotificationHelper.subscribe("teacher")

        val teacher = UserHelper.teacher
        teacher?.classesAndSubjects?.keys?.forEach { className ->
            val classNameAdj = className.replace(" ", "_")
            NotificationHelper.subscribe(NOTIFICATION_TOPIC + classNameAdj)
            NotificationHelper.subscribe(INITIAL_SUBSCRPTION + classNameAdj)

        }
    }


    private fun subscribeToTeacherMessages() {

        val academicYr = AppUtils.getCurrentAcademicYear()
        val uid = UserHelper.getCurrentUser().uid!!

        val teacher = UserHelper.teacher

        teacher?.classesAndSubjects?.keys?.let {
            val classes = it.toList()
            messageFacade.getAllChatRooms(
                    academicYr, uid, classes, object : RequestCallback<List<MessageChannel>, Error> {
                override fun onError(error: Error?) {}
                override fun onSuccess(res: List<MessageChannel>?) {
                    if (res != null) {
                        subscribeChannels(res)
                    }
                }
            })
        }
    }


    private fun subscribeToStudentMessages() {
        val academicYr = AppUtils.getCurrentAcademicYear()
        val uid = UserHelper.getCurrentUser().uid!!
        val student = UserHelper.student!!
        val className = student.getClass(academicYr)

        val classNameAdj = className?.replace(" ", "_")
        NotificationHelper.subscribe(NOTIFICATION_TOPIC + classNameAdj)

        if (className != null) {
            messageFacade.getStudentChatRooms(
                    academicYr, uid, className, object : RequestCallback<List<MessageChannel>, Error> {
                override fun onError(error: Error?) {}
                override fun onSuccess(res: List<MessageChannel>?) {
                    if (res != null) {
                        subscribeChannels(res)
                    }
                }
            })
        }
    }


    private fun subscribeChannels(channels: List<MessageChannel>) {
        ThreadUtils.runOnBg {
            channels.forEach {
                NotificationHelper.subscribe(it.id!!)
            }
        }
    }


    fun subscribeMessageChannel(channelId: String) {
        NotificationHelper.subscribe(channelId)
    }

    fun unsubscribeAll() {
        NotificationHelper.unsubscribeAll()
    }

}