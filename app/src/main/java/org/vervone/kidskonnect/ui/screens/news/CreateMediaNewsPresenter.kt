package org.vervone.kidskonnect.ui.screens.news

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.cloudstorage.CloudStorage
import org.vervone.kidskonnect.core.domain.DomainConsts
import org.vervone.kidskonnect.core.domain.DomainError
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.model.News
import org.vervone.kidskonnect.core.helper.PreferenceHelper
import org.vervone.kidskonnect.ui.screens.base.BasePresenter
import org.vervone.kidskonnect.ui.utils.AppUtils
import org.vervone.kidskonnect.ui.utils.UserHelper
import java.util.concurrent.Executors

class CreateMediaNewsPresenter : BasePresenter<CreateMediaNewsView>() {
    private val schoolFacade = FacadeFactory.schoolFacade
    private val newsFacade = FacadeFactory.newsFacade

    private val executor = Executors.newFixedThreadPool(3)

    fun fetchClasses() {
        view?.showProgressBar()
        if (UserHelper.isAdmin()) {
            schoolFacade.getClasses(classesCallback)
        } else {
            schoolFacade.getClasses(academicYr = AppUtils.getCurrentAcademicYear(),
                                    teacherId = UserHelper.getCurrentUser().uid ?: "",
                                    callback = classesCallback)
        }
    }


    private val classesCallback = object : RequestCallback<List<String>, DomainError> {
        override fun onError(error: DomainError?) {
            view?.showError()
        }

        override fun onSuccess(res: List<String>?) {
            //view?.hideProgressBar()
            val list = res?.map { CheckedItem(it, false) }
            view?.setClasses(list!!)
        }
    }


    fun postNews(news: News, cloudStorage: CloudStorage) {
        view?.showProgressBar()

        executor.execute {

            val year = PreferenceHelper.getString(DomainConsts.KEY_CUR_ACADEMIC_YR)
            if (year != null) {
                val newsId = newsFacade.generateNewsId(year)
                news.id = newsId
                newsFacade.postNews(year, news, cloudStorage, object : RequestCallback<Void, Error> {
                    override fun onSuccess(res: Void?) {
                        view?.closeWindow()
                    }

                    override fun onError(error: Error?) {
                        view?.showError()
                    }
                })
            }

        }

    }


}