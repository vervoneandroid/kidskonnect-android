package org.vervone.kidskonnect.ui.utils

import org.vervone.kidskonnect.core.helper.PreferenceHelper

class CommentsCount {

    fun getUnreadCount(): Int {
        val counts = PreferenceHelper.getString(COMMENTS_COUNT)
        val split = counts?.split("|")

        if (split != null) {
            return split[1].toInt() - split[0].toInt()
        }
        return 0
    }

    fun resetCount() {
        PreferenceHelper.remove(COMMENTS_COUNT)
    }

    fun markAsRead() {
        val counts = PreferenceHelper.getString(COMMENTS_COUNT)
        val split = counts?.split("|")
        if (split != null) {
            val count = split[1].toInt()
            PreferenceHelper.save("$count|$count", COMMENTS_COUNT)
        }
    }

    fun setUnreadCount(newCount: Int): Int {
        val counts = PreferenceHelper.getString(COMMENTS_COUNT)
        val split = counts?.split("|")
        val unread : Int
        if (split != null) {
            val readCount = split[0].toInt()
            PreferenceHelper.save("$readCount|$newCount", COMMENTS_COUNT)
            unread = newCount - readCount
        } else {
            PreferenceHelper.save("$newCount|$newCount", COMMENTS_COUNT)
            unread = 0
        }

        return if (unread > 0) unread else 0
    }

    companion object {
        private const val COMMENTS_COUNT = "COMMENTS_COUNT"
    }
}