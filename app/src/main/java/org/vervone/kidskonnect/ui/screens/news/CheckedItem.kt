package org.vervone.kidskonnect.ui.screens.news

import java.io.Serializable

data class CheckedItem(var name: String, var isChecked: Boolean) : Serializable