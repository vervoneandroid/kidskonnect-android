package org.vervone.kidskonnect.ui.utils

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.support.annotation.StringRes
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import org.vervone.kidskonnect.R

object DialogFactory {

    fun createSimpleOkErrorDialog(context: Context, title: String?, message: String): Dialog {
        val alertDialog = AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setNeutralButton(R.string.ok, null)
        return alertDialog.create()
    }

    fun createSimpleOkErrorDialog(
            context: Context,
            @StringRes titleResource: Int,
            @StringRes messageResource: Int
    ): Dialog {

        return createSimpleOkErrorDialog(
                context,
                context.getString(titleResource),
                context.getString(messageResource)
        )
    }

    fun createGenericErrorDialog(context: Context, message: String): Dialog {
        val alertDialog = AlertDialog.Builder(context)
            .setTitle(context.getString(R.string.error))
            .setMessage(message)
            .setNeutralButton(R.string.ok, null)
        return alertDialog.create()
    }

    fun createGenericErrorDialog(context: Context, @StringRes messageResource: Int): Dialog {
        return createGenericErrorDialog(context, context.getString(messageResource))
    }


    fun createProgressDialog(context: Context): DialogFragment {
        val progress = ProgressBarFragment()
        progress.isCancelable = false
        return progress
    }

    fun createGenericErrorDialog(context: Context, message: String,
                                 listener: DialogInterface.OnClickListener): Dialog {
        val alertDialog = AlertDialog.Builder(context)
            .setTitle(context.getString(R.string.error))
            .setMessage(message)
            .setNeutralButton(R.string.ok, listener)
        return alertDialog.create()
    }

}