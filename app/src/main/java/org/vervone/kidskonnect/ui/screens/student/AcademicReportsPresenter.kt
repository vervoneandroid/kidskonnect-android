package org.vervone.kidskonnect.ui.screens.student

import org.vervone.kidskonnect.core.RequestCallback
import org.vervone.kidskonnect.core.domain.DomainError
import org.vervone.kidskonnect.core.domain.FacadeFactory
import org.vervone.kidskonnect.core.domain.model.student.AcademicReport
import org.vervone.kidskonnect.ui.screens.base.BasePresenter

class AcademicReportsPresenter(val studentId: String) : BasePresenter<AcademicReportsView>() {

    val schoolFacade = FacadeFactory.schoolFacade

    fun getAcademicReports() {
        schoolFacade.getAcademicReport(studentId, AcademicReportCallback())
    }

    inner class AcademicReportCallback : RequestCallback<ArrayList<AcademicReport>, DomainError> {
        override fun onSuccess(res: ArrayList<AcademicReport>?) {
            view?.setData(res!!)
        }

        override fun onError(error: DomainError?) {
            view?.showError()
        }
    }
}